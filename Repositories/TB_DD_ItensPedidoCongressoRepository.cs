using System.Collections.Generic;
using System.Linq;
using IntegrationApasTotvs.Models;
using IntegrationApasTotvs.Models.DbContexts;
using IntegrationApasTotvs.Repositories.Interfaces;

namespace IntegrationApasTotvs.Repositories
{
    public class TB_DD_ItensPedidoCongressoRepository : ITB_DD_ItensPedidoCongressoRepository
    {
        private readonly TB_DD_ItensPedidoCongressoDbContext _contexto;
        public TB_DD_ItensPedidoCongressoRepository(TB_DD_ItensPedidoCongressoDbContext ctx)
        {
            _contexto = ctx;
        }
        public void Add(TB_DD_ItensPedidoCongresso item)
        {
            _contexto.ItensPedidoCongressos.Add(item);
            _contexto.SaveChanges();
        }

        public TB_DD_ItensPedidoCongresso Find(int itemId)
        {
            return _contexto.ItensPedidoCongressos.FirstOrDefault(x => x.cdItem == itemId);
        }

        public IEnumerable<TB_DD_ItensPedidoCongresso> GetAll()
        {
            return _contexto.ItensPedidoCongressos.ToList();
        }

        public void Remove(int itemId)
        {
            var item = _contexto.ItensPedidoCongressos.First(x => x.cdItem == itemId);
            _contexto.ItensPedidoCongressos.Remove(item);
            _contexto.SaveChanges();
        }

        public void Update(TB_DD_ItensPedidoCongresso item)
        {
            _contexto.ItensPedidoCongressos.Update(item);
            _contexto.SaveChanges();
        }
    }
}