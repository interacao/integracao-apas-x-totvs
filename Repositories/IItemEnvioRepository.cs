using System.Collections.Generic;
using IntegrationApasTotvs.Models;

namespace IntegrationApasTotvs.Repositories
{
    public interface IItemEnvioRepository
    {
         void Add(ItemEnvio item);
         IEnumerable<ItemEnvio> GetAll();
         ItemEnvio Find(int itemId);
         void Remove(int itemId);
         void Update(ItemEnvio item);        
    }
}
