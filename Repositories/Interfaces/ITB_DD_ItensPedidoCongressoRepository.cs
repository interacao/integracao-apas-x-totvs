using System.Collections.Generic;
using IntegrationApasTotvs.Models;

namespace IntegrationApasTotvs.Repositories.Interfaces
{
    public interface ITB_DD_ItensPedidoCongressoRepository
    {
         void Add(TB_DD_ItensPedidoCongresso item);
         IEnumerable<TB_DD_ItensPedidoCongresso> GetAll();
         TB_DD_ItensPedidoCongresso Find(int itemId);
         void Remove(int itemId);
         void Update(TB_DD_ItensPedidoCongresso item);
    }
}