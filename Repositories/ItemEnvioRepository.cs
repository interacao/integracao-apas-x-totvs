using IntegrationApasTotvs.Repositories;
using IntegrationApasTotvs.Models;
using System.Collections.Generic;
using System.Linq;
using IntegrationApasTotvs.Models.DbContexts;

namespace IntegrationApasTotvs.Repositories
{
    public class ItemEnvioRepository : IItemEnvioRepository
    {
        private readonly ItemEnvioDbContext _contexto;
        public ItemEnvioRepository(ItemEnvioDbContext ctx)
        {
            _contexto = ctx;
        }

        public void Add(ItemEnvio item)
        {
            _contexto.ItemEnvios.Add(item);
            _contexto.SaveChanges();
        }

        public ItemEnvio Find(int itemId)
        {
            return _contexto.ItemEnvios.FirstOrDefault(x => x.item_envio_id == itemId);
        }

        public IEnumerable<ItemEnvio> GetAll()
        {
            return _contexto.ItemEnvios.ToList();
        }

        public void Remove(int itemId)
        {
            var item = _contexto.ItemEnvios.First(x => x.item_envio_id == itemId);
            _contexto.ItemEnvios.Remove(item);
            _contexto.SaveChanges();
        }

        public void Update(ItemEnvio item)
        {
            _contexto.ItemEnvios.Update(item);
            _contexto.SaveChanges();
        }
    }   
}