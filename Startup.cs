﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using IntegrationApasTotvs.Models.DbContexts;
using IntegrationApasTotvs.Repositories.Interfaces;
using IntegrationApasTotvs.Repositories;
using IntegrationApasTotvs.Models;
using Microsoft.Extensions.Hosting;
using IntegrationApasTotvs.Services;

namespace IntegrationApasTotvs
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddDbContext<ItemEnvioDbContext>(options =>
           // options.UseSqlServer(Configuration.GetConnectionString("API")));
            services.AddDbContext<TB_DD_ItensPedidoCongressoDbContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("APAS")));
            services.AddTransient<ITB_DD_ItensPedidoCongressoRepository,TB_DD_ItensPedidoCongressoRepository>();
            
            services.AddSingleton<JsonSender>();
            services.AddSingleton<IHostedService, TesteService>();




            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
