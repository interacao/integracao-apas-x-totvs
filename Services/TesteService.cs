using System;
using System.Threading;
using System.Threading.Tasks;
using IntegrationApasTotvs.Models;

namespace IntegrationApasTotvs.Services
{
    public class TesteService : HostedService
    {
        JsonSender jsender = new JsonSender();
        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
        {
            await jsender.GetItem(cancellationToken);
            await Task.Delay(TimeSpan.FromSeconds(5), cancellationToken);
        }
        }
    }
}