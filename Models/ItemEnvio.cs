using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace IntegrationApasTotvs.Models{
	[Table("ItemEnvio")]
    public class ItemEnvio{
	public ItemEnvio(){}

	[Key]
	public int item_envio_id { get; set; }
	
	public string acao { get; set; }
	public string data_venda { get; set; }
	public string ident_bancario { get; set; }
	public string tipo { get; set; }
	public string descricao_tipo { get; set; }
	public string cc { get; set; }
	public string cr { get; set; }
	public string pr { get; set; }
	public string co { get; set; }
	public decimal quantidade { get; set; }
	public decimal valor_liquido { get; set; }
	public decimal valor_bruto { get; set; }
	public decimal valor_desconto { get; set; }
	public decimal valor_acrescimo { get; set; }
	public decimal valor_unitario { get; set; }
	public string tipo_pagamento { get; set; }
	public string moeda { get; set; }
}

 enum TipoPagamento
 {
	Boleto = 1,
	Credito = 2,
	Debito = 3,
	Dinheiro = 4,
	Deposito_TED_DOC = 5
 }

enum Moeda
{
	Real = 1,
	Dolar = 2,
	Euro = 3
}
}

