using Microsoft.EntityFrameworkCore;

namespace IntegrationApasTotvs.Models.DbContexts
{
    public class TB_DD_ItensPedidoCongressoDbContext : DbContext
    {
        public TB_DD_ItensPedidoCongressoDbContext(DbContextOptions<TB_DD_ItensPedidoCongressoDbContext> options)
            :base(options){}

        public DbSet<TB_DD_ItensPedidoCongresso> ItensPedidoCongressos { get; set; }
    }
}