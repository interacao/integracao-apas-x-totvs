using Microsoft.EntityFrameworkCore;

namespace IntegrationApasTotvs.Models.DbContexts
{
    public class ItemEnvioDbContext: DbContext
    {
        public ItemEnvioDbContext(DbContextOptions<ItemEnvioDbContext> options)
            :base(options){}

        public DbSet<ItemEnvio> ItemEnvios { get; set; }
    }
}