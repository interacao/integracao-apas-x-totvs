﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace EasyLibSys
{
    public class FWK_DAO_DD_ItensPedidoCongresso
    {
        public Interanet_v4_easyContext DB_contexto = new Interanet_v4_easyContext(new ConnectionStringEasy().Conexao);

        public FWK_DAO_DD_ItensPedidoCongresso()
        {
            if (DB_contexto == null)
                DB_contexto = new Interanet_v4_easyContext(new ConnectionStringEasy().Conexao);
        }

        public List<TB_DD_ItensPedidoCongresso> MS_ObterTodos()
        {
            return (from TP in DB_contexto.TB_DD_ItensPedidoCongresso select TP).ToList();
        }

        public List<TB_DD_ItensPedidoCongresso> MS_GetNewAndUpdated(DateTime data)
        {
            var linq = from itens in DB_contexto.TB_DD_ItensPedidoCongresso
                       where itens.dtInclusao.Value >= data ||
                       itens.dtAlteracao.Value >= data
                       select itens;

            return linq.ToList();
        }
    }
}
