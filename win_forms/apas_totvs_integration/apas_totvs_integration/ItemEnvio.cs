namespace apas_totvs_integration
{
    using Newtonsoft.Json;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("ItemEnvio")]
    public partial class ItemEnvio
    {
        [Key,JsonIgnore]
        public int item_envio_id { get; set; }

        [StringLength(1)]
        public string acao { get; set; }

        [StringLength(8)]
        public string data_venda { get; set; }

        [StringLength(30)]
        public string ident_bancario { get; set; }

        [StringLength(10)]
        public string tipo { get; set; }

        [StringLength(50)]
        public string descricao_tipo { get; set; }

        [StringLength(5)]
        public string cc { get; set; }

        [StringLength(6)]
        public string cr { get; set; }

        [StringLength(6)]
        public string pr { get; set; }

        [StringLength(6)]
        public string co { get; set; }

        public decimal? quantidade { get; set; }

        public decimal? valor_liquido { get; set; }

        public decimal? valor_bruto { get; set; }

        public decimal? valor_desconto { get; set; }

        public decimal? valor_acrescimo { get; set; }

        public decimal? valor_unitario { get; set; }

        [StringLength(1)]
        public string tipo_pagamento { get; set; }

        [StringLength(1)]
        public string moeda { get; set; }
        [JsonIgnore]
        public DateTime? data_envio { get; set; }
        [JsonIgnore]
        public int? status_envio { get; set; }
        public string pedido_origem { get; set; }
        public string item_origem { get; set; }
    }
}
