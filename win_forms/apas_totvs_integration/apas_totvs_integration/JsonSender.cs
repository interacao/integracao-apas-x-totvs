using apas_totvs_integration;
using EasyLibSys;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace IntegrationApasTotvs.Models{
    public class JsonSender{

        public string token;
        public string origem;
        public List<ItemEnvio> linhas;

        public JsonSender(List<ItemEnvio> itens)
        {
                token = Settings.tokenTotvs;
                origem = "Interacao";
                linhas = itens;
        }    	
    }
}

