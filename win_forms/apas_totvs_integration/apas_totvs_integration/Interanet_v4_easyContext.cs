﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyLibSys
{
    using System.Linq;

    #region Unit of work

    public interface IInteranet_v4_easyContext : System.IDisposable
    {
        System.Data.Entity.DbSet<TB_AS_Alternativa_Questionario> TB_AS_Alternativa_Questionario { get; set; } // TB_AS_Alternativa_Questionario
        System.Data.Entity.DbSet<TB_AS_AlternativaPadrao_Questionario> TB_AS_AlternativaPadrao_Questionario { get; set; } // TB_AS_AlternativaPadrao_Questionario
        System.Data.Entity.DbSet<TB_AS_CodigoPromocional_tb_Pedido> TB_AS_CodigoPromocional_tb_Pedido { get; set; } // TB_AS_CodigoPromocional_tb_Pedido
        System.Data.Entity.DbSet<TB_AS_Filial_Funcionario> TB_AS_Filial_Funcionario { get; set; } // TB_AS_Filial_Funcionario
        System.Data.Entity.DbSet<TB_AS_Pedido_FormaPagamento> TB_AS_Pedido_FormaPagamento { get; set; } // TB_AS_Pedido_FormaPagamento
        System.Data.Entity.DbSet<TB_AS_Pedido_Recibo> TB_AS_Pedido_Recibo { get; set; } // TB_AS_Pedido_Recibo
        System.Data.Entity.DbSet<TB_AS_RegraMarketingSimples_ProdutoOperacional> TB_AS_RegraMarketingSimples_ProdutoOperacional { get; set; } // TB_AS_RegraMarketingSimples_ProdutoOperacional
        System.Data.Entity.DbSet<TB_AS_TipoCredenciamento_Edicao> TB_AS_TipoCredenciamento_Edicao { get; set; } // TB_AS_TipoCredenciamento_Edicao
        System.Data.Entity.DbSet<TB_CF_Edicao> TB_CF_Edicao { get; set; } // TB_CF_Edicao
        System.Data.Entity.DbSet<TB_CF_EstruturaPadrao> TB_CF_EstruturaPadrao { get; set; } // TB_CF_EstruturaPadrao
        System.Data.Entity.DbSet<TB_CF_ExecaoFormularioManualEletronico> TB_CF_ExecaoFormularioManualEletronico { get; set; } // TB_CF_ExecaoFormularioManualEletronico
        System.Data.Entity.DbSet<TB_CF_Formulario> TB_CF_Formulario { get; set; } // TB_CF_Formulario
        System.Data.Entity.DbSet<TB_CF_FormularioGenerico> TB_CF_FormularioGenerico { get; set; } // TB_CF_FormularioGenerico
        System.Data.Entity.DbSet<TB_CF_FormularioManualEletronico> TB_CF_FormularioManualEletronico { get; set; } // TB_CF_FormularioManualEletronico
        System.Data.Entity.DbSet<TB_CF_FormularioSistemaIntegrado> TB_CF_FormularioSistemaIntegrado { get; set; } // TB_CF_FormularioSistemaIntegrado
        System.Data.Entity.DbSet<TB_CF_INT_Geral> TB_CF_INT_Geral { get; set; } // TB_CF_INT_Geral
        System.Data.Entity.DbSet<TB_CF_Permissoes> TB_CF_Permissoes { get; set; } // TB_CF_Permissoes
        System.Data.Entity.DbSet<TB_CF_Recibo> TB_CF_Recibo { get; set; } // TB_CF_Recibo
        System.Data.Entity.DbSet<TB_CF_VendaDados> TB_CF_VendaDados { get; set; } // TB_CF_VendaDados
        System.Data.Entity.DbSet<TB_DD_Agendamento> TB_DD_Agendamento { get; set; } // TB_DD_Agendamento
        System.Data.Entity.DbSet<TB_DD_Alternativa> TB_DD_Alternativa { get; set; } // TB_DD_Alternativa
        System.Data.Entity.DbSet<TB_DD_AlternativaQuestionarioManual> TB_DD_AlternativaQuestionarioManual { get; set; } // TB_DD_AlternativaQuestionarioManual
        System.Data.Entity.DbSet<TB_DD_App_ConvideAmigo> TB_DD_App_ConvideAmigo { get; set; } // TB_DD_App_ConvideAmigo
        System.Data.Entity.DbSet<TB_DD_App_NoticiaAviso> TB_DD_App_NoticiaAviso { get; set; } // TB_DD_App_NoticiaAviso
        System.Data.Entity.DbSet<TB_DD_AprovacaoReprovacaoDeProjeto> TB_DD_AprovacaoReprovacaoDeProjeto { get; set; } // TB_DD_AprovacaoReprovacaoDeProjeto
        System.Data.Entity.DbSet<TB_DD_AssistentePreenchimento> TB_DD_AssistentePreenchimento { get; set; } // TB_DD_AssistentePreenchimento
        System.Data.Entity.DbSet<TB_DD_ATOM> TB_DD_ATOM { get; set; } // TB_DD_ATOM
        System.Data.Entity.DbSet<TB_DD_BloqueioContratoProduto> TB_DD_BloqueioContratoProduto { get; set; } // TB_DD_BloqueioContratoProduto
        System.Data.Entity.DbSet<TB_DD_CarrinhoOperacional> TB_DD_CarrinhoOperacional { get; set; } // TB_DD_CarrinhoOperacional
        System.Data.Entity.DbSet<TB_DD_CarrinhoOperacional_VendaDados> TB_DD_CarrinhoOperacional_VendaDados { get; set; } // TB_DD_CarrinhoOperacional_VendaDados
        System.Data.Entity.DbSet<TB_DD_CatalogoOficial> TB_DD_CatalogoOficial { get; set; } // TB_DD_CatalogoOficial
        System.Data.Entity.DbSet<TB_DD_Cnab> TB_DD_Cnab { get; set; } // TB_DD_Cnab
        System.Data.Entity.DbSet<TB_DD_CodigoPromocional> TB_DD_CodigoPromocional { get; set; } // TB_DD_CodigoPromocional
        System.Data.Entity.DbSet<TB_DD_Codigos> TB_DD_Codigos { get; set; } // TB_DD_Codigos
        System.Data.Entity.DbSet<TB_DD_Coletor> TB_DD_Coletor { get; set; } // TB_DD_Coletor
        System.Data.Entity.DbSet<TB_DD_Comunicados> TB_DD_Comunicados { get; set; } // TB_DD_Comunicados
        System.Data.Entity.DbSet<TB_DD_ConfExtracaoPersonalizada> TB_DD_ConfExtracaoPersonalizada { get; set; } // TB_DD_ConfExtracaoPersonalizada
        System.Data.Entity.DbSet<TB_DD_ContratoExpositor> TB_DD_ContratoExpositor { get; set; } // TB_DD_ContratoExpositor
        System.Data.Entity.DbSet<TB_DD_Convite> TB_DD_Convite { get; set; } // TB_DD_Convite
        System.Data.Entity.DbSet<TB_DD_ConviteEletronico> TB_DD_ConviteEletronico { get; set; } // TB_DD_ConviteEletronico
        System.Data.Entity.DbSet<TB_DD_ConviteEletronicoCancelado> TB_DD_ConviteEletronicoCancelado { get; set; } // TB_DD_ConviteEletronicoCancelado
        System.Data.Entity.DbSet<TB_DD_ConviteEletronicoVip> TB_DD_ConviteEletronicoVip { get; set; } // TB_DD_ConviteEletronicoVip
        System.Data.Entity.DbSet<TB_DD_ConvitesImpressos> TB_DD_ConvitesImpressos { get; set; } // TB_DD_ConvitesImpressos
        System.Data.Entity.DbSet<TB_DD_CorCarpeteColuna> TB_DD_CorCarpeteColuna { get; set; } // TB_DD_CorCarpeteColuna
        System.Data.Entity.DbSet<TB_DD_Credencial> TB_DD_Credencial { get; set; } // TB_DD_Credencial
        System.Data.Entity.DbSet<TB_DD_DestinatarioComunicado> TB_DD_DestinatarioComunicado { get; set; } // TB_DD_DestinatarioComunicado
        System.Data.Entity.DbSet<TB_DD_EletricaBasica> TB_DD_EletricaBasica { get; set; } // TB_DD_EletricaBasica
        System.Data.Entity.DbSet<TB_DD_EmailMarketingImportados> TB_DD_EmailMarketingImportados { get; set; } // TB_DD_EmailMarketingImportados
        System.Data.Entity.DbSet<TB_DD_EmissaoCredencial> TB_DD_EmissaoCredencial { get; set; } // TB_DD_EmissaoCredencial
        System.Data.Entity.DbSet<TB_DD_Empresa> TB_DD_Empresa { get; set; } // TB_DD_Empresa
        System.Data.Entity.DbSet<TB_DD_EntregaCredencialVIP> TB_DD_EntregaCredencialVIP { get; set; } // TB_DD_EntregaCredencialVIP
        System.Data.Entity.DbSet<TB_DD_EstruturaCartaPersonalizada> TB_DD_EstruturaCartaPersonalizada { get; set; } // TB_DD_EstruturaCartaPersonalizada
        System.Data.Entity.DbSet<TB_DD_EstruturaPersonalizada> TB_DD_EstruturaPersonalizada { get; set; } // TB_DD_EstruturaPersonalizada
        System.Data.Entity.DbSet<TB_DD_Evento> TB_DD_Evento { get; set; } // TB_DD_Evento
        System.Data.Entity.DbSet<TB_DD_ExtracaoPersonalizada> TB_DD_ExtracaoPersonalizada { get; set; } // TB_DD_ExtracaoPersonalizada
        System.Data.Entity.DbSet<TB_DD_FaqRespostas> TB_DD_FaqRespostas { get; set; } // TB_DD_FaqRespostas
        System.Data.Entity.DbSet<TB_DD_Filial> TB_DD_Filial { get; set; } // TB_DD_Filial
        System.Data.Entity.DbSet<TB_DD_FotoPessoa> TB_DD_FotoPessoa { get; set; } // TB_DD_FotoPessoa
        System.Data.Entity.DbSet<TB_DD_Funcionario> TB_DD_Funcionario { get; set; } // TB_DD_Funcionario
        System.Data.Entity.DbSet<TB_DD_Geradora_Registro> TB_DD_Geradora_Registro { get; set; } // TB_DD_Geradora_Registro
        System.Data.Entity.DbSet<TB_DD_GrupoProdutoCongresso> TB_DD_GrupoProdutoCongresso { get; set; } // TB_DD_GrupoProdutoCongresso
        System.Data.Entity.DbSet<TB_DD_GrupoResponsavelCongresso> TB_DD_GrupoResponsavelCongresso { get; set; } // TB_DD_GrupoResponsavelCongresso
        System.Data.Entity.DbSet<TB_DD_HelpDesk> TB_DD_HelpDesk { get; set; } // TB_DD_HelpDesk
        System.Data.Entity.DbSet<TB_DD_ImagemAssistentePreenchimento> TB_DD_ImagemAssistentePreenchimento { get; set; } // TB_DD_ImagemAssistentePreenchimento
        System.Data.Entity.DbSet<TB_DD_ImportacaoPessoa> TB_DD_ImportacaoPessoa { get; set; } // TB_DD_ImportacaoPessoa
        System.Data.Entity.DbSet<TB_DD_ImpostoAPAS> TB_DD_ImpostoAPAS { get; set; } // TB_DD_ImpostoAPAS
        System.Data.Entity.DbSet<TB_DD_InteracaoHelp> TB_DD_InteracaoHelp { get; set; } // TB_DD_InteracaoHelp
        System.Data.Entity.DbSet<TB_DD_ItensLote> TB_DD_ItensLote { get; set; } // TB_DD_ItensLote
        System.Data.Entity.DbSet<TB_DD_ItensPedidoCongresso> TB_DD_ItensPedidoCongresso { get; set; } // TB_DD_ItensPedidoCongresso
        System.Data.Entity.DbSet<TB_DD_ItensPedidoOperacional> TB_DD_ItensPedidoOperacional { get; set; } // TB_DD_ItensPedidoOperacional
        System.Data.Entity.DbSet<TB_DD_LeituraRegulamento> TB_DD_LeituraRegulamento { get; set; } // TB_DD_LeituraRegulamento
        System.Data.Entity.DbSet<TB_DD_LimiteAplicacao> TB_DD_LimiteAplicacao { get; set; } // TB_DD_LimiteAplicacao
        System.Data.Entity.DbSet<TB_DD_LogImportacaoAPAS> TB_DD_LogImportacaoAPAS { get; set; } // TB_DD_LogImportacaoAPAS
        System.Data.Entity.DbSet<TB_DD_LoteImpressao> TB_DD_LoteImpressao { get; set; } // TB_DD_LoteImpressao
        System.Data.Entity.DbSet<TB_DD_MaquinaseVeiculos> TB_DD_MaquinaseVeiculos { get; set; } // TB_DD_MaquinaseVeiculos
        System.Data.Entity.DbSet<TB_DD_Monitoria> TB_DD_Monitoria { get; set; } // TB_DD_Monitoria
        System.Data.Entity.DbSet<TB_DD_MonitoriaCielo> TB_DD_MonitoriaCielo { get; set; } // TB_DD_MonitoriaCielo
        System.Data.Entity.DbSet<TB_DD_MonitoriaPayPal> TB_DD_MonitoriaPayPal { get; set; } // TB_DD_MonitoriaPayPal
        System.Data.Entity.DbSet<TB_DD_MonitoriaRedeCard> TB_DD_MonitoriaRedeCard { get; set; } // TB_DD_MonitoriaRedeCard
        System.Data.Entity.DbSet<TB_DD_MonitoriaShopLineItau> TB_DD_MonitoriaShopLineItau { get; set; } // TB_DD_MonitoriaShopLineItau
        System.Data.Entity.DbSet<TB_DD_MonitoriaStone> TB_DD_MonitoriaStone { get; set; } // TB_DD_MonitoriaStone
        System.Data.Entity.DbSet<TB_DD_Movimentacao> TB_DD_Movimentacao { get; set; } // TB_DD_Movimentacao
        System.Data.Entity.DbSet<TB_DD_News> TB_DD_News { get; set; } // TB_DD_News
        System.Data.Entity.DbSet<TB_DD_Pagamento> TB_DD_Pagamento { get; set; } // TB_DD_Pagamento
        System.Data.Entity.DbSet<TB_DD_ParcelamentoBoleto> TB_DD_ParcelamentoBoleto { get; set; } // TB_DD_ParcelamentoBoleto
        System.Data.Entity.DbSet<TB_DD_Pedido> TB_DD_Pedido { get; set; } // TB_DD_Pedido
        System.Data.Entity.DbSet<TB_DD_Pergunta> TB_DD_Pergunta { get; set; } // TB_DD_Pergunta
        System.Data.Entity.DbSet<TB_DD_PermissaoCorCarpeteColuna> TB_DD_PermissaoCorCarpeteColuna { get; set; } // TB_DD_PermissaoCorCarpeteColuna
        System.Data.Entity.DbSet<TB_DD_Pessoa> TB_DD_Pessoa { get; set; } // TB_DD_Pessoa
        System.Data.Entity.DbSet<TB_DD_PlacaIdentificacao> TB_DD_PlacaIdentificacao { get; set; } // TB_DD_PlacaIdentificacao
        System.Data.Entity.DbSet<TB_DD_PlacaIdentificacaoColuna> TB_DD_PlacaIdentificacaoColuna { get; set; } // TB_DD_PlacaIdentificacaoColuna
        System.Data.Entity.DbSet<TB_DD_PrecoProdutoCongresso> TB_DD_PrecoProdutoCongresso { get; set; } // TB_DD_PrecoProdutoCongresso
        System.Data.Entity.DbSet<TB_DD_PrecoProdutoOperacional> TB_DD_PrecoProdutoOperacional { get; set; } // TB_DD_PrecoProdutoOperacional
        System.Data.Entity.DbSet<TB_DD_PresencaCampanhaSejaBemVindo> TB_DD_PresencaCampanhaSejaBemVindo { get; set; } // TB_DD_PresencaCampanhaSejaBemVindo
        System.Data.Entity.DbSet<TB_DD_ProdutoCongresso> TB_DD_ProdutoCongresso { get; set; } // TB_DD_ProdutoCongresso
        System.Data.Entity.DbSet<TB_DD_ProdutoExpositores> TB_DD_ProdutoExpositores { get; set; } // TB_DD_ProdutoExpositores
        System.Data.Entity.DbSet<TB_DD_ProdutoOperacional> TB_DD_ProdutoOperacional { get; set; } // TB_DD_ProdutoOperacional
        System.Data.Entity.DbSet<TB_DD_Questionario> TB_DD_Questionario { get; set; } // TB_DD_Questionario
        System.Data.Entity.DbSet<TB_DD_QuestionarioManual> TB_DD_QuestionarioManual { get; set; } // TB_DD_QuestionarioManual
        System.Data.Entity.DbSet<TB_DD_ReenvioConvite> TB_DD_ReenvioConvite { get; set; } // TB_DD_ReenvioConvite
        System.Data.Entity.DbSet<TB_DD_ReenvioConviteVip> TB_DD_ReenvioConviteVip { get; set; } // TB_DD_ReenvioConviteVip
        System.Data.Entity.DbSet<TB_DD_Registro> TB_DD_Registro { get; set; } // TB_DD_Registro
        System.Data.Entity.DbSet<TB_DD_Regulamento> TB_DD_Regulamento { get; set; } // TB_DD_Regulamento
        System.Data.Entity.DbSet<TB_DD_Relatorio> TB_DD_Relatorio { get; set; } // TB_DD_Relatorio
        System.Data.Entity.DbSet<TB_DD_ResponsavelCongresso> TB_DD_ResponsavelCongresso { get; set; } // TB_DD_ResponsavelCongresso
        System.Data.Entity.DbSet<TB_DD_ResponsavelEmpresa> TB_DD_ResponsavelEmpresa { get; set; } // TB_DD_ResponsavelEmpresa
        System.Data.Entity.DbSet<TB_DD_Resposta> TB_DD_Resposta { get; set; } // TB_DD_Resposta
        System.Data.Entity.DbSet<TB_DD_RespostaQuestionarioManual> TB_DD_RespostaQuestionarioManual { get; set; } // TB_DD_RespostaQuestionarioManual
        System.Data.Entity.DbSet<TB_DD_Simultaneidade> TB_DD_Simultaneidade { get; set; } // TB_DD_Simultaneidade
        System.Data.Entity.DbSet<TB_DD_SistemaHabilitadoToken> TB_DD_SistemaHabilitadoToken { get; set; } // TB_DD_SistemaHabilitadoToken
        System.Data.Entity.DbSet<TB_DD_SiteColuna> TB_DD_SiteColuna { get; set; } // TB_DD_SiteColuna
        System.Data.Entity.DbSet<TB_DD_Stand> TB_DD_Stand { get; set; } // TB_DD_Stand
        System.Data.Entity.DbSet<TB_DD_SubAlternativaQuestionarioManual> TB_DD_SubAlternativaQuestionarioManual { get; set; } // TB_DD_SubAlternativaQuestionarioManual
        System.Data.Entity.DbSet<TB_DD_SubCategoria> TB_DD_SubCategoria { get; set; } // TB_DD_SubCategoria
        System.Data.Entity.DbSet<TB_DD_Ticket> TB_DD_Ticket { get; set; } // TB_DD_Ticket
        System.Data.Entity.DbSet<TB_DD_Token> TB_DD_Token { get; set; } // TB_DD_Token
        System.Data.Entity.DbSet<TB_DD_Usuario> TB_DD_Usuario { get; set; } // TB_DD_Usuario
        System.Data.Entity.DbSet<TB_DD_ValorContrato> TB_DD_ValorContrato { get; set; } // TB_DD_ValorContrato
        System.Data.Entity.DbSet<TB_DD_ValorContratoParcela> TB_DD_ValorContratoParcela { get; set; } // TB_DD_ValorContratoParcela
        System.Data.Entity.DbSet<TB_LG_Acesso> TB_LG_Acesso { get; set; } // TB_LG_Acesso
        System.Data.Entity.DbSet<TB_LG_AlteraSubCategoria> TB_LG_AlteraSubCategoria { get; set; } // TB_LG_AlteraSubCategoria
        System.Data.Entity.DbSet<TB_LG_CancelamentoAutomaticoPedido> TB_LG_CancelamentoAutomaticoPedido { get; set; } // TB_LG_CancelamentoAutomaticoPedido
        System.Data.Entity.DbSet<TB_LG_Erro> TB_LG_Erro { get; set; } // TB_LG_Erro
        System.Data.Entity.DbSet<TB_LG_FormularioManualEletronico> TB_LG_FormularioManualEletronico { get; set; } // TB_LG_FormularioManualEletronico
        System.Data.Entity.DbSet<TB_LG_Geral> TB_LG_Geral { get; set; } // TB_LG_Geral
        System.Data.Entity.DbSet<TB_LG_GeralDepEasy> TB_LG_GeralDepEasy { get; set; } // TB_LG_GeralDepEasy
        System.Data.Entity.DbSet<TB_LG_GeralDepFinanceiro> TB_LG_GeralDepFinanceiro { get; set; } // TB_LG_GeralDepFinanceiro
        System.Data.Entity.DbSet<TB_LG_GeralDepUsuario> TB_LG_GeralDepUsuario { get; set; } // TB_LG_GeralDepUsuario
        System.Data.Entity.DbSet<TB_LG_PaypalPlus> TB_LG_PaypalPlus { get; set; } // TB_LG_PaypalPlus
        System.Data.Entity.DbSet<TB_LG_RF_GeralAcao> TB_LG_RF_GeralAcao { get; set; } // TB_LG_RF_GeralAcao
        System.Data.Entity.DbSet<TB_LG_RF_GeralDepartamento> TB_LG_RF_GeralDepartamento { get; set; } // TB_LG_RF_GeralDepartamento
        System.Data.Entity.DbSet<TB_LG_RF_GeralUsuario> TB_LG_RF_GeralUsuario { get; set; } // TB_LG_RF_GeralUsuario
        System.Data.Entity.DbSet<TB_RF_AliquotaImposto> TB_RF_AliquotaImposto { get; set; } // TB_RF_AliquotaImposto
        System.Data.Entity.DbSet<TB_RF_AlternativaPadrao> TB_RF_AlternativaPadrao { get; set; } // TB_RF_AlternativaPadrao
        System.Data.Entity.DbSet<TB_RF_AreaAtuacao> TB_RF_AreaAtuacao { get; set; } // TB_RF_AreaAtuacao
        System.Data.Entity.DbSet<TB_RF_Atividades> TB_RF_Atividades { get; set; } // TB_RF_Atividades
        System.Data.Entity.DbSet<TB_RF_Categoria> TB_RF_Categoria { get; set; } // TB_RF_Categoria
        System.Data.Entity.DbSet<TB_RF_CnaeSebrae> TB_RF_CnaeSebrae { get; set; } // TB_RF_CnaeSebrae
        System.Data.Entity.DbSet<TB_RF_ConfImpostoRetencaoValorAcimaDe> TB_RF_ConfImpostoRetencaoValorAcimaDe { get; set; } // TB_RF_ConfImpostoRetencaoValorAcimaDe
        System.Data.Entity.DbSet<TB_RF_ConfImpostos> TB_RF_ConfImpostos { get; set; } // TB_RF_ConfImpostos
        System.Data.Entity.DbSet<TB_RF_Cor> TB_RF_Cor { get; set; } // TB_RF_Cor
        System.Data.Entity.DbSet<TB_RF_Cotacao> TB_RF_Cotacao { get; set; } // TB_RF_Cotacao
        System.Data.Entity.DbSet<TB_RF_EventoStand> TB_RF_EventoStand { get; set; } // TB_RF_EventoStand
        System.Data.Entity.DbSet<TB_RF_ExclusaoEmpresa> TB_RF_ExclusaoEmpresa { get; set; } // TB_RF_ExclusaoEmpresa
        System.Data.Entity.DbSet<TB_RF_Faq> TB_RF_Faq { get; set; } // TB_RF_Faq
        System.Data.Entity.DbSet<TB_RF_FormaPagamento> TB_RF_FormaPagamento { get; set; } // TB_RF_FormaPagamento
        System.Data.Entity.DbSet<TB_RF_Generica> TB_RF_Generica { get; set; } // TB_RF_Generica
        System.Data.Entity.DbSet<TB_RF_GruposDadosCadastrais> TB_RF_GruposDadosCadastrais { get; set; } // TB_RF_GruposDadosCadastrais
        System.Data.Entity.DbSet<TB_RF_Idioma> TB_RF_Idioma { get; set; } // TB_RF_Idioma
        System.Data.Entity.DbSet<TB_RF_ImpostoPedido> TB_RF_ImpostoPedido { get; set; } // TB_RF_ImpostoPedido
        System.Data.Entity.DbSet<TB_RF_Leitura_Atividade> TB_RF_Leitura_Atividade { get; set; } // TB_RF_Leitura_Atividade
        System.Data.Entity.DbSet<TB_RF_Leitura_Local> TB_RF_Leitura_Local { get; set; } // TB_RF_Leitura_Local
        System.Data.Entity.DbSet<TB_RF_Moeda> TB_RF_Moeda { get; set; } // TB_RF_Moeda
        System.Data.Entity.DbSet<TB_RF_PerguntaPadrao> TB_RF_PerguntaPadrao { get; set; } // TB_RF_PerguntaPadrao
        System.Data.Entity.DbSet<TB_RF_PreenchimentoFormulario> TB_RF_PreenchimentoFormulario { get; set; } // TB_RF_PreenchimentoFormulario
        System.Data.Entity.DbSet<TB_RF_QuestionarioPadrao> TB_RF_QuestionarioPadrao { get; set; } // TB_RF_QuestionarioPadrao
        System.Data.Entity.DbSet<TB_RF_RamoAtividade> TB_RF_RamoAtividade { get; set; } // TB_RF_RamoAtividade
        System.Data.Entity.DbSet<TB_RF_RegraMarketingSimples> TB_RF_RegraMarketingSimples { get; set; } // TB_RF_RegraMarketingSimples
        System.Data.Entity.DbSet<TB_RF_Saudacao> TB_RF_Saudacao { get; set; } // TB_RF_Saudacao
        System.Data.Entity.DbSet<TB_RF_Sexo> TB_RF_Sexo { get; set; } // TB_RF_Sexo
        System.Data.Entity.DbSet<TB_RF_SubCategoriaValidacao> TB_RF_SubCategoriaValidacao { get; set; } // TB_RF_SubCategoriaValidacao
        System.Data.Entity.DbSet<TB_RF_TipoCarta> TB_RF_TipoCarta { get; set; } // TB_RF_TipoCarta
        System.Data.Entity.DbSet<TB_RF_TipoContrato> TB_RF_TipoContrato { get; set; } // TB_RF_TipoContrato
        System.Data.Entity.DbSet<TB_RF_TipoCredenciamento> TB_RF_TipoCredenciamento { get; set; } // TB_RF_TipoCredenciamento
        System.Data.Entity.DbSet<TB_RF_TipoDocumento> TB_RF_TipoDocumento { get; set; } // TB_RF_TipoDocumento
        System.Data.Entity.DbSet<TB_RF_TipoEmpresa> TB_RF_TipoEmpresa { get; set; } // TB_RF_TipoEmpresa
        System.Data.Entity.DbSet<TB_RF_TipoFormulario> TB_RF_TipoFormulario { get; set; } // TB_RF_TipoFormulario
        System.Data.Entity.DbSet<TB_RF_TipoObjeto> TB_RF_TipoObjeto { get; set; } // TB_RF_TipoObjeto
        System.Data.Entity.DbSet<TB_RF_TipoPedido> TB_RF_TipoPedido { get; set; } // TB_RF_TipoPedido
        System.Data.Entity.DbSet<TB_RF_TipoRelatorio> TB_RF_TipoRelatorio { get; set; } // TB_RF_TipoRelatorio
        System.Data.Entity.DbSet<TB_RF_TipoValidacao> TB_RF_TipoValidacao { get; set; } // TB_RF_TipoValidacao

        int SaveChanges();
        System.Data.Entity.Infrastructure.DbChangeTracker ChangeTracker { get; }
        System.Data.Entity.Infrastructure.DbContextConfiguration Configuration { get; }
        System.Data.Entity.Database Database { get; }
        System.Data.Entity.Infrastructure.DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        System.Data.Entity.Infrastructure.DbEntityEntry Entry(object entity);
        System.Collections.Generic.IEnumerable<System.Data.Entity.Validation.DbEntityValidationResult> GetValidationErrors();
        System.Data.Entity.DbSet Set(System.Type entityType);
        System.Data.Entity.DbSet<TEntity> Set<TEntity>() where TEntity : class;
        string ToString();
    }

    #endregion

    #region Database context

    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class Interanet_v4_easyContext : System.Data.Entity.DbContext, IInteranet_v4_easyContext
    {
        public System.Data.Entity.DbSet<TB_AS_Alternativa_Questionario> TB_AS_Alternativa_Questionario { get; set; } // TB_AS_Alternativa_Questionario
        public System.Data.Entity.DbSet<TB_AS_AlternativaPadrao_Questionario> TB_AS_AlternativaPadrao_Questionario { get; set; } // TB_AS_AlternativaPadrao_Questionario
        public System.Data.Entity.DbSet<TB_AS_CodigoPromocional_tb_Pedido> TB_AS_CodigoPromocional_tb_Pedido { get; set; } // TB_AS_CodigoPromocional_tb_Pedido
        public System.Data.Entity.DbSet<TB_AS_Filial_Funcionario> TB_AS_Filial_Funcionario { get; set; } // TB_AS_Filial_Funcionario
        public System.Data.Entity.DbSet<TB_AS_Pedido_FormaPagamento> TB_AS_Pedido_FormaPagamento { get; set; } // TB_AS_Pedido_FormaPagamento
        public System.Data.Entity.DbSet<TB_AS_Pedido_Recibo> TB_AS_Pedido_Recibo { get; set; } // TB_AS_Pedido_Recibo
        public System.Data.Entity.DbSet<TB_AS_RegraMarketingSimples_ProdutoOperacional> TB_AS_RegraMarketingSimples_ProdutoOperacional { get; set; } // TB_AS_RegraMarketingSimples_ProdutoOperacional
        public System.Data.Entity.DbSet<TB_AS_TipoCredenciamento_Edicao> TB_AS_TipoCredenciamento_Edicao { get; set; } // TB_AS_TipoCredenciamento_Edicao
        public System.Data.Entity.DbSet<TB_CF_Edicao> TB_CF_Edicao { get; set; } // TB_CF_Edicao
        public System.Data.Entity.DbSet<TB_CF_EstruturaPadrao> TB_CF_EstruturaPadrao { get; set; } // TB_CF_EstruturaPadrao
        public System.Data.Entity.DbSet<TB_CF_ExecaoFormularioManualEletronico> TB_CF_ExecaoFormularioManualEletronico { get; set; } // TB_CF_ExecaoFormularioManualEletronico
        public System.Data.Entity.DbSet<TB_CF_Formulario> TB_CF_Formulario { get; set; } // TB_CF_Formulario
        public System.Data.Entity.DbSet<TB_CF_FormularioGenerico> TB_CF_FormularioGenerico { get; set; } // TB_CF_FormularioGenerico
        public System.Data.Entity.DbSet<TB_CF_FormularioManualEletronico> TB_CF_FormularioManualEletronico { get; set; } // TB_CF_FormularioManualEletronico
        public System.Data.Entity.DbSet<TB_CF_FormularioSistemaIntegrado> TB_CF_FormularioSistemaIntegrado { get; set; } // TB_CF_FormularioSistemaIntegrado
        public System.Data.Entity.DbSet<TB_CF_INT_Geral> TB_CF_INT_Geral { get; set; } // TB_CF_INT_Geral
        public System.Data.Entity.DbSet<TB_CF_Permissoes> TB_CF_Permissoes { get; set; } // TB_CF_Permissoes
        public System.Data.Entity.DbSet<TB_CF_Recibo> TB_CF_Recibo { get; set; } // TB_CF_Recibo
        public System.Data.Entity.DbSet<TB_CF_VendaDados> TB_CF_VendaDados { get; set; } // TB_CF_VendaDados
        public System.Data.Entity.DbSet<TB_DD_Agendamento> TB_DD_Agendamento { get; set; } // TB_DD_Agendamento
        public System.Data.Entity.DbSet<TB_DD_Alternativa> TB_DD_Alternativa { get; set; } // TB_DD_Alternativa
        public System.Data.Entity.DbSet<TB_DD_AlternativaQuestionarioManual> TB_DD_AlternativaQuestionarioManual { get; set; } // TB_DD_AlternativaQuestionarioManual
        public System.Data.Entity.DbSet<TB_DD_App_ConvideAmigo> TB_DD_App_ConvideAmigo { get; set; } // TB_DD_App_ConvideAmigo
        public System.Data.Entity.DbSet<TB_DD_App_NoticiaAviso> TB_DD_App_NoticiaAviso { get; set; } // TB_DD_App_NoticiaAviso
        public System.Data.Entity.DbSet<TB_DD_AprovacaoReprovacaoDeProjeto> TB_DD_AprovacaoReprovacaoDeProjeto { get; set; } // TB_DD_AprovacaoReprovacaoDeProjeto
        public System.Data.Entity.DbSet<TB_DD_AssistentePreenchimento> TB_DD_AssistentePreenchimento { get; set; } // TB_DD_AssistentePreenchimento
        public System.Data.Entity.DbSet<TB_DD_ATOM> TB_DD_ATOM { get; set; } // TB_DD_ATOM
        public System.Data.Entity.DbSet<TB_DD_BloqueioContratoProduto> TB_DD_BloqueioContratoProduto { get; set; } // TB_DD_BloqueioContratoProduto
        public System.Data.Entity.DbSet<TB_DD_CarrinhoOperacional> TB_DD_CarrinhoOperacional { get; set; } // TB_DD_CarrinhoOperacional
        public System.Data.Entity.DbSet<TB_DD_CarrinhoOperacional_VendaDados> TB_DD_CarrinhoOperacional_VendaDados { get; set; } // TB_DD_CarrinhoOperacional_VendaDados
        public System.Data.Entity.DbSet<TB_DD_CatalogoOficial> TB_DD_CatalogoOficial { get; set; } // TB_DD_CatalogoOficial
        public System.Data.Entity.DbSet<TB_DD_Cnab> TB_DD_Cnab { get; set; } // TB_DD_Cnab
        public System.Data.Entity.DbSet<TB_DD_CodigoPromocional> TB_DD_CodigoPromocional { get; set; } // TB_DD_CodigoPromocional
        public System.Data.Entity.DbSet<TB_DD_Codigos> TB_DD_Codigos { get; set; } // TB_DD_Codigos
        public System.Data.Entity.DbSet<TB_DD_Coletor> TB_DD_Coletor { get; set; } // TB_DD_Coletor
        public System.Data.Entity.DbSet<TB_DD_Comunicados> TB_DD_Comunicados { get; set; } // TB_DD_Comunicados
        public System.Data.Entity.DbSet<TB_DD_ConfExtracaoPersonalizada> TB_DD_ConfExtracaoPersonalizada { get; set; } // TB_DD_ConfExtracaoPersonalizada
        public System.Data.Entity.DbSet<TB_DD_ContratoExpositor> TB_DD_ContratoExpositor { get; set; } // TB_DD_ContratoExpositor
        public System.Data.Entity.DbSet<TB_DD_Convite> TB_DD_Convite { get; set; } // TB_DD_Convite
        public System.Data.Entity.DbSet<TB_DD_ConviteEletronico> TB_DD_ConviteEletronico { get; set; } // TB_DD_ConviteEletronico
        public System.Data.Entity.DbSet<TB_DD_ConviteEletronicoCancelado> TB_DD_ConviteEletronicoCancelado { get; set; } // TB_DD_ConviteEletronicoCancelado
        public System.Data.Entity.DbSet<TB_DD_ConviteEletronicoVip> TB_DD_ConviteEletronicoVip { get; set; } // TB_DD_ConviteEletronicoVip
        public System.Data.Entity.DbSet<TB_DD_ConvitesImpressos> TB_DD_ConvitesImpressos { get; set; } // TB_DD_ConvitesImpressos
        public System.Data.Entity.DbSet<TB_DD_CorCarpeteColuna> TB_DD_CorCarpeteColuna { get; set; } // TB_DD_CorCarpeteColuna
        public System.Data.Entity.DbSet<TB_DD_Credencial> TB_DD_Credencial { get; set; } // TB_DD_Credencial
        public System.Data.Entity.DbSet<TB_DD_DestinatarioComunicado> TB_DD_DestinatarioComunicado { get; set; } // TB_DD_DestinatarioComunicado
        public System.Data.Entity.DbSet<TB_DD_EletricaBasica> TB_DD_EletricaBasica { get; set; } // TB_DD_EletricaBasica
        public System.Data.Entity.DbSet<TB_DD_EmailMarketingImportados> TB_DD_EmailMarketingImportados { get; set; } // TB_DD_EmailMarketingImportados
        public System.Data.Entity.DbSet<TB_DD_EmissaoCredencial> TB_DD_EmissaoCredencial { get; set; } // TB_DD_EmissaoCredencial
        public System.Data.Entity.DbSet<TB_DD_Empresa> TB_DD_Empresa { get; set; } // TB_DD_Empresa
        public System.Data.Entity.DbSet<TB_DD_EntregaCredencialVIP> TB_DD_EntregaCredencialVIP { get; set; } // TB_DD_EntregaCredencialVIP
        public System.Data.Entity.DbSet<TB_DD_EstruturaCartaPersonalizada> TB_DD_EstruturaCartaPersonalizada { get; set; } // TB_DD_EstruturaCartaPersonalizada
        public System.Data.Entity.DbSet<TB_DD_EstruturaPersonalizada> TB_DD_EstruturaPersonalizada { get; set; } // TB_DD_EstruturaPersonalizada
        public System.Data.Entity.DbSet<TB_DD_Evento> TB_DD_Evento { get; set; } // TB_DD_Evento
        public System.Data.Entity.DbSet<TB_DD_ExtracaoPersonalizada> TB_DD_ExtracaoPersonalizada { get; set; } // TB_DD_ExtracaoPersonalizada
        public System.Data.Entity.DbSet<TB_DD_FaqRespostas> TB_DD_FaqRespostas { get; set; } // TB_DD_FaqRespostas
        public System.Data.Entity.DbSet<TB_DD_Filial> TB_DD_Filial { get; set; } // TB_DD_Filial
        public System.Data.Entity.DbSet<TB_DD_FotoPessoa> TB_DD_FotoPessoa { get; set; } // TB_DD_FotoPessoa
        public System.Data.Entity.DbSet<TB_DD_Funcionario> TB_DD_Funcionario { get; set; } // TB_DD_Funcionario
        public System.Data.Entity.DbSet<TB_DD_Geradora_Registro> TB_DD_Geradora_Registro { get; set; } // TB_DD_Geradora_Registro
        public System.Data.Entity.DbSet<TB_DD_GrupoProdutoCongresso> TB_DD_GrupoProdutoCongresso { get; set; } // TB_DD_GrupoProdutoCongresso
        public System.Data.Entity.DbSet<TB_DD_GrupoResponsavelCongresso> TB_DD_GrupoResponsavelCongresso { get; set; } // TB_DD_GrupoResponsavelCongresso
        public System.Data.Entity.DbSet<TB_DD_HelpDesk> TB_DD_HelpDesk { get; set; } // TB_DD_HelpDesk
        public System.Data.Entity.DbSet<TB_DD_ImagemAssistentePreenchimento> TB_DD_ImagemAssistentePreenchimento { get; set; } // TB_DD_ImagemAssistentePreenchimento
        public System.Data.Entity.DbSet<TB_DD_ImportacaoPessoa> TB_DD_ImportacaoPessoa { get; set; } // TB_DD_ImportacaoPessoa
        public System.Data.Entity.DbSet<TB_DD_ImpostoAPAS> TB_DD_ImpostoAPAS { get; set; } // TB_DD_ImpostoAPAS
        public System.Data.Entity.DbSet<TB_DD_InteracaoHelp> TB_DD_InteracaoHelp { get; set; } // TB_DD_InteracaoHelp
        public System.Data.Entity.DbSet<TB_DD_ItensLote> TB_DD_ItensLote { get; set; } // TB_DD_ItensLote
        public System.Data.Entity.DbSet<TB_DD_ItensPedidoCongresso> TB_DD_ItensPedidoCongresso { get; set; } // TB_DD_ItensPedidoCongresso
        public System.Data.Entity.DbSet<TB_DD_ItensPedidoOperacional> TB_DD_ItensPedidoOperacional { get; set; } // TB_DD_ItensPedidoOperacional
        public System.Data.Entity.DbSet<TB_DD_LeituraRegulamento> TB_DD_LeituraRegulamento { get; set; } // TB_DD_LeituraRegulamento
        public System.Data.Entity.DbSet<TB_DD_LimiteAplicacao> TB_DD_LimiteAplicacao { get; set; } // TB_DD_LimiteAplicacao
        public System.Data.Entity.DbSet<TB_DD_LogImportacaoAPAS> TB_DD_LogImportacaoAPAS { get; set; } // TB_DD_LogImportacaoAPAS
        public System.Data.Entity.DbSet<TB_DD_LoteImpressao> TB_DD_LoteImpressao { get; set; } // TB_DD_LoteImpressao
        public System.Data.Entity.DbSet<TB_DD_MaquinaseVeiculos> TB_DD_MaquinaseVeiculos { get; set; } // TB_DD_MaquinaseVeiculos
        public System.Data.Entity.DbSet<TB_DD_Monitoria> TB_DD_Monitoria { get; set; } // TB_DD_Monitoria
        public System.Data.Entity.DbSet<TB_DD_MonitoriaCielo> TB_DD_MonitoriaCielo { get; set; } // TB_DD_MonitoriaCielo
        public System.Data.Entity.DbSet<TB_DD_MonitoriaPayPal> TB_DD_MonitoriaPayPal { get; set; } // TB_DD_MonitoriaPayPal
        public System.Data.Entity.DbSet<TB_DD_MonitoriaRedeCard> TB_DD_MonitoriaRedeCard { get; set; } // TB_DD_MonitoriaRedeCard
        public System.Data.Entity.DbSet<TB_DD_MonitoriaShopLineItau> TB_DD_MonitoriaShopLineItau { get; set; } // TB_DD_MonitoriaShopLineItau
        public System.Data.Entity.DbSet<TB_DD_MonitoriaStone> TB_DD_MonitoriaStone { get; set; } // TB_DD_MonitoriaStone
        public System.Data.Entity.DbSet<TB_DD_Movimentacao> TB_DD_Movimentacao { get; set; } // TB_DD_Movimentacao
        public System.Data.Entity.DbSet<TB_DD_News> TB_DD_News { get; set; } // TB_DD_News
        public System.Data.Entity.DbSet<TB_DD_Pagamento> TB_DD_Pagamento { get; set; } // TB_DD_Pagamento
        public System.Data.Entity.DbSet<TB_DD_ParcelamentoBoleto> TB_DD_ParcelamentoBoleto { get; set; } // TB_DD_ParcelamentoBoleto
        public System.Data.Entity.DbSet<TB_DD_Pedido> TB_DD_Pedido { get; set; } // TB_DD_Pedido
        public System.Data.Entity.DbSet<TB_DD_Pergunta> TB_DD_Pergunta { get; set; } // TB_DD_Pergunta
        public System.Data.Entity.DbSet<TB_DD_PermissaoCorCarpeteColuna> TB_DD_PermissaoCorCarpeteColuna { get; set; } // TB_DD_PermissaoCorCarpeteColuna
        public System.Data.Entity.DbSet<TB_DD_Pessoa> TB_DD_Pessoa { get; set; } // TB_DD_Pessoa
        public System.Data.Entity.DbSet<TB_DD_PlacaIdentificacao> TB_DD_PlacaIdentificacao { get; set; } // TB_DD_PlacaIdentificacao
        public System.Data.Entity.DbSet<TB_DD_PlacaIdentificacaoColuna> TB_DD_PlacaIdentificacaoColuna { get; set; } // TB_DD_PlacaIdentificacaoColuna
        public System.Data.Entity.DbSet<TB_DD_PrecoProdutoCongresso> TB_DD_PrecoProdutoCongresso { get; set; } // TB_DD_PrecoProdutoCongresso
        public System.Data.Entity.DbSet<TB_DD_PrecoProdutoOperacional> TB_DD_PrecoProdutoOperacional { get; set; } // TB_DD_PrecoProdutoOperacional
        public System.Data.Entity.DbSet<TB_DD_PresencaCampanhaSejaBemVindo> TB_DD_PresencaCampanhaSejaBemVindo { get; set; } // TB_DD_PresencaCampanhaSejaBemVindo
        public System.Data.Entity.DbSet<TB_DD_ProdutoCongresso> TB_DD_ProdutoCongresso { get; set; } // TB_DD_ProdutoCongresso
        public System.Data.Entity.DbSet<TB_DD_ProdutoExpositores> TB_DD_ProdutoExpositores { get; set; } // TB_DD_ProdutoExpositores
        public System.Data.Entity.DbSet<TB_DD_ProdutoOperacional> TB_DD_ProdutoOperacional { get; set; } // TB_DD_ProdutoOperacional
        public System.Data.Entity.DbSet<TB_DD_Questionario> TB_DD_Questionario { get; set; } // TB_DD_Questionario
        public System.Data.Entity.DbSet<TB_DD_QuestionarioManual> TB_DD_QuestionarioManual { get; set; } // TB_DD_QuestionarioManual
        public System.Data.Entity.DbSet<TB_DD_ReenvioConvite> TB_DD_ReenvioConvite { get; set; } // TB_DD_ReenvioConvite
        public System.Data.Entity.DbSet<TB_DD_ReenvioConviteVip> TB_DD_ReenvioConviteVip { get; set; } // TB_DD_ReenvioConviteVip
        public System.Data.Entity.DbSet<TB_DD_Registro> TB_DD_Registro { get; set; } // TB_DD_Registro
        public System.Data.Entity.DbSet<TB_DD_Regulamento> TB_DD_Regulamento { get; set; } // TB_DD_Regulamento
        public System.Data.Entity.DbSet<TB_DD_Relatorio> TB_DD_Relatorio { get; set; } // TB_DD_Relatorio
        public System.Data.Entity.DbSet<TB_DD_ResponsavelCongresso> TB_DD_ResponsavelCongresso { get; set; } // TB_DD_ResponsavelCongresso
        public System.Data.Entity.DbSet<TB_DD_ResponsavelEmpresa> TB_DD_ResponsavelEmpresa { get; set; } // TB_DD_ResponsavelEmpresa
        public System.Data.Entity.DbSet<TB_DD_Resposta> TB_DD_Resposta { get; set; } // TB_DD_Resposta
        public System.Data.Entity.DbSet<TB_DD_RespostaQuestionarioManual> TB_DD_RespostaQuestionarioManual { get; set; } // TB_DD_RespostaQuestionarioManual
        public System.Data.Entity.DbSet<TB_DD_Simultaneidade> TB_DD_Simultaneidade { get; set; } // TB_DD_Simultaneidade
        public System.Data.Entity.DbSet<TB_DD_SistemaHabilitadoToken> TB_DD_SistemaHabilitadoToken { get; set; } // TB_DD_SistemaHabilitadoToken
        public System.Data.Entity.DbSet<TB_DD_SiteColuna> TB_DD_SiteColuna { get; set; } // TB_DD_SiteColuna
        public System.Data.Entity.DbSet<TB_DD_Stand> TB_DD_Stand { get; set; } // TB_DD_Stand
        public System.Data.Entity.DbSet<TB_DD_SubAlternativaQuestionarioManual> TB_DD_SubAlternativaQuestionarioManual { get; set; } // TB_DD_SubAlternativaQuestionarioManual
        public System.Data.Entity.DbSet<TB_DD_SubCategoria> TB_DD_SubCategoria { get; set; } // TB_DD_SubCategoria
        public System.Data.Entity.DbSet<TB_DD_Ticket> TB_DD_Ticket { get; set; } // TB_DD_Ticket
        public System.Data.Entity.DbSet<TB_DD_Token> TB_DD_Token { get; set; } // TB_DD_Token
        public System.Data.Entity.DbSet<TB_DD_Usuario> TB_DD_Usuario { get; set; } // TB_DD_Usuario
        public System.Data.Entity.DbSet<TB_DD_ValorContrato> TB_DD_ValorContrato { get; set; } // TB_DD_ValorContrato
        public System.Data.Entity.DbSet<TB_DD_ValorContratoParcela> TB_DD_ValorContratoParcela { get; set; } // TB_DD_ValorContratoParcela
        public System.Data.Entity.DbSet<TB_LG_Acesso> TB_LG_Acesso { get; set; } // TB_LG_Acesso
        public System.Data.Entity.DbSet<TB_LG_AlteraSubCategoria> TB_LG_AlteraSubCategoria { get; set; } // TB_LG_AlteraSubCategoria
        public System.Data.Entity.DbSet<TB_LG_CancelamentoAutomaticoPedido> TB_LG_CancelamentoAutomaticoPedido { get; set; } // TB_LG_CancelamentoAutomaticoPedido
        public System.Data.Entity.DbSet<TB_LG_Erro> TB_LG_Erro { get; set; } // TB_LG_Erro
        public System.Data.Entity.DbSet<TB_LG_FormularioManualEletronico> TB_LG_FormularioManualEletronico { get; set; } // TB_LG_FormularioManualEletronico
        public System.Data.Entity.DbSet<TB_LG_Geral> TB_LG_Geral { get; set; } // TB_LG_Geral
        public System.Data.Entity.DbSet<TB_LG_GeralDepEasy> TB_LG_GeralDepEasy { get; set; } // TB_LG_GeralDepEasy
        public System.Data.Entity.DbSet<TB_LG_GeralDepFinanceiro> TB_LG_GeralDepFinanceiro { get; set; } // TB_LG_GeralDepFinanceiro
        public System.Data.Entity.DbSet<TB_LG_GeralDepUsuario> TB_LG_GeralDepUsuario { get; set; } // TB_LG_GeralDepUsuario
        public System.Data.Entity.DbSet<TB_LG_PaypalPlus> TB_LG_PaypalPlus { get; set; } // TB_LG_PaypalPlus
        public System.Data.Entity.DbSet<TB_LG_RF_GeralAcao> TB_LG_RF_GeralAcao { get; set; } // TB_LG_RF_GeralAcao
        public System.Data.Entity.DbSet<TB_LG_RF_GeralDepartamento> TB_LG_RF_GeralDepartamento { get; set; } // TB_LG_RF_GeralDepartamento
        public System.Data.Entity.DbSet<TB_LG_RF_GeralUsuario> TB_LG_RF_GeralUsuario { get; set; } // TB_LG_RF_GeralUsuario
        public System.Data.Entity.DbSet<TB_RF_AliquotaImposto> TB_RF_AliquotaImposto { get; set; } // TB_RF_AliquotaImposto
        public System.Data.Entity.DbSet<TB_RF_AlternativaPadrao> TB_RF_AlternativaPadrao { get; set; } // TB_RF_AlternativaPadrao
        public System.Data.Entity.DbSet<TB_RF_AreaAtuacao> TB_RF_AreaAtuacao { get; set; } // TB_RF_AreaAtuacao
        public System.Data.Entity.DbSet<TB_RF_Atividades> TB_RF_Atividades { get; set; } // TB_RF_Atividades
        public System.Data.Entity.DbSet<TB_RF_Categoria> TB_RF_Categoria { get; set; } // TB_RF_Categoria
        public System.Data.Entity.DbSet<TB_RF_CnaeSebrae> TB_RF_CnaeSebrae { get; set; } // TB_RF_CnaeSebrae
        public System.Data.Entity.DbSet<TB_RF_ConfImpostoRetencaoValorAcimaDe> TB_RF_ConfImpostoRetencaoValorAcimaDe { get; set; } // TB_RF_ConfImpostoRetencaoValorAcimaDe
        public System.Data.Entity.DbSet<TB_RF_ConfImpostos> TB_RF_ConfImpostos { get; set; } // TB_RF_ConfImpostos
        public System.Data.Entity.DbSet<TB_RF_Cor> TB_RF_Cor { get; set; } // TB_RF_Cor
        public System.Data.Entity.DbSet<TB_RF_Cotacao> TB_RF_Cotacao { get; set; } // TB_RF_Cotacao
        public System.Data.Entity.DbSet<TB_RF_EventoStand> TB_RF_EventoStand { get; set; } // TB_RF_EventoStand
        public System.Data.Entity.DbSet<TB_RF_ExclusaoEmpresa> TB_RF_ExclusaoEmpresa { get; set; } // TB_RF_ExclusaoEmpresa
        public System.Data.Entity.DbSet<TB_RF_Faq> TB_RF_Faq { get; set; } // TB_RF_Faq
        public System.Data.Entity.DbSet<TB_RF_FormaPagamento> TB_RF_FormaPagamento { get; set; } // TB_RF_FormaPagamento
        public System.Data.Entity.DbSet<TB_RF_Generica> TB_RF_Generica { get; set; } // TB_RF_Generica
        public System.Data.Entity.DbSet<TB_RF_GruposDadosCadastrais> TB_RF_GruposDadosCadastrais { get; set; } // TB_RF_GruposDadosCadastrais
        public System.Data.Entity.DbSet<TB_RF_Idioma> TB_RF_Idioma { get; set; } // TB_RF_Idioma
        public System.Data.Entity.DbSet<TB_RF_ImpostoPedido> TB_RF_ImpostoPedido { get; set; } // TB_RF_ImpostoPedido
        public System.Data.Entity.DbSet<TB_RF_Leitura_Atividade> TB_RF_Leitura_Atividade { get; set; } // TB_RF_Leitura_Atividade
        public System.Data.Entity.DbSet<TB_RF_Leitura_Local> TB_RF_Leitura_Local { get; set; } // TB_RF_Leitura_Local
        public System.Data.Entity.DbSet<TB_RF_Moeda> TB_RF_Moeda { get; set; } // TB_RF_Moeda
        public System.Data.Entity.DbSet<TB_RF_PerguntaPadrao> TB_RF_PerguntaPadrao { get; set; } // TB_RF_PerguntaPadrao
        public System.Data.Entity.DbSet<TB_RF_PreenchimentoFormulario> TB_RF_PreenchimentoFormulario { get; set; } // TB_RF_PreenchimentoFormulario
        public System.Data.Entity.DbSet<TB_RF_QuestionarioPadrao> TB_RF_QuestionarioPadrao { get; set; } // TB_RF_QuestionarioPadrao
        public System.Data.Entity.DbSet<TB_RF_RamoAtividade> TB_RF_RamoAtividade { get; set; } // TB_RF_RamoAtividade
        public System.Data.Entity.DbSet<TB_RF_RegraMarketingSimples> TB_RF_RegraMarketingSimples { get; set; } // TB_RF_RegraMarketingSimples
        public System.Data.Entity.DbSet<TB_RF_Saudacao> TB_RF_Saudacao { get; set; } // TB_RF_Saudacao
        public System.Data.Entity.DbSet<TB_RF_Sexo> TB_RF_Sexo { get; set; } // TB_RF_Sexo
        public System.Data.Entity.DbSet<TB_RF_SubCategoriaValidacao> TB_RF_SubCategoriaValidacao { get; set; } // TB_RF_SubCategoriaValidacao
        public System.Data.Entity.DbSet<TB_RF_TipoCarta> TB_RF_TipoCarta { get; set; } // TB_RF_TipoCarta
        public System.Data.Entity.DbSet<TB_RF_TipoContrato> TB_RF_TipoContrato { get; set; } // TB_RF_TipoContrato
        public System.Data.Entity.DbSet<TB_RF_TipoCredenciamento> TB_RF_TipoCredenciamento { get; set; } // TB_RF_TipoCredenciamento
        public System.Data.Entity.DbSet<TB_RF_TipoDocumento> TB_RF_TipoDocumento { get; set; } // TB_RF_TipoDocumento
        public System.Data.Entity.DbSet<TB_RF_TipoEmpresa> TB_RF_TipoEmpresa { get; set; } // TB_RF_TipoEmpresa
        public System.Data.Entity.DbSet<TB_RF_TipoFormulario> TB_RF_TipoFormulario { get; set; } // TB_RF_TipoFormulario
        public System.Data.Entity.DbSet<TB_RF_TipoObjeto> TB_RF_TipoObjeto { get; set; } // TB_RF_TipoObjeto
        public System.Data.Entity.DbSet<TB_RF_TipoPedido> TB_RF_TipoPedido { get; set; } // TB_RF_TipoPedido
        public System.Data.Entity.DbSet<TB_RF_TipoRelatorio> TB_RF_TipoRelatorio { get; set; } // TB_RF_TipoRelatorio
        public System.Data.Entity.DbSet<TB_RF_TipoValidacao> TB_RF_TipoValidacao { get; set; } // TB_RF_TipoValidacao

        static Interanet_v4_easyContext()
        {
            System.Data.Entity.Database.SetInitializer<Interanet_v4_easyContext>(null);
        }

        public Interanet_v4_easyContext()
            : base("Name=Interanet_v4_easyContext")
        {
        }

        public Interanet_v4_easyContext(string connectionString)
            : base(connectionString)
        {
        }

        public Interanet_v4_easyContext(string connectionString, System.Data.Entity.Infrastructure.DbCompiledModel model)
            : base(connectionString, model)
        {
        }

        public Interanet_v4_easyContext(System.Data.Common.DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        {
        }

        public Interanet_v4_easyContext(System.Data.Common.DbConnection existingConnection, System.Data.Entity.Infrastructure.DbCompiledModel model, bool contextOwnsConnection)
            : base(existingConnection, model, contextOwnsConnection)
        {
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public bool IsSqlParameterNull(System.Data.SqlClient.SqlParameter param)
        {
            var sqlValue = param.SqlValue;
            var nullableValue = sqlValue as System.Data.SqlTypes.INullable;
            if (nullableValue != null)
                return nullableValue.IsNull;
            return (sqlValue == null || sqlValue == System.DBNull.Value);
        }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new TB_AS_Alternativa_QuestionarioConfiguration());
            modelBuilder.Configurations.Add(new TB_AS_AlternativaPadrao_QuestionarioConfiguration());
            modelBuilder.Configurations.Add(new TB_AS_CodigoPromocional_tb_PedidoConfiguration());
            modelBuilder.Configurations.Add(new TB_AS_Filial_FuncionarioConfiguration());
            modelBuilder.Configurations.Add(new TB_AS_Pedido_FormaPagamentoConfiguration());
            modelBuilder.Configurations.Add(new TB_AS_Pedido_ReciboConfiguration());
            modelBuilder.Configurations.Add(new TB_AS_RegraMarketingSimples_ProdutoOperacionalConfiguration());
            modelBuilder.Configurations.Add(new TB_AS_TipoCredenciamento_EdicaoConfiguration());
            modelBuilder.Configurations.Add(new TB_CF_EdicaoConfiguration());
            modelBuilder.Configurations.Add(new TB_CF_EstruturaPadraoConfiguration());
            modelBuilder.Configurations.Add(new TB_CF_ExecaoFormularioManualEletronicoConfiguration());
            modelBuilder.Configurations.Add(new TB_CF_FormularioConfiguration());
            modelBuilder.Configurations.Add(new TB_CF_FormularioGenericoConfiguration());
            modelBuilder.Configurations.Add(new TB_CF_FormularioManualEletronicoConfiguration());
            modelBuilder.Configurations.Add(new TB_CF_FormularioSistemaIntegradoConfiguration());
            modelBuilder.Configurations.Add(new TB_CF_INT_GeralConfiguration());
            modelBuilder.Configurations.Add(new TB_CF_PermissoesConfiguration());
            modelBuilder.Configurations.Add(new TB_CF_ReciboConfiguration());
            modelBuilder.Configurations.Add(new TB_CF_VendaDadosConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_AgendamentoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_AlternativaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_AlternativaQuestionarioManualConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_App_ConvideAmigoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_App_NoticiaAvisoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_AprovacaoReprovacaoDeProjetoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_AssistentePreenchimentoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ATOMConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_BloqueioContratoProdutoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_CarrinhoOperacionalConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_CarrinhoOperacional_VendaDadosConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_CatalogoOficialConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_CnabConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_CodigoPromocionalConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_CodigosConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ColetorConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ComunicadosConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ConfExtracaoPersonalizadaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ContratoExpositorConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ConviteConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ConviteEletronicoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ConviteEletronicoCanceladoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ConviteEletronicoVipConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ConvitesImpressosConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_CorCarpeteColunaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_CredencialConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_DestinatarioComunicadoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_EletricaBasicaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_EmailMarketingImportadosConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_EmissaoCredencialConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_EmpresaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_EntregaCredencialVIPConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_EstruturaCartaPersonalizadaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_EstruturaPersonalizadaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_EventoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ExtracaoPersonalizadaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_FaqRespostasConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_FilialConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_FotoPessoaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_FuncionarioConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_Geradora_RegistroConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_GrupoProdutoCongressoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_GrupoResponsavelCongressoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_HelpDeskConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ImagemAssistentePreenchimentoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ImportacaoPessoaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ImpostoAPASConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_InteracaoHelpConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ItensLoteConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ItensPedidoCongressoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ItensPedidoOperacionalConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_LeituraRegulamentoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_LimiteAplicacaoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_LogImportacaoAPASConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_LoteImpressaoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_MaquinaseVeiculosConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_MonitoriaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_MonitoriaCieloConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_MonitoriaPayPalConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_MonitoriaRedeCardConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_MonitoriaShopLineItauConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_MonitoriaStoneConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_MovimentacaoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_NewsConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_PagamentoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ParcelamentoBoletoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_PedidoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_PerguntaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_PermissaoCorCarpeteColunaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_PessoaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_PlacaIdentificacaoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_PlacaIdentificacaoColunaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_PrecoProdutoCongressoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_PrecoProdutoOperacionalConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_PresencaCampanhaSejaBemVindoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ProdutoCongressoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ProdutoExpositoresConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ProdutoOperacionalConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_QuestionarioConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_QuestionarioManualConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ReenvioConviteConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ReenvioConviteVipConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_RegistroConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_RegulamentoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_RelatorioConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ResponsavelCongressoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ResponsavelEmpresaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_RespostaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_RespostaQuestionarioManualConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_SimultaneidadeConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_SistemaHabilitadoTokenConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_SiteColunaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_StandConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_SubAlternativaQuestionarioManualConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_SubCategoriaConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_TicketConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_TokenConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_UsuarioConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ValorContratoConfiguration());
            modelBuilder.Configurations.Add(new TB_DD_ValorContratoParcelaConfiguration());
            modelBuilder.Configurations.Add(new TB_LG_AcessoConfiguration());
            modelBuilder.Configurations.Add(new TB_LG_AlteraSubCategoriaConfiguration());
            modelBuilder.Configurations.Add(new TB_LG_CancelamentoAutomaticoPedidoConfiguration());
            modelBuilder.Configurations.Add(new TB_LG_ErroConfiguration());
            modelBuilder.Configurations.Add(new TB_LG_FormularioManualEletronicoConfiguration());
            modelBuilder.Configurations.Add(new TB_LG_GeralConfiguration());
            modelBuilder.Configurations.Add(new TB_LG_GeralDepEasyConfiguration());
            modelBuilder.Configurations.Add(new TB_LG_GeralDepFinanceiroConfiguration());
            modelBuilder.Configurations.Add(new TB_LG_GeralDepUsuarioConfiguration());
            modelBuilder.Configurations.Add(new TB_LG_PaypalPlusConfiguration());
            modelBuilder.Configurations.Add(new TB_LG_RF_GeralAcaoConfiguration());
            modelBuilder.Configurations.Add(new TB_LG_RF_GeralDepartamentoConfiguration());
            modelBuilder.Configurations.Add(new TB_LG_RF_GeralUsuarioConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_AliquotaImpostoConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_AlternativaPadraoConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_AreaAtuacaoConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_AtividadesConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_CategoriaConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_CnaeSebraeConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_ConfImpostoRetencaoValorAcimaDeConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_ConfImpostosConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_CorConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_CotacaoConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_EventoStandConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_ExclusaoEmpresaConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_FaqConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_FormaPagamentoConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_GenericaConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_GruposDadosCadastraisConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_IdiomaConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_ImpostoPedidoConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_Leitura_AtividadeConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_Leitura_LocalConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_MoedaConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_PerguntaPadraoConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_PreenchimentoFormularioConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_QuestionarioPadraoConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_RamoAtividadeConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_RegraMarketingSimplesConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_SaudacaoConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_SexoConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_SubCategoriaValidacaoConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_TipoCartaConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_TipoContratoConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_TipoCredenciamentoConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_TipoDocumentoConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_TipoEmpresaConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_TipoFormularioConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_TipoObjetoConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_TipoPedidoConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_TipoRelatorioConfiguration());
            modelBuilder.Configurations.Add(new TB_RF_TipoValidacaoConfiguration());
        }

        public static System.Data.Entity.DbModelBuilder CreateModel(System.Data.Entity.DbModelBuilder modelBuilder, string schema)
        {
            modelBuilder.Configurations.Add(new TB_AS_Alternativa_QuestionarioConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_AS_AlternativaPadrao_QuestionarioConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_AS_CodigoPromocional_tb_PedidoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_AS_Filial_FuncionarioConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_AS_Pedido_FormaPagamentoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_AS_Pedido_ReciboConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_AS_RegraMarketingSimples_ProdutoOperacionalConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_AS_TipoCredenciamento_EdicaoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_CF_EdicaoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_CF_EstruturaPadraoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_CF_ExecaoFormularioManualEletronicoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_CF_FormularioConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_CF_FormularioGenericoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_CF_FormularioManualEletronicoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_CF_FormularioSistemaIntegradoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_CF_INT_GeralConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_CF_PermissoesConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_CF_ReciboConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_CF_VendaDadosConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_AgendamentoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_AlternativaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_AlternativaQuestionarioManualConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_App_ConvideAmigoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_App_NoticiaAvisoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_AprovacaoReprovacaoDeProjetoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_AssistentePreenchimentoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ATOMConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_BloqueioContratoProdutoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_CarrinhoOperacionalConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_CarrinhoOperacional_VendaDadosConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_CatalogoOficialConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_CnabConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_CodigoPromocionalConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_CodigosConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ColetorConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ComunicadosConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ConfExtracaoPersonalizadaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ContratoExpositorConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ConviteConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ConviteEletronicoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ConviteEletronicoCanceladoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ConviteEletronicoVipConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ConvitesImpressosConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_CorCarpeteColunaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_CredencialConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_DestinatarioComunicadoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_EletricaBasicaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_EmailMarketingImportadosConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_EmissaoCredencialConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_EmpresaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_EntregaCredencialVIPConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_EstruturaCartaPersonalizadaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_EstruturaPersonalizadaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_EventoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ExtracaoPersonalizadaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_FaqRespostasConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_FilialConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_FotoPessoaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_FuncionarioConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_Geradora_RegistroConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_GrupoProdutoCongressoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_GrupoResponsavelCongressoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_HelpDeskConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ImagemAssistentePreenchimentoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ImportacaoPessoaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ImpostoAPASConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_InteracaoHelpConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ItensLoteConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ItensPedidoCongressoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ItensPedidoOperacionalConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_LeituraRegulamentoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_LimiteAplicacaoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_LogImportacaoAPASConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_LoteImpressaoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_MaquinaseVeiculosConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_MonitoriaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_MonitoriaCieloConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_MonitoriaPayPalConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_MonitoriaRedeCardConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_MonitoriaShopLineItauConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_MonitoriaStoneConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_MovimentacaoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_NewsConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_PagamentoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ParcelamentoBoletoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_PedidoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_PerguntaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_PermissaoCorCarpeteColunaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_PessoaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_PlacaIdentificacaoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_PlacaIdentificacaoColunaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_PrecoProdutoCongressoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_PrecoProdutoOperacionalConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_PresencaCampanhaSejaBemVindoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ProdutoCongressoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ProdutoExpositoresConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ProdutoOperacionalConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_QuestionarioConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_QuestionarioManualConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ReenvioConviteConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ReenvioConviteVipConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_RegistroConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_RegulamentoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_RelatorioConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ResponsavelCongressoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ResponsavelEmpresaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_RespostaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_RespostaQuestionarioManualConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_SimultaneidadeConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_SistemaHabilitadoTokenConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_SiteColunaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_StandConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_SubAlternativaQuestionarioManualConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_SubCategoriaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_TicketConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_TokenConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_UsuarioConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ValorContratoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_DD_ValorContratoParcelaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_LG_AcessoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_LG_AlteraSubCategoriaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_LG_CancelamentoAutomaticoPedidoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_LG_ErroConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_LG_FormularioManualEletronicoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_LG_GeralConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_LG_GeralDepEasyConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_LG_GeralDepFinanceiroConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_LG_GeralDepUsuarioConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_LG_PaypalPlusConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_LG_RF_GeralAcaoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_LG_RF_GeralDepartamentoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_LG_RF_GeralUsuarioConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_AliquotaImpostoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_AlternativaPadraoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_AreaAtuacaoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_AtividadesConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_CategoriaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_CnaeSebraeConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_ConfImpostoRetencaoValorAcimaDeConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_ConfImpostosConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_CorConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_CotacaoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_EventoStandConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_ExclusaoEmpresaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_FaqConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_FormaPagamentoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_GenericaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_GruposDadosCadastraisConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_IdiomaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_ImpostoPedidoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_Leitura_AtividadeConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_Leitura_LocalConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_MoedaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_PerguntaPadraoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_PreenchimentoFormularioConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_QuestionarioPadraoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_RamoAtividadeConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_RegraMarketingSimplesConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_SaudacaoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_SexoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_SubCategoriaValidacaoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_TipoCartaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_TipoContratoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_TipoCredenciamentoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_TipoDocumentoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_TipoEmpresaConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_TipoFormularioConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_TipoObjetoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_TipoPedidoConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_TipoRelatorioConfiguration(schema));
            modelBuilder.Configurations.Add(new TB_RF_TipoValidacaoConfiguration(schema));
            return modelBuilder;
        }
    }
    #endregion

    #region Database context factory

    public class Interanet_v4_easyContextFactory : System.Data.Entity.Infrastructure.IDbContextFactory<Interanet_v4_easyContext>
    {
        public Interanet_v4_easyContext Create()
        {
            return new Interanet_v4_easyContext();
        }
    }

    #endregion

    #region Fake Database context

    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class FakeInteranet_v4_easyContext : IInteranet_v4_easyContext
    {
        public System.Data.Entity.DbSet<TB_AS_Alternativa_Questionario> TB_AS_Alternativa_Questionario { get; set; }
        public System.Data.Entity.DbSet<TB_AS_AlternativaPadrao_Questionario> TB_AS_AlternativaPadrao_Questionario { get; set; }
        public System.Data.Entity.DbSet<TB_AS_CodigoPromocional_tb_Pedido> TB_AS_CodigoPromocional_tb_Pedido { get; set; }
        public System.Data.Entity.DbSet<TB_AS_Filial_Funcionario> TB_AS_Filial_Funcionario { get; set; }
        public System.Data.Entity.DbSet<TB_AS_Pedido_FormaPagamento> TB_AS_Pedido_FormaPagamento { get; set; }
        public System.Data.Entity.DbSet<TB_AS_Pedido_Recibo> TB_AS_Pedido_Recibo { get; set; }
        public System.Data.Entity.DbSet<TB_AS_RegraMarketingSimples_ProdutoOperacional> TB_AS_RegraMarketingSimples_ProdutoOperacional { get; set; }
        public System.Data.Entity.DbSet<TB_AS_TipoCredenciamento_Edicao> TB_AS_TipoCredenciamento_Edicao { get; set; }
        public System.Data.Entity.DbSet<TB_CF_Edicao> TB_CF_Edicao { get; set; }
        public System.Data.Entity.DbSet<TB_CF_EstruturaPadrao> TB_CF_EstruturaPadrao { get; set; }
        public System.Data.Entity.DbSet<TB_CF_ExecaoFormularioManualEletronico> TB_CF_ExecaoFormularioManualEletronico { get; set; }
        public System.Data.Entity.DbSet<TB_CF_Formulario> TB_CF_Formulario { get; set; }
        public System.Data.Entity.DbSet<TB_CF_FormularioGenerico> TB_CF_FormularioGenerico { get; set; }
        public System.Data.Entity.DbSet<TB_CF_FormularioManualEletronico> TB_CF_FormularioManualEletronico { get; set; }
        public System.Data.Entity.DbSet<TB_CF_FormularioSistemaIntegrado> TB_CF_FormularioSistemaIntegrado { get; set; }
        public System.Data.Entity.DbSet<TB_CF_INT_Geral> TB_CF_INT_Geral { get; set; }
        public System.Data.Entity.DbSet<TB_CF_Permissoes> TB_CF_Permissoes { get; set; }
        public System.Data.Entity.DbSet<TB_CF_Recibo> TB_CF_Recibo { get; set; }
        public System.Data.Entity.DbSet<TB_CF_VendaDados> TB_CF_VendaDados { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Agendamento> TB_DD_Agendamento { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Alternativa> TB_DD_Alternativa { get; set; }
        public System.Data.Entity.DbSet<TB_DD_AlternativaQuestionarioManual> TB_DD_AlternativaQuestionarioManual { get; set; }
        public System.Data.Entity.DbSet<TB_DD_App_ConvideAmigo> TB_DD_App_ConvideAmigo { get; set; }
        public System.Data.Entity.DbSet<TB_DD_App_NoticiaAviso> TB_DD_App_NoticiaAviso { get; set; }
        public System.Data.Entity.DbSet<TB_DD_AprovacaoReprovacaoDeProjeto> TB_DD_AprovacaoReprovacaoDeProjeto { get; set; }
        public System.Data.Entity.DbSet<TB_DD_AssistentePreenchimento> TB_DD_AssistentePreenchimento { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ATOM> TB_DD_ATOM { get; set; }
        public System.Data.Entity.DbSet<TB_DD_BloqueioContratoProduto> TB_DD_BloqueioContratoProduto { get; set; }
        public System.Data.Entity.DbSet<TB_DD_CarrinhoOperacional> TB_DD_CarrinhoOperacional { get; set; }
        public System.Data.Entity.DbSet<TB_DD_CarrinhoOperacional_VendaDados> TB_DD_CarrinhoOperacional_VendaDados { get; set; }
        public System.Data.Entity.DbSet<TB_DD_CatalogoOficial> TB_DD_CatalogoOficial { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Cnab> TB_DD_Cnab { get; set; }
        public System.Data.Entity.DbSet<TB_DD_CodigoPromocional> TB_DD_CodigoPromocional { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Codigos> TB_DD_Codigos { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Coletor> TB_DD_Coletor { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Comunicados> TB_DD_Comunicados { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ConfExtracaoPersonalizada> TB_DD_ConfExtracaoPersonalizada { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ContratoExpositor> TB_DD_ContratoExpositor { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Convite> TB_DD_Convite { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ConviteEletronico> TB_DD_ConviteEletronico { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ConviteEletronicoCancelado> TB_DD_ConviteEletronicoCancelado { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ConviteEletronicoVip> TB_DD_ConviteEletronicoVip { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ConvitesImpressos> TB_DD_ConvitesImpressos { get; set; }
        public System.Data.Entity.DbSet<TB_DD_CorCarpeteColuna> TB_DD_CorCarpeteColuna { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Credencial> TB_DD_Credencial { get; set; }
        public System.Data.Entity.DbSet<TB_DD_DestinatarioComunicado> TB_DD_DestinatarioComunicado { get; set; }
        public System.Data.Entity.DbSet<TB_DD_EletricaBasica> TB_DD_EletricaBasica { get; set; }
        public System.Data.Entity.DbSet<TB_DD_EmailMarketingImportados> TB_DD_EmailMarketingImportados { get; set; }
        public System.Data.Entity.DbSet<TB_DD_EmissaoCredencial> TB_DD_EmissaoCredencial { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Empresa> TB_DD_Empresa { get; set; }
        public System.Data.Entity.DbSet<TB_DD_EntregaCredencialVIP> TB_DD_EntregaCredencialVIP { get; set; }
        public System.Data.Entity.DbSet<TB_DD_EstruturaCartaPersonalizada> TB_DD_EstruturaCartaPersonalizada { get; set; }
        public System.Data.Entity.DbSet<TB_DD_EstruturaPersonalizada> TB_DD_EstruturaPersonalizada { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Evento> TB_DD_Evento { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ExtracaoPersonalizada> TB_DD_ExtracaoPersonalizada { get; set; }
        public System.Data.Entity.DbSet<TB_DD_FaqRespostas> TB_DD_FaqRespostas { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Filial> TB_DD_Filial { get; set; }
        public System.Data.Entity.DbSet<TB_DD_FotoPessoa> TB_DD_FotoPessoa { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Funcionario> TB_DD_Funcionario { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Geradora_Registro> TB_DD_Geradora_Registro { get; set; }
        public System.Data.Entity.DbSet<TB_DD_GrupoProdutoCongresso> TB_DD_GrupoProdutoCongresso { get; set; }
        public System.Data.Entity.DbSet<TB_DD_GrupoResponsavelCongresso> TB_DD_GrupoResponsavelCongresso { get; set; }
        public System.Data.Entity.DbSet<TB_DD_HelpDesk> TB_DD_HelpDesk { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ImagemAssistentePreenchimento> TB_DD_ImagemAssistentePreenchimento { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ImportacaoPessoa> TB_DD_ImportacaoPessoa { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ImpostoAPAS> TB_DD_ImpostoAPAS { get; set; }
        public System.Data.Entity.DbSet<TB_DD_InteracaoHelp> TB_DD_InteracaoHelp { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ItensLote> TB_DD_ItensLote { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ItensPedidoCongresso> TB_DD_ItensPedidoCongresso { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ItensPedidoOperacional> TB_DD_ItensPedidoOperacional { get; set; }
        public System.Data.Entity.DbSet<TB_DD_LeituraRegulamento> TB_DD_LeituraRegulamento { get; set; }
        public System.Data.Entity.DbSet<TB_DD_LimiteAplicacao> TB_DD_LimiteAplicacao { get; set; }
        public System.Data.Entity.DbSet<TB_DD_LogImportacaoAPAS> TB_DD_LogImportacaoAPAS { get; set; }
        public System.Data.Entity.DbSet<TB_DD_LoteImpressao> TB_DD_LoteImpressao { get; set; }
        public System.Data.Entity.DbSet<TB_DD_MaquinaseVeiculos> TB_DD_MaquinaseVeiculos { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Monitoria> TB_DD_Monitoria { get; set; }
        public System.Data.Entity.DbSet<TB_DD_MonitoriaCielo> TB_DD_MonitoriaCielo { get; set; }
        public System.Data.Entity.DbSet<TB_DD_MonitoriaPayPal> TB_DD_MonitoriaPayPal { get; set; }
        public System.Data.Entity.DbSet<TB_DD_MonitoriaRedeCard> TB_DD_MonitoriaRedeCard { get; set; }
        public System.Data.Entity.DbSet<TB_DD_MonitoriaShopLineItau> TB_DD_MonitoriaShopLineItau { get; set; }
        public System.Data.Entity.DbSet<TB_DD_MonitoriaStone> TB_DD_MonitoriaStone { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Movimentacao> TB_DD_Movimentacao { get; set; }
        public System.Data.Entity.DbSet<TB_DD_News> TB_DD_News { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Pagamento> TB_DD_Pagamento { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ParcelamentoBoleto> TB_DD_ParcelamentoBoleto { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Pedido> TB_DD_Pedido { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Pergunta> TB_DD_Pergunta { get; set; }
        public System.Data.Entity.DbSet<TB_DD_PermissaoCorCarpeteColuna> TB_DD_PermissaoCorCarpeteColuna { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Pessoa> TB_DD_Pessoa { get; set; }
        public System.Data.Entity.DbSet<TB_DD_PlacaIdentificacao> TB_DD_PlacaIdentificacao { get; set; }
        public System.Data.Entity.DbSet<TB_DD_PlacaIdentificacaoColuna> TB_DD_PlacaIdentificacaoColuna { get; set; }
        public System.Data.Entity.DbSet<TB_DD_PrecoProdutoCongresso> TB_DD_PrecoProdutoCongresso { get; set; }
        public System.Data.Entity.DbSet<TB_DD_PrecoProdutoOperacional> TB_DD_PrecoProdutoOperacional { get; set; }
        public System.Data.Entity.DbSet<TB_DD_PresencaCampanhaSejaBemVindo> TB_DD_PresencaCampanhaSejaBemVindo { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ProdutoCongresso> TB_DD_ProdutoCongresso { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ProdutoExpositores> TB_DD_ProdutoExpositores { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ProdutoOperacional> TB_DD_ProdutoOperacional { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Questionario> TB_DD_Questionario { get; set; }
        public System.Data.Entity.DbSet<TB_DD_QuestionarioManual> TB_DD_QuestionarioManual { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ReenvioConvite> TB_DD_ReenvioConvite { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ReenvioConviteVip> TB_DD_ReenvioConviteVip { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Registro> TB_DD_Registro { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Regulamento> TB_DD_Regulamento { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Relatorio> TB_DD_Relatorio { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ResponsavelCongresso> TB_DD_ResponsavelCongresso { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ResponsavelEmpresa> TB_DD_ResponsavelEmpresa { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Resposta> TB_DD_Resposta { get; set; }
        public System.Data.Entity.DbSet<TB_DD_RespostaQuestionarioManual> TB_DD_RespostaQuestionarioManual { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Simultaneidade> TB_DD_Simultaneidade { get; set; }
        public System.Data.Entity.DbSet<TB_DD_SistemaHabilitadoToken> TB_DD_SistemaHabilitadoToken { get; set; }
        public System.Data.Entity.DbSet<TB_DD_SiteColuna> TB_DD_SiteColuna { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Stand> TB_DD_Stand { get; set; }
        public System.Data.Entity.DbSet<TB_DD_SubAlternativaQuestionarioManual> TB_DD_SubAlternativaQuestionarioManual { get; set; }
        public System.Data.Entity.DbSet<TB_DD_SubCategoria> TB_DD_SubCategoria { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Ticket> TB_DD_Ticket { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Token> TB_DD_Token { get; set; }
        public System.Data.Entity.DbSet<TB_DD_Usuario> TB_DD_Usuario { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ValorContrato> TB_DD_ValorContrato { get; set; }
        public System.Data.Entity.DbSet<TB_DD_ValorContratoParcela> TB_DD_ValorContratoParcela { get; set; }
        public System.Data.Entity.DbSet<TB_LG_Acesso> TB_LG_Acesso { get; set; }
        public System.Data.Entity.DbSet<TB_LG_AlteraSubCategoria> TB_LG_AlteraSubCategoria { get; set; }
        public System.Data.Entity.DbSet<TB_LG_CancelamentoAutomaticoPedido> TB_LG_CancelamentoAutomaticoPedido { get; set; }
        public System.Data.Entity.DbSet<TB_LG_Erro> TB_LG_Erro { get; set; }
        public System.Data.Entity.DbSet<TB_LG_FormularioManualEletronico> TB_LG_FormularioManualEletronico { get; set; }
        public System.Data.Entity.DbSet<TB_LG_Geral> TB_LG_Geral { get; set; }
        public System.Data.Entity.DbSet<TB_LG_GeralDepEasy> TB_LG_GeralDepEasy { get; set; }
        public System.Data.Entity.DbSet<TB_LG_GeralDepFinanceiro> TB_LG_GeralDepFinanceiro { get; set; }
        public System.Data.Entity.DbSet<TB_LG_GeralDepUsuario> TB_LG_GeralDepUsuario { get; set; }
        public System.Data.Entity.DbSet<TB_LG_PaypalPlus> TB_LG_PaypalPlus { get; set; }
        public System.Data.Entity.DbSet<TB_LG_RF_GeralAcao> TB_LG_RF_GeralAcao { get; set; }
        public System.Data.Entity.DbSet<TB_LG_RF_GeralDepartamento> TB_LG_RF_GeralDepartamento { get; set; }
        public System.Data.Entity.DbSet<TB_LG_RF_GeralUsuario> TB_LG_RF_GeralUsuario { get; set; }
        public System.Data.Entity.DbSet<TB_RF_AliquotaImposto> TB_RF_AliquotaImposto { get; set; }
        public System.Data.Entity.DbSet<TB_RF_AlternativaPadrao> TB_RF_AlternativaPadrao { get; set; }
        public System.Data.Entity.DbSet<TB_RF_AreaAtuacao> TB_RF_AreaAtuacao { get; set; }
        public System.Data.Entity.DbSet<TB_RF_Atividades> TB_RF_Atividades { get; set; }
        public System.Data.Entity.DbSet<TB_RF_Categoria> TB_RF_Categoria { get; set; }
        public System.Data.Entity.DbSet<TB_RF_CnaeSebrae> TB_RF_CnaeSebrae { get; set; }
        public System.Data.Entity.DbSet<TB_RF_ConfImpostoRetencaoValorAcimaDe> TB_RF_ConfImpostoRetencaoValorAcimaDe { get; set; }
        public System.Data.Entity.DbSet<TB_RF_ConfImpostos> TB_RF_ConfImpostos { get; set; }
        public System.Data.Entity.DbSet<TB_RF_Cor> TB_RF_Cor { get; set; }
        public System.Data.Entity.DbSet<TB_RF_Cotacao> TB_RF_Cotacao { get; set; }
        public System.Data.Entity.DbSet<TB_RF_EventoStand> TB_RF_EventoStand { get; set; }
        public System.Data.Entity.DbSet<TB_RF_ExclusaoEmpresa> TB_RF_ExclusaoEmpresa { get; set; }
        public System.Data.Entity.DbSet<TB_RF_Faq> TB_RF_Faq { get; set; }
        public System.Data.Entity.DbSet<TB_RF_FormaPagamento> TB_RF_FormaPagamento { get; set; }
        public System.Data.Entity.DbSet<TB_RF_Generica> TB_RF_Generica { get; set; }
        public System.Data.Entity.DbSet<TB_RF_GruposDadosCadastrais> TB_RF_GruposDadosCadastrais { get; set; }
        public System.Data.Entity.DbSet<TB_RF_Idioma> TB_RF_Idioma { get; set; }
        public System.Data.Entity.DbSet<TB_RF_ImpostoPedido> TB_RF_ImpostoPedido { get; set; }
        public System.Data.Entity.DbSet<TB_RF_Leitura_Atividade> TB_RF_Leitura_Atividade { get; set; }
        public System.Data.Entity.DbSet<TB_RF_Leitura_Local> TB_RF_Leitura_Local { get; set; }
        public System.Data.Entity.DbSet<TB_RF_Moeda> TB_RF_Moeda { get; set; }
        public System.Data.Entity.DbSet<TB_RF_PerguntaPadrao> TB_RF_PerguntaPadrao { get; set; }
        public System.Data.Entity.DbSet<TB_RF_PreenchimentoFormulario> TB_RF_PreenchimentoFormulario { get; set; }
        public System.Data.Entity.DbSet<TB_RF_QuestionarioPadrao> TB_RF_QuestionarioPadrao { get; set; }
        public System.Data.Entity.DbSet<TB_RF_RamoAtividade> TB_RF_RamoAtividade { get; set; }
        public System.Data.Entity.DbSet<TB_RF_RegraMarketingSimples> TB_RF_RegraMarketingSimples { get; set; }
        public System.Data.Entity.DbSet<TB_RF_Saudacao> TB_RF_Saudacao { get; set; }
        public System.Data.Entity.DbSet<TB_RF_Sexo> TB_RF_Sexo { get; set; }
        public System.Data.Entity.DbSet<TB_RF_SubCategoriaValidacao> TB_RF_SubCategoriaValidacao { get; set; }
        public System.Data.Entity.DbSet<TB_RF_TipoCarta> TB_RF_TipoCarta { get; set; }
        public System.Data.Entity.DbSet<TB_RF_TipoContrato> TB_RF_TipoContrato { get; set; }
        public System.Data.Entity.DbSet<TB_RF_TipoCredenciamento> TB_RF_TipoCredenciamento { get; set; }
        public System.Data.Entity.DbSet<TB_RF_TipoDocumento> TB_RF_TipoDocumento { get; set; }
        public System.Data.Entity.DbSet<TB_RF_TipoEmpresa> TB_RF_TipoEmpresa { get; set; }
        public System.Data.Entity.DbSet<TB_RF_TipoFormulario> TB_RF_TipoFormulario { get; set; }
        public System.Data.Entity.DbSet<TB_RF_TipoObjeto> TB_RF_TipoObjeto { get; set; }
        public System.Data.Entity.DbSet<TB_RF_TipoPedido> TB_RF_TipoPedido { get; set; }
        public System.Data.Entity.DbSet<TB_RF_TipoRelatorio> TB_RF_TipoRelatorio { get; set; }
        public System.Data.Entity.DbSet<TB_RF_TipoValidacao> TB_RF_TipoValidacao { get; set; }

        public FakeInteranet_v4_easyContext()
        {
            TB_AS_Alternativa_Questionario = new FakeDbSet<TB_AS_Alternativa_Questionario>("cdAlternativa_Questionario");
            TB_AS_AlternativaPadrao_Questionario = new FakeDbSet<TB_AS_AlternativaPadrao_Questionario>("cdAlternativaPadrao_Questionario");
            TB_AS_CodigoPromocional_tb_Pedido = new FakeDbSet<TB_AS_CodigoPromocional_tb_Pedido>("cdPromocionalPedido");
            TB_AS_Filial_Funcionario = new FakeDbSet<TB_AS_Filial_Funcionario>("cdAssociacao");
            TB_AS_Pedido_FormaPagamento = new FakeDbSet<TB_AS_Pedido_FormaPagamento>("cdPedidoFormaPagamento");
            TB_AS_Pedido_Recibo = new FakeDbSet<TB_AS_Pedido_Recibo>("cdRecibo");
            TB_AS_RegraMarketingSimples_ProdutoOperacional = new FakeDbSet<TB_AS_RegraMarketingSimples_ProdutoOperacional>("cdRegraProdutoOperacional");
            TB_AS_TipoCredenciamento_Edicao = new FakeDbSet<TB_AS_TipoCredenciamento_Edicao>("cdTipoCredenciamentoHasEdicao");
            TB_CF_Edicao = new FakeDbSet<TB_CF_Edicao>("cdEdicao");
            TB_CF_EstruturaPadrao = new FakeDbSet<TB_CF_EstruturaPadrao>("cdEstruturaPadrao");
            TB_CF_ExecaoFormularioManualEletronico = new FakeDbSet<TB_CF_ExecaoFormularioManualEletronico>("cdExecao");
            TB_CF_Formulario = new FakeDbSet<TB_CF_Formulario>("cdFormulario");
            TB_CF_FormularioGenerico = new FakeDbSet<TB_CF_FormularioGenerico>("cdFormularioGenerico");
            TB_CF_FormularioManualEletronico = new FakeDbSet<TB_CF_FormularioManualEletronico>("cdFormulario");
            TB_CF_FormularioSistemaIntegrado = new FakeDbSet<TB_CF_FormularioSistemaIntegrado>("cdFormulario");
            TB_CF_INT_Geral = new FakeDbSet<TB_CF_INT_Geral>("cdConfig");
            TB_CF_Permissoes = new FakeDbSet<TB_CF_Permissoes>("cdPermissao");
            TB_CF_Recibo = new FakeDbSet<TB_CF_Recibo>("cdConfiguracao");
            TB_CF_VendaDados = new FakeDbSet<TB_CF_VendaDados>("cdVendaDados");
            TB_DD_Agendamento = new FakeDbSet<TB_DD_Agendamento>("cdAgendamento");
            TB_DD_Alternativa = new FakeDbSet<TB_DD_Alternativa>("cdAlternativa");
            TB_DD_AlternativaQuestionarioManual = new FakeDbSet<TB_DD_AlternativaQuestionarioManual>("cdAlternativaQuestionarioManual");
            TB_DD_App_ConvideAmigo = new FakeDbSet<TB_DD_App_ConvideAmigo>("cdConvite");
            TB_DD_App_NoticiaAviso = new FakeDbSet<TB_DD_App_NoticiaAviso>("cdNoticiaAviso");
            TB_DD_AprovacaoReprovacaoDeProjeto = new FakeDbSet<TB_DD_AprovacaoReprovacaoDeProjeto>("cdAprovacaoReprovacao");
            TB_DD_AssistentePreenchimento = new FakeDbSet<TB_DD_AssistentePreenchimento>("cdAssistentePreenchimento");
            TB_DD_ATOM = new FakeDbSet<TB_DD_ATOM>("cdGeracao");
            TB_DD_BloqueioContratoProduto = new FakeDbSet<TB_DD_BloqueioContratoProduto>("cdPermissao");
            TB_DD_CarrinhoOperacional = new FakeDbSet<TB_DD_CarrinhoOperacional>("cdItem");
            TB_DD_CarrinhoOperacional_VendaDados = new FakeDbSet<TB_DD_CarrinhoOperacional_VendaDados>("cdCarrinhoOperacionalVendaDados");
            TB_DD_CatalogoOficial = new FakeDbSet<TB_DD_CatalogoOficial>("cdCatalogoOficial");
            TB_DD_Cnab = new FakeDbSet<TB_DD_Cnab>("cdLinha");
            TB_DD_CodigoPromocional = new FakeDbSet<TB_DD_CodigoPromocional>("cdPromocional");
            TB_DD_Codigos = new FakeDbSet<TB_DD_Codigos>("ObjRef");
            TB_DD_Coletor = new FakeDbSet<TB_DD_Coletor>("cdColetor");
            TB_DD_Comunicados = new FakeDbSet<TB_DD_Comunicados>("cdComunicado");
            TB_DD_ConfExtracaoPersonalizada = new FakeDbSet<TB_DD_ConfExtracaoPersonalizada>("cdConfExtracaoPersonalizada");
            TB_DD_ContratoExpositor = new FakeDbSet<TB_DD_ContratoExpositor>("cdContrato");
            TB_DD_Convite = new FakeDbSet<TB_DD_Convite>("cdConvite");
            TB_DD_ConviteEletronico = new FakeDbSet<TB_DD_ConviteEletronico>("ObjRef");
            TB_DD_ConviteEletronicoCancelado = new FakeDbSet<TB_DD_ConviteEletronicoCancelado>("cdConviteCancelado");
            TB_DD_ConviteEletronicoVip = new FakeDbSet<TB_DD_ConviteEletronicoVip>("ObjRef");
            TB_DD_ConvitesImpressos = new FakeDbSet<TB_DD_ConvitesImpressos>("cdConvite");
            TB_DD_CorCarpeteColuna = new FakeDbSet<TB_DD_CorCarpeteColuna>("cdPreenchimento");
            TB_DD_Credencial = new FakeDbSet<TB_DD_Credencial>("cdCredencial");
            TB_DD_DestinatarioComunicado = new FakeDbSet<TB_DD_DestinatarioComunicado>("ObjRef");
            TB_DD_EletricaBasica = new FakeDbSet<TB_DD_EletricaBasica>("cdEletricaBasica");
            TB_DD_EmailMarketingImportados = new FakeDbSet<TB_DD_EmailMarketingImportados>("cdEmailMarketingImportado");
            TB_DD_EmissaoCredencial = new FakeDbSet<TB_DD_EmissaoCredencial>("cdEmissao");
            TB_DD_Empresa = new FakeDbSet<TB_DD_Empresa>("cdEmpresa");
            TB_DD_EntregaCredencialVIP = new FakeDbSet<TB_DD_EntregaCredencialVIP>("cdEntrega");
            TB_DD_EstruturaCartaPersonalizada = new FakeDbSet<TB_DD_EstruturaCartaPersonalizada>("cdEstruturaCartaPersonalizada");
            TB_DD_EstruturaPersonalizada = new FakeDbSet<TB_DD_EstruturaPersonalizada>("cdEstruturaPersonalizada");
            TB_DD_Evento = new FakeDbSet<TB_DD_Evento>("cdEvento");
            TB_DD_ExtracaoPersonalizada = new FakeDbSet<TB_DD_ExtracaoPersonalizada>("cdExtracaoPersonalizada");
            TB_DD_FaqRespostas = new FakeDbSet<TB_DD_FaqRespostas>("cdFaqRespostas");
            TB_DD_Filial = new FakeDbSet<TB_DD_Filial>("cdFilial");
            TB_DD_FotoPessoa = new FakeDbSet<TB_DD_FotoPessoa>("cdPessoa");
            TB_DD_Funcionario = new FakeDbSet<TB_DD_Funcionario>("cdFuncionario");
            TB_DD_Geradora_Registro = new FakeDbSet<TB_DD_Geradora_Registro>("cdCodigo");
            TB_DD_GrupoProdutoCongresso = new FakeDbSet<TB_DD_GrupoProdutoCongresso>("cdGrupoProdutoCongresso");
            TB_DD_GrupoResponsavelCongresso = new FakeDbSet<TB_DD_GrupoResponsavelCongresso>("cdGrupoResponsavelCongresso");
            TB_DD_HelpDesk = new FakeDbSet<TB_DD_HelpDesk>("cdChamado");
            TB_DD_ImagemAssistentePreenchimento = new FakeDbSet<TB_DD_ImagemAssistentePreenchimento>("cdImagemAssistente");
            TB_DD_ImportacaoPessoa = new FakeDbSet<TB_DD_ImportacaoPessoa>("cdImportacaoPessoa");
            TB_DD_ImpostoAPAS = new FakeDbSet<TB_DD_ImpostoAPAS>("cdImpostoAPAS");
            TB_DD_InteracaoHelp = new FakeDbSet<TB_DD_InteracaoHelp>("cdInteracao");
            TB_DD_ItensLote = new FakeDbSet<TB_DD_ItensLote>("cdItem");
            TB_DD_ItensPedidoCongresso = new FakeDbSet<TB_DD_ItensPedidoCongresso>("cdItem");
            TB_DD_ItensPedidoOperacional = new FakeDbSet<TB_DD_ItensPedidoOperacional>("cdItem");
            TB_DD_LeituraRegulamento = new FakeDbSet<TB_DD_LeituraRegulamento>("cdLeituraRegulamento");
            TB_DD_LimiteAplicacao = new FakeDbSet<TB_DD_LimiteAplicacao>("cdLimiteAplicacao");
            TB_DD_LogImportacaoAPAS = new FakeDbSet<TB_DD_LogImportacaoAPAS>("cdLogImportacao");
            TB_DD_LoteImpressao = new FakeDbSet<TB_DD_LoteImpressao>("cdLote");
            TB_DD_MaquinaseVeiculos = new FakeDbSet<TB_DD_MaquinaseVeiculos>("cdMaquinas");
            TB_DD_Monitoria = new FakeDbSet<TB_DD_Monitoria>("cdAcesso");
            TB_DD_MonitoriaCielo = new FakeDbSet<TB_DD_MonitoriaCielo>("cdMonitoria");
            TB_DD_MonitoriaPayPal = new FakeDbSet<TB_DD_MonitoriaPayPal>("cdMonitoriaPaypal");
            TB_DD_MonitoriaRedeCard = new FakeDbSet<TB_DD_MonitoriaRedeCard>("cdMonitoriaRedeCard");
            TB_DD_MonitoriaShopLineItau = new FakeDbSet<TB_DD_MonitoriaShopLineItau>("cdMonitoriaShoplineItau");
            TB_DD_MonitoriaStone = new FakeDbSet<TB_DD_MonitoriaStone>("cdMonitoriaStone");
            TB_DD_Movimentacao = new FakeDbSet<TB_DD_Movimentacao>("cdMovimentacao");
            TB_DD_News = new FakeDbSet<TB_DD_News>("cdNews");
            TB_DD_Pagamento = new FakeDbSet<TB_DD_Pagamento>("cdPagamento");
            TB_DD_ParcelamentoBoleto = new FakeDbSet<TB_DD_ParcelamentoBoleto>("cdBoleto");
            TB_DD_Pedido = new FakeDbSet<TB_DD_Pedido>("cdPedido");
            TB_DD_Pergunta = new FakeDbSet<TB_DD_Pergunta>("cdPergunta");
            TB_DD_PermissaoCorCarpeteColuna = new FakeDbSet<TB_DD_PermissaoCorCarpeteColuna>("cdPermissao");
            TB_DD_Pessoa = new FakeDbSet<TB_DD_Pessoa>("cdPessoa");
            TB_DD_PlacaIdentificacao = new FakeDbSet<TB_DD_PlacaIdentificacao>("cdPreenchimento");
            TB_DD_PlacaIdentificacaoColuna = new FakeDbSet<TB_DD_PlacaIdentificacaoColuna>("cdPreenchimento");
            TB_DD_PrecoProdutoCongresso = new FakeDbSet<TB_DD_PrecoProdutoCongresso>("cdPreco");
            TB_DD_PrecoProdutoOperacional = new FakeDbSet<TB_DD_PrecoProdutoOperacional>("cdPreco");
            TB_DD_PresencaCampanhaSejaBemVindo = new FakeDbSet<TB_DD_PresencaCampanhaSejaBemVindo>("cdPresencaCampanha");
            TB_DD_ProdutoCongresso = new FakeDbSet<TB_DD_ProdutoCongresso>("cdProduto");
            TB_DD_ProdutoExpositores = new FakeDbSet<TB_DD_ProdutoExpositores>("cdProdutoExpositores");
            TB_DD_ProdutoOperacional = new FakeDbSet<TB_DD_ProdutoOperacional>("cdProduto");
            TB_DD_Questionario = new FakeDbSet<TB_DD_Questionario>("cdQuestionario");
            TB_DD_QuestionarioManual = new FakeDbSet<TB_DD_QuestionarioManual>("cdQuestionarioManual");
            TB_DD_ReenvioConvite = new FakeDbSet<TB_DD_ReenvioConvite>("cdReenvio");
            TB_DD_ReenvioConviteVip = new FakeDbSet<TB_DD_ReenvioConviteVip>("cdReenvio");
            TB_DD_Registro = new FakeDbSet<TB_DD_Registro>("cdRegistro");
            TB_DD_Regulamento = new FakeDbSet<TB_DD_Regulamento>("cdRegulamento");
            TB_DD_Relatorio = new FakeDbSet<TB_DD_Relatorio>("cdRelatorio");
            TB_DD_ResponsavelCongresso = new FakeDbSet<TB_DD_ResponsavelCongresso>("cdResponsavelCongresso");
            TB_DD_ResponsavelEmpresa = new FakeDbSet<TB_DD_ResponsavelEmpresa>("cdResponsavel");
            TB_DD_Resposta = new FakeDbSet<TB_DD_Resposta>("cdResposta");
            TB_DD_RespostaQuestionarioManual = new FakeDbSet<TB_DD_RespostaQuestionarioManual>("cdRespostaQuestionarioManual");
            TB_DD_Simultaneidade = new FakeDbSet<TB_DD_Simultaneidade>("cdSimultaneidade");
            TB_DD_SistemaHabilitadoToken = new FakeDbSet<TB_DD_SistemaHabilitadoToken>("cdSistemaHabilitado");
            TB_DD_SiteColuna = new FakeDbSet<TB_DD_SiteColuna>("cdSite");
            TB_DD_Stand = new FakeDbSet<TB_DD_Stand>("cdStand");
            TB_DD_SubAlternativaQuestionarioManual = new FakeDbSet<TB_DD_SubAlternativaQuestionarioManual>("cdSubAlternativaQuestionarioManual");
            TB_DD_SubCategoria = new FakeDbSet<TB_DD_SubCategoria>("cdSubCategoria");
            TB_DD_Ticket = new FakeDbSet<TB_DD_Ticket>("cdTicket");
            TB_DD_Token = new FakeDbSet<TB_DD_Token>("cdToken");
            TB_DD_Usuario = new FakeDbSet<TB_DD_Usuario>("cdUsuario");
            TB_DD_ValorContrato = new FakeDbSet<TB_DD_ValorContrato>("cdValorContrato");
            TB_DD_ValorContratoParcela = new FakeDbSet<TB_DD_ValorContratoParcela>("cdValorContratoParcela");
            TB_LG_Acesso = new FakeDbSet<TB_LG_Acesso>("cdLogAcesso");
            TB_LG_AlteraSubCategoria = new FakeDbSet<TB_LG_AlteraSubCategoria>("cdLogAlteracaoSubCategoria");
            TB_LG_CancelamentoAutomaticoPedido = new FakeDbSet<TB_LG_CancelamentoAutomaticoPedido>("cdCancelaPedido");
            TB_LG_Erro = new FakeDbSet<TB_LG_Erro>("cdLogErro");
            TB_LG_FormularioManualEletronico = new FakeDbSet<TB_LG_FormularioManualEletronico>("cdPreenchimento");
            TB_LG_Geral = new FakeDbSet<TB_LG_Geral>("cdLog");
            TB_LG_GeralDepEasy = new FakeDbSet<TB_LG_GeralDepEasy>("TB_LG_GeralUsuariocdLog");
            TB_LG_GeralDepFinanceiro = new FakeDbSet<TB_LG_GeralDepFinanceiro>("TB_LG_GeralUsuariocdLog");
            TB_LG_GeralDepUsuario = new FakeDbSet<TB_LG_GeralDepUsuario>("TB_LG_GeralUsuariocdLog");
            TB_LG_PaypalPlus = new FakeDbSet<TB_LG_PaypalPlus>("cdPaypalPlus");
            TB_LG_RF_GeralAcao = new FakeDbSet<TB_LG_RF_GeralAcao>("cdAcao");
            TB_LG_RF_GeralDepartamento = new FakeDbSet<TB_LG_RF_GeralDepartamento>("cdDepartamento");
            TB_LG_RF_GeralUsuario = new FakeDbSet<TB_LG_RF_GeralUsuario>("cdLog");
            TB_RF_AliquotaImposto = new FakeDbSet<TB_RF_AliquotaImposto>("cdAliquota");
            TB_RF_AlternativaPadrao = new FakeDbSet<TB_RF_AlternativaPadrao>("cdAlternativaPadrao");
            TB_RF_AreaAtuacao = new FakeDbSet<TB_RF_AreaAtuacao>("cdAreaAtuacao");
            TB_RF_Atividades = new FakeDbSet<TB_RF_Atividades>("cdAtividade");
            TB_RF_Categoria = new FakeDbSet<TB_RF_Categoria>("cdCategoria");
            TB_RF_CnaeSebrae = new FakeDbSet<TB_RF_CnaeSebrae>("cdCnaeSebrae");
            TB_RF_ConfImpostoRetencaoValorAcimaDe = new FakeDbSet<TB_RF_ConfImpostoRetencaoValorAcimaDe>("cdConfImpostoRetencaoValorAcimaDe");
            TB_RF_ConfImpostos = new FakeDbSet<TB_RF_ConfImpostos>("cdConfiguracao");
            TB_RF_Cor = new FakeDbSet<TB_RF_Cor>("cdCor");
            TB_RF_Cotacao = new FakeDbSet<TB_RF_Cotacao>("cdCotacao");
            TB_RF_EventoStand = new FakeDbSet<TB_RF_EventoStand>("cdEventoStand");
            TB_RF_ExclusaoEmpresa = new FakeDbSet<TB_RF_ExclusaoEmpresa>("cdExclusao");
            TB_RF_Faq = new FakeDbSet<TB_RF_Faq>("cdFaq");
            TB_RF_FormaPagamento = new FakeDbSet<TB_RF_FormaPagamento>("cdPagamento");
            TB_RF_Generica = new FakeDbSet<TB_RF_Generica>("cdReferencia");
            TB_RF_GruposDadosCadastrais = new FakeDbSet<TB_RF_GruposDadosCadastrais>("cdGruposDadosCadastrais");
            TB_RF_Idioma = new FakeDbSet<TB_RF_Idioma>("cdIdioma");
            TB_RF_ImpostoPedido = new FakeDbSet<TB_RF_ImpostoPedido>("cdImposto");
            TB_RF_Leitura_Atividade = new FakeDbSet<TB_RF_Leitura_Atividade>("cdAtividade");
            TB_RF_Leitura_Local = new FakeDbSet<TB_RF_Leitura_Local>("cdLocal");
            TB_RF_Moeda = new FakeDbSet<TB_RF_Moeda>("cdMoeda");
            TB_RF_PerguntaPadrao = new FakeDbSet<TB_RF_PerguntaPadrao>("cdPerguntaPadrao");
            TB_RF_PreenchimentoFormulario = new FakeDbSet<TB_RF_PreenchimentoFormulario>("cdPreenchimento");
            TB_RF_QuestionarioPadrao = new FakeDbSet<TB_RF_QuestionarioPadrao>("cdQuestionarioPadrao");
            TB_RF_RamoAtividade = new FakeDbSet<TB_RF_RamoAtividade>("cdRamoAtividade");
            TB_RF_RegraMarketingSimples = new FakeDbSet<TB_RF_RegraMarketingSimples>("cdRegraMarketingSimples");
            TB_RF_Saudacao = new FakeDbSet<TB_RF_Saudacao>("cdSaudacao");
            TB_RF_Sexo = new FakeDbSet<TB_RF_Sexo>("cdSexo");
            TB_RF_SubCategoriaValidacao = new FakeDbSet<TB_RF_SubCategoriaValidacao>("cdSubCategoriaValidacao");
            TB_RF_TipoCarta = new FakeDbSet<TB_RF_TipoCarta>("cdTipoCarta");
            TB_RF_TipoContrato = new FakeDbSet<TB_RF_TipoContrato>("cdTipoContrato");
            TB_RF_TipoCredenciamento = new FakeDbSet<TB_RF_TipoCredenciamento>("cdTipo");
            TB_RF_TipoDocumento = new FakeDbSet<TB_RF_TipoDocumento>("cdTipoDocumento");
            TB_RF_TipoEmpresa = new FakeDbSet<TB_RF_TipoEmpresa>("cdTipo");
            TB_RF_TipoFormulario = new FakeDbSet<TB_RF_TipoFormulario>("cdTipoFormulario");
            TB_RF_TipoObjeto = new FakeDbSet<TB_RF_TipoObjeto>("cdTipoObjeto");
            TB_RF_TipoPedido = new FakeDbSet<TB_RF_TipoPedido>("cdTipoPedido");
            TB_RF_TipoRelatorio = new FakeDbSet<TB_RF_TipoRelatorio>("cdTipo");
            TB_RF_TipoValidacao = new FakeDbSet<TB_RF_TipoValidacao>("cdTipoValidacao");
        }

        public int SaveChangesCount { get; private set; }
        public int SaveChanges()
        {
            ++SaveChangesCount;
            return 1;
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private System.Data.Entity.Infrastructure.DbChangeTracker _changeTracker;
        public System.Data.Entity.Infrastructure.DbChangeTracker ChangeTracker { get { return _changeTracker; } }
        private System.Data.Entity.Infrastructure.DbContextConfiguration _configuration;
        public System.Data.Entity.Infrastructure.DbContextConfiguration Configuration { get { return _configuration; } }
        private System.Data.Entity.Database _database;
        public System.Data.Entity.Database Database { get { return _database; } }
        public System.Data.Entity.Infrastructure.DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class
        {
            throw new System.NotImplementedException();
        }
        public System.Data.Entity.Infrastructure.DbEntityEntry Entry(object entity)
        {
            throw new System.NotImplementedException();
        }
        public System.Collections.Generic.IEnumerable<System.Data.Entity.Validation.DbEntityValidationResult> GetValidationErrors()
        {
            throw new System.NotImplementedException();
        }
        public System.Data.Entity.DbSet Set(System.Type entityType)
        {
            throw new System.NotImplementedException();
        }
        public System.Data.Entity.DbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            throw new System.NotImplementedException();
        }
        public override string ToString()
        {
            throw new System.NotImplementedException();
        }

    }

    // ************************************************************************
    // Fake DbSet
    // Implementing Find:
    //      The Find method is difficult to implement in a generic fashion. If
    //      you need to test code that makes use of the Find method it is
    //      easiest to create a test DbSet for each of the entity types that
    //      need to support find. You can then write logic to find that
    //      particular type of entity, as shown below:
    //      public class FakeBlogDbSet : FakeDbSet<Blog>
    //      {
    //          public override Blog Find(params object[] keyValues)
    //          {
    //              var id = (int) keyValues.Single();
    //              return this.SingleOrDefault(b => b.BlogId == id);
    //          }
    //      }
    //      Read more about it here: https://msdn.microsoft.com/en-us/data/dn314431.aspx
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class FakeDbSet<TEntity> : System.Data.Entity.DbSet<TEntity>, IQueryable, System.Collections.Generic.IEnumerable<TEntity> where TEntity : class
    {
        private readonly System.Reflection.PropertyInfo[] _primaryKeys;
        private readonly System.Collections.ObjectModel.ObservableCollection<TEntity> _data;
        private readonly IQueryable _query;

        public FakeDbSet()
        {
            _data = new System.Collections.ObjectModel.ObservableCollection<TEntity>();
            _query = _data.AsQueryable();
        }

        public FakeDbSet(params string[] primaryKeys)
        {
            _primaryKeys = typeof(TEntity).GetProperties().Where(x => primaryKeys.Contains(x.Name)).ToArray();
            _data = new System.Collections.ObjectModel.ObservableCollection<TEntity>();
            _query = _data.AsQueryable();
        }

        public override TEntity Find(params object[] keyValues)
        {
            if (_primaryKeys == null)
                throw new System.ArgumentException("No primary keys defined");
            if (keyValues.Length != _primaryKeys.Length)
                throw new System.ArgumentException("Incorrect number of keys passed to Find method");

            var keyQuery = this.AsQueryable();
            keyQuery = keyValues
                .Select((t, i) => i)
                .Aggregate(keyQuery,
                    (current, x) =>
                        current.Where(entity => _primaryKeys[x].GetValue(entity, null).Equals(keyValues[x])));

            return keyQuery.SingleOrDefault();
        }

        public override System.Collections.Generic.IEnumerable<TEntity> AddRange(System.Collections.Generic.IEnumerable<TEntity> entities)
        {
            if (entities == null) throw new System.ArgumentNullException("entities");
            var items = entities.ToList();
            foreach (var entity in items)
            {
                _data.Add(entity);
            }
            return items;
        }

        public override TEntity Add(TEntity item)
        {
            if (item == null) throw new System.ArgumentNullException("item");
            _data.Add(item);
            return item;
        }

        public override System.Collections.Generic.IEnumerable<TEntity> RemoveRange(System.Collections.Generic.IEnumerable<TEntity> entities)
        {
            if (entities == null) throw new System.ArgumentNullException("entities");
            var items = entities.ToList();
            foreach (var entity in items)
            {
                _data.Remove(entity);
            }
            return items;
        }

        public override TEntity Remove(TEntity item)
        {
            if (item == null) throw new System.ArgumentNullException("item");
            _data.Remove(item);
            return item;
        }

        public override TEntity Attach(TEntity item)
        {
            if (item == null) throw new System.ArgumentNullException("item");
            _data.Add(item);
            return item;
        }

        public override TEntity Create()
        {
            return System.Activator.CreateInstance<TEntity>();
        }

        public override TDerivedEntity Create<TDerivedEntity>()
        {
            return System.Activator.CreateInstance<TDerivedEntity>();
        }

        public override System.Collections.ObjectModel.ObservableCollection<TEntity> Local
        {
            get { return _data; }
        }

        System.Type IQueryable.ElementType
        {
            get { return _query.ElementType; }
        }

        System.Linq.Expressions.Expression IQueryable.Expression
        {
            get { return _query.Expression; }
        }

        IQueryProvider IQueryable.Provider
        {
            get { return _query.Provider; }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        System.Collections.Generic.IEnumerator<TEntity> System.Collections.Generic.IEnumerable<TEntity>.GetEnumerator()
        {
            return _data.GetEnumerator();
        }
    }

    #endregion

    #region POCO classes

    // TB_AS_Alternativa_Questionario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_AS_Alternativa_Questionario
    {
        public int cdAlternativa_Questionario { get; set; } // cdAlternativa_Questionario (Primary key)
        public int cdAlternativa { get; set; } // cdAlternativa
        public int cdQuestionario { get; set; } // cdQuestionario
        public int nuOrdem { get; set; } // nuOrdem
        public int dsSubAlternativaDa { get; set; } // dsSubAlternativaDa
        public string dsOutros { get; set; } // dsOutros (length: 1)
        public string fgSituacao { get; set; } // fgSituacao (length: 1)
        public System.DateTime? dtCancelado { get; set; } // dtCancelado
        public int? cdUsuarioCancelado { get; set; } // cdUsuarioCancelado

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Alternativa pointed by [TB_AS_Alternativa_Questionario].([cdAlternativa]) (FK_TB_AS_Alternativa_Questionario_TB_DD_Alternativa)
        /// </summary>
        public virtual TB_DD_Alternativa TB_DD_Alternativa_cdAlternativa { get; set; } // FK_TB_AS_Alternativa_Questionario_TB_DD_Alternativa

        /// <summary>
        /// Parent TB_DD_Alternativa pointed by [TB_AS_Alternativa_Questionario].([dsSubAlternativaDa]) (FK_TB_AS_Alternativa_Questionario_dsSubAlternativaDa)
        /// </summary>
        public virtual TB_DD_Alternativa TB_DD_Alternativa_dsSubAlternativaDa { get; set; } // FK_TB_AS_Alternativa_Questionario_dsSubAlternativaDa

        /// <summary>
        /// Parent TB_DD_Questionario pointed by [TB_AS_Alternativa_Questionario].([cdQuestionario]) (FK_TB_AS_Alternativa_Questionario_TB_DD_Questionario)
        /// </summary>
        public virtual TB_DD_Questionario TB_DD_Questionario { get; set; } // FK_TB_AS_Alternativa_Questionario_TB_DD_Questionario
    }

    // TB_AS_AlternativaPadrao_Questionario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_AS_AlternativaPadrao_Questionario
    {
        public int cdAlternativaPadrao_Questionario { get; set; } // cdAlternativaPadrao_Questionario (Primary key)
        public int cdQuestionarioPadrao { get; set; } // cdQuestionarioPadrao
        public int cdAlternativaPadrao { get; set; } // cdAlternativaPadrao
        public int cdAlternativaPadraoSub { get; set; } // cdAlternativaPadraoSub
        public int nuOrdem { get; set; } // nuOrdem
        public string dsOutros { get; set; } // dsOutros (length: 1)
        public string fgSituacao { get; set; } // fgSituacao (length: 1)

        // Foreign keys

        /// <summary>
        /// Parent TB_RF_AlternativaPadrao pointed by [TB_AS_AlternativaPadrao_Questionario].([cdAlternativaPadrao]) (FK_AlternativaPadrao_Questionario_AlternativaPadrao)
        /// </summary>
        public virtual TB_RF_AlternativaPadrao TB_RF_AlternativaPadrao_cdAlternativaPadrao { get; set; } // FK_AlternativaPadrao_Questionario_AlternativaPadrao

        /// <summary>
        /// Parent TB_RF_AlternativaPadrao pointed by [TB_AS_AlternativaPadrao_Questionario].([cdAlternativaPadraoSub]) (FK_AlternativaPadrao_Questionario_AlternativaPadraoSub)
        /// </summary>
        public virtual TB_RF_AlternativaPadrao TB_RF_AlternativaPadrao_cdAlternativaPadraoSub { get; set; } // FK_AlternativaPadrao_Questionario_AlternativaPadraoSub

        /// <summary>
        /// Parent TB_RF_QuestionarioPadrao pointed by [TB_AS_AlternativaPadrao_Questionario].([cdQuestionarioPadrao]) (FK_AlternativaPadrao_Questionario_QuestionarioPadrao)
        /// </summary>
        public virtual TB_RF_QuestionarioPadrao TB_RF_QuestionarioPadrao { get; set; } // FK_AlternativaPadrao_Questionario_QuestionarioPadrao
    }

    // TB_AS_CodigoPromocional_tb_Pedido
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_AS_CodigoPromocional_tb_Pedido
    {
        public int cdPromocionalPedido { get; set; } // cdPromocionalPedido (Primary key)
        public int cdPromocional { get; set; } // cdPromocional
        public int cdPedido { get; set; } // cdPedido
        public System.DateTime DtaInclusao { get; set; } // DtaInclusao

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_CodigoPromocional pointed by [TB_AS_CodigoPromocional_tb_Pedido].([cdPromocional]) (PK_TB_AS_CodigoPromocional_Pedido_CodigoPromocional)
        /// </summary>
        public virtual TB_DD_CodigoPromocional TB_DD_CodigoPromocional { get; set; } // PK_TB_AS_CodigoPromocional_Pedido_CodigoPromocional

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_AS_CodigoPromocional_tb_Pedido].([cdPedido]) (PK_CodigoPromocional_Pedido)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido { get; set; } // PK_CodigoPromocional_Pedido
    }

    // TB_AS_Filial_Funcionario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_AS_Filial_Funcionario
    {
        public int cdAssociacao { get; set; } // cdAssociacao (Primary key)
        public int? cdFilial { get; set; } // cdFilial
        public int? cdFuncionario { get; set; } // cdFuncionario
        public string fgHabilitado { get; set; } // fgHabilitado (length: 1)

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Filial pointed by [TB_AS_Filial_Funcionario].([cdFilial]) (FK_TB_AS_Filial_Funcionario_cdFilial)
        /// </summary>
        public virtual TB_DD_Filial TB_DD_Filial { get; set; } // FK_TB_AS_Filial_Funcionario_cdFilial

        /// <summary>
        /// Parent TB_DD_Funcionario pointed by [TB_AS_Filial_Funcionario].([cdFuncionario]) (FK_TB_AS_Filial_Funcionario_cdFuncionario)
        /// </summary>
        public virtual TB_DD_Funcionario TB_DD_Funcionario { get; set; } // FK_TB_AS_Filial_Funcionario_cdFuncionario

        public TB_AS_Filial_Funcionario()
        {
            fgHabilitado = "S";
        }
    }

    // TB_AS_Pedido_FormaPagamento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_AS_Pedido_FormaPagamento
    {
        public int cdPedidoFormaPagamento { get; set; } // cdPedidoFormaPagamento (Primary key)
        public int TB_Pedido_cdPedido { get; set; } // TB_Pedido_cdPedido
        public int TB_FormaPagamento_cdPagamento { get; set; } // TB_FormaPagamento_cdPagamento
        public string dsValor { get; set; } // dsValor (length: 100)
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public System.DateTime? dtGeracao { get; set; } // dtGeracao
        public int? cdContaFaturamento { get; set; } // cdContaFaturamento
        public int? cdContaCielo { get; set; } // cdContaCielo

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_AS_Pedido_FormaPagamento].([TB_Pedido_cdPedido]) (FK_TB_AS_Pedido_FormaPagamento_TB_Pedido_cdPedido)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido { get; set; } // FK_TB_AS_Pedido_FormaPagamento_TB_Pedido_cdPedido

        /// <summary>
        /// Parent TB_RF_FormaPagamento pointed by [TB_AS_Pedido_FormaPagamento].([TB_FormaPagamento_cdPagamento]) (FK_TB_AS_Pedido_FormaPagamento_TB_FormaPagamento_cdPagamento)
        /// </summary>
        public virtual TB_RF_FormaPagamento TB_RF_FormaPagamento { get; set; } // FK_TB_AS_Pedido_FormaPagamento_TB_FormaPagamento_cdPagamento
    }

    // TB_AS_Pedido_Recibo
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_AS_Pedido_Recibo
    {
        public int cdRecibo { get; set; } // cdRecibo (Primary key)
        public int cdPedido { get; set; } // cdPedido
        public int cdConfiguracao { get; set; } // cdConfiguracao
        public string dsNumero { get; set; } // dsNumero
        public System.DateTime? dtEmissao { get; set; } // dtEmissao

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Recibo pointed by [TB_AS_Pedido_Recibo].([cdConfiguracao]) (FK_TB_AS_Pedido_Recibo_TB_CF_Recibo)
        /// </summary>
        public virtual TB_CF_Recibo TB_CF_Recibo { get; set; } // FK_TB_AS_Pedido_Recibo_TB_CF_Recibo

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_AS_Pedido_Recibo].([cdPedido]) (FK_TB_AS_Pedido_Recibo_TB_DD_Pedido)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido { get; set; } // FK_TB_AS_Pedido_Recibo_TB_DD_Pedido
    }

    // TB_AS_RegraMarketingSimples_ProdutoOperacional
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_AS_RegraMarketingSimples_ProdutoOperacional
    {
        public int cdRegraProdutoOperacional { get; set; } // cdRegraProdutoOperacional (Primary key)
        public int cdRegraMarketingSimples { get; set; } // cdRegraMarketingSimples
        public int cdProduto { get; set; } // cdProduto
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public int cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public decimal? dsMetragem { get; set; } // dsMetragem
        public decimal? vlMinimo { get; set; } // vlMinimo
        public string fgRegraUmUm { get; set; } // fgRegraUmUm (length: 1)
        public string fgRegraMeioMeio { get; set; } // fgRegraMeioMeio (length: 1)

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_ProdutoOperacional pointed by [TB_AS_RegraMarketingSimples_ProdutoOperacional].([cdProduto]) (FK_RegraMarketingSimples_ProdutoOperacional_Produto)
        /// </summary>
        public virtual TB_DD_ProdutoOperacional TB_DD_ProdutoOperacional { get; set; } // FK_RegraMarketingSimples_ProdutoOperacional_Produto

        /// <summary>
        /// Parent TB_RF_RegraMarketingSimples pointed by [TB_AS_RegraMarketingSimples_ProdutoOperacional].([cdRegraMarketingSimples]) (FK_RegraMarketingSimples_ProdutoOperacional_Marketing)
        /// </summary>
        public virtual TB_RF_RegraMarketingSimples TB_RF_RegraMarketingSimples { get; set; } // FK_RegraMarketingSimples_ProdutoOperacional_Marketing

        public TB_AS_RegraMarketingSimples_ProdutoOperacional()
        {
            dsSituacao = "ATIVO";
        }
    }

    // TB_AS_TipoCredenciamento_Edicao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_AS_TipoCredenciamento_Edicao
    {
        public int cdTipoCredenciamentoHasEdicao { get; set; } // cdTipoCredenciamentoHasEdicao (Primary key)
        public int TB_TipoCredenciamento_cdTipo { get; set; } // TB_TipoCredenciamento_cdTipo
        public int TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public int? cdCategoria { get; set; } // cdCategoria

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_AS_TipoCredenciamento_Edicao].([TB_Edicao_cdEdicao]) (FK_TB_AS_TipoCredenciamento_Edicao_TB_CF_Edicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_AS_TipoCredenciamento_Edicao_TB_CF_Edicao

        /// <summary>
        /// Parent TB_RF_Categoria pointed by [TB_AS_TipoCredenciamento_Edicao].([cdCategoria]) (FK_TB_AS_TipoCredenciamento_Edicao_TB_RF_Categoria)
        /// </summary>
        public virtual TB_RF_Categoria TB_RF_Categoria { get; set; } // FK_TB_AS_TipoCredenciamento_Edicao_TB_RF_Categoria

        /// <summary>
        /// Parent TB_RF_TipoCredenciamento pointed by [TB_AS_TipoCredenciamento_Edicao].([TB_TipoCredenciamento_cdTipo]) (FK_TB_AS_TipoCredenciamento_Edicao_TB_RF_TipoCredenciamento)
        /// </summary>
        public virtual TB_RF_TipoCredenciamento TB_RF_TipoCredenciamento { get; set; } // FK_TB_AS_TipoCredenciamento_Edicao_TB_RF_TipoCredenciamento
    }

    // TB_CF_Edicao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_Edicao
    {
        public int cdEdicao { get; set; } // cdEdicao (Primary key)
        public int cdEvento { get; set; } // cdEvento
        public string dsEdicao { get; set; } // dsEdicao (length: 100)
        public System.DateTime? dtRealizacao { get; set; } // dtRealizacao
        public string dsUF { get; set; } // dsUF (length: 20)
        public string dsCidade { get; set; } // dsCidade (length: 100)
        public string AplicaImposto { get; set; } // AplicaImposto (length: 1)
        public string dsMoeda { get; set; } // dsMoeda (length: 100)
        public string dsDiretorioManual { get; set; } // dsDiretorioManual
        public string dsUsuarioBoleto { get; set; } // dsUsuarioBoleto
        public string dsLinkManual { get; set; } // dsLinkManual
        public string dsAssuntoPor { get; set; } // dsAssuntoPor
        public string dsAssuntoIng { get; set; } // dsAssuntoIng
        public string dsAplicaRegraCredencial { get; set; } // dsAplicaRegraCredencial
        public string nmEvento { get; set; } // nmEvento
        public string dsEmailNotificacao1 { get; set; } // dsEmailNotificacao1
        public string dsEmailNotificacao2 { get; set; } // dsEmailNotificacao2
        public string dsEmailNotificacao3 { get; set; } // dsEmailNotificacao3
        public string dsEmailNotificacao4 { get; set; } // dsEmailNotificacao4
        public string dsEmailNotificacao5 { get; set; } // dsEmailNotificacao5
        public string dsUrlCredVisitante { get; set; } // dsUrlCredVisitante
        public string dsUrlCredVip { get; set; } // dsUrlCredVip
        public string dsUrlCredCng { get; set; } // dsUrlCredCng
        public string dsEmailSaida { get; set; } // dsEmailSaida
        public string dsSenhaEmail { get; set; } // dsSenhaEmail
        public string dsAliasEmail { get; set; } // dsAliasEmail
        public string dsAssuntoConviteEletronico { get; set; } // dsAssuntoConviteEletronico
        public string dsAssuntoConviteEletronicoVIP { get; set; } // dsAssuntoConviteEletronicoVIP
        public string dsAssuntoConviteEletronicoIng { get; set; } // dsAssuntoConviteEletronicoIng
        public string dsAssuntoConviteEletronicoVIPIng { get; set; } // dsAssuntoConviteEletronicoVIPIng
        public string dsTelefoneAtendimento { get; set; } // dsTelefoneAtendimento
        public string dsEmailAtendimento { get; set; } // dsEmailAtendimento
        public int? qtDiasCarenciaBoletoManual { get; set; } // qtDiasCarenciaBoletoManual
        public System.DateTime? dtLimiteVencimentoBoletoManual { get; set; } // dtLimiteVencimentoBoletoManual
        public string dsHabilitado { get; set; } // dsHabilitado
        public string dsAssuntoCredenciamentoConfirmacaoInscricaoPortugues { get; set; } // dsAssuntoCredenciamentoConfirmacaoInscricaoPortugues (length: 500)
        public string dsAssuntoCredenciamentoConfirmacaoInscricaoIngles { get; set; } // dsAssuntoCredenciamentoConfirmacaoInscricaoIngles (length: 500)
        public string dsAssuntoCredenciamentoConfirmacaoInscricaoEspanhol { get; set; } // dsAssuntoCredenciamentoConfirmacaoInscricaoEspanhol (length: 500)
        public string dsAssuntoCredenciamentoConfirmacaoPagamentoPortugues { get; set; } // dsAssuntoCredenciamentoConfirmacaoPagamentoPortugues (length: 500)
        public string dsAssuntoCredenciamentoConfirmacaoPagamentoIngles { get; set; } // dsAssuntoCredenciamentoConfirmacaoPagamentoIngles (length: 500)
        public string dsAssuntoCredenciamentoConfirmacaoPagamentoEspanhol { get; set; } // dsAssuntoCredenciamentoConfirmacaoPagamentoEspanhol (length: 500)
        public string dsAssuntoCredenciamentoConfirmacaoPedidoPortugues { get; set; } // dsAssuntoCredenciamentoConfirmacaoPedidoPortugues (length: 500)
        public string dsAssuntoCredenciamentoConfirmacaoPedidoIngles { get; set; } // dsAssuntoCredenciamentoConfirmacaoPedidoIngles (length: 500)
        public string dsAssuntoCredenciamentoConfirmacaoPedidoEspanhol { get; set; } // dsAssuntoCredenciamentoConfirmacaoPedidoEspanhol (length: 500)
        public string dsCieloLoja { get; set; } // dsCieloLoja (length: 500)
        public string dsCieloLojaChave { get; set; } // dsCieloLojaChave (length: 800)
        public string fgHabilitadoCampanhaSejaBemVindo { get; set; } // fgHabilitadoCampanhaSejaBemVindo (length: 1)
        public string fgPermiteSelecao { get; set; } // fgPermiteSelecao (length: 1)

        // Reverse navigation

        /// <summary>
        /// Child TB_AS_TipoCredenciamento_Edicao where [TB_AS_TipoCredenciamento_Edicao].[TB_Edicao_cdEdicao] point to this entity (FK_TB_AS_TipoCredenciamento_Edicao_TB_CF_Edicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_TipoCredenciamento_Edicao> TB_AS_TipoCredenciamento_Edicao { get; set; } // TB_AS_TipoCredenciamento_Edicao.FK_TB_AS_TipoCredenciamento_Edicao_TB_CF_Edicao
        /// <summary>
        /// Child TB_CF_FormularioManualEletronico where [TB_CF_FormularioManualEletronico].[TB_Edicao_cdEdicao] point to this entity (FK_TB_CF_FormularioManualEletronico_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_CF_FormularioManualEletronico> TB_CF_FormularioManualEletronico { get; set; } // TB_CF_FormularioManualEletronico.FK_TB_CF_FormularioManualEletronico_cdEdicao
        /// <summary>
        /// Child TB_CF_Permissoes where [TB_CF_Permissoes].[cdEdicao] point to this entity (FK_TB_CF_Permissoes_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_CF_Permissoes> TB_CF_Permissoes { get; set; } // TB_CF_Permissoes.FK_TB_CF_Permissoes_cdEdicao
        /// <summary>
        /// Child TB_DD_ATOM where [TB_DD_ATOM].[cdEdicao] point to this entity (FK_DD_Atom_Edicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ATOM> TB_DD_ATOM { get; set; } // TB_DD_ATOM.FK_DD_Atom_Edicao
        /// <summary>
        /// Child TB_DD_CarrinhoOperacional where [TB_DD_CarrinhoOperacional].[cdEdicao] point to this entity (FK_TB_DD_CarrinhoOperacional_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_CarrinhoOperacional> TB_DD_CarrinhoOperacional { get; set; } // TB_DD_CarrinhoOperacional.FK_TB_DD_CarrinhoOperacional_cdEdicao
        /// <summary>
        /// Child TB_DD_CatalogoOficial where [TB_DD_CatalogoOficial].[TB_Edicao_cdEdicao] point to this entity (FK_TB_DD_CatalogoOficial_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_CatalogoOficial> TB_DD_CatalogoOficial { get; set; } // TB_DD_CatalogoOficial.FK_TB_DD_CatalogoOficial_cdEdicao
        /// <summary>
        /// Child TB_DD_Coletor where [TB_DD_Coletor].[cdEdicao] point to this entity (FK_TB_DD_Coletor_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Coletor> TB_DD_Coletor { get; set; } // TB_DD_Coletor.FK_TB_DD_Coletor_cdEdicao
        /// <summary>
        /// Child TB_DD_Comunicados where [TB_DD_Comunicados].[cdEdicao] point to this entity (FK_TB_DD_Comunicados_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Comunicados> TB_DD_Comunicados { get; set; } // TB_DD_Comunicados.FK_TB_DD_Comunicados_cdEdicao
        /// <summary>
        /// Child TB_DD_ContratoExpositor where [TB_DD_ContratoExpositor].[TB_Edicao_cdEdicao] point to this entity (FK_ContratoExpositor_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ContratoExpositor> TB_DD_ContratoExpositor { get; set; } // TB_DD_ContratoExpositor.FK_ContratoExpositor_cdEdicao
        /// <summary>
        /// Child TB_DD_ConviteEletronico where [TB_DD_ConviteEletronico].[cdEdicao] point to this entity (FK_TB_DD_ConviteEletronico_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ConviteEletronico> TB_DD_ConviteEletronico { get; set; } // TB_DD_ConviteEletronico.FK_TB_DD_ConviteEletronico_cdEdicao
        /// <summary>
        /// Child TB_DD_ConviteEletronicoVip where [TB_DD_ConviteEletronicoVip].[cdEdicao] point to this entity (FK_TB_DD_ConviteEletronicoVip_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ConviteEletronicoVip> TB_DD_ConviteEletronicoVip { get; set; } // TB_DD_ConviteEletronicoVip.FK_TB_DD_ConviteEletronicoVip_cdEdicao
        /// <summary>
        /// Child TB_DD_CorCarpeteColuna where [TB_DD_CorCarpeteColuna].[cdEdicao] point to this entity (FK_TB_DD_CorCarpeteColuna_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_CorCarpeteColuna> TB_DD_CorCarpeteColuna { get; set; } // TB_DD_CorCarpeteColuna.FK_TB_DD_CorCarpeteColuna_cdEdicao
        /// <summary>
        /// Child TB_DD_EletricaBasica where [TB_DD_EletricaBasica].[cdEdicao] point to this entity (FK_TB_DD_EletricaBasica_cdStand)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_EletricaBasica> TB_DD_EletricaBasica { get; set; } // TB_DD_EletricaBasica.FK_TB_DD_EletricaBasica_cdStand
        /// <summary>
        /// Child TB_DD_EntregaCredencialVIP where [TB_DD_EntregaCredencialVIP].[TB_Edicao_cdEdicao] point to this entity (FK_TB_DD_EntregaCredencialVIP_Edicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_EntregaCredencialVIP> TB_DD_EntregaCredencialVIP { get; set; } // TB_DD_EntregaCredencialVIP.FK_TB_DD_EntregaCredencialVIP_Edicao
        /// <summary>
        /// Child TB_DD_GrupoResponsavelCongresso where [TB_DD_GrupoResponsavelCongresso].[cdEdicao] point to this entity (FK_GrupoResponsavel_Edicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_GrupoResponsavelCongresso> TB_DD_GrupoResponsavelCongresso { get; set; } // TB_DD_GrupoResponsavelCongresso.FK_GrupoResponsavel_Edicao
        /// <summary>
        /// Child TB_DD_HelpDesk where [TB_DD_HelpDesk].[cdEdicao] point to this entity (FK_TB_DD_HelpDesk_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_HelpDesk> TB_DD_HelpDesk { get; set; } // TB_DD_HelpDesk.FK_TB_DD_HelpDesk_cdEdicao
        /// <summary>
        /// Child TB_DD_ItensPedidoCongresso where [TB_DD_ItensPedidoCongresso].[TB_Edicao_cdEdicao] point to this entity (FK_TB_DD_ItensPedidoCongresso_TB_CF_Edicao_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ItensPedidoCongresso> TB_DD_ItensPedidoCongresso { get; set; } // TB_DD_ItensPedidoCongresso.FK_TB_DD_ItensPedidoCongresso_TB_CF_Edicao_cdEdicao
        /// <summary>
        /// Child TB_DD_LimiteAplicacao where [TB_DD_LimiteAplicacao].[cdEdicao] point to this entity (FK_TB_DD_LimiteAplicacao_TB_CF_Edicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_LimiteAplicacao> TB_DD_LimiteAplicacao { get; set; } // TB_DD_LimiteAplicacao.FK_TB_DD_LimiteAplicacao_TB_CF_Edicao
        /// <summary>
        /// Child TB_DD_MaquinaseVeiculos where [TB_DD_MaquinaseVeiculos].[TB_Edicao_cdEdicao] point to this entity (FK_TB_DD_MaquinaseVeiculos_TB_CF_Edicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_MaquinaseVeiculos> TB_DD_MaquinaseVeiculos { get; set; } // TB_DD_MaquinaseVeiculos.FK_TB_DD_MaquinaseVeiculos_TB_CF_Edicao
        /// <summary>
        /// Child TB_DD_Pedido where [TB_DD_Pedido].[TB_Edicao_cdEdicao] point to this entity (FK_TB_DD_Pedido_TB_CF_Edicao_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Pedido> TB_DD_Pedido { get; set; } // TB_DD_Pedido.FK_TB_DD_Pedido_TB_CF_Edicao_cdEdicao
        /// <summary>
        /// Child TB_DD_PlacaIdentificacao where [TB_DD_PlacaIdentificacao].[cdEdicao] point to this entity (TB_DD_PlacaIdentificacao_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_PlacaIdentificacao> TB_DD_PlacaIdentificacao { get; set; } // TB_DD_PlacaIdentificacao.TB_DD_PlacaIdentificacao_cdEdicao
        /// <summary>
        /// Child TB_DD_PlacaIdentificacaoColuna where [TB_DD_PlacaIdentificacaoColuna].[cdEdicao] point to this entity (FK_TB_DD_PlacaIdentificacaoColuna_TB_CF_Edicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_PlacaIdentificacaoColuna> TB_DD_PlacaIdentificacaoColuna { get; set; } // TB_DD_PlacaIdentificacaoColuna.FK_TB_DD_PlacaIdentificacaoColuna_TB_CF_Edicao
        /// <summary>
        /// Child TB_DD_ProdutoExpositores where [TB_DD_ProdutoExpositores].[cdEdicao] point to this entity (FK_TB_DD_ProdutoExpositores_TB_CF_Edicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ProdutoExpositores> TB_DD_ProdutoExpositores { get; set; } // TB_DD_ProdutoExpositores.FK_TB_DD_ProdutoExpositores_TB_CF_Edicao
        /// <summary>
        /// Child TB_DD_ProdutoOperacional where [TB_DD_ProdutoOperacional].[TB_Edicao_cdEdicao] point to this entity (FK_TB_DD_ProdutoOperacional_TB_CF_Edicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ProdutoOperacional> TB_DD_ProdutoOperacional { get; set; } // TB_DD_ProdutoOperacional.FK_TB_DD_ProdutoOperacional_TB_CF_Edicao
        /// <summary>
        /// Child TB_DD_Registro where [TB_DD_Registro].[TB_Edicao_cdEdicao] point to this entity (FK_TB_DD_Registro_TB_CF_Edicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Registro> TB_DD_Registro { get; set; } // TB_DD_Registro.FK_TB_DD_Registro_TB_CF_Edicao
        /// <summary>
        /// Child TB_DD_Regulamento where [TB_DD_Regulamento].[cdEdicao] point to this entity (FK_TB_DD_Regulamento_TB_CF_Edicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Regulamento> TB_DD_Regulamento { get; set; } // TB_DD_Regulamento.FK_TB_DD_Regulamento_TB_CF_Edicao
        /// <summary>
        /// Child TB_DD_Relatorio where [TB_DD_Relatorio].[cdEdicao] point to this entity (FK_TB_DD_Relatorio_TB_CF_Edicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Relatorio> TB_DD_Relatorio { get; set; } // TB_DD_Relatorio.FK_TB_DD_Relatorio_TB_CF_Edicao
        /// <summary>
        /// Child TB_DD_Stand where [TB_DD_Stand].[TB_Edicao_cdEdicao] point to this entity (FK_TB_DD_Stand_TB_CF_Edicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Stand> TB_DD_Stand { get; set; } // TB_DD_Stand.FK_TB_DD_Stand_TB_CF_Edicao
        /// <summary>
        /// Child TB_LG_Acesso where [TB_LG_Acesso].[cdEdicao] point to this entity (FK_TB_LG_Acesso_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_LG_Acesso> TB_LG_Acesso { get; set; } // TB_LG_Acesso.FK_TB_LG_Acesso_cdEdicao
        /// <summary>
        /// Child TB_LG_FormularioManualEletronico where [TB_LG_FormularioManualEletronico].[cdEdicao] point to this entity (FK_TB_LG_FormularioManualEletronico_TB_CF_Edicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_LG_FormularioManualEletronico> TB_LG_FormularioManualEletronico { get; set; } // TB_LG_FormularioManualEletronico.FK_TB_LG_FormularioManualEletronico_TB_CF_Edicao
        /// <summary>
        /// Child TB_RF_AliquotaImposto where [TB_RF_AliquotaImposto].[cdEdicao] point to this entity (FK_TB_RF_AliquotaImposto_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_RF_AliquotaImposto> TB_RF_AliquotaImposto { get; set; } // TB_RF_AliquotaImposto.FK_TB_RF_AliquotaImposto_cdEdicao
        /// <summary>
        /// Child TB_RF_AreaAtuacao where [TB_RF_AreaAtuacao].[cdEdicao] point to this entity (FK_TB_RF_AreaAtuacao_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_RF_AreaAtuacao> TB_RF_AreaAtuacao { get; set; } // TB_RF_AreaAtuacao.FK_TB_RF_AreaAtuacao_cdEdicao
        /// <summary>
        /// Child TB_RF_Atividades where [TB_RF_Atividades].[TB_Edicao_cdEdicao] point to this entity (FK_TB_RF_Atividades_TB_CF_Edicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_RF_Atividades> TB_RF_Atividades { get; set; } // TB_RF_Atividades.FK_TB_RF_Atividades_TB_CF_Edicao
        /// <summary>
        /// Child TB_RF_Faq where [TB_RF_Faq].[cdEdicao] point to this entity (FK_TB_RF_Faq_cdEdicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_RF_Faq> TB_RF_Faq { get; set; } // TB_RF_Faq.FK_TB_RF_Faq_cdEdicao
        /// <summary>
        /// Child TB_RF_Leitura_Local where [TB_RF_Leitura_Local].[cdEdicao] point to this entity (FK_TB_RF_Leitura_Local)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_RF_Leitura_Local> TB_RF_Leitura_Local { get; set; } // TB_RF_Leitura_Local.FK_TB_RF_Leitura_Local
        /// <summary>
        /// Child TB_RF_PreenchimentoFormulario where [TB_RF_PreenchimentoFormulario].[TB_FormularioManualEletronico_TB_Edicao_cdEdicao] point to this entity (FK_TB_RF_PreenchimentoFormulario_TB_CF_Edicao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_RF_PreenchimentoFormulario> TB_RF_PreenchimentoFormulario { get; set; } // TB_RF_PreenchimentoFormulario.FK_TB_RF_PreenchimentoFormulario_TB_CF_Edicao

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Evento pointed by [TB_CF_Edicao].([cdEvento]) (FK_TB_CF_Edicao_TB_DD_Evento)
        /// </summary>
        public virtual TB_DD_Evento TB_DD_Evento { get; set; } // FK_TB_CF_Edicao_TB_DD_Evento

        public TB_CF_Edicao()
        {
            TB_AS_TipoCredenciamento_Edicao = new System.Collections.Generic.List<TB_AS_TipoCredenciamento_Edicao>();
            TB_CF_FormularioManualEletronico = new System.Collections.Generic.List<TB_CF_FormularioManualEletronico>();
            TB_CF_Permissoes = new System.Collections.Generic.List<TB_CF_Permissoes>();
            TB_DD_ATOM = new System.Collections.Generic.List<TB_DD_ATOM>();
            TB_DD_CarrinhoOperacional = new System.Collections.Generic.List<TB_DD_CarrinhoOperacional>();
            TB_DD_CatalogoOficial = new System.Collections.Generic.List<TB_DD_CatalogoOficial>();
            TB_DD_Coletor = new System.Collections.Generic.List<TB_DD_Coletor>();
            TB_DD_Comunicados = new System.Collections.Generic.List<TB_DD_Comunicados>();
            TB_DD_ContratoExpositor = new System.Collections.Generic.List<TB_DD_ContratoExpositor>();
            TB_DD_ConviteEletronico = new System.Collections.Generic.List<TB_DD_ConviteEletronico>();
            TB_DD_ConviteEletronicoVip = new System.Collections.Generic.List<TB_DD_ConviteEletronicoVip>();
            TB_DD_CorCarpeteColuna = new System.Collections.Generic.List<TB_DD_CorCarpeteColuna>();
            TB_DD_EletricaBasica = new System.Collections.Generic.List<TB_DD_EletricaBasica>();
            TB_DD_EntregaCredencialVIP = new System.Collections.Generic.List<TB_DD_EntregaCredencialVIP>();
            TB_DD_GrupoResponsavelCongresso = new System.Collections.Generic.List<TB_DD_GrupoResponsavelCongresso>();
            TB_DD_HelpDesk = new System.Collections.Generic.List<TB_DD_HelpDesk>();
            TB_DD_ItensPedidoCongresso = new System.Collections.Generic.List<TB_DD_ItensPedidoCongresso>();
            TB_DD_LimiteAplicacao = new System.Collections.Generic.List<TB_DD_LimiteAplicacao>();
            TB_DD_MaquinaseVeiculos = new System.Collections.Generic.List<TB_DD_MaquinaseVeiculos>();
            TB_DD_Pedido = new System.Collections.Generic.List<TB_DD_Pedido>();
            TB_DD_PlacaIdentificacao = new System.Collections.Generic.List<TB_DD_PlacaIdentificacao>();
            TB_DD_PlacaIdentificacaoColuna = new System.Collections.Generic.List<TB_DD_PlacaIdentificacaoColuna>();
            TB_DD_ProdutoExpositores = new System.Collections.Generic.List<TB_DD_ProdutoExpositores>();
            TB_DD_ProdutoOperacional = new System.Collections.Generic.List<TB_DD_ProdutoOperacional>();
            TB_DD_Registro = new System.Collections.Generic.List<TB_DD_Registro>();
            TB_DD_Regulamento = new System.Collections.Generic.List<TB_DD_Regulamento>();
            TB_DD_Relatorio = new System.Collections.Generic.List<TB_DD_Relatorio>();
            TB_DD_Stand = new System.Collections.Generic.List<TB_DD_Stand>();
            TB_LG_Acesso = new System.Collections.Generic.List<TB_LG_Acesso>();
            TB_LG_FormularioManualEletronico = new System.Collections.Generic.List<TB_LG_FormularioManualEletronico>();
            TB_RF_AliquotaImposto = new System.Collections.Generic.List<TB_RF_AliquotaImposto>();
            TB_RF_AreaAtuacao = new System.Collections.Generic.List<TB_RF_AreaAtuacao>();
            TB_RF_Atividades = new System.Collections.Generic.List<TB_RF_Atividades>();
            TB_RF_Faq = new System.Collections.Generic.List<TB_RF_Faq>();
            TB_RF_Leitura_Local = new System.Collections.Generic.List<TB_RF_Leitura_Local>();
            TB_RF_PreenchimentoFormulario = new System.Collections.Generic.List<TB_RF_PreenchimentoFormulario>();
        }
    }

    // TB_CF_EstruturaPadrao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_EstruturaPadrao
    {
        public int cdEstruturaPadrao { get; set; } // cdEstruturaPadrao (Primary key)
        public int cdFormulario { get; set; } // cdFormulario
        public int cdTipoObjetoPortugues { get; set; } // cdTipoObjetoPortugues
        public int cdTipoValidacao { get; set; } // cdTipoValidacao
        public string dsPortugues { get; set; } // dsPortugues (length: 200)
        public string dsIngles { get; set; } // dsIngles (length: 200)
        public string dsEspanhol { get; set; } // dsEspanhol (length: 200)
        public string nmObjeto { get; set; } // nmObjeto (length: 200)
        public string fgObrigatorioPortugues { get; set; } // fgObrigatorioPortugues (length: 1)
        public string fgObrigatorioIngles { get; set; } // fgObrigatorioIngles (length: 1)
        public string fgObrigatorioEspanhol { get; set; } // fgObrigatorioEspanhol (length: 1)
        public string dsMensagemObrigatorioPortugues { get; set; } // dsMensagemObrigatorioPortugues (length: 800)
        public string dsMensagemObrigatorioIngles { get; set; } // dsMensagemObrigatorioIngles (length: 800)
        public string dsMensagemObrigatorioEspanhol { get; set; } // dsMensagemObrigatorioEspanhol (length: 800)
        public int nuQtdCaracteres { get; set; } // nuQtdCaracteres
        public int nuWidthObjeto { get; set; } // nuWidthObjeto
        public int nuOrdem { get; set; } // nuOrdem
        public string nmGrupoExibicao { get; set; } // nmGrupoExibicao (length: 100)
        public int? cdTipoObjetoIngles { get; set; } // cdTipoObjetoIngles
        public int? cdTipoObjetoEspanhol { get; set; } // cdTipoObjetoEspanhol
        public int? cdGruposDadosCadastrais { get; set; } // cdGruposDadosCadastrais
        public string fgBasico { get; set; } // fgBasico (length: 1)
        public string fgCompleto { get; set; } // fgCompleto (length: 1)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_EstruturaPersonalizada where [TB_DD_EstruturaPersonalizada].[cdEstruturaPadrao] point to this entity (FK_TB_DD_EstruturaPersonalizada_TB_CF_Manual)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_EstruturaPersonalizada> TB_DD_EstruturaPersonalizada { get; set; } // TB_DD_EstruturaPersonalizada.FK_TB_DD_EstruturaPersonalizada_TB_CF_Manual

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Formulario pointed by [TB_CF_EstruturaPadrao].([cdFormulario]) (FK_TB_CF_EstruturaPadrao_TB_CF_Formulario)
        /// </summary>
        public virtual TB_CF_Formulario TB_CF_Formulario { get; set; } // FK_TB_CF_EstruturaPadrao_TB_CF_Formulario

        /// <summary>
        /// Parent TB_RF_GruposDadosCadastrais pointed by [TB_CF_EstruturaPadrao].([cdGruposDadosCadastrais]) (FK_TB_CF_EstruturaPadrao_TB_RF_GruposDadosCadastrais)
        /// </summary>
        public virtual TB_RF_GruposDadosCadastrais TB_RF_GruposDadosCadastrais { get; set; } // FK_TB_CF_EstruturaPadrao_TB_RF_GruposDadosCadastrais

        /// <summary>
        /// Parent TB_RF_TipoObjeto pointed by [TB_CF_EstruturaPadrao].([cdTipoObjetoEspanhol]) (FK_TB_CF_EstruturaPadrao_TB_RF_TipoObjetoEspanhol)
        /// </summary>
        public virtual TB_RF_TipoObjeto TB_RF_TipoObjeto_cdTipoObjetoEspanhol { get; set; } // FK_TB_CF_EstruturaPadrao_TB_RF_TipoObjetoEspanhol

        /// <summary>
        /// Parent TB_RF_TipoObjeto pointed by [TB_CF_EstruturaPadrao].([cdTipoObjetoIngles]) (FK_TB_CF_EstruturaPadrao_TB_RF_TipoObjetoIngles)
        /// </summary>
        public virtual TB_RF_TipoObjeto TB_RF_TipoObjeto_cdTipoObjetoIngles { get; set; } // FK_TB_CF_EstruturaPadrao_TB_RF_TipoObjetoIngles

        /// <summary>
        /// Parent TB_RF_TipoObjeto pointed by [TB_CF_EstruturaPadrao].([cdTipoObjetoPortugues]) (FK_TB_CF_EstruturaPadrao_TB_RF_TipoObjeto)
        /// </summary>
        public virtual TB_RF_TipoObjeto TB_RF_TipoObjeto_cdTipoObjetoPortugues { get; set; } // FK_TB_CF_EstruturaPadrao_TB_RF_TipoObjeto

        /// <summary>
        /// Parent TB_RF_TipoValidacao pointed by [TB_CF_EstruturaPadrao].([cdTipoValidacao]) (FK_tb_cf_EstruturaPadrao_tb_RF_TipoValidacao)
        /// </summary>
        public virtual TB_RF_TipoValidacao TB_RF_TipoValidacao { get; set; } // FK_tb_cf_EstruturaPadrao_tb_RF_TipoValidacao

        public TB_CF_EstruturaPadrao()
        {
            fgBasico = "N";
            TB_DD_EstruturaPersonalizada = new System.Collections.Generic.List<TB_DD_EstruturaPersonalizada>();
        }
    }

    // TB_CF_ExecaoFormularioManualEletronico
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_ExecaoFormularioManualEletronico
    {
        public int cdExecao { get; set; } // cdExecao (Primary key)
        public System.DateTime? dtExcecao { get; set; } // dtExcecao
        public int? cdFormulario { get; set; } // cdFormulario
        public int? cdStand { get; set; } // cdStand
        public System.DateTime? dtLimite { get; set; } // dtLimite
        public int? qtdCredencial { get; set; } // qtdCredencial
        public int? cdUsuario { get; set; } // cdUsuario
        public string fgHabilitado { get; set; } // fgHabilitado (length: 1)
    }

    // TB_CF_Formulario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_Formulario
    {
        public int cdFormulario { get; set; } // cdFormulario (Primary key)
        public int cdTipoFormulario { get; set; } // cdTipoFormulario
        public string nmFormularioPortugues { get; set; } // nmFormularioPortugues (length: 200)
        public string nmFormularioIngles { get; set; } // nmFormularioIngles (length: 200)
        public string nmFormularioEspanhol { get; set; } // nmFormularioEspanhol (length: 200)
        public string dsCodigoPagina { get; set; } // dsCodigoPagina (length: 100)

        // Reverse navigation

        /// <summary>
        /// Child TB_CF_EstruturaPadrao where [TB_CF_EstruturaPadrao].[cdFormulario] point to this entity (FK_TB_CF_EstruturaPadrao_TB_CF_Formulario)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_CF_EstruturaPadrao> TB_CF_EstruturaPadrao { get; set; } // TB_CF_EstruturaPadrao.FK_TB_CF_EstruturaPadrao_TB_CF_Formulario
        /// <summary>
        /// Child TB_DD_EstruturaPersonalizada where [TB_DD_EstruturaPersonalizada].[cdFormulario] point to this entity (FK_tb_dd_EstruturaPersonalizada_tb_cf_formulario)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_EstruturaPersonalizada> TB_DD_EstruturaPersonalizada { get; set; } // TB_DD_EstruturaPersonalizada.FK_tb_dd_EstruturaPersonalizada_tb_cf_formulario
        /// <summary>
        /// Child TB_LG_Erro where [TB_LG_Erro].[cdFormulario] point to this entity (FK_TB_LG_Erro_TB_CF_Formulario)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_LG_Erro> TB_LG_Erro { get; set; } // TB_LG_Erro.FK_TB_LG_Erro_TB_CF_Formulario
        /// <summary>
        /// Child TB_LG_Geral where [TB_LG_Geral].[cdFormulario] point to this entity (FK_TB_LG_Geral_TB_CF_Formulario)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_LG_Geral> TB_LG_Geral { get; set; } // TB_LG_Geral.FK_TB_LG_Geral_TB_CF_Formulario

        // Foreign keys

        /// <summary>
        /// Parent TB_RF_TipoFormulario pointed by [TB_CF_Formulario].([cdTipoFormulario]) (FK_TB_CF_Formulario_TB_RF_TipoFormulario)
        /// </summary>
        public virtual TB_RF_TipoFormulario TB_RF_TipoFormulario { get; set; } // FK_TB_CF_Formulario_TB_RF_TipoFormulario

        public TB_CF_Formulario()
        {
            TB_CF_EstruturaPadrao = new System.Collections.Generic.List<TB_CF_EstruturaPadrao>();
            TB_DD_EstruturaPersonalizada = new System.Collections.Generic.List<TB_DD_EstruturaPersonalizada>();
            TB_LG_Erro = new System.Collections.Generic.List<TB_LG_Erro>();
            TB_LG_Geral = new System.Collections.Generic.List<TB_LG_Geral>();
        }
    }

    // TB_CF_FormularioGenerico
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_FormularioGenerico
    {
        public System.Guid cdFormularioGenerico { get; set; } // cdFormularioGenerico (Primary key)
        public int cdProdutoOperacional { get; set; } // cdProdutoOperacional
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public string dsObjeto { get; set; } // dsObjeto (length: 100)
        public int nuOrdem { get; set; } // nuOrdem
        public int qtdCaracteres { get; set; } // qtdCaracteres
        public int cdTipoValidacao { get; set; } // cdTipoValidacao
        public string nmPortugues { get; set; } // nmPortugues (length: 500)
        public string nmIngles { get; set; } // nmIngles (length: 500)
        public string nmEspanhol { get; set; } // nmEspanhol (length: 500)
        public string fgHabilitaPortugues { get; set; } // fgHabilitaPortugues (length: 1)
        public string fgHabilitaIngles { get; set; } // fgHabilitaIngles (length: 1)
        public string fgHabilitaEspanhol { get; set; } // fgHabilitaEspanhol (length: 1)
        public string fgObrigatorioPortugues { get; set; } // fgObrigatorioPortugues (length: 1)
        public string fgObrigatorioIngles { get; set; } // fgObrigatorioIngles (length: 1)
        public string fgObrigatorioEspanhol { get; set; } // fgObrigatorioEspanhol (length: 1)
        public string dsPreenchimentoObrigatorioPortugues { get; set; } // dsPreenchimentoObrigatorioPortugues (length: 500)
        public string dsPreenchimentoObrigatorioIngles { get; set; } // dsPreenchimentoObrigatorioIngles (length: 500)
        public string dsPreenchimentoObrigatorioEspanhol { get; set; } // dsPreenchimentoObrigatorioEspanhol (length: 500)
        public string dsPreenchimentoIncorretoPortugues { get; set; } // dsPreenchimentoIncorretoPortugues (length: 500)
        public string dsPreenchimentoIncorretoIngles { get; set; } // dsPreenchimentoIncorretoIngles (length: 500)
        public string dsPreenchimentoIncorretoEspanhol { get; set; } // dsPreenchimentoIncorretoEspanhol (length: 500)
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public int cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_ProdutoOperacional pointed by [TB_CF_FormularioGenerico].([cdProdutoOperacional]) (FK_FormularioGenerico_ProdutoOperacional)
        /// </summary>
        public virtual TB_DD_ProdutoOperacional TB_DD_ProdutoOperacional { get; set; } // FK_FormularioGenerico_ProdutoOperacional

        /// <summary>
        /// Parent TB_RF_TipoValidacao pointed by [TB_CF_FormularioGenerico].([cdTipoValidacao]) (FK_FormularioGenerico_TipoValidacao)
        /// </summary>
        public virtual TB_RF_TipoValidacao TB_RF_TipoValidacao { get; set; } // FK_FormularioGenerico_TipoValidacao

        public TB_CF_FormularioGenerico()
        {
            cdFormularioGenerico = System.Guid.NewGuid();
        }
    }

    // TB_CF_FormularioManualEletronico
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_FormularioManualEletronico
    {
        public int cdFormulario { get; set; } // cdFormulario (Primary key)
        public int TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public int? nuOrdem { get; set; } // nuOrdem
        public string nmFormulario { get; set; } // nmFormulario
        public string dsFormulario { get; set; } // dsFormulario
        public string nmFormularioIngl { get; set; } // nmFormularioIngl
        public string dsFormularioIngl { get; set; } // dsFormularioIngl
        public string dsUrl { get; set; } // dsUrl
        public System.DateTime? dtLimite { get; set; } // dtLimite
        public string dsObrigatoriedade { get; set; } // dsObrigatoriedade (length: 100)
        public string dsExpositor { get; set; } // dsExpositor (length: 100)
        public string dsMontadora { get; set; } // dsMontadora (length: 100)
        public string dsServico { get; set; } // dsServico (length: 100)
        public string dsCarrinho { get; set; } // dsCarrinho (length: 1)
        public string dsQtdFracao { get; set; } // dsQtdFracao (length: 1)
        public string dsTipo { get; set; } // dsTipo (length: 100)
        public string dsVendaUnica { get; set; } // dsVendaUnica (length: 10)
        public string dsHabilitado { get; set; } // dsHabilitado (length: 100)
        public string dsPermiteImportacaoCredencial { get; set; } // dsPermiteImportacaoCredencial (length: 1)
        public string dsPermiteAlteracaoEmpresaCracha { get; set; } // dsPermiteAlteracaoEmpresaCracha (length: 1)
        public int? dsTipoCredencial { get; set; } // dsTipoCredencial
        public string dsCPFObrigatorio { get; set; } // dsCPFObrigatorio
        public string dsPassportObrigatorio { get; set; } // dsPassportObrigatorio
        public string dsTopo { get; set; } // dsTopo
        public string dsRodape { get; set; } // dsRodape
        public string dsTopoIng { get; set; } // dsTopoIng
        public string dsRodapeIng { get; set; } // dsRodapeIng
        public string dsCarta { get; set; } // dsCarta
        public string dsCartaIng { get; set; } // dsCartaIng
        public string fgHabilitaVendaDados { get; set; } // fgHabilitaVendaDados (length: 1)
        public string dsIncideImposto { get; set; } // dsIncideImposto (length: 1)
        public string dsCaex { get; set; } // dsCaex (length: 1)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_CarrinhoOperacional where [TB_DD_CarrinhoOperacional].[cdFormulario] point to this entity (FK_TB_DD_CarrinhoOperacional_cdFormulario)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_CarrinhoOperacional> TB_DD_CarrinhoOperacional { get; set; } // TB_DD_CarrinhoOperacional.FK_TB_DD_CarrinhoOperacional_cdFormulario
        /// <summary>
        /// Child TB_DD_PermissaoCorCarpeteColuna where [TB_DD_PermissaoCorCarpeteColuna].[cdFormulario] point to this entity (FK_TB_DD_PermissaoCorCarpeteColuna_TB_CF_FormularioManualEletronico)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_PermissaoCorCarpeteColuna> TB_DD_PermissaoCorCarpeteColuna { get; set; } // TB_DD_PermissaoCorCarpeteColuna.FK_TB_DD_PermissaoCorCarpeteColuna_TB_CF_FormularioManualEletronico
        /// <summary>
        /// Child TB_LG_FormularioManualEletronico where [TB_LG_FormularioManualEletronico].[cdFormulario] point to this entity (FK_TB_LG_FormularioManualEletronico_TB_CF_FormularioManualEletronico)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_LG_FormularioManualEletronico> TB_LG_FormularioManualEletronico { get; set; } // TB_LG_FormularioManualEletronico.FK_TB_LG_FormularioManualEletronico_TB_CF_FormularioManualEletronico
        /// <summary>
        /// Child TB_RF_Cor where [TB_RF_Cor].[cdFormulario] point to this entity (FK_TB_RF_Cor_TB_CF_FormularioManualEletronico)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_RF_Cor> TB_RF_Cor { get; set; } // TB_RF_Cor.FK_TB_RF_Cor_TB_CF_FormularioManualEletronico
        /// <summary>
        /// Child TB_RF_PreenchimentoFormulario where [TB_RF_PreenchimentoFormulario].[TB_FormularioManualEletronico_cdFormulario] point to this entity (FK_TB_RF_PreenchimentoFormulario_TB_CF_FormularioManualEletronico)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_RF_PreenchimentoFormulario> TB_RF_PreenchimentoFormulario { get; set; } // TB_RF_PreenchimentoFormulario.FK_TB_RF_PreenchimentoFormulario_TB_CF_FormularioManualEletronico

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_CF_FormularioManualEletronico].([TB_Edicao_cdEdicao]) (FK_TB_CF_FormularioManualEletronico_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_CF_FormularioManualEletronico_cdEdicao

        public TB_CF_FormularioManualEletronico()
        {
            TB_DD_CarrinhoOperacional = new System.Collections.Generic.List<TB_DD_CarrinhoOperacional>();
            TB_DD_PermissaoCorCarpeteColuna = new System.Collections.Generic.List<TB_DD_PermissaoCorCarpeteColuna>();
            TB_LG_FormularioManualEletronico = new System.Collections.Generic.List<TB_LG_FormularioManualEletronico>();
            TB_RF_Cor = new System.Collections.Generic.List<TB_RF_Cor>();
            TB_RF_PreenchimentoFormulario = new System.Collections.Generic.List<TB_RF_PreenchimentoFormulario>();
        }
    }

    // TB_CF_FormularioSistemaIntegrado
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_FormularioSistemaIntegrado
    {
        public int cdFormulario { get; set; } // cdFormulario (Primary key)
        public string dsFormulario { get; set; } // dsFormulario (length: 100)
        public string dsGrupo { get; set; } // dsGrupo (length: 100)
        public string dsStatus { get; set; } // dsStatus (length: 10)

        // Reverse navigation

        /// <summary>
        /// Child TB_CF_Permissoes where [TB_CF_Permissoes].[cdFormulario] point to this entity (FK_TB_CF_Permissoes_TB_CF_FormularioSistemaIntegrado)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_CF_Permissoes> TB_CF_Permissoes { get; set; } // TB_CF_Permissoes.FK_TB_CF_Permissoes_TB_CF_FormularioSistemaIntegrado

        public TB_CF_FormularioSistemaIntegrado()
        {
            TB_CF_Permissoes = new System.Collections.Generic.List<TB_CF_Permissoes>();
        }
    }

    // TB_CF_INT_Geral
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_INT_Geral
    {
        public int cdConfig { get; set; } // cdConfig (Primary key)
        public string wkDiretorioFotoFuncionario { get; set; } // wkDiretorioFotoFuncionario
    }

    // TB_CF_Permissoes
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_Permissoes
    {
        public int cdPermissao { get; set; } // cdPermissao (Primary key)
        public int cdFormulario { get; set; } // cdFormulario
        public int cdUsuario { get; set; } // cdUsuario
        public string dsRead { get; set; } // dsRead (length: 30)
        public string dsWrite { get; set; } // dsWrite (length: 30)
        public int cdEdicao { get; set; } // cdEdicao

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_CF_Permissoes].([cdEdicao]) (FK_TB_CF_Permissoes_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_CF_Permissoes_cdEdicao

        /// <summary>
        /// Parent TB_CF_FormularioSistemaIntegrado pointed by [TB_CF_Permissoes].([cdFormulario]) (FK_TB_CF_Permissoes_TB_CF_FormularioSistemaIntegrado)
        /// </summary>
        public virtual TB_CF_FormularioSistemaIntegrado TB_CF_FormularioSistemaIntegrado { get; set; } // FK_TB_CF_Permissoes_TB_CF_FormularioSistemaIntegrado

        /// <summary>
        /// Parent TB_DD_Usuario pointed by [TB_CF_Permissoes].([cdUsuario]) (FK_TB_CF_Permissoes_cdUsuario)
        /// </summary>
        public virtual TB_DD_Usuario TB_DD_Usuario { get; set; } // FK_TB_CF_Permissoes_cdUsuario
    }

    // TB_CF_Recibo
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_Recibo
    {
        public int cdConfiguracao { get; set; } // cdConfiguracao (Primary key)
        public string dsEmpresa { get; set; } // dsEmpresa (length: 200)
        public string dsCNPJ { get; set; } // dsCNPJ (length: 20)
        public string dsLogradouro { get; set; } // dsLogradouro (length: 200)
        public string dsLogradouroNumero { get; set; } // dsLogradouroNumero (length: 200)
        public string dsLogradouroBairro { get; set; } // dsLogradouroBairro (length: 200)
        public string dsLogradouroCep { get; set; } // dsLogradouroCep (length: 200)
        public string dsLogradouroCidade { get; set; } // dsLogradouroCidade (length: 200)
        public string dsLogradouroEstado { get; set; } // dsLogradouroEstado (length: 200)
        public string dsInicioRecibo { get; set; } // dsInicioRecibo (length: 200)
        public string fgHabilitado { get; set; } // fgHabilitado (length: 1)

        // Reverse navigation

        /// <summary>
        /// Child TB_AS_Pedido_Recibo where [TB_AS_Pedido_Recibo].[cdConfiguracao] point to this entity (FK_TB_AS_Pedido_Recibo_TB_CF_Recibo)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_Pedido_Recibo> TB_AS_Pedido_Recibo { get; set; } // TB_AS_Pedido_Recibo.FK_TB_AS_Pedido_Recibo_TB_CF_Recibo

        public TB_CF_Recibo()
        {
            TB_AS_Pedido_Recibo = new System.Collections.Generic.List<TB_AS_Pedido_Recibo>();
        }
    }

    // TB_CF_VendaDados
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_VendaDados
    {
        public int cdVendaDados { get; set; } // cdVendaDados (Primary key)
        public int cdProdutoOperacional { get; set; } // cdProdutoOperacional
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public string dsObjeto { get; set; } // dsObjeto (length: 100)
        public int nuOrdem { get; set; } // nuOrdem
        public int? qtdCaracteres { get; set; } // qtdCaracteres
        public int cdTipoValidacao { get; set; } // cdTipoValidacao
        public string nmPortugues { get; set; } // nmPortugues (length: 200)
        public string nmIngles { get; set; } // nmIngles (length: 200)
        public string nmEspanhol { get; set; } // nmEspanhol (length: 200)
        public string fgHabilitaPortugues { get; set; } // fgHabilitaPortugues (length: 1)
        public string fgHabilitaIngles { get; set; } // fgHabilitaIngles (length: 1)
        public string fgHabilitaEspanhol { get; set; } // fgHabilitaEspanhol (length: 1)
        public string fgObrigatorioPortugues { get; set; } // fgObrigatorioPortugues (length: 1)
        public string fgObrigatorioIngles { get; set; } // fgObrigatorioIngles (length: 1)
        public string fgObrigatorioEspanhol { get; set; } // fgObrigatorioEspanhol (length: 1)
        public string dsPreenchimentoObrigatorioPortugues { get; set; } // dsPreenchimentoObrigatorioPortugues (length: 500)
        public string dsPreenchimentoObrigatorioIngles { get; set; } // dsPreenchimentoObrigatorioIngles (length: 500)
        public string dsPreenchimentoObrigatorioEspanhol { get; set; } // dsPreenchimentoObrigatorioEspanhol (length: 500)
        public string dsPreenchimentoIncorretoPortugues { get; set; } // dsPreenchimentoIncorretoPortugues (length: 500)
        public string dsPreenchimentoIncorretoIngles { get; set; } // dsPreenchimentoIncorretoIngles (length: 500)
        public string dsPreenchimentoIncorretoEspanhol { get; set; } // dsPreenchimentoIncorretoEspanhol (length: 500)
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public int cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_CarrinhoOperacional_VendaDados where [TB_DD_CarrinhoOperacional_VendaDados].[cdVendaDados] point to this entity (FK_DD_CarrinhoOperacional_VendaDados_CF_VendaDados)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_CarrinhoOperacional_VendaDados> TB_DD_CarrinhoOperacional_VendaDados { get; set; } // TB_DD_CarrinhoOperacional_VendaDados.FK_DD_CarrinhoOperacional_VendaDados_CF_VendaDados

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_ProdutoOperacional pointed by [TB_CF_VendaDados].([cdProdutoOperacional]) (PK_VendaDados_ProdutoOperacional)
        /// </summary>
        public virtual TB_DD_ProdutoOperacional TB_DD_ProdutoOperacional { get; set; } // PK_VendaDados_ProdutoOperacional

        /// <summary>
        /// Parent TB_RF_TipoValidacao pointed by [TB_CF_VendaDados].([cdTipoValidacao]) (PK_VendaDados_TipoValidacao)
        /// </summary>
        public virtual TB_RF_TipoValidacao TB_RF_TipoValidacao { get; set; } // PK_VendaDados_TipoValidacao

        public TB_CF_VendaDados()
        {
            TB_DD_CarrinhoOperacional_VendaDados = new System.Collections.Generic.List<TB_DD_CarrinhoOperacional_VendaDados>();
        }
    }

    // TB_DD_Agendamento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Agendamento
    {
        public int cdAgendamento { get; set; } // cdAgendamento (Primary key)
        public int cdUsuario { get; set; } // cdUsuario
        public System.DateTime dtAgendamento { get; set; } // dtAgendamento
        public string dsNome { get; set; } // dsNome (length: 200)
        public string dsTelefone { get; set; } // dsTelefone (length: 100)
        public string dsStatus { get; set; } // dsStatus (length: 50)
        public string dsObs { get; set; } // dsObs

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Usuario pointed by [TB_DD_Agendamento].([cdUsuario]) (FK_DD_Agendamento_Usuario)
        /// </summary>
        public virtual TB_DD_Usuario TB_DD_Usuario { get; set; } // FK_DD_Agendamento_Usuario
    }

    // TB_DD_Alternativa
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Alternativa
    {
        public int cdAlternativa { get; set; } // cdAlternativa (Primary key)
        public string dsSigla { get; set; } // dsSigla (length: 255)
        public string dsPortugues { get; set; } // dsPortugues (length: 255)
        public string dsIngles { get; set; } // dsIngles (length: 255)
        public string dsEspanhol { get; set; } // dsEspanhol (length: 255)

        // Reverse navigation

        /// <summary>
        /// Child TB_AS_Alternativa_Questionario where [TB_AS_Alternativa_Questionario].[dsSubAlternativaDa] point to this entity (FK_TB_AS_Alternativa_Questionario_dsSubAlternativaDa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_Alternativa_Questionario> dsSubAlternativaDa { get; set; } // TB_AS_Alternativa_Questionario.FK_TB_AS_Alternativa_Questionario_dsSubAlternativaDa
        /// <summary>
        /// Child TB_AS_Alternativa_Questionario where [TB_AS_Alternativa_Questionario].[cdAlternativa] point to this entity (FK_TB_AS_Alternativa_Questionario_TB_DD_Alternativa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_Alternativa_Questionario> TB_AS_Alternativa_Questionario_cdAlternativa { get; set; } // TB_AS_Alternativa_Questionario.FK_TB_AS_Alternativa_Questionario_TB_DD_Alternativa
        /// <summary>
        /// Child TB_DD_Resposta where [TB_DD_Resposta].[TB_Alternativa_cdAlternativa] point to this entity (FK_TB_DD_Resposta_TB_DD_Alternativa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Resposta> TB_DD_Resposta { get; set; } // TB_DD_Resposta.FK_TB_DD_Resposta_TB_DD_Alternativa

        public TB_DD_Alternativa()
        {
            TB_AS_Alternativa_Questionario_cdAlternativa = new System.Collections.Generic.List<TB_AS_Alternativa_Questionario>();
            dsSubAlternativaDa = new System.Collections.Generic.List<TB_AS_Alternativa_Questionario>();
            TB_DD_Resposta = new System.Collections.Generic.List<TB_DD_Resposta>();
        }
    }

    // TB_DD_AlternativaQuestionarioManual
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_AlternativaQuestionarioManual
    {
        public int cdAlternativaQuestionarioManual { get; set; } // cdAlternativaQuestionarioManual (Primary key)
        public int cdQuestionarioManual { get; set; } // cdQuestionarioManual
        public int nuOrdem { get; set; } // nuOrdem
        public string dsPortugues { get; set; } // dsPortugues (length: 800)
        public string dsIngles { get; set; } // dsIngles (length: 800)
        public string dsEspanhol { get; set; } // dsEspanhol (length: 800)
        public string dsOutros { get; set; } // dsOutros (length: 1)
        public string dsSituacao { get; set; } // dsSituacao (length: 800)
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public int cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_RespostaQuestionarioManual where [TB_DD_RespostaQuestionarioManual].[cdAlternativaQuestionarioManual] point to this entity (FK_TB_DD_RespostaQuestionarioManual_TB_DD_AlternativaQuestionarioManual)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_RespostaQuestionarioManual> TB_DD_RespostaQuestionarioManual { get; set; } // TB_DD_RespostaQuestionarioManual.FK_TB_DD_RespostaQuestionarioManual_TB_DD_AlternativaQuestionarioManual
        /// <summary>
        /// Child TB_DD_SubAlternativaQuestionarioManual where [TB_DD_SubAlternativaQuestionarioManual].[cdAlternativaQuestionarioManual] point to this entity (FK_TB_DD_SubAlternativaQuestionarioManual_TB_DD_AlternativaQuestionarioManual)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_SubAlternativaQuestionarioManual> TB_DD_SubAlternativaQuestionarioManual { get; set; } // TB_DD_SubAlternativaQuestionarioManual.FK_TB_DD_SubAlternativaQuestionarioManual_TB_DD_AlternativaQuestionarioManual

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_QuestionarioManual pointed by [TB_DD_AlternativaQuestionarioManual].([cdQuestionarioManual]) (FK_TB_DD_AlternativaQuestionarioManual_TB_DD_QuestionarioManual)
        /// </summary>
        public virtual TB_DD_QuestionarioManual TB_DD_QuestionarioManual { get; set; } // FK_TB_DD_AlternativaQuestionarioManual_TB_DD_QuestionarioManual

        public TB_DD_AlternativaQuestionarioManual()
        {
            TB_DD_RespostaQuestionarioManual = new System.Collections.Generic.List<TB_DD_RespostaQuestionarioManual>();
            TB_DD_SubAlternativaQuestionarioManual = new System.Collections.Generic.List<TB_DD_SubAlternativaQuestionarioManual>();
        }
    }

    // TB_DD_App_ConvideAmigo
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_App_ConvideAmigo
    {
        public int cdConvite { get; set; } // cdConvite (Primary key)
        public int TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public System.DateTime dtInclusao { get; set; } // dtInclusao
        public int TB_Pessoa_cdPessoa { get; set; } // TB_Pessoa_cdPessoa
        public string dsNomeConvidado { get; set; } // dsNomeConvidado (length: 250)
        public string dsEmailConvidado { get; set; } // dsEmailConvidado (length: 250)
        public string dsCodigoBarras { get; set; } // dsCodigoBarras (length: 50)
        public string fgEfetivado { get; set; } // fgEfetivado (length: 1)
        public System.DateTime? dtEfetivado { get; set; } // dtEfetivado
    }

    // TB_DD_App_NoticiaAviso
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_App_NoticiaAviso
    {
        public int cdNoticiaAviso { get; set; } // cdNoticiaAviso (Primary key)
        public int TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public int cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public string dsTituloPortugues { get; set; } // dsTituloPortugues (length: 2000)
        public string dsNoticiaAvisoPortugues { get; set; } // dsNoticiaAvisoPortugues (length: 2000)
        public string dsTituloIngles { get; set; } // dsTituloIngles (length: 2000)
        public string dsNoticiaAvisoIngles { get; set; } // dsNoticiaAvisoIngles (length: 2000)
        public string dsTipo { get; set; } // dsTipo (length: 50)
        public string dsSituacao { get; set; } // dsSituacao (length: 15)
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao
    }

    // TB_DD_AprovacaoReprovacaoDeProjeto
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_AprovacaoReprovacaoDeProjeto
    {
        public int cdAprovacaoReprovacao { get; set; } // cdAprovacaoReprovacao (Primary key)
        public int TB_Empresa_cdEmpresa { get; set; } // TB_Empresa_cdEmpresa
        public int TB_Stand_cdStand { get; set; } // TB_Stand_cdStand
        public int TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public string dsDocumento { get; set; } // dsDocumento (length: 800)
        public string dsSituacao { get; set; } // dsSituacao (length: 50)
        public System.DateTime dtInclusao { get; set; } // dtInclusao
        public int cdUsuarioInclusao { get; set; } // cdUsuarioInclusao
        public System.DateTime? dtAprovacaoReprovacao { get; set; } // dtAprovacaoReprovacao
        public int? cdUsuarioAprovacaoReprovacao { get; set; } // cdUsuarioAprovacaoReprovacao
        public System.DateTime? dtAltercao { get; set; } // dtAltercao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao
    }

    // TB_DD_AssistentePreenchimento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_AssistentePreenchimento
    {
        public int cdAssistentePreenchimento { get; set; } // cdAssistentePreenchimento (Primary key)
        public int cdUsuarioInclusao { get; set; } // cdUsuarioInclusao
        public int cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public System.DateTime dtAlteracao { get; set; } // dtAlteracao
        public string dsNomeFormulario { get; set; } // dsNomeFormulario
        public int cdFormulario { get; set; } // cdFormulario
        public string dsTextoPortugues { get; set; } // dsTextoPortugues
        public string dsTextoIngles { get; set; } // dsTextoIngles
        public string fgHabilitado { get; set; } // fgHabilitado (length: 1)
        public int? cdSistema { get; set; } // cdSistema
        public string dsTextoEspanhol { get; set; } // dsTextoEspanhol
    }

    // TB_DD_ATOM
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ATOM
    {
        public int cdGeracao { get; set; } // cdGeracao (Primary key)
        public System.DateTime dtCriacao { get; set; } // dtCriacao
        public int UsuarioInsercao { get; set; } // UsuarioInsercao
        public string DataGeracaoArquivo { get; set; } // DataGeracaoArquivo (length: 500)
        public string DAT { get; set; } // DAT (length: 500)
        public string DEM { get; set; } // DEM (length: 500)
        public string TRA { get; set; } // TRA (length: 500)
        public int? cdEdicao { get; set; } // cdEdicao

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_ATOM].([cdEdicao]) (FK_DD_Atom_Edicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_DD_Atom_Edicao
    }

    // TB_DD_BloqueioContratoProduto
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_BloqueioContratoProduto
    {
        public int cdPermissao { get; set; } // cdPermissao (Primary key)
        public int? cdProduto { get; set; } // cdProduto
        public int? cdTipoContrato { get; set; } // cdTipoContrato
        public string dsSituacao { get; set; } // dsSituacao (length: 20)

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_ProdutoOperacional pointed by [TB_DD_BloqueioContratoProduto].([cdProduto]) (FK_TB_DD_BloqueioContratoProduto_cdProduto)
        /// </summary>
        public virtual TB_DD_ProdutoOperacional TB_DD_ProdutoOperacional { get; set; } // FK_TB_DD_BloqueioContratoProduto_cdProduto

        /// <summary>
        /// Parent TB_RF_TipoContrato pointed by [TB_DD_BloqueioContratoProduto].([cdTipoContrato]) (FK_TB_DD_BloqueioContratoProduto_cdTipoContrato)
        /// </summary>
        public virtual TB_RF_TipoContrato TB_RF_TipoContrato { get; set; } // FK_TB_DD_BloqueioContratoProduto_cdTipoContrato
    }

    // TB_DD_CarrinhoOperacional
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_CarrinhoOperacional
    {
        public int cdItem { get; set; } // cdItem (Primary key)
        public int? cdEmpresa { get; set; } // cdEmpresa
        public int? cdStand { get; set; } // cdStand
        public int? cdProduto { get; set; } // cdProduto
        public string QtdProduto { get; set; } // QtdProduto (length: 100)
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public int? cdPedido { get; set; } // cdPedido
        public int? cdFormulario { get; set; } // cdFormulario
        public int? cdEdicao { get; set; } // cdEdicao
        public int? cdPessoa { get; set; } // cdPessoa
        public string CodigoBarras { get; set; } // CodigoBarras (length: 30)
        public string dsDescricaoOpcional { get; set; } // dsDescricaoOpcional
        public int? cdEmpresaSegundario { get; set; } // cdEmpresaSegundario

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_CarrinhoOperacional_VendaDados where [TB_DD_CarrinhoOperacional_VendaDados].[cdCarrinhoOperacional] point to this entity (FK_DD_CarrinhoOperacional_VendaDados_DD_CarrinhoOperacional)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_CarrinhoOperacional_VendaDados> TB_DD_CarrinhoOperacional_VendaDados { get; set; } // TB_DD_CarrinhoOperacional_VendaDados.FK_DD_CarrinhoOperacional_VendaDados_DD_CarrinhoOperacional

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_CarrinhoOperacional].([cdEdicao]) (FK_TB_DD_CarrinhoOperacional_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_CarrinhoOperacional_cdEdicao

        /// <summary>
        /// Parent TB_CF_FormularioManualEletronico pointed by [TB_DD_CarrinhoOperacional].([cdFormulario]) (FK_TB_DD_CarrinhoOperacional_cdFormulario)
        /// </summary>
        public virtual TB_CF_FormularioManualEletronico TB_CF_FormularioManualEletronico { get; set; } // FK_TB_DD_CarrinhoOperacional_cdFormulario

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_CarrinhoOperacional].([cdEmpresa]) (FK_TB_DD_CarrinhoOperacional_cdEmpresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_DD_CarrinhoOperacional_cdEmpresa

        /// <summary>
        /// Parent TB_DD_ProdutoOperacional pointed by [TB_DD_CarrinhoOperacional].([cdProduto]) (FK_TB_DD_CarrinhoOperacional_TB_DD_ProdutoOperacional)
        /// </summary>
        public virtual TB_DD_ProdutoOperacional TB_DD_ProdutoOperacional { get; set; } // FK_TB_DD_CarrinhoOperacional_TB_DD_ProdutoOperacional

        /// <summary>
        /// Parent TB_DD_Stand pointed by [TB_DD_CarrinhoOperacional].([cdStand]) (FK_TB_DD_CarrinhoOperacional_cdStand)
        /// </summary>
        public virtual TB_DD_Stand TB_DD_Stand { get; set; } // FK_TB_DD_CarrinhoOperacional_cdStand

        public TB_DD_CarrinhoOperacional()
        {
            TB_DD_CarrinhoOperacional_VendaDados = new System.Collections.Generic.List<TB_DD_CarrinhoOperacional_VendaDados>();
        }
    }

    // TB_DD_CarrinhoOperacional_VendaDados
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_CarrinhoOperacional_VendaDados
    {
        public int cdCarrinhoOperacionalVendaDados { get; set; } // cdCarrinhoOperacionalVendaDados (Primary key)
        public int cdCarrinhoOperacional { get; set; } // cdCarrinhoOperacional
        public int cdVendaDados { get; set; } // cdVendaDados
        public string dsConteudo { get; set; } // dsConteudo (length: 800)
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public int cdUsuarioCadastro { get; set; } // cdUsuarioCadastro

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_VendaDados pointed by [TB_DD_CarrinhoOperacional_VendaDados].([cdVendaDados]) (FK_DD_CarrinhoOperacional_VendaDados_CF_VendaDados)
        /// </summary>
        public virtual TB_CF_VendaDados TB_CF_VendaDados { get; set; } // FK_DD_CarrinhoOperacional_VendaDados_CF_VendaDados

        /// <summary>
        /// Parent TB_DD_CarrinhoOperacional pointed by [TB_DD_CarrinhoOperacional_VendaDados].([cdCarrinhoOperacional]) (FK_DD_CarrinhoOperacional_VendaDados_DD_CarrinhoOperacional)
        /// </summary>
        public virtual TB_DD_CarrinhoOperacional TB_DD_CarrinhoOperacional { get; set; } // FK_DD_CarrinhoOperacional_VendaDados_DD_CarrinhoOperacional
    }

    // TB_DD_CatalogoOficial
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_CatalogoOficial
    {
        public int cdCatalogoOficial { get; set; } // cdCatalogoOficial (Primary key)
        public int cdEmpresa { get; set; } // cdEmpresa
        public int? cdStand { get; set; } // cdStand
        public int TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public int TB_Pais_cdPais { get; set; } // TB_Pais_cdPais
        public string dsNomeFantasia { get; set; } // dsNomeFantasia (length: 100)
        public string dsLogradouro { get; set; } // dsLogradouro (length: 100)
        public string dsComplementoLogradouro { get; set; } // dsComplementoLogradouro (length: 100)
        public string dsNumeroLogradouro { get; set; } // dsNumeroLogradouro (length: 100)
        public string dsCepLogradouro { get; set; } // dsCepLogradouro (length: 100)
        public string dsBairroLogradouro { get; set; } // dsBairroLogradouro (length: 100)
        public string dsCidadeLogradouro { get; set; } // dsCidadeLogradouro (length: 100)
        public string dsUfLogradouro { get; set; } // dsUfLogradouro (length: 100)
        public string dsEmail { get; set; } // dsEmail (length: 100)
        public string dsSite { get; set; } // dsSite (length: 100)
        public string dsSegmento { get; set; } // dsSegmento (length: 100)
        public string dsLocalizacaoPlanta { get; set; } // dsLocalizacaoPlanta (length: 100)
        public string dsAreaAtuacao { get; set; } // dsAreaAtuacao (length: 100)
        public string dsTexto { get; set; } // dsTexto (length: 300)
        public string dsIncialEmpresa { get; set; } // dsIncialEmpresa (length: 100)
        public string dsAtividadePor { get; set; } // dsAtividadePor
        public string dsAtividadeIng { get; set; } // dsAtividadeIng
        public string dsTelefone { get; set; } // dsTelefone (length: 100)
        public string dsFax { get; set; } // dsFax (length: 100)
        public string dsContato { get; set; } // dsContato (length: 100)
        public string dsCargo { get; set; } // dsCargo (length: 255)
        public string dsContato2 { get; set; } // dsContato2 (length: 255)
        public string dsCargo2 { get; set; } // dsCargo2 (length: 255)
        public string dsCargo3 { get; set; } // dsCargo3 (length: 255)
        public string dsContato3 { get; set; } // dsContato3 (length: 255)
        public string dsLetra { get; set; } // dsLetra (length: 2)
        public string dsAtividades { get; set; } // dsAtividades
        public string dsNumeroStandIndoor { get; set; } // dsNumeroStandIndoor
        public string dsNumeroStandOutdoor { get; set; } // dsNumeroStandOutdoor
        public string dsDDITelefone { get; set; } // dsDDITelefone
        public string dsDDDTelefone { get; set; } // dsDDDTelefone
        public string dsDDIFax { get; set; } // dsDDIFax
        public string dsDDDFax { get; set; } // dsDDDFax
        public string dsAtividadePrincipal { get; set; } // dsAtividadePrincipal
        public string dsAtividadeSecundaria { get; set; } // dsAtividadeSecundaria
        public string dsAtividadeTerciaria { get; set; } // dsAtividadeTerciaria
        public string dsResCatalogo1 { get; set; } // dsResCatalogo1
        public string dsResCatalogo2 { get; set; } // dsResCatalogo2 (length: 800)
        public string dsResCatalogo3 { get; set; } // dsResCatalogo3 (length: 800)
        public string dsResCatalogo4 { get; set; } // dsResCatalogo4 (length: 800)
        public string dsResCatalogo5 { get; set; } // dsResCatalogo5 (length: 800)
        public string dsResCatalogo6 { get; set; } // dsResCatalogo6 (length: 800)
        public string dsNumeroStand { get; set; } // dsNumeroStand
        public string dsResCatalogo7 { get; set; } // dsResCatalogo7

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_CatalogoOficial].([TB_Edicao_cdEdicao]) (FK_TB_DD_CatalogoOficial_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_CatalogoOficial_cdEdicao

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_CatalogoOficial].([cdEmpresa]) (FK_TB_DD_CatalogoOficial_cdEmpresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_DD_CatalogoOficial_cdEmpresa

        /// <summary>
        /// Parent TB_DD_Stand pointed by [TB_DD_CatalogoOficial].([cdStand]) (FK_TB_DD_CatalogoOficial_cdStand)
        /// </summary>
        public virtual TB_DD_Stand TB_DD_Stand { get; set; } // FK_TB_DD_CatalogoOficial_cdStand
    }

    // TB_DD_Cnab
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Cnab
    {
        public int cdLinha { get; set; } // cdLinha (Primary key)
        public int? NuSequenciaArquivo { get; set; } // NuSequenciaArquivo
        public int? NuLoteRetorno { get; set; } // NuLoteRetorno
        public int? NuAgencia { get; set; } // NuAgencia
        public int? NuConta { get; set; } // NuConta
        public int cdPedido { get; set; } // cdPedido
        public string vlPago { get; set; } // vlPago (length: 100)
        public System.DateTime? dtPagamento { get; set; } // dtPagamento
        public System.DateTime? dtProcessamentoArquivo { get; set; } // dtProcessamentoArquivo
        public int? cdUsuarioProcessamento { get; set; } // cdUsuarioProcessamento
        public string Chave { get; set; } // Chave

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_DD_Cnab].([cdPedido]) (FK_TB_DD_Cnab_TB_DD_Pedido)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido { get; set; } // FK_TB_DD_Cnab_TB_DD_Pedido
    }

    // TB_DD_CodigoPromocional
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_CodigoPromocional
    {
        public int cdPromocional { get; set; } // cdPromocional (Primary key)
        public string dsPalavra { get; set; } // dsPalavra (length: 100)
        public string dsPromocional { get; set; } // dsPromocional (length: 100)
        public string dsDesconto { get; set; } // dsDesconto (length: 10)
        public string dsTipo { get; set; } // dsTipo (length: 10)
        public string dsUso { get; set; } // dsUso (length: 10)
        public int? cdProduto { get; set; } // cdProduto
        public int? cdSistema { get; set; } // cdSistema
        public string fgSituacao { get; set; } // fgSituacao (length: 10)
        public string dsQuantidade { get; set; } // dsQuantidade (length: 10)

        // Reverse navigation

        /// <summary>
        /// Child TB_AS_CodigoPromocional_tb_Pedido where [TB_AS_CodigoPromocional_tb_Pedido].[cdPromocional] point to this entity (PK_TB_AS_CodigoPromocional_Pedido_CodigoPromocional)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_CodigoPromocional_tb_Pedido> TB_AS_CodigoPromocional_tb_Pedido { get; set; } // TB_AS_CodigoPromocional_tb_Pedido.PK_TB_AS_CodigoPromocional_Pedido_CodigoPromocional

        public TB_DD_CodigoPromocional()
        {
            TB_AS_CodigoPromocional_tb_Pedido = new System.Collections.Generic.List<TB_AS_CodigoPromocional_tb_Pedido>();
        }
    }

    // TB_DD_Codigos
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Codigos
    {
        public int ObjRef { get; set; } // ObjRef (Primary key)
        public string CodFicha { get; set; } // CodFicha
        public string USO { get; set; } // USO
        public string CodigoBarras { get; set; } // CodigoBarras
        public string Internacional { get; set; } // Internacional
        public string Cobrado { get; set; } // Cobrado (length: 1)
        public int? TB_Categoria_cdCategoria { get; set; } // TB_Categoria_cdCategoria
        public string TipoCodigo { get; set; } // TipoCodigo
        public string TipoConvite { get; set; } // TipoConvite
        public decimal? vlFicha { get; set; } // vlFicha
        public string flVendido { get; set; } // flVendido (length: 1)

        // Foreign keys

        /// <summary>
        /// Parent TB_RF_Categoria pointed by [TB_DD_Codigos].([TB_Categoria_cdCategoria]) (FK_TB_DD_Codigos_TB_RF_Categoria_cdCategoria)
        /// </summary>
        public virtual TB_RF_Categoria TB_RF_Categoria { get; set; } // FK_TB_DD_Codigos_TB_RF_Categoria_cdCategoria
    }

    // TB_DD_Coletor
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Coletor
    {
        public string dsColetor { get; set; } // dsColetor (length: 100)
        public string TB_Registro_dsCodigoBarras { get; set; } // TB_Registro_dsCodigoBarras (length: 100)
        public System.DateTime? dtEntrada { get; set; } // dtEntrada
        public System.DateTime? dtSaida { get; set; } // dtSaida
        public int? cdEdicao { get; set; } // cdEdicao
        public string dsAcesso { get; set; } // dsAcesso
        public System.Guid cdColetor { get; set; } // cdColetor (Primary key)
        public string dsSiglaProduto { get; set; } // dsSiglaProduto (length: 800)
        public string dsCodigoCompleto { get; set; } // dsCodigoCompleto (length: 100)
        public string dsTemAcompanhante { get; set; } // dsTemAcompanhante (length: 100)
        public System.Guid? cdColetorSalaPalestra { get; set; } // cdColetorSalaPalestra
        public System.Guid? cdColetorSalaPalestraVoucher { get; set; } // cdColetorSalaPalestraVoucher

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_Coletor].([cdEdicao]) (FK_TB_DD_Coletor_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_Coletor_cdEdicao

        public TB_DD_Coletor()
        {
            cdColetor = System.Guid.NewGuid();
        }
    }

    // TB_DD_Comunicados
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Comunicados
    {
        public int cdComunicado { get; set; } // cdComunicado (Primary key)
        public int? cdEdicao { get; set; } // cdEdicao
        public System.DateTime? DataInclusao { get; set; } // DataInclusao
        public int? cdUsuarioInclusao { get; set; } // cdUsuarioInclusao
        public string dsAssunto { get; set; } // dsAssunto (length: 100)
        public string dsMensagem { get; set; } // dsMensagem
        public string dsStatus { get; set; } // dsStatus (length: 10)
        public string dsAssuntoIng { get; set; } // dsAssuntoIng (length: 100)
        public string dsMensagemIng { get; set; } // dsMensagemIng

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_Comunicados].([cdEdicao]) (FK_TB_DD_Comunicados_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_Comunicados_cdEdicao
    }

    // TB_DD_ConfExtracaoPersonalizada
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ConfExtracaoPersonalizada
    {
        public int cdConfExtracaoPersonalizada { get; set; } // cdConfExtracaoPersonalizada (Primary key)
        public int cdExtracaoPersonalizada { get; set; } // cdExtracaoPersonalizada
        public string dsTipo { get; set; } // dsTipo (length: 500)
        public string dsValor { get; set; } // dsValor (length: 500)

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_ExtracaoPersonalizada pointed by [TB_DD_ConfExtracaoPersonalizada].([cdExtracaoPersonalizada]) (FK_ConfExtracaoPersonalizada)
        /// </summary>
        public virtual TB_DD_ExtracaoPersonalizada TB_DD_ExtracaoPersonalizada { get; set; } // FK_ConfExtracaoPersonalizada
    }

    // TB_DD_ContratoExpositor
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ContratoExpositor
    {
        public int cdContrato { get; set; } // cdContrato (Primary key)
        public int TB_Empresa_cdEmpresa { get; set; } // TB_Empresa_cdEmpresa
        public int TB_Stand_cdStand { get; set; } // TB_Stand_cdStand
        public int TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public string dsNomeExpositorContrato { get; set; } // dsNomeExpositorContrato (length: 200)
        public string dsRgExpositorContrato { get; set; } // dsRgExpositorContrato (length: 100)
        public string dsCpfExpositorContrato { get; set; } // dsCpfExpositorContrato (length: 100)
        public string dsNomeTestemunhaContrato { get; set; } // dsNomeTestemunhaContrato (length: 200)
        public string dsRgTestemunhaContrato { get; set; } // dsRgTestemunhaContrato (length: 100)
        public string dsCpfTestemunhaContrato { get; set; } // dsCpfTestemunhaContrato (length: 100)
        public string dsNomeFuncionarioPrimario { get; set; } // dsNomeFuncionarioPrimario (length: 200)
        public string dsCpfFuncionarioPrimario { get; set; } // dsCpfFuncionarioPrimario (length: 100)
        public string dsCargoFuncionarioPrimario { get; set; } // dsCargoFuncionarioPrimario (length: 200)
        public string dsEmailFuncionarioPrimario { get; set; } // dsEmailFuncionarioPrimario (length: 200)
        public string dsTelefoneFuncionarioPrimario { get; set; } // dsTelefoneFuncionarioPrimario (length: 100)
        public string dsNomeFuncionarioSecundario { get; set; } // dsNomeFuncionarioSecundario (length: 200)
        public string dsCpfFuncionarioSecundario { get; set; } // dsCpfFuncionarioSecundario (length: 100)
        public string dsCargoFuncionarioSecundario { get; set; } // dsCargoFuncionarioSecundario (length: 200)
        public string dsEmailFuncionarioSecundario { get; set; } // dsEmailFuncionarioSecundario (length: 200)
        public string dsTelefoneFuncionarioSecundario { get; set; } // dsTelefoneFuncionarioSecundario (length: 100)
        public int? cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public System.DateTime? dtCadastro { get; set; } // dtCadastro
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_ContratoExpositor].([TB_Edicao_cdEdicao]) (FK_ContratoExpositor_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_ContratoExpositor_cdEdicao

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_ContratoExpositor].([TB_Empresa_cdEmpresa]) (FK_ContratoExpositor_cdEmpresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_ContratoExpositor_cdEmpresa

        /// <summary>
        /// Parent TB_DD_Stand pointed by [TB_DD_ContratoExpositor].([TB_Stand_cdStand]) (FK_ContratoExpositor_cdStand)
        /// </summary>
        public virtual TB_DD_Stand TB_DD_Stand { get; set; } // FK_ContratoExpositor_cdStand
    }

    // TB_DD_Convite
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Convite
    {
        public int cdConvite { get; set; } // cdConvite (Primary key)
        public string dsNome { get; set; } // dsNome (length: 255)
        public string dsEmailConvidado { get; set; } // dsEmailConvidado (length: 255)
        public string nmEvento { get; set; } // nmEvento (length: 255)
        public System.DateTime dtConvite { get; set; } // dtConvite
        public int? cdPessoa { get; set; } // cdPessoa
        public string dsEmail { get; set; } // dsEmail (length: 255)

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Pessoa pointed by [TB_DD_Convite].([cdPessoa]) (FK_TB_DD_Convite_TB_DD_Pessoa)
        /// </summary>
        public virtual TB_DD_Pessoa TB_DD_Pessoa { get; set; } // FK_TB_DD_Convite_TB_DD_Pessoa
    }

    // TB_DD_ConviteEletronico
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ConviteEletronico
    {
        public int ObjRef { get; set; } // ObjRef (Primary key)
        public int cdEdicao { get; set; } // cdEdicao
        public int? IdEmpresa { get; set; } // IdEmpresa
        public string NomeConvidado { get; set; } // NomeConvidado (length: 60)
        public string EmailConvidado { get; set; } // EmailConvidado (length: 100)
        public string Efetivado { get; set; } // Efetivado (length: 1)
        public System.DateTime? DataInclusao { get; set; } // DataInclusao
        public System.DateTime? DataEfetivacao { get; set; } // DataEfetivacao
        public int? IdEmpresaEfetivada { get; set; } // IdEmpresaEfetivada
        public string CodigoBarras { get; set; } // CodigoBarras (length: 20)
        public int? IdTransacao { get; set; } // IdTransacao
        public string Empresa { get; set; } // Empresa
        public string Evento { get; set; } // Evento
        public string fgConviteEnviado { get; set; } // fgConviteEnviado (length: 1)

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_ConviteEletronico].([cdEdicao]) (FK_TB_DD_ConviteEletronico_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_ConviteEletronico_cdEdicao

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_ConviteEletronico].([IdEmpresa]) (FK_TB_DD_ConviteEletronico_IdEmpresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_DD_ConviteEletronico_IdEmpresa
    }

    // TB_DD_ConviteEletronicoCancelado
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ConviteEletronicoCancelado
    {
        public System.Guid cdConviteCancelado { get; set; } // cdConviteCancelado (Primary key)
        public System.DateTime dtCancelamento { get; set; } // dtCancelamento
        public int cdUsuarioCancelamento { get; set; } // cdUsuarioCancelamento
        public int ObjRef { get; set; } // ObjRef
        public int cdEdicao { get; set; } // cdEdicao
        public int IdEmpresa { get; set; } // IdEmpresa
        public string NomeConvidado { get; set; } // NomeConvidado (length: 60)
        public string EmailConvidado { get; set; } // EmailConvidado (length: 100)
        public string Efetivado { get; set; } // Efetivado (length: 1)
        public System.DateTime? DataInclusao { get; set; } // DataInclusao
        public System.DateTime? DataEfetivacao { get; set; } // DataEfetivacao
        public int? IdEmpresaEfetivada { get; set; } // IdEmpresaEfetivada
        public string CodigoBarras { get; set; } // CodigoBarras (length: 20)
        public int? IdTransacao { get; set; } // IdTransacao
        public string Empresa { get; set; } // Empresa (length: 800)
        public string Evento { get; set; } // Evento (length: 800)
        public string fgConviteEnviado { get; set; } // fgConviteEnviado (length: 1)

        public TB_DD_ConviteEletronicoCancelado()
        {
            cdConviteCancelado = System.Guid.NewGuid();
            dtCancelamento = System.DateTime.Now;
        }
    }

    // TB_DD_ConviteEletronicoVip
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ConviteEletronicoVip
    {
        public int ObjRef { get; set; } // ObjRef (Primary key)
        public int cdEdicao { get; set; } // cdEdicao
        public int? IdEmpresa { get; set; } // IdEmpresa
        public string NomeConvidado { get; set; } // NomeConvidado
        public string EmailConvidado { get; set; } // EmailConvidado
        public string Efetivado { get; set; } // Efetivado (length: 1)
        public System.DateTime? DataInclusao { get; set; } // DataInclusao
        public System.DateTime? DataEfetivacao { get; set; } // DataEfetivacao
        public int? IdEmpresaEfetivada { get; set; } // IdEmpresaEfetivada
        public string CodigoBarras { get; set; } // CodigoBarras (length: 20)
        public int? IdTransacao { get; set; } // IdTransacao
        public string Chave { get; set; } // Chave
        public int? cdPessoa { get; set; } // cdPessoa
        public string Enviado { get; set; } // Enviado (length: 100)
        public string Empresa { get; set; } // Empresa
        public string Evento { get; set; } // Evento
        public string TipoConvite { get; set; } // TipoConvite

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_ConviteEletronicoVip].([cdEdicao]) (FK_TB_DD_ConviteEletronicoVip_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_ConviteEletronicoVip_cdEdicao

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_ConviteEletronicoVip].([IdEmpresa]) (FK_TB_DD_ConviteEletronicoVip_IdEmpresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_DD_ConviteEletronicoVip_IdEmpresa
    }

    // TB_DD_ConvitesImpressos
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ConvitesImpressos
    {
        public int cdConvite { get; set; } // cdConvite (Primary key)
        public int TB_Stand_cdStand { get; set; } // TB_Stand_cdStand
        public string dsQtdPort { get; set; } // dsQtdPort
        public string dsQtdIng { get; set; } // dsQtdIng
        public string dsQtdEsp { get; set; } // dsQtdEsp
        public string dsNomeContato { get; set; } // dsNomeContato
        public string dsLogradouro { get; set; } // dsLogradouro
        public string dsNumero { get; set; } // dsNumero
        public string dsCep { get; set; } // dsCep
        public string dsCidade { get; set; } // dsCidade
        public string dsEstado { get; set; } // dsEstado
        public string dsTelefone { get; set; } // dsTelefone
        public string dsFax { get; set; } // dsFax
        public string dsEmail { get; set; } // dsEmail

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Stand pointed by [TB_DD_ConvitesImpressos].([TB_Stand_cdStand]) (FK_TB_DD_ConvitesImpressos_TB_DD_Stand)
        /// </summary>
        public virtual TB_DD_Stand TB_DD_Stand { get; set; } // FK_TB_DD_ConvitesImpressos_TB_DD_Stand
    }

    // TB_DD_CorCarpeteColuna
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_CorCarpeteColuna
    {
        public int cdPreenchimento { get; set; } // cdPreenchimento (Primary key)
        public int? cdEdicao { get; set; } // cdEdicao
        public int? cdEmpresa { get; set; } // cdEmpresa
        public int? cdStand { get; set; } // cdStand
        public System.DateTime? DataInclusao { get; set; } // DataInclusao
        public int? UsuarioInsercao { get; set; } // UsuarioInsercao
        public System.DateTime? DataAlteracao { get; set; } // DataAlteracao
        public int? UsuarioAlteracao { get; set; } // UsuarioAlteracao
        public string TipoMontagem { get; set; } // TipoMontagem (length: 10)
        public int? cdCorCarpete { get; set; } // cdCorCarpete
        public int? cdCorColuna { get; set; } // cdCorColuna
        public string dsSituacao { get; set; } // dsSituacao (length: 100)

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_CorCarpeteColuna].([cdEdicao]) (FK_TB_DD_CorCarpeteColuna_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_CorCarpeteColuna_cdEdicao

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_CorCarpeteColuna].([cdEmpresa]) (FK_TB_DD_CorCarpeteColuna_cdEmpresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_DD_CorCarpeteColuna_cdEmpresa

        /// <summary>
        /// Parent TB_DD_ResponsavelEmpresa pointed by [TB_DD_CorCarpeteColuna].([UsuarioAlteracao]) (FK_TB_DD_CorCarpeteColuna_UsuarioAlteracao)
        /// </summary>
        public virtual TB_DD_ResponsavelEmpresa TB_DD_ResponsavelEmpresa_UsuarioAlteracao { get; set; } // FK_TB_DD_CorCarpeteColuna_UsuarioAlteracao

        /// <summary>
        /// Parent TB_DD_ResponsavelEmpresa pointed by [TB_DD_CorCarpeteColuna].([UsuarioInsercao]) (FK_TB_DD_CorCarpeteColuna_UsuarioInsercao)
        /// </summary>
        public virtual TB_DD_ResponsavelEmpresa TB_DD_ResponsavelEmpresa_UsuarioInsercao { get; set; } // FK_TB_DD_CorCarpeteColuna_UsuarioInsercao

        /// <summary>
        /// Parent TB_DD_Stand pointed by [TB_DD_CorCarpeteColuna].([cdStand]) (FK_TB_DD_CorCarpeteColuna_cdStand)
        /// </summary>
        public virtual TB_DD_Stand TB_DD_Stand { get; set; } // FK_TB_DD_CorCarpeteColuna_cdStand
    }

    // TB_DD_Credencial
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Credencial
    {
        public int cdCredencial { get; set; } // cdCredencial (Primary key)
        public string TB_Registro_dsCodigoBarras { get; set; } // TB_Registro_dsCodigoBarras (length: 100)
        public int TB_Pedido_cdPedido { get; set; } // TB_Pedido_cdPedido
        public int? TB_Empresa_CdEmpresaPrimario { get; set; } // TB_Empresa_CdEmpresaPrimario
        public int? TB_Empresa_CdEmpresaSegundario { get; set; } // TB_Empresa_CdEmpresaSegundario
        public int? cdStand { get; set; } // cdStand

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_Credencial].([TB_Empresa_CdEmpresaPrimario]) (FK_TB_DD_Credencial_TB_DD_Empresa_CdEmpresaPrimario)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa_TB_Empresa_CdEmpresaPrimario { get; set; } // FK_TB_DD_Credencial_TB_DD_Empresa_CdEmpresaPrimario

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_Credencial].([TB_Empresa_CdEmpresaPrimario]) (FK_TB_DD_Credencial_TB_DD_Empresa_CdEmpresaSegundario)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa1 { get; set; } // FK_TB_DD_Credencial_TB_DD_Empresa_CdEmpresaSegundario

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_DD_Credencial].([TB_Pedido_cdPedido]) (FK_TB_DD_Credencial_TB_Pedido_cdPedido)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido { get; set; } // FK_TB_DD_Credencial_TB_Pedido_cdPedido

        /// <summary>
        /// Parent TB_DD_Stand pointed by [TB_DD_Credencial].([cdStand]) (FK_TB_DD_Credencial_cdStand)
        /// </summary>
        public virtual TB_DD_Stand TB_DD_Stand { get; set; } // FK_TB_DD_Credencial_cdStand
    }

    // TB_DD_DestinatarioComunicado
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_DestinatarioComunicado
    {
        public int ObjRef { get; set; } // ObjRef (Primary key)
        public int cdDestinatarioComunicado { get; set; } // cdDestinatarioComunicado
        public int? cdComunicado { get; set; } // cdComunicado
        public int? cdEmpresa { get; set; } // cdEmpresa
        public string Lida { get; set; } // Lida (length: 10)
        public System.DateTime? DataLeitura { get; set; } // DataLeitura
        public int? cdUsuarioLeitura { get; set; } // cdUsuarioLeitura

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_ResponsavelEmpresa pointed by [TB_DD_DestinatarioComunicado].([cdUsuarioLeitura]) (FK_TB_DD_DestinatarioComunicado_cdUsuarioLeitura)
        /// </summary>
        public virtual TB_DD_ResponsavelEmpresa TB_DD_ResponsavelEmpresa { get; set; } // FK_TB_DD_DestinatarioComunicado_cdUsuarioLeitura
    }

    // TB_DD_EletricaBasica
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_EletricaBasica
    {
        public int cdEletricaBasica { get; set; } // cdEletricaBasica (Primary key)
        public string qtEletricaBasica { get; set; } // qtEletricaBasica (length: 100)
        public string qtEletricaMinima { get; set; } // qtEletricaMinima (length: 100)
        public int? cdEdicao { get; set; } // cdEdicao

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_EletricaBasica].([cdEdicao]) (FK_TB_DD_EletricaBasica_cdStand)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_EletricaBasica_cdStand
    }

    // TB_DD_EmailMarketingImportados
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_EmailMarketingImportados
    {
        public int cdEmailMarketingImportado { get; set; } // cdEmailMarketingImportado (Primary key)
        public int cdEmailMarketing { get; set; } // cdEmailMarketing
        public string dsEmail { get; set; } // dsEmail (length: 500)
        public string dsNome { get; set; } // dsNome (length: 500)
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public int cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public System.DateTime? dtProcessado { get; set; } // dtProcessado
        public string fgProcessado { get; set; } // fgProcessado (length: 1)
        public int? cdDialogue { get; set; } // cdDialogue
        public string dsOrganizacao { get; set; } // dsOrganizacao (length: 800)
        public string dsContato { get; set; } // dsContato (length: 800)
        public string dsEmailContato { get; set; } // dsEmailContato (length: 800)
        public string dsCodigoBarras { get; set; } // dsCodigoBarras (length: 100)

        public TB_DD_EmailMarketingImportados()
        {
            fgProcessado = "N";
        }
    }

    // TB_DD_EmissaoCredencial
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_EmissaoCredencial
    {
        public int cdEmissao { get; set; } // cdEmissao (Primary key)
        public string TB_Registro_dsCodigoBarras { get; set; } // TB_Registro_dsCodigoBarras (length: 100)
        public System.DateTime? dtDataEmissao { get; set; } // dtDataEmissao
        public string dsOrigemImpressao { get; set; } // dsOrigemImpressao (length: 800)

    }

    // TB_DD_Empresa
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Empresa
    {
        public int cdEmpresa { get; set; } // cdEmpresa (Primary key)
        public int TB_TipoEmpresa_cdTipo { get; set; } // TB_TipoEmpresa_cdTipo
        public int? cdTipoDocumento { get; set; } // cdTipoDocumento
        public string dsCnpj { get; set; } // dsCnpj (length: 100)
        public string dsInscricaoEstadual { get; set; } // dsInscricaoEstadual (length: 100)
        public string dsInscricaoMunicipal { get; set; } // dsInscricaoMunicipal (length: 100)
        public string dsRazaoSocial { get; set; } // dsRazaoSocial
        public string dsNomeFantasia { get; set; } // dsNomeFantasia
        public string dsEmpresaCracha { get; set; } // dsEmpresaCracha
        public string dsLogradouroComercial { get; set; } // dsLogradouroComercial (length: 100)
        public string dsComplementoLogradouroComercial { get; set; } // dsComplementoLogradouroComercial (length: 100)
        public string dsNumeroLogradouroComercial { get; set; } // dsNumeroLogradouroComercial (length: 100)
        public string dsCepLogradouroComercial { get; set; } // dsCepLogradouroComercial (length: 100)
        public string dsBairroLogradouroComercial { get; set; } // dsBairroLogradouroComercial (length: 100)
        public string dsCidadeLogradouroComercial { get; set; } // dsCidadeLogradouroComercial (length: 100)
        public string dsUfLogradouroComercial { get; set; } // dsUfLogradouroComercial (length: 100)
        public int TB_Pais_cdPaisComercial { get; set; } // TB_Pais_cdPaisComercial
        public string dsSite { get; set; } // dsSite (length: 100)
        public string dsDDITelefoneEmpresa1 { get; set; } // dsDDITelefoneEmpresa1
        public string dsDDDTelefoneEmpresa1 { get; set; } // dsDDDTelefoneEmpresa1
        public string dsNumeroTelefoneEmpresa1 { get; set; } // dsNumeroTelefoneEmpresa1
        public string dsRamalTelefoneEmpresa1 { get; set; } // dsRamalTelefoneEmpresa1
        public string dsDDITelefoneEmpresa2 { get; set; } // dsDDITelefoneEmpresa2
        public string dsDDDTelefoneEmpresa2 { get; set; } // dsDDDTelefoneEmpresa2
        public string dsNumeroTelefoneEmpresa2 { get; set; } // dsNumeroTelefoneEmpresa2
        public string dsRamalTelefoneEmpresa2 { get; set; } // dsRamalTelefoneEmpresa2
        public string dsDDIFaxEmpresa { get; set; } // dsDDIFaxEmpresa
        public string dsDDDFaxEmpresa { get; set; } // dsDDDFaxEmpresa
        public string dsNumeroFaxEmpresa { get; set; } // dsNumeroFaxEmpresa
        public string dsRamoAtividade { get; set; } // dsRamoAtividade (length: 100)
        public string dsAreaAtuacao { get; set; } // dsAreaAtuacao (length: 100)
        public string dsFaturamento { get; set; } // dsFaturamento (length: 100)
        public string EnEmail { get; set; } // EnEmail (length: 100)
        public string SINDIPROM { get; set; } // SINDIPROM (length: 100)
        public string NRegistroPoliciaFederal { get; set; } // NRegistroPoliciaFederal (length: 30)
        public string dsVeiculo { get; set; } // dsVeiculo
        public string dsEditoria { get; set; } // dsEditoria
        public string dsTwitter { get; set; } // dsTwitter
        public string dsFacebook { get; set; } // dsFacebook
        public string dsObservacao { get; set; } // dsObservacao
        public string dsIdioma { get; set; } // dsIdioma (length: 10)
        public string dsResPessoa1 { get; set; } // dsResPessoa1 (length: 100)
        public string dsResPessoa2 { get; set; } // dsResPessoa2 (length: 100)
        public string dsResPessoa3 { get; set; } // dsResPessoa3 (length: 100)
        public string dsResPessoa4 { get; set; } // dsResPessoa4 (length: 100)
        public string dsResPessoa5 { get; set; } // dsResPessoa5 (length: 100)
        public string dsResPessoa6 { get; set; } // dsResPessoa6 (length: 100)
        public string dsResPessoa7 { get; set; } // dsResPessoa7 (length: 100)
        public string dsResPessoa8 { get; set; } // dsResPessoa8 (length: 100)
        public string dsResPessoa9 { get; set; } // dsResPessoa9 (length: 100)
        public string dsResPessoa10 { get; set; } // dsResPessoa10 (length: 100)
        public int? cdCnaeSebrae { get; set; } // cdCnaeSebrae
        public string dsCEPEmpresaSebrae { get; set; } // dsCEPEmpresaSebrae (length: 100)
        public string dsLogradouroEmpresaSebrae { get; set; } // dsLogradouroEmpresaSebrae (length: 800)
        public string dsComplementoEmpresaSebrae { get; set; } // dsComplementoEmpresaSebrae (length: 800)
        public int? cdPaisEmpresaSebrae { get; set; } // cdPaisEmpresaSebrae
        public int? cdEstadoEmpresaSebrae { get; set; } // cdEstadoEmpresaSebrae
        public int? cdCidadeEmpresaSebrae { get; set; } // cdCidadeEmpresaSebrae
        public int? cdBairroEmpresaSebrae { get; set; } // cdBairroEmpresaSebrae
        public System.DateTime? dtAberturaSebrae { get; set; } // dtAberturaSebrae
        public string CodConstSebrae { get; set; } // CodConstSebrae (length: 800)
        public string CodProdutoRuralSebrae { get; set; } // CodProdutoRuralSebrae (length: 800)
        public string CodDapSebrae { get; set; } // CodDapSebrae (length: 800)
        public string CodPescadorSebrae { get; set; } // CodPescadorSebrae (length: 800)
        public string dsNumeroLogradouroEmpresaSebrae { get; set; } // dsNumeroLogradouroEmpresaSebrae (length: 100)
        public string dsRegimeTributacao { get; set; } // dsRegimeTributacao (length: 500)
        public int? cdEmpresaOriginal { get; set; } // cdEmpresaOriginal
        public string dsResEmpresa01 { get; set; } // dsResEmpresa01 (length: 800)
        public string ACCOUNT_ID_CRM { get; set; } // ACCOUNT_ID_CRM (length: 800)
        public string CodigoClienteAPAS { get; set; } // CodigoClienteAPAS (length: 800)
        public string dsRegionalDistrital { get; set; } // dsRegionalDistrital (length: 800)
        public int? cdCoIrmas { get; set; } // cdCoIrmas
        public string dsSenha { get; set; } // dsSenha (length: 800)
        public string nmResponsavel { get; set; } // nmResponsavel (length: 800)
        public string dsEmailResponsavel { get; set; } // dsEmailResponsavel (length: 800)
        public string dsPerfil { get; set; } // dsPerfil (length: 800)
        public string dsCodigoCNAE { get; set; } // dsCodigoCNAE (length: 800)
        public string dsResEmpresa1 { get; set; } // dsResEmpresa1 (length: 800)
        public string dsResEmpresa2 { get; set; } // dsResEmpresa2 (length: 800)
        public string dsResEmpresa3 { get; set; } // dsResEmpresa3 (length: 800)
        public string dsResEmpresa4 { get; set; } // dsResEmpresa4 (length: 800)
        public string dsResEmpresa5 { get; set; } // dsResEmpresa5 (length: 800)
        public string dsResEmpresa6 { get; set; } // dsResEmpresa6 (length: 800)
        public string dsResEmpresa7 { get; set; } // dsResEmpresa7 (length: 800)
        public string dsResEmpresa8 { get; set; } // dsResEmpresa8 (length: 800)
        public string dsResEmpresa9 { get; set; } // dsResEmpresa9 (length: 800)
        public string dsResEmpresa10 { get; set; } // dsResEmpresa10 (length: 800)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_CarrinhoOperacional where [TB_DD_CarrinhoOperacional].[cdEmpresa] point to this entity (FK_TB_DD_CarrinhoOperacional_cdEmpresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_CarrinhoOperacional> TB_DD_CarrinhoOperacional { get; set; } // TB_DD_CarrinhoOperacional.FK_TB_DD_CarrinhoOperacional_cdEmpresa
        /// <summary>
        /// Child TB_DD_CatalogoOficial where [TB_DD_CatalogoOficial].[cdEmpresa] point to this entity (FK_TB_DD_CatalogoOficial_cdEmpresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_CatalogoOficial> TB_DD_CatalogoOficial { get; set; } // TB_DD_CatalogoOficial.FK_TB_DD_CatalogoOficial_cdEmpresa
        /// <summary>
        /// Child TB_DD_ContratoExpositor where [TB_DD_ContratoExpositor].[TB_Empresa_cdEmpresa] point to this entity (FK_ContratoExpositor_cdEmpresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ContratoExpositor> TB_DD_ContratoExpositor { get; set; } // TB_DD_ContratoExpositor.FK_ContratoExpositor_cdEmpresa
        /// <summary>
        /// Child TB_DD_ConviteEletronico where [TB_DD_ConviteEletronico].[IdEmpresa] point to this entity (FK_TB_DD_ConviteEletronico_IdEmpresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ConviteEletronico> TB_DD_ConviteEletronico { get; set; } // TB_DD_ConviteEletronico.FK_TB_DD_ConviteEletronico_IdEmpresa
        /// <summary>
        /// Child TB_DD_ConviteEletronicoVip where [TB_DD_ConviteEletronicoVip].[IdEmpresa] point to this entity (FK_TB_DD_ConviteEletronicoVip_IdEmpresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ConviteEletronicoVip> TB_DD_ConviteEletronicoVip { get; set; } // TB_DD_ConviteEletronicoVip.FK_TB_DD_ConviteEletronicoVip_IdEmpresa
        /// <summary>
        /// Child TB_DD_CorCarpeteColuna where [TB_DD_CorCarpeteColuna].[cdEmpresa] point to this entity (FK_TB_DD_CorCarpeteColuna_cdEmpresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_CorCarpeteColuna> TB_DD_CorCarpeteColuna { get; set; } // TB_DD_CorCarpeteColuna.FK_TB_DD_CorCarpeteColuna_cdEmpresa
        /// <summary>
        /// Child TB_DD_Credencial where [TB_DD_Credencial].[TB_Empresa_CdEmpresaPrimario] point to this entity (FK_TB_DD_Credencial_TB_DD_Empresa_CdEmpresaSegundario)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Credencial> TB_DD_Credencial_TB_Empresa_CdEmpresaPrimario { get; set; } // TB_DD_Credencial.FK_TB_DD_Credencial_TB_DD_Empresa_CdEmpresaSegundario
        /// <summary>
        /// Child TB_DD_Credencial where [TB_DD_Credencial].[TB_Empresa_CdEmpresaPrimario] point to this entity (FK_TB_DD_Credencial_TB_DD_Empresa_CdEmpresaPrimario)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Credencial> TB_Empresa_CdEmpresaPrimario { get; set; } // TB_DD_Credencial.FK_TB_DD_Credencial_TB_DD_Empresa_CdEmpresaPrimario
        /// <summary>
        /// Child TB_DD_EntregaCredencialVIP where [TB_DD_EntregaCredencialVIP].[TB_Empresa_cdEmpresa] point to this entity (FK_TB_DD_EntregaCredencialVIP_Empresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_EntregaCredencialVIP> TB_DD_EntregaCredencialVIP { get; set; } // TB_DD_EntregaCredencialVIP.FK_TB_DD_EntregaCredencialVIP_Empresa
        /// <summary>
        /// Child TB_DD_HelpDesk where [TB_DD_HelpDesk].[cdEmpresa] point to this entity (FK_TB_DD_HelpDesk_cdEmpresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_HelpDesk> TB_DD_HelpDesk { get; set; } // TB_DD_HelpDesk.FK_TB_DD_HelpDesk_cdEmpresa
        /// <summary>
        /// Child TB_DD_LoteImpressao where [TB_DD_LoteImpressao].[cdEmpresa] point to this entity (FK_TB_DD_LoteImpressao_TB_DD_Empresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_LoteImpressao> TB_DD_LoteImpressao { get; set; } // TB_DD_LoteImpressao.FK_TB_DD_LoteImpressao_TB_DD_Empresa
        /// <summary>
        /// Child TB_DD_Movimentacao where [TB_DD_Movimentacao].[TB_Empresa_cdEmpresaPrimario] point to this entity (FK_TB_DD_Movimentacao_TB_DD_Empresa_cdEmpresaPrimario)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Movimentacao> TB_Empresa_cdEmpresaPrimario { get; set; } // TB_DD_Movimentacao.FK_TB_DD_Movimentacao_TB_DD_Empresa_cdEmpresaPrimario
        /// <summary>
        /// Child TB_DD_Movimentacao where [TB_DD_Movimentacao].[TB_Empresa_cdEmpresaSecundario] point to this entity (FK_TB_DD_Movimentacao_TB_DD_Empresa_cdEmpresaSecundario)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Movimentacao> TB_Empresa_cdEmpresaSecundario { get; set; } // TB_DD_Movimentacao.FK_TB_DD_Movimentacao_TB_DD_Empresa_cdEmpresaSecundario
        /// <summary>
        /// Child TB_DD_Pedido where [TB_DD_Pedido].[TB_Empresa_cdEmpresa] point to this entity (FK_TB_DD_Pedido_TB_DD_Empresa_cdEmpresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Pedido> TB_DD_Pedido { get; set; } // TB_DD_Pedido.FK_TB_DD_Pedido_TB_DD_Empresa_cdEmpresa
        /// <summary>
        /// Child TB_DD_PlacaIdentificacao where [TB_DD_PlacaIdentificacao].[cdEmpresa] point to this entity (FK_TB_DD_PlacaIdentificacao_TB_DD_Empresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_PlacaIdentificacao> TB_DD_PlacaIdentificacao { get; set; } // TB_DD_PlacaIdentificacao.FK_TB_DD_PlacaIdentificacao_TB_DD_Empresa
        /// <summary>
        /// Child TB_DD_PlacaIdentificacaoColuna where [TB_DD_PlacaIdentificacaoColuna].[cdEmpresa] point to this entity (FK_TB_DD_PlacaIdentificacaoColuna_TB_DD_Empresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_PlacaIdentificacaoColuna> TB_DD_PlacaIdentificacaoColuna { get; set; } // TB_DD_PlacaIdentificacaoColuna.FK_TB_DD_PlacaIdentificacaoColuna_TB_DD_Empresa
        /// <summary>
        /// Child TB_DD_ReenvioConvite where [TB_DD_ReenvioConvite].[IdEmpresa] point to this entity (FK_TB_DD_ReenvioConvite_TB_DD_Empresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ReenvioConvite> TB_DD_ReenvioConvite { get; set; } // TB_DD_ReenvioConvite.FK_TB_DD_ReenvioConvite_TB_DD_Empresa
        /// <summary>
        /// Child TB_DD_ReenvioConviteVip where [TB_DD_ReenvioConviteVip].[IdEmpresa] point to this entity (FK_TB_DD_ReenvioConviteVip_TB_DD_Empresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ReenvioConviteVip> TB_DD_ReenvioConviteVip { get; set; } // TB_DD_ReenvioConviteVip.FK_TB_DD_ReenvioConviteVip_TB_DD_Empresa
        /// <summary>
        /// Child TB_DD_Registro where [TB_DD_Registro].[TB_Empresa_cdEmpresa] point to this entity (FK_TB_DD_Registro_TB_DD_Empresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Registro> TB_DD_Registro { get; set; } // TB_DD_Registro.FK_TB_DD_Registro_TB_DD_Empresa
        /// <summary>
        /// Child TB_DD_Stand where [TB_DD_Stand].[TB_Empresa_cdEmpresa] point to this entity (FK_TB_DD_Stand_TB_DD_Empresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Stand> TB_DD_Stand { get; set; } // TB_DD_Stand.FK_TB_DD_Stand_TB_DD_Empresa
        /// <summary>
        /// Child TB_LG_Acesso where [TB_LG_Acesso].[cdEmpresa] point to this entity (FK_TB_LG_Acesso_cdEmpresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_LG_Acesso> TB_LG_Acesso { get; set; } // TB_LG_Acesso.FK_TB_LG_Acesso_cdEmpresa
        /// <summary>
        /// Child TB_RF_AliquotaImposto where [TB_RF_AliquotaImposto].[cdEmpresa] point to this entity (FK_TB_RF_AliquotaImposto_cdEmpresa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_RF_AliquotaImposto> TB_RF_AliquotaImposto { get; set; } // TB_RF_AliquotaImposto.FK_TB_RF_AliquotaImposto_cdEmpresa

        // Foreign keys

        /// <summary>
        /// Parent TB_RF_TipoDocumento pointed by [TB_DD_Empresa].([cdTipoDocumento]) (FK_TB_DD_Empresa_cdTipoDocumento)
        /// </summary>
        public virtual TB_RF_TipoDocumento TB_RF_TipoDocumento { get; set; } // FK_TB_DD_Empresa_cdTipoDocumento

        /// <summary>
        /// Parent TB_RF_TipoEmpresa pointed by [TB_DD_Empresa].([TB_TipoEmpresa_cdTipo]) (FK_TB_DD_Empresa_TB_RF_TipoEmpresa_cdTipo)
        /// </summary>
        public virtual TB_RF_TipoEmpresa TB_RF_TipoEmpresa { get; set; } // FK_TB_DD_Empresa_TB_RF_TipoEmpresa_cdTipo

        public TB_DD_Empresa()
        {
            TB_DD_CarrinhoOperacional = new System.Collections.Generic.List<TB_DD_CarrinhoOperacional>();
            TB_DD_CatalogoOficial = new System.Collections.Generic.List<TB_DD_CatalogoOficial>();
            TB_DD_ContratoExpositor = new System.Collections.Generic.List<TB_DD_ContratoExpositor>();
            TB_DD_ConviteEletronico = new System.Collections.Generic.List<TB_DD_ConviteEletronico>();
            TB_DD_ConviteEletronicoVip = new System.Collections.Generic.List<TB_DD_ConviteEletronicoVip>();
            TB_DD_CorCarpeteColuna = new System.Collections.Generic.List<TB_DD_CorCarpeteColuna>();
            TB_Empresa_CdEmpresaPrimario = new System.Collections.Generic.List<TB_DD_Credencial>();
            TB_DD_Credencial_TB_Empresa_CdEmpresaPrimario = new System.Collections.Generic.List<TB_DD_Credencial>();
            TB_DD_EntregaCredencialVIP = new System.Collections.Generic.List<TB_DD_EntregaCredencialVIP>();
            TB_DD_HelpDesk = new System.Collections.Generic.List<TB_DD_HelpDesk>();
            TB_DD_LoteImpressao = new System.Collections.Generic.List<TB_DD_LoteImpressao>();
            TB_Empresa_cdEmpresaPrimario = new System.Collections.Generic.List<TB_DD_Movimentacao>();
            TB_Empresa_cdEmpresaSecundario = new System.Collections.Generic.List<TB_DD_Movimentacao>();
            TB_DD_Pedido = new System.Collections.Generic.List<TB_DD_Pedido>();
            TB_DD_PlacaIdentificacao = new System.Collections.Generic.List<TB_DD_PlacaIdentificacao>();
            TB_DD_PlacaIdentificacaoColuna = new System.Collections.Generic.List<TB_DD_PlacaIdentificacaoColuna>();
            TB_DD_ReenvioConvite = new System.Collections.Generic.List<TB_DD_ReenvioConvite>();
            TB_DD_ReenvioConviteVip = new System.Collections.Generic.List<TB_DD_ReenvioConviteVip>();
            TB_DD_Registro = new System.Collections.Generic.List<TB_DD_Registro>();
            TB_DD_Stand = new System.Collections.Generic.List<TB_DD_Stand>();
            TB_LG_Acesso = new System.Collections.Generic.List<TB_LG_Acesso>();
            TB_RF_AliquotaImposto = new System.Collections.Generic.List<TB_RF_AliquotaImposto>();
        }
    }

    // TB_DD_EntregaCredencialVIP
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_EntregaCredencialVIP
    {
        public int cdEntrega { get; set; } // cdEntrega (Primary key)
        public int TB_Empresa_cdEmpresa { get; set; } // TB_Empresa_cdEmpresa
        public string dsRazaoSocial { get; set; } // dsRazaoSocial (length: 100)
        public string dsLogradouro { get; set; } // dsLogradouro (length: 200)
        public string dsComplementoLogradouro { get; set; } // dsComplementoLogradouro (length: 100)
        public string dsNumeroLogradouro { get; set; } // dsNumeroLogradouro (length: 100)
        public string dsCepLogradouro { get; set; } // dsCepLogradouro (length: 100)
        public string dsBairroLogradouro { get; set; } // dsBairroLogradouro (length: 100)
        public string dsCidadeLogradouro { get; set; } // dsCidadeLogradouro (length: 100)
        public string dsUfLogradouro { get; set; } // dsUfLogradouro (length: 100)
        public string dsEmail { get; set; } // dsEmail (length: 100)
        public string dsResponsavel { get; set; } // dsResponsavel (length: 100)
        public string dsNumeroStand { get; set; } // dsNumeroStand (length: 100)
        public string dsNumeroTelefone { get; set; } // dsNumeroTelefone (length: 15)
        public string dsNumeroCelular { get; set; } // dsNumeroCelular (length: 15)
        public string dsTipoEnvio { get; set; } // dsTipoEnvio (length: 100)
        public System.DateTime? dtInclusao { get; set; } // dtInclusao
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public int TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public int TB_Pessoa_cdPessoa { get; set; } // TB_Pessoa_cdPessoa
        public int cdStand { get; set; } // cdStand

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_EntregaCredencialVIP].([TB_Edicao_cdEdicao]) (FK_TB_DD_EntregaCredencialVIP_Edicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_EntregaCredencialVIP_Edicao

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_EntregaCredencialVIP].([TB_Empresa_cdEmpresa]) (FK_TB_DD_EntregaCredencialVIP_Empresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_DD_EntregaCredencialVIP_Empresa

        /// <summary>
        /// Parent TB_DD_Pessoa pointed by [TB_DD_EntregaCredencialVIP].([TB_Pessoa_cdPessoa]) (FK_TB_DD_EntregaCredencialVIP_Pessoa)
        /// </summary>
        public virtual TB_DD_Pessoa TB_DD_Pessoa { get; set; } // FK_TB_DD_EntregaCredencialVIP_Pessoa

        /// <summary>
        /// Parent TB_DD_Stand pointed by [TB_DD_EntregaCredencialVIP].([cdStand]) (FK_TB_DD_EntregaCredencialVIP_Stand)
        /// </summary>
        public virtual TB_DD_Stand TB_DD_Stand { get; set; } // FK_TB_DD_EntregaCredencialVIP_Stand
    }

    // TB_DD_EstruturaCartaPersonalizada
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_EstruturaCartaPersonalizada
    {
        public int cdEstruturaCartaPersonalizada { get; set; } // cdEstruturaCartaPersonalizada (Primary key)
        public int cdFormulario { get; set; } // cdFormulario
        public int cdTipoCarta { get; set; } // cdTipoCarta
        public int cdUsuarioInclusao { get; set; } // cdUsuarioInclusao
        public System.DateTime dtInclusao { get; set; } // dtInclusao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public string dsTextoCartaPortugues { get; set; } // dsTextoCartaPortugues
        public string dsTextoCartaIngles { get; set; } // dsTextoCartaIngles
        public string dsTextoCartaEspanhol { get; set; } // dsTextoCartaEspanhol
        public string dsRodapeCartaPortugues { get; set; } // dsRodapeCartaPortugues
        public string dsRodapeCartaIngles { get; set; } // dsRodapeCartaIngles
        public string dsRodapeCartaEspanhol { get; set; } // dsRodapeCartaEspanhol
        public int cdEdicao { get; set; } // cdEdicao
        public int? cdSistema { get; set; } // cdSistema
        public string dsCorTituloRodapePortugues { get; set; } // dsCorTituloRodapePortugues (length: 200)
        public string dsCorTituloRodapeIngles { get; set; } // dsCorTituloRodapeIngles (length: 200)
        public string dsCorTituloRodapeEspanhol { get; set; } // dsCorTituloRodapeEspanhol (length: 200)

        // Foreign keys

        /// <summary>
        /// Parent TB_RF_TipoCarta pointed by [TB_DD_EstruturaCartaPersonalizada].([cdTipoCarta]) (FK_TB_DD_EstruturaCartaPersonalizada_TB_RF_TipoCarta)
        /// </summary>
        public virtual TB_RF_TipoCarta TB_RF_TipoCarta { get; set; } // FK_TB_DD_EstruturaCartaPersonalizada_TB_RF_TipoCarta
    }

    // TB_DD_EstruturaPersonalizada
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_EstruturaPersonalizada
    {
        public int cdEstruturaPersonalizada { get; set; } // cdEstruturaPersonalizada (Primary key)
        public int cdFormulario { get; set; } // cdFormulario
        public int cdEstruturaPadrao { get; set; } // cdEstruturaPadrao
        public string dsPortugues { get; set; } // dsPortugues
        public string dsIngles { get; set; } // dsIngles
        public string dsEspanhol { get; set; } // dsEspanhol
        public string fgObrigatorioPortugues { get; set; } // fgObrigatorioPortugues (length: 1)
        public string fgObrigatorioIngles { get; set; } // fgObrigatorioIngles (length: 1)
        public string fgObrigatorioEspanhol { get; set; } // fgObrigatorioEspanhol (length: 1)
        public string dsMensagemObrigatorioPortugues { get; set; } // dsMensagemObrigatorioPortugues (length: 800)
        public string dsMensagemObrigatorioIngles { get; set; } // dsMensagemObrigatorioIngles (length: 800)
        public string dsMensagemObrigatorioEspanhol { get; set; } // dsMensagemObrigatorioEspanhol (length: 800)
        public string fgExibirPortugues { get; set; } // fgExibirPortugues (length: 1)
        public string fgExibirIngles { get; set; } // fgExibirIngles (length: 1)
        public string fgExibirEspanhol { get; set; } // fgExibirEspanhol (length: 1)
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public int cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao
        public int? nuOrdemPortugues { get; set; } // nuOrdemPortugues
        public int? nuOrdemIngles { get; set; } // nuOrdemIngles
        public int? nuOrdemEspanhol { get; set; } // nuOrdemEspanhol
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public int cdEdicao { get; set; } // cdEdicao
        public int? cdTipoValidacao { get; set; } // cdTipoValidacao
        public int? nuCaracteres { get; set; } // nuCaracteres
        public string dsMensagemPreenchimentoIncorretoPortugues { get; set; } // dsMensagemPreenchimentoIncorretoPortugues (length: 800)
        public string dsMensagemPreenchimentoIncorretoIngles { get; set; } // dsMensagemPreenchimentoIncorretoIngles (length: 800)
        public string dsMensagemPreenchimentoIncorretoEspanhol { get; set; } // dsMensagemPreenchimentoIncorretoEspanhol (length: 800)
        public int? cdSistema { get; set; } // cdSistema
        public int? cdFormularioManual { get; set; } // cdFormularioManual
        public int nuEspaco { get; set; } // nuEspaco
        public string dsRestringirDuplicidadeCNPJ { get; set; } // dsRestringirDuplicidadeCNPJ (length: 1)
        public int? dsQtdDuplicidadeCNPJ { get; set; } // dsQtdDuplicidadeCNPJ
        public string dsExcecoesDuplicidadeCNPJ { get; set; } // dsExcecoesDuplicidadeCNPJ (length: 800)
        public string dsHabilitarRestricaoCampo { get; set; } // dsHabilitarRestricaoCampo (length: 1)
        public string dsRestricoesCampo { get; set; } // dsRestricoesCampo
        public string dsHabilitarRestricaoCampoNaoPermitirDuplicidade { get; set; } // dsHabilitarRestricaoCampoNaoPermitirDuplicidade (length: 1)

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_EstruturaPadrao pointed by [TB_DD_EstruturaPersonalizada].([cdEstruturaPadrao]) (FK_TB_DD_EstruturaPersonalizada_TB_CF_Manual)
        /// </summary>
        public virtual TB_CF_EstruturaPadrao TB_CF_EstruturaPadrao { get; set; } // FK_TB_DD_EstruturaPersonalizada_TB_CF_Manual

        /// <summary>
        /// Parent TB_CF_Formulario pointed by [TB_DD_EstruturaPersonalizada].([cdFormulario]) (FK_tb_dd_EstruturaPersonalizada_tb_cf_formulario)
        /// </summary>
        public virtual TB_CF_Formulario TB_CF_Formulario { get; set; } // FK_tb_dd_EstruturaPersonalizada_tb_cf_formulario

        public TB_DD_EstruturaPersonalizada()
        {
            nuEspaco = 0;
        }
    }

    // TB_DD_Evento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Evento
    {
        public int cdEvento { get; set; } // cdEvento (Primary key)
        public string nmEvento { get; set; } // nmEvento (length: 100)

        // Reverse navigation

        /// <summary>
        /// Child TB_CF_Edicao where [TB_CF_Edicao].[cdEvento] point to this entity (FK_TB_CF_Edicao_TB_DD_Evento)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_CF_Edicao> TB_CF_Edicao { get; set; } // TB_CF_Edicao.FK_TB_CF_Edicao_TB_DD_Evento

        public TB_DD_Evento()
        {
            TB_CF_Edicao = new System.Collections.Generic.List<TB_CF_Edicao>();
        }
    }

    // TB_DD_ExtracaoPersonalizada
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ExtracaoPersonalizada
    {
        public int cdExtracaoPersonalizada { get; set; } // cdExtracaoPersonalizada (Primary key)
        public string dsNomeRelatorioExtracaoPersonalizada { get; set; } // dsNomeRelatorioExtracaoPersonalizada (length: 200)
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public int cdUsuarioInclusao { get; set; } // cdUsuarioInclusao
        public System.DateTime dtInclusao { get; set; } // dtInclusao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_ConfExtracaoPersonalizada where [TB_DD_ConfExtracaoPersonalizada].[cdExtracaoPersonalizada] point to this entity (FK_ConfExtracaoPersonalizada)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ConfExtracaoPersonalizada> TB_DD_ConfExtracaoPersonalizada { get; set; } // TB_DD_ConfExtracaoPersonalizada.FK_ConfExtracaoPersonalizada

        public TB_DD_ExtracaoPersonalizada()
        {
            dsSituacao = "ATIVO";
            TB_DD_ConfExtracaoPersonalizada = new System.Collections.Generic.List<TB_DD_ConfExtracaoPersonalizada>();
        }
    }

    // TB_DD_FaqRespostas
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_FaqRespostas
    {
        public int cdFaqRespostas { get; set; } // cdFaqRespostas (Primary key)
        public int? cdFaq { get; set; } // cdFaq
        public System.DateTime? DataInclusao { get; set; } // DataInclusao
        public System.DateTime? DataAlteracao { get; set; } // DataAlteracao
        public string dsResposta { get; set; } // dsResposta

        // Foreign keys

        /// <summary>
        /// Parent TB_RF_Faq pointed by [TB_DD_FaqRespostas].([cdFaq]) (FK_TB_DD_FaqRespostas_cdFaq)
        /// </summary>
        public virtual TB_RF_Faq TB_RF_Faq { get; set; } // FK_TB_DD_FaqRespostas_cdFaq
    }

    // TB_DD_Filial
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Filial
    {
        public int cdFilial { get; set; } // cdFilial (Primary key)
        public System.DateTime? dtAbertura { get; set; } // dtAbertura
        public System.DateTime? dtEncerramento { get; set; } // dtEncerramento
        public string nmFilial { get; set; } // nmFilial
        public string flHabilitado { get; set; } // flHabilitado (length: 1)

        // Reverse navigation

        /// <summary>
        /// Child TB_AS_Filial_Funcionario where [TB_AS_Filial_Funcionario].[cdFilial] point to this entity (FK_TB_AS_Filial_Funcionario_cdFilial)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_Filial_Funcionario> TB_AS_Filial_Funcionario { get; set; } // TB_AS_Filial_Funcionario.FK_TB_AS_Filial_Funcionario_cdFilial

        public TB_DD_Filial()
        {
            flHabilitado = "S";
            TB_AS_Filial_Funcionario = new System.Collections.Generic.List<TB_AS_Filial_Funcionario>();
        }
    }

    // TB_DD_FotoPessoa
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_FotoPessoa
    {
        public int cdPessoa { get; set; } // cdPessoa (Primary key)
        public byte[] imgBinary { get; set; } // imgBinary
        public System.DateTime dtInclusao { get; set; } // dtInclusao
    }

    // TB_DD_Funcionario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Funcionario
    {
        public int cdFuncionario { get; set; } // cdFuncionario (Primary key)
        public string nmCompleto { get; set; } // nmCompleto
        public string dsCargo { get; set; } // dsCargo
        public string dsDepartamento { get; set; } // dsDepartamento
        public string dsEmailLogin { get; set; } // dsEmailLogin
        public string dsSenha { get; set; } // dsSenha
        public string dsTelefoneResidencial { get; set; } // dsTelefoneResidencial
        public string dsTelefoneCelular { get; set; } // dsTelefoneCelular
        public string dsNextel { get; set; } // dsNextel
        public string dsIDNextel { get; set; } // dsIDNextel
        public string flHabilitado { get; set; } // flHabilitado (length: 1)

        // Reverse navigation

        /// <summary>
        /// Child TB_AS_Filial_Funcionario where [TB_AS_Filial_Funcionario].[cdFuncionario] point to this entity (FK_TB_AS_Filial_Funcionario_cdFuncionario)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_Filial_Funcionario> TB_AS_Filial_Funcionario { get; set; } // TB_AS_Filial_Funcionario.FK_TB_AS_Filial_Funcionario_cdFuncionario

        public TB_DD_Funcionario()
        {
            flHabilitado = "S";
            TB_AS_Filial_Funcionario = new System.Collections.Generic.List<TB_AS_Filial_Funcionario>();
        }
    }

    // TB_DD_Geradora_Registro
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Geradora_Registro
    {
        public long cdCodigo { get; set; } // cdCodigo (Primary key)
        public System.DateTime? dtGeracao { get; set; } // dtGeracao
        public string dsDestino { get; set; } // dsDestino (length: 500)
        public string wkRowID { get; set; } // wkRowID (length: 200)
    }

    // TB_DD_GrupoProdutoCongresso
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_GrupoProdutoCongresso
    {
        public int cdGrupoProdutoCongresso { get; set; } // cdGrupoProdutoCongresso (Primary key)
        public int cdSistema { get; set; } // cdSistema
        public string dsPortugues { get; set; } // dsPortugues (length: 800)
        public string dsIngles { get; set; } // dsIngles (length: 800)
        public string dsEspanhol { get; set; } // dsEspanhol (length: 800)
        public System.DateTime dtInclusao { get; set; } // dtInclusao
        public int cdUsuarioInclusao { get; set; } // cdUsuarioInclusao
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public int nuOrdem { get; set; } // nuOrdem
        public string dsIdGrupo { get; set; } // dsIdGrupo (length: 500)
        public string dsCor { get; set; } // dsCor
        public int? nuQtdProdutoPorGrupo { get; set; } // nuQtdProdutoPorGrupo
    }

    // TB_DD_GrupoResponsavelCongresso
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_GrupoResponsavelCongresso
    {
        public int cdGrupoResponsavelCongresso { get; set; } // cdGrupoResponsavelCongresso (Primary key)
        public int cdResponsavelCongresso { get; set; } // cdResponsavelCongresso
        public int cdPessoa { get; set; } // cdPessoa
        public int cdEdicao { get; set; } // cdEdicao
        public int cdSistema { get; set; } // cdSistema
        public System.DateTime dtInclusao { get; set; } // dtInclusao
        public string dsCodigoBarras { get; set; } // dsCodigoBarras (length: 200)

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_GrupoResponsavelCongresso].([cdEdicao]) (FK_GrupoResponsavel_Edicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_GrupoResponsavel_Edicao

        /// <summary>
        /// Parent TB_DD_Pessoa pointed by [TB_DD_GrupoResponsavelCongresso].([cdPessoa]) (FK_GrupoResponsavel_Pessoal)
        /// </summary>
        public virtual TB_DD_Pessoa TB_DD_Pessoa { get; set; } // FK_GrupoResponsavel_Pessoal

        /// <summary>
        /// Parent TB_DD_ResponsavelCongresso pointed by [TB_DD_GrupoResponsavelCongresso].([cdResponsavelCongresso]) (FK_GrupoResponsavel_Responsavel)
        /// </summary>
        public virtual TB_DD_ResponsavelCongresso TB_DD_ResponsavelCongresso { get; set; } // FK_GrupoResponsavel_Responsavel
    }

    // TB_DD_HelpDesk
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_HelpDesk
    {
        public int cdChamado { get; set; } // cdChamado (Primary key)
        public int? cdEdicao { get; set; } // cdEdicao
        public int? cdEmpresa { get; set; } // cdEmpresa
        public int? cdPessoa { get; set; } // cdPessoa
        public System.DateTime? DataInclusao { get; set; } // DataInclusao
        public string dsAssunto { get; set; } // dsAssunto (length: 100)
        public string dsStatus { get; set; } // dsStatus (length: 100)

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_HelpDesk].([cdEdicao]) (FK_TB_DD_HelpDesk_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_HelpDesk_cdEdicao

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_HelpDesk].([cdEmpresa]) (FK_TB_DD_HelpDesk_cdEmpresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_DD_HelpDesk_cdEmpresa
    }

    // TB_DD_ImagemAssistentePreenchimento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ImagemAssistentePreenchimento
    {
        public int cdImagemAssistente { get; set; } // cdImagemAssistente (Primary key)
        public int cdUsuarioInclusao { get; set; } // cdUsuarioInclusao
        public System.DateTime dtInclusao { get; set; } // dtInclusao
        public string dsCaminho { get; set; } // dsCaminho
        public int? cdSistema { get; set; } // cdSistema
        public string dsUrl { get; set; } // dsUrl (length: 800)
    }

    // TB_DD_ImportacaoPessoa
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ImportacaoPessoa
    {
        public int cdImportacaoPessoa { get; set; } // cdImportacaoPessoa (Primary key)
        public string dsCPF { get; set; } // dsCPF (length: 100)
        public string nmCompleto { get; set; } // nmCompleto (length: 100)
        public string nmCracha { get; set; } // nmCracha (length: 100)
        public string dsCargo { get; set; } // dsCargo (length: 100)
        public string dsEmail { get; set; } // dsEmail (length: 100)
        public System.DateTime? dtInclusao { get; set; } // dtInclusao
        public System.DateTime? dtImportacao { get; set; } // dtImportacao
        public string dsImportado { get; set; } // dsImportado (length: 100)
        public int? TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public string Res1 { get; set; } // Res1 (length: 100)
        public string Res2 { get; set; } // Res2 (length: 100)
        public string Res3 { get; set; } // Res3 (length: 100)
        public string Res4 { get; set; } // Res4 (length: 100)
        public string Res5 { get; set; } // Res5 (length: 100)
        public int? cdUsuario_Insercao { get; set; } // cdUsuario_Insercao
        public int? cdEmpresa_Insercao { get; set; } // cdEmpresa_Insercao
        public int? cdUsuario_Importacao { get; set; } // cdUsuario_Importacao
        public int? TB_Categoria_cdCategoria { get; set; } // TB_Categoria_cdCategoria
        public string RowId { get; set; } // RowId (length: 800)
    }

    // TB_DD_ImpostoAPAS
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ImpostoAPAS
    {
        public System.Guid cdImpostoAPAS { get; set; } // cdImpostoAPAS (Primary key)
        public int cdPedido { get; set; } // cdPedido
        public int cdItem { get; set; } // cdItem
        public int cdNumeroProduto { get; set; } // cdNumeroProduto
        public int cdSequencial { get; set; } // cdSequencial
        public string dsBilhete { get; set; } // dsBilhete (length: 800)
        public decimal vlImposto { get; set; } // vlImposto
        public System.DateTime dtInclusao { get; set; } // dtInclusao
        public string dsSituacao { get; set; } // dsSituacao (length: 100)

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_ItensPedidoCongresso pointed by [TB_DD_ImpostoAPAS].([cdItem]) (FK_ImpostoAPAS_ItemPedidoCongresso)
        /// </summary>
        public virtual TB_DD_ItensPedidoCongresso TB_DD_ItensPedidoCongresso { get; set; } // FK_ImpostoAPAS_ItemPedidoCongresso

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_DD_ImpostoAPAS].([cdPedido]) (FK_ImpostoAPAS_Pedido)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido { get; set; } // FK_ImpostoAPAS_Pedido

        public TB_DD_ImpostoAPAS()
        {
            cdImpostoAPAS = System.Guid.NewGuid();
            dsSituacao = "ATIVO";
        }
    }

    // TB_DD_InteracaoHelp
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_InteracaoHelp
    {
        public int cdInteracao { get; set; } // cdInteracao (Primary key)
        public int? cdChamado { get; set; } // cdChamado
        public System.DateTime? DataInclusao { get; set; } // DataInclusao
        public int? cdPessoa { get; set; } // cdPessoa
        public string dsMensagem { get; set; } // dsMensagem
    }

    // TB_DD_ItensLote
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ItensLote
    {
        public int cdItem { get; set; } // cdItem (Primary key)
        public string dsCodigoBarras { get; set; } // dsCodigoBarras (length: 100)
        public int? cdLote { get; set; } // cdLote

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_LoteImpressao pointed by [TB_DD_ItensLote].([cdLote]) (FK_TB_DD_ItensLote_cdLote)
        /// </summary>
        public virtual TB_DD_LoteImpressao TB_DD_LoteImpressao { get; set; } // FK_TB_DD_ItensLote_cdLote
    }

    // TB_DD_ItensPedidoCongresso
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ItensPedidoCongresso
    {
        public int cdItem { get; set; } // cdItem (Primary key)
        public int TB_Pessoa_cdPessoa { get; set; } // TB_Pessoa_cdPessoa
        public int TB_ProdutoCongresso_cdProduto { get; set; } // TB_ProdutoCongresso_cdProduto
        public int TB_Pedido_cdPedido { get; set; } // TB_Pedido_cdPedido
        public int? TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public int? cdPreco { get; set; } // cdPreco
        public System.DateTime? dtItem { get; set; } // dtItem
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public string dsCodigoBarras { get; set; } // dsCodigoBarras (length: 100)
        public string dsNome { get; set; } // dsNome (length: 500)
        public string dsEmail { get; set; } // dsEmail (length: 500)
        public int? cdTicket { get; set; } // cdTicket
        public int? cdItemTroca { get; set; } // cdItemTroca
        public int? cdPessoalSubstituida { get; set; } // cdPessoalSubstituida
        public System.DateTime? dtsubstituicao { get; set; } // dtsubstituicao
        public int? cdUsuarioSubstituicao { get; set; } // cdUsuarioSubstituicao
        public int? cdFicha { get; set; } // cdFicha
        public string dsIngressoEntregue { get; set; } // dsIngressoEntregue (length: 1)
        public System.Guid? cdPessoaCongresso { get; set; } // cdPessoaCongresso

        public string Valor_Item { get; set; } // Valor_Item (length: 100)
        public string Vlr_Desconto_Adm { get; set; } // Vlr_Desconto_Adm (length: 100)
        public string Valor_Desconto_Regra { get; set; } // Valor_Desconto_Regra (length: 100)
        public string Valor_Desconto_Codigo_Promocao { get; set; } // Valor_Desconto_Codigo_Promocao (length: 100)
        public string Valor_Liquido { get; set; } // Valor_Liquido (length: 100)

        public System.DateTime? dtInclusao { get; set; } // dtItem
        public int? cdUsuarioInclusao { get; set; } // cdPreco
        public System.DateTime? dtAlteracao { get; set; } // dtItem
        public int? cdUsuarioAlteracao { get; set; } // cdPreco

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_ImpostoAPAS where [TB_DD_ImpostoAPAS].[cdItem] point to this entity (FK_ImpostoAPAS_ItemPedidoCongresso)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ImpostoAPAS> TB_DD_ImpostoAPAS { get; set; } // TB_DD_ImpostoAPAS.FK_ImpostoAPAS_ItemPedidoCongresso

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_ItensPedidoCongresso].([TB_Edicao_cdEdicao]) (FK_TB_DD_ItensPedidoCongresso_TB_CF_Edicao_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_ItensPedidoCongresso_TB_CF_Edicao_cdEdicao

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_DD_ItensPedidoCongresso].([TB_Pedido_cdPedido]) (FK_TB_DD_ItensPedidoCongresso_TB_Pedido_cdPedido)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido { get; set; } // FK_TB_DD_ItensPedidoCongresso_TB_Pedido_cdPedido

        /// <summary>
        /// Parent TB_DD_Pessoa pointed by [TB_DD_ItensPedidoCongresso].([TB_Pessoa_cdPessoa]) (FK_TB_DD_ItensPedidoCongresso_TB_DD_Pessoa_cdPessoa)
        /// </summary>
        public virtual TB_DD_Pessoa TB_DD_Pessoa { get; set; } // FK_TB_DD_ItensPedidoCongresso_TB_DD_Pessoa_cdPessoa

        /// <summary>
        /// Parent TB_DD_ProdutoCongresso pointed by [TB_DD_ItensPedidoCongresso].([TB_ProdutoCongresso_cdProduto]) (FK_TB_DD_ItensPedidoCongresso_TB_DD_ProdutoCongresso_cdProduto)
        /// </summary>
        public virtual TB_DD_ProdutoCongresso TB_DD_ProdutoCongresso { get; set; } // FK_TB_DD_ItensPedidoCongresso_TB_DD_ProdutoCongresso_cdProduto

        public TB_DD_ItensPedidoCongresso()
        {
            TB_DD_ImpostoAPAS = new System.Collections.Generic.List<TB_DD_ImpostoAPAS>();
        }
    }

    // TB_DD_ItensPedidoOperacional
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ItensPedidoOperacional
    {
        public int cdItem { get; set; } // cdItem (Primary key)
        public int TB_ProdutoOperacional_cdProduto { get; set; } // TB_ProdutoOperacional_cdProduto
        public int TB_Pedido_cdPedido { get; set; } // TB_Pedido_cdPedido
        public string QtdProduto { get; set; } // QtdProduto (length: 100)
        public string dsValor { get; set; } // dsValor (length: 100)
        public int? cdStand { get; set; } // cdStand
        public int? cdPessoa { get; set; } // cdPessoa
        public string CodigoBarras { get; set; } // CodigoBarras (length: 30)
        public string dsSituacao { get; set; } // dsSituacao (length: 10)
        public string dsDescricaoOpcional { get; set; } // dsDescricaoOpcional
        public int? cdEmpresaSegundario { get; set; } // cdEmpresaSegundario
        public string dsReservado1 { get; set; } // dsReservado1 (length: 800)
        public string dsReservado2 { get; set; } // dsReservado2 (length: 800)
        public string dsReservado3 { get; set; } // dsReservado3 (length: 800)
        public string dsReservado4 { get; set; } // dsReservado4 (length: 800)
        public string dsReservado5 { get; set; } // dsReservado5 (length: 800)
        public string dsReservado6 { get; set; } // dsReservado6 (length: 800)
        public string dsReservado7 { get; set; } // dsReservado7 (length: 800)
        public string dsReservado8 { get; set; } // dsReservado8 (length: 800)
        public string dsReservado9 { get; set; } // dsReservado9 (length: 800)
        public string dsReservado10 { get; set; } // dsReservado10 (length: 800)
        public string dsReservado11 { get; set; } // dsReservado11 (length: 800)
        public string dsReservado12 { get; set; } // dsReservado12 (length: 800)
        public string dsReservado13 { get; set; } // dsReservado13 (length: 800)
        public string dsReservado14 { get; set; } // dsReservado14 (length: 800)
        public string dsReservado15 { get; set; } // dsReservado15 (length: 800)
        public string dsReservado16 { get; set; } // dsReservado16 (length: 800)
        public string dsReservado17 { get; set; } // dsReservado17 (length: 800)
        public string dsReservado18 { get; set; } // dsReservado18 (length: 800)
        public string dsReservado19 { get; set; } // dsReservado19 (length: 800)
        public string dsReservado20 { get; set; } // dsReservado20 (length: 800)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_RespostaQuestionarioManual where [TB_DD_RespostaQuestionarioManual].[cdItenPedidoOperacional] point to this entity (FK_TB_DD_RespostaQuestionarioManual_TB_DD_ItensPedidoOperacional)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_RespostaQuestionarioManual> TB_DD_RespostaQuestionarioManual { get; set; } // TB_DD_RespostaQuestionarioManual.FK_TB_DD_RespostaQuestionarioManual_TB_DD_ItensPedidoOperacional

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_DD_ItensPedidoOperacional].([TB_Pedido_cdPedido]) (FK_TB_DD_ItensPedidoOperacional_TB_Pedido_cdPedido)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido { get; set; } // FK_TB_DD_ItensPedidoOperacional_TB_Pedido_cdPedido

        /// <summary>
        /// Parent TB_DD_ProdutoOperacional pointed by [TB_DD_ItensPedidoOperacional].([TB_ProdutoOperacional_cdProduto]) (FK_TB_DD_ItensPedidoOperacional_TB_DD_ProdutoOperacional_cdProduto)
        /// </summary>
        public virtual TB_DD_ProdutoOperacional TB_DD_ProdutoOperacional { get; set; } // FK_TB_DD_ItensPedidoOperacional_TB_DD_ProdutoOperacional_cdProduto

        /// <summary>
        /// Parent TB_DD_Stand pointed by [TB_DD_ItensPedidoOperacional].([cdStand]) (FK_TB_DD_ItensPedidoOperacional_cdStand)
        /// </summary>
        public virtual TB_DD_Stand TB_DD_Stand { get; set; } // FK_TB_DD_ItensPedidoOperacional_cdStand

        public TB_DD_ItensPedidoOperacional()
        {
            TB_DD_RespostaQuestionarioManual = new System.Collections.Generic.List<TB_DD_RespostaQuestionarioManual>();
        }
    }

    // TB_DD_LeituraRegulamento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_LeituraRegulamento
    {
        public int cdLeituraRegulamento { get; set; } // cdLeituraRegulamento (Primary key)
        public System.DateTime? DataDownload { get; set; } // DataDownload
        public int? cdRegulamento { get; set; } // cdRegulamento
        public int? cdUsuario { get; set; } // cdUsuario

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Regulamento pointed by [TB_DD_LeituraRegulamento].([cdRegulamento]) (FK_TB_DD_LeituraRegulamento_TB_DD_Regulamento)
        /// </summary>
        public virtual TB_DD_Regulamento TB_DD_Regulamento_cdRegulamento { get; set; } // FK_TB_DD_LeituraRegulamento_TB_DD_Regulamento

        /// <summary>
        /// Parent TB_DD_Regulamento pointed by [TB_DD_LeituraRegulamento].([cdRegulamento]) (FK_TB_DD_LeituraRegulamento_cdRegulamento)
        /// </summary>
        public virtual TB_DD_Regulamento TB_DD_Regulamento1 { get; set; } // FK_TB_DD_LeituraRegulamento_cdRegulamento
    }

    // TB_DD_LimiteAplicacao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_LimiteAplicacao
    {
        public int cdLimiteAplicacao { get; set; } // cdLimiteAplicacao (Primary key)
        public System.DateTime dtInicio { get; set; } // dtInicio
        public System.DateTime dtLimite { get; set; } // dtLimite
        public string nmMensagemIngles { get; set; } // nmMensagemIngles (length: 255)
        public string nmMensagemEspanhol { get; set; } // nmMensagemEspanhol (length: 255)
        public string dsMensagemPortugues { get; set; } // dsMensagemPortugues (length: 255)
        public int cdEdicao { get; set; } // cdEdicao
        public int cdUsuario_Inclusao { get; set; } // cdUsuario_Inclusao
        public System.DateTime dtInclusao { get; set; } // dtInclusao
        public int cdUsuario_Alteracao { get; set; } // cdUsuario_Alteracao
        public System.DateTime dtAlteracao { get; set; } // dtAlteracao

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_LimiteAplicacao].([cdEdicao]) (FK_TB_DD_LimiteAplicacao_TB_CF_Edicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_LimiteAplicacao_TB_CF_Edicao

        /// <summary>
        /// Parent TB_DD_Usuario pointed by [TB_DD_LimiteAplicacao].([cdUsuario_Alteracao]) (FK_TB_DD_LimiteAplicacao_TB_DD_Usuario_Alteracao)
        /// </summary>
        public virtual TB_DD_Usuario TB_DD_Usuario_cdUsuario_Alteracao { get; set; } // FK_TB_DD_LimiteAplicacao_TB_DD_Usuario_Alteracao

        /// <summary>
        /// Parent TB_DD_Usuario pointed by [TB_DD_LimiteAplicacao].([cdUsuario_Inclusao]) (FK_TB_DD_LimiteAplicacao_TB_DD_Usuario_Inclusao)
        /// </summary>
        public virtual TB_DD_Usuario TB_DD_Usuario_cdUsuario_Inclusao { get; set; } // FK_TB_DD_LimiteAplicacao_TB_DD_Usuario_Inclusao
    }

    // TB_DD_LogImportacaoAPAS
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_LogImportacaoAPAS
    {
        public System.Guid cdLogImportacao { get; set; } // cdLogImportacao (Primary key)
        public System.DateTime? dtImportacao { get; set; } // dtImportacao
        public string lote { get; set; } // lote (length: 800)
        public string Tipo_de_Convite { get; set; } // Tipo_de_Convite (length: 800)
        public string CONTACT_ID_CRM { get; set; } // CONTACT_ID_CRM (length: 800)
        public string NOME { get; set; } // NOME (length: 800)
        public string NOME_CRACHA { get; set; } // NOME_CRACHA (length: 800)
        public string DATA_DE_NASCIMENTO { get; set; } // DATA_DE_NASCIMENTO (length: 800)
        public string MES_DE_NASCIMENTO { get; set; } // MES_DE_NASCIMENTO (length: 800)
        public string ANO_DE_NASCIMENTO { get; set; } // ANO_DE_NASCIMENTO (length: 800)
        public string SEXO { get; set; } // SEXO (length: 800)
        public string CARGO { get; set; } // CARGO (length: 800)
        public string AREA { get; set; } // AREA (length: 800)
        public string CPF { get; set; } // CPF (length: 800)
        public string Contato_DDI_TELEFONE { get; set; } // Contato_DDI_TELEFONE (length: 800)
        public string Contato_DDD_TELEFONE { get; set; } // Contato_DDD_TELEFONE (length: 800)
        public string Contato_TELEFONE { get; set; } // Contato_TELEFONE (length: 800)
        public string DDI_CELULAR { get; set; } // DDI_CELULAR (length: 800)
        public string DDD_CELULAR { get; set; } // DDD_CELULAR (length: 800)
        public string CELULAR { get; set; } // CELULAR (length: 800)
        public string E_MAIL_Profissional { get; set; } // E_MAIL_Profissional (length: 800)
        public string E_MAIL_Pessoal { get; set; } // E_MAIL_Pessoal (length: 800)
        public string ACCOUNT_ID_CRM { get; set; } // ACCOUNT_ID_CRM (length: 800)
        public string RAZAO_SOCIAL { get; set; } // RAZAO_SOCIAL (length: 800)
        public string NOME_FANTASIA { get; set; } // NOME_FANTASIA (length: 800)
        public string CNPJ { get; set; } // CNPJ (length: 800)
        public string Codigo_de_Cliente_APAS { get; set; } // Codigo_de_Cliente_APAS (length: 800)
        public string Nivel_Hierarquico { get; set; } // Nivel_Hierarquico (length: 800)
        public string ATIVIDADE_DE_ATUACAO { get; set; } // ATIVIDADE_DE_ATUACAO (length: 800)
        public string REGIONAL_DISTRITAL { get; set; } // REGIONAL_DISTRITAL (length: 800)
        public string ASSOCIADO { get; set; } // ASSOCIADO (length: 800)
        public string DDI_TELEFONE { get; set; } // DDI_TELEFONE (length: 800)
        public string DDD_TELEFONE { get; set; } // DDD_TELEFONE (length: 800)
        public string TELEFONE { get; set; } // TELEFONE (length: 800)
        public string CEP { get; set; } // CEP (length: 800)
        public string ENDERECO { get; set; } // ENDERECO (length: 800)
        public string NUMERO { get; set; } // NUMERO (length: 800)
        public string COMPLEMENTO { get; set; } // COMPLEMENTO (length: 800)
        public string BAIRRO { get; set; } // BAIRRO (length: 800)
        public string CIDADE { get; set; } // CIDADE (length: 800)
        public string ESTADO { get; set; } // ESTADO (length: 800)
        public string PAIS { get; set; } // PAIS (length: 800)
        public string EMAIL { get; set; } // EMAIL (length: 800)
        public string SITE { get; set; } // SITE (length: 800)
        public string dsImportado { get; set; } // dsImportado (length: 1)

        public TB_DD_LogImportacaoAPAS()
        {
            cdLogImportacao = System.Guid.NewGuid();
        }
    }

    // TB_DD_LoteImpressao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_LoteImpressao
    {
        public int cdLote { get; set; } // cdLote (Primary key)
        public int? cdEmpresa { get; set; } // cdEmpresa
        public int? cdUsuario { get; set; } // cdUsuario
        public System.DateTime? dtCadastro { get; set; } // dtCadastro

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_ItensLote where [TB_DD_ItensLote].[cdLote] point to this entity (FK_TB_DD_ItensLote_cdLote)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ItensLote> TB_DD_ItensLote { get; set; } // TB_DD_ItensLote.FK_TB_DD_ItensLote_cdLote

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_LoteImpressao].([cdEmpresa]) (FK_TB_DD_LoteImpressao_TB_DD_Empresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_DD_LoteImpressao_TB_DD_Empresa

        public TB_DD_LoteImpressao()
        {
            TB_DD_ItensLote = new System.Collections.Generic.List<TB_DD_ItensLote>();
        }
    }

    // TB_DD_MaquinaseVeiculos
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_MaquinaseVeiculos
    {
        public int cdMaquinas { get; set; } // cdMaquinas (Primary key)
        public int TB_Stand_cdStand { get; set; } // TB_Stand_cdStand
        public int TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public string dsSituacao { get; set; } // dsSituacao
        public System.DateTime? dtInclusao { get; set; } // dtInclusao
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public string dsTipoMaquinario { get; set; } // dsTipoMaquinario
        public string dsLargura { get; set; } // dsLargura
        public string dsAltura { get; set; } // dsAltura
        public string dsProfundidade { get; set; } // dsProfundidade
        public string dsPeso { get; set; } // dsPeso (length: 100)
        public string dsFormaExposicao { get; set; } // dsFormaExposicao (length: 100)
        public string dsFumaca { get; set; } // dsFumaca
        public string dsNivelBarulho { get; set; } // dsNivelBarulho (length: 100)

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_MaquinaseVeiculos].([TB_Edicao_cdEdicao]) (FK_TB_DD_MaquinaseVeiculos_TB_CF_Edicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_MaquinaseVeiculos_TB_CF_Edicao

        /// <summary>
        /// Parent TB_DD_Stand pointed by [TB_DD_MaquinaseVeiculos].([TB_Stand_cdStand]) (FK_TB_DD_MaquinaseVeiculos_TB_DD_Stand)
        /// </summary>
        public virtual TB_DD_Stand TB_DD_Stand { get; set; } // FK_TB_DD_MaquinaseVeiculos_TB_DD_Stand
    }

    // TB_DD_Monitoria
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Monitoria
    {
        public int cdAcesso { get; set; } // cdAcesso (Primary key)
        public string chSessao { get; set; } // chSessao (length: 500)
        public string dsNavegador { get; set; } // dsNavegador (length: 500)
        public System.DateTime? dtAcesso { get; set; } // dtAcesso
        public string dsSistema { get; set; } // dsSistema (length: 800)
        public string tl_Acesso { get; set; } // tl_Acesso (length: 1)
        public string tl_Escolha { get; set; } // tl_Escolha (length: 1)
        public System.DateTime? dtEscolha { get; set; } // dtEscolha
        public string tl_Dados { get; set; } // tl_Dados (length: 1)
        public System.DateTime? dtDados { get; set; } // dtDados
        public string tl_Questionario { get; set; } // tl_Questionario (length: 1)
        public System.DateTime? dtQuestionario { get; set; } // dtQuestionario
        public string tl_liConcordo { get; set; } // tl_liConcordo (length: 1)
        public System.DateTime? dtliConcordo { get; set; } // dtliConcordo
        public string tl_Produto { get; set; } // tl_Produto (length: 1)
        public System.DateTime? dtProduto { get; set; } // dtProduto
        public string tl_Carta { get; set; } // tl_Carta (length: 1)
        public System.DateTime? dtCarta { get; set; } // dtCarta
        public string dsIP { get; set; } // dsIP (length: 100)
        public string dsEmail { get; set; } // dsEmail (length: 500)
        public string dsFacebook { get; set; } // dsFacebook (length: 500)
        public string dsEvento { get; set; } // dsEvento (length: 500)
        public int? cdEdicao { get; set; } // cdEdicao
        public int? cdCategoria { get; set; } // cdCategoria
        public int? dsTentativas { get; set; } // dsTentativas
        public string dsIdioma { get; set; } // dsIdioma (length: 2)
    }

    // TB_DD_MonitoriaCielo
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_MonitoriaCielo
    {
        public int cdMonitoria { get; set; } // cdMonitoria (Primary key)
        public string dsMonitoria { get; set; } // dsMonitoria (length: 800)
        public System.DateTime? dtMonitoria { get; set; } // dtMonitoria
        public string dsNavegador { get; set; } // dsNavegador (length: 800)
        public string dsIP { get; set; } // dsIP (length: 800)
        public string dsRowId { get; set; } // dsRowId (length: 800)
        public string dsParametrosRecebidos { get; set; } // dsParametrosRecebidos (length: 800)
        public string order_number { get; set; } // order_number (length: 800)
        public string amount { get; set; } // amount (length: 800)
        public string checkout_cielo_order_number { get; set; } // checkout_cielo_order_number (length: 800)
        public System.DateTime? created_date { get; set; } // created_date
        public string customer_name { get; set; } // customer_name (length: 800)
        public string customer_phone { get; set; } // customer_phone (length: 800)
        public string customer_identity { get; set; } // customer_identity (length: 800)
        public string customer_email { get; set; } // customer_email (length: 800)
        public string shipping_type { get; set; } // shipping_type (length: 800)
        public string payment_method_type { get; set; } // payment_method_type (length: 800)
        public string payment_method_brand { get; set; } // payment_method_brand (length: 800)
        public string payment_maskedcreditcard { get; set; } // payment_maskedcreditcard (length: 800)
        public string payment_installments { get; set; } // payment_installments (length: 800)
        public string payment_status { get; set; } // payment_status (length: 800)
        public string tid { get; set; } // tid (length: 800)
    }

    // TB_DD_MonitoriaPayPal
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_MonitoriaPayPal
    {
        public int cdMonitoriaPaypal { get; set; } // cdMonitoriaPaypal (Primary key)
        public string dsMonitoriaPayPal { get; set; } // dsMonitoriaPayPal (length: 800)
        public string numPedido { get; set; } // numPedido (length: 100)
        public string numSistema { get; set; } // numSistema (length: 100)
        public System.DateTime? dtInclusao { get; set; } // dtInclusao
        public string dsFormAllKeys { get; set; } // dsFormAllKeys (length: 2000)
        public string dsQueryStringAllKey { get; set; } // dsQueryStringAllKey (length: 2000)
    }

    // TB_DD_MonitoriaRedeCard
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_MonitoriaRedeCard
    {
        public int cdMonitoriaRedeCard { get; set; } // cdMonitoriaRedeCard (Primary key)
        public string dsParametrosRecebidos { get; set; } // dsParametrosRecebidos (length: 800)
        public int cdPedido { get; set; } // cdPedido
        public System.DateTime DataTransacao { get; set; } // DataTransacao
        public string Data { get; set; } // Data (length: 100)
        public string NumPedido { get; set; } // NumPedido (length: 100)
        public string Nr_Cartao { get; set; } // Nr_Cartao (length: 100)
        public string Origem_Bin { get; set; } // Origem_Bin (length: 100)
        public string NumAutor { get; set; } // NumAutor (length: 100)
        public string NumCV { get; set; } // NumCV (length: 100)
        public string NumAutent { get; set; } // NumAutent (length: 100)
        public string NumSqn { get; set; } // NumSqn (length: 100)
        public string Data_Expi { get; set; } // Data_Expi (length: 100)
        public string CodRet { get; set; } // CodRet (length: 100)
        public string MsgRet { get; set; } // MsgRet (length: 800)
        public System.DateTime? DataConfirmacao { get; set; } // DataConfirmacao
        public string dsMovimentacao { get; set; } // dsMovimentacao (length: 100)
    }

    // TB_DD_MonitoriaShopLineItau
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_MonitoriaShopLineItau
    {
        public int cdMonitoriaShoplineItau { get; set; } // cdMonitoriaShoplineItau (Primary key)
        public System.DateTime? dtProcessamento { get; set; } // dtProcessamento
        public System.Guid dsRowId { get; set; } // dsRowId
        public string dsFormAllKeys { get; set; } // dsFormAllKeys (length: 8000)
        public string dsQueryStringAllKey { get; set; } // dsQueryStringAllKey (length: 8000)
        public int? cdEvento { get; set; } // cdEvento
        public int? cdEdicao { get; set; } // cdEdicao
        public string dscodEmp { get; set; } // dscodEmp (length: 800)
        public string dsDC { get; set; } // dsDC (length: 800)
        public string dsPedido { get; set; } // dsPedido (length: 800)
        public string dsChave { get; set; } // dsChave (length: 800)
        public string dsTipPag { get; set; } // dsTipPag (length: 800)
        public string dsSitPag { get; set; } // dsSitPag (length: 800)
        public string dsDtPag { get; set; } // dsDtPag (length: 800)
        public string dsCodAut { get; set; } // dsCodAut (length: 800)
        public string dsNumId { get; set; } // dsNumId (length: 800)
        public string dsCompVend { get; set; } // dsCompVend (length: 800)
        public string dsTipCart { get; set; } // dsTipCart (length: 800)
    }

    // TB_DD_MonitoriaStone
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_MonitoriaStone
    {
        public int cdMonitoriaStone { get; set; } // cdMonitoriaStone (Primary key)
        public System.DateTime dtTransacao { get; set; } // dtTransacao
        public int cdPedido { get; set; } // cdPedido
        public string dsCodigoRetorno { get; set; } // dsCodigoRetorno (length: 800)
        public string dsChavePedido { get; set; } // dsChavePedido (length: 800)
        public string dsMensagem { get; set; } // dsMensagem (length: 800)
        public string MerchantKey { get; set; } // MerchantKey (length: 800)
        public string RequestKey { get; set; } // RequestKey (length: 800)
        public string AcquirerMessage { get; set; } // AcquirerMessage (length: 800)
        public string AcquirerName { get; set; } // AcquirerName (length: 800)
        public string AcquirerReturnCode { get; set; } // AcquirerReturnCode (length: 800)
        public string AffiliationCode { get; set; } // AffiliationCode (length: 800)
        public string AmountInCents { get; set; } // AmountInCents (length: 800)
        public string AuthorizationCode { get; set; } // AuthorizationCode (length: 800)
        public string AuthorizedAmountInCents { get; set; } // AuthorizedAmountInCents (length: 800)
        public string CapturedAmountInCents { get; set; } // CapturedAmountInCents (length: 800)
        public System.DateTime? CapturedDate { get; set; } // CapturedDate
        public string CreditCardBrand { get; set; } // CreditCardBrand (length: 800)
        public string InstantBuyKey { get; set; } // InstantBuyKey (length: 800)
        public string IsExpiredCreditCard { get; set; } // IsExpiredCreditCard (length: 800)
        public string MaskedCreditCardNumber { get; set; } // MaskedCreditCardNumber (length: 800)
        public string CreditCardOperation { get; set; } // CreditCardOperation (length: 800)
        public string CreditCardTransactionStatus { get; set; } // CreditCardTransactionStatus (length: 800)
        public string PaymentMethodName { get; set; } // PaymentMethodName (length: 800)
        public string TransactionIdentifier { get; set; } // TransactionIdentifier (length: 800)
        public string TransactionKey { get; set; } // TransactionKey (length: 800)
        public string TransactionKeyToAcquirer { get; set; } // TransactionKeyToAcquirer (length: 800)
        public string TransactionReference { get; set; } // TransactionReference (length: 800)
        public string UniqueSequentialNumber { get; set; } // UniqueSequentialNumber (length: 800)
        public string OrderKey { get; set; } // OrderKey (length: 800)
        public string OrderReference { get; set; } // OrderReference (length: 800)

        public TB_DD_MonitoriaStone()
        {
            dtTransacao = System.DateTime.Now;
        }
    }

    // TB_DD_Movimentacao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Movimentacao
    {
        public int cdMovimentacao { get; set; } // cdMovimentacao (Primary key)
        public int TB_Categoria_cdCategoria { get; set; } // TB_Categoria_cdCategoria
        public int TB_Empresa_cdEmpresaPrimario { get; set; } // TB_Empresa_cdEmpresaPrimario
        public int TB_Empresa_cdEmpresaSecundario { get; set; } // TB_Empresa_cdEmpresaSecundario
        public int? nuLimite { get; set; } // nuLimite
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public int? cdStand { get; set; } // cdStand

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_Movimentacao].([TB_Empresa_cdEmpresaPrimario]) (FK_TB_DD_Movimentacao_TB_DD_Empresa_cdEmpresaPrimario)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa_TB_Empresa_cdEmpresaPrimario { get; set; } // FK_TB_DD_Movimentacao_TB_DD_Empresa_cdEmpresaPrimario

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_Movimentacao].([TB_Empresa_cdEmpresaSecundario]) (FK_TB_DD_Movimentacao_TB_DD_Empresa_cdEmpresaSecundario)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa_TB_Empresa_cdEmpresaSecundario { get; set; } // FK_TB_DD_Movimentacao_TB_DD_Empresa_cdEmpresaSecundario

        /// <summary>
        /// Parent TB_DD_Stand pointed by [TB_DD_Movimentacao].([cdStand]) (FK_TB_DD_Movimentacao_TB_DD_Stand)
        /// </summary>
        public virtual TB_DD_Stand TB_DD_Stand { get; set; } // FK_TB_DD_Movimentacao_TB_DD_Stand

        /// <summary>
        /// Parent TB_RF_Categoria pointed by [TB_DD_Movimentacao].([TB_Categoria_cdCategoria]) (FK_TB_DD_Movimentacao_TB_RF_Categoria)
        /// </summary>
        public virtual TB_RF_Categoria TB_RF_Categoria { get; set; } // FK_TB_DD_Movimentacao_TB_RF_Categoria
    }

    // TB_DD_News
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_News
    {
        public int cdNews { get; set; } // cdNews (Primary key)
        public string dsTituloPortugues { get; set; } // dsTituloPortugues (length: 500)
        public string dsTituloIngles { get; set; } // dsTituloIngles (length: 500)
        public string dsTituloEspanhol { get; set; } // dsTituloEspanhol (length: 500)
        public string dsDescricaoPortugues { get; set; } // dsDescricaoPortugues (length: 500)
        public string dsDescricaoIngles { get; set; } // dsDescricaoIngles (length: 500)
        public string dsDescricaoEspanhol { get; set; } // dsDescricaoEspanhol (length: 500)
        public int cdSistema { get; set; } // cdSistema
        public System.DateTime dtInicio { get; set; } // dtInicio
        public System.DateTime dtFim { get; set; } // dtFim
        public int cdUsuarioInclusao { get; set; } // cdUsuarioInclusao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao
        public string fgHabilitado { get; set; } // fgHabilitado (length: 1)
    }

    // TB_DD_Pagamento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Pagamento
    {
        public int cdPagamento { get; set; } // cdPagamento (Primary key)
        public int? cdPedido { get; set; } // cdPedido
        public int? cdFormaPagamento { get; set; } // cdFormaPagamento
        public string dsValor { get; set; } // dsValor (length: 100)
        public string Status { get; set; } // Status (length: 100)
        public System.DateTime? dtInclusao { get; set; } // dtInclusao
        public System.DateTime? dtCancelamento { get; set; } // dtCancelamento
        public int? cdUsuarioInclusao { get; set; } // cdUsuarioInclusao
        public int? cdUsuarioCancelamento { get; set; } // cdUsuarioCancelamento

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_DD_Pagamento].([cdPedido]) (FK_TB_DD_Pagamento_cdPedido)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido { get; set; } // FK_TB_DD_Pagamento_cdPedido

        /// <summary>
        /// Parent TB_RF_FormaPagamento pointed by [TB_DD_Pagamento].([cdFormaPagamento]) (FK_TB_DD_Pagamento_cdFormaPagamento)
        /// </summary>
        public virtual TB_RF_FormaPagamento TB_RF_FormaPagamento { get; set; } // FK_TB_DD_Pagamento_cdFormaPagamento
    }

    // TB_DD_ParcelamentoBoleto
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ParcelamentoBoleto
    {
        public int cdBoleto { get; set; } // cdBoleto (Primary key)
        public int? cdPedido { get; set; } // cdPedido
        public int? nuParcela { get; set; } // nuParcela
        public System.DateTime? dtVencimento { get; set; } // dtVencimento
        public System.DateTime? dtPagamento { get; set; } // dtPagamento
        public int? cdUsuarioBaixa { get; set; } // cdUsuarioBaixa
        public string dsValorParcela { get; set; } // dsValorParcela
        public string dsSituacao { get; set; } // dsSituacao
        public string dsValorDesconto { get; set; } // dsValorDesconto
        public string dsValorOriginalParcela { get; set; } // dsValorOriginalParcela
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao
        public System.DateTime? dtCNAB { get; set; } // dtCNAB
        public int? cdControle { get; set; } // cdControle

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_DD_ParcelamentoBoleto].([cdPedido]) (FK_TB_DD_ParcelamentoBoleto_cdPedido)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido { get; set; } // FK_TB_DD_ParcelamentoBoleto_cdPedido

        public TB_DD_ParcelamentoBoleto()
        {
            dtCNAB = System.DateTime.Now;
        }
    }

    // TB_DD_Pedido
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Pedido
    {
        public int cdPedido { get; set; } // cdPedido (Primary key)
        public int? TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public int TB_Empresa_cdEmpresa { get; set; } // TB_Empresa_cdEmpresa
        public System.DateTime? dtPedido { get; set; } // dtPedido
        public System.DateTime? dtVencimento { get; set; } // dtVencimento
        public System.DateTime? dtPagamento { get; set; } // dtPagamento
        public string dsPedido { get; set; } // dsPedido (length: 100)
        public System.DateTime? dtCancelamento { get; set; } // dtCancelamento
        public string dsPago { get; set; } // dsPago (length: 10)
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public string vlTotal { get; set; } // vlTotal (length: 100)
        public string vlDesconto { get; set; } // vlDesconto (length: 100)
        public int? nuParcela { get; set; } // nuParcela
        public string TipoPessoaFaturamento { get; set; } // TipoPessoaFaturamento (length: 100)
        public string nmFaturamento { get; set; } // nmFaturamento
        public string dsCpfCnpjFaturamento { get; set; } // dsCpfCnpjFaturamento (length: 100)
        public string dsEnderecoFaturamento { get; set; } // dsEnderecoFaturamento (length: 100)
        public string dsCepfaturamento { get; set; } // dsCepfaturamento (length: 100)
        public string dsComplementoFaturamento { get; set; } // dsComplementoFaturamento (length: 100)
        public string dsCidadeFaturamento { get; set; } // dsCidadeFaturamento (length: 100)
        public string dsUfFaturamento { get; set; } // dsUfFaturamento (length: 100)
        public string dsInscricaoEstadualFaturamento { get; set; } // dsInscricaoEstadualFaturamento (length: 100)
        public int? cdPessoa { get; set; } // cdPessoa
        public string dsMoeda { get; set; } // dsMoeda
        public string dsEstorno { get; set; } // dsEstorno (length: 1)
        public string vlAcrescimo { get; set; } // vlAcrescimo
        public int? UsuarioBaixaPagamento { get; set; } // UsuarioBaixaPagamento
        public int? UsuarioCancelamento { get; set; } // UsuarioCancelamento
        public string dsObs { get; set; } // dsObs (length: 100)
        public int? cdTipoPedido { get; set; } // cdTipoPedido
        public string dsIdioma { get; set; } // dsIdioma (length: 100)
        public int? cdUsuario { get; set; } // cdUsuario
        public string dsNumeroFaturamento { get; set; } // dsNumeroFaturamento (length: 100)
        public string oTid { get; set; } // oTid (length: 800)
        public int? cdSistema { get; set; } // cdSistema
        public int? cdResponsavelCongresso { get; set; } // cdResponsavelCongresso
        public int? cdRegraDesconto { get; set; } // cdRegraDesconto
        public string dsTid { get; set; } // dsTid (length: 500)
        public string dsBandeiraCielo { get; set; } // dsBandeiraCielo (length: 100)
        public int? cdRegraDescontoPorProduto { get; set; } // cdRegraDescontoPorProduto
        public System.DateTime? dtCancelamentoAutomatico { get; set; } // dtCancelamentoAutomatico
        public string dsTipoEmpresa { get; set; } // dsTipoEmpresa (length: 100)
        public string dsDescontoAdministrativo { get; set; } // dsDescontoAdministrativo (length: 100)
        public string dsAcrescimoAdministrativo { get; set; } // dsAcrescimoAdministrativo (length: 100)
        public string dsLiberado { get; set; } // dsLiberado (length: 1)
        public string dsSaleIdPaypalPlus { get; set; } // dsSaleIdPaypalPlus (length: 200)
        public System.DateTime? dtSalesPaypalPlus { get; set; } // dtSalesPaypalPlus
        public string dsSalesStatusPayPalPlus { get; set; } // dsSalesStatusPayPalPlus (length: 500)
        public string dsIncideImposto { get; set; } // dsIncideImposto (length: 1)
        public string vlImposto { get; set; } // vlImposto (length: 500)
        public string vlParaCalculoImposto { get; set; } // vlParaCalculoImposto (length: 800)
        public string dsComentarioLiberacao { get; set; } // dsComentarioLiberacao (length: 800)
        public string dsTipoPedido { get; set; } // dsTipoPedido (length: 100)
        public string dsObservacaoInterna { get; set; } // dsObservacaoInterna (length: 2000)
        public string idPagamento { get; set; } // idPagamento (length: 100)

        // Reverse navigation

        /// <summary>
        /// Child TB_AS_CodigoPromocional_tb_Pedido where [TB_AS_CodigoPromocional_tb_Pedido].[cdPedido] point to this entity (PK_CodigoPromocional_Pedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_CodigoPromocional_tb_Pedido> TB_AS_CodigoPromocional_tb_Pedido { get; set; } // TB_AS_CodigoPromocional_tb_Pedido.PK_CodigoPromocional_Pedido
        /// <summary>
        /// Child TB_AS_Pedido_FormaPagamento where [TB_AS_Pedido_FormaPagamento].[TB_Pedido_cdPedido] point to this entity (FK_TB_AS_Pedido_FormaPagamento_TB_Pedido_cdPedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_Pedido_FormaPagamento> TB_AS_Pedido_FormaPagamento { get; set; } // TB_AS_Pedido_FormaPagamento.FK_TB_AS_Pedido_FormaPagamento_TB_Pedido_cdPedido
        /// <summary>
        /// Child TB_AS_Pedido_Recibo where [TB_AS_Pedido_Recibo].[cdPedido] point to this entity (FK_TB_AS_Pedido_Recibo_TB_DD_Pedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_Pedido_Recibo> TB_AS_Pedido_Recibo { get; set; } // TB_AS_Pedido_Recibo.FK_TB_AS_Pedido_Recibo_TB_DD_Pedido
        /// <summary>
        /// Child TB_DD_Cnab where [TB_DD_Cnab].[cdPedido] point to this entity (FK_TB_DD_Cnab_TB_DD_Pedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Cnab> TB_DD_Cnab { get; set; } // TB_DD_Cnab.FK_TB_DD_Cnab_TB_DD_Pedido
        /// <summary>
        /// Child TB_DD_Credencial where [TB_DD_Credencial].[TB_Pedido_cdPedido] point to this entity (FK_TB_DD_Credencial_TB_Pedido_cdPedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Credencial> TB_DD_Credencial { get; set; } // TB_DD_Credencial.FK_TB_DD_Credencial_TB_Pedido_cdPedido
        /// <summary>
        /// Child TB_DD_ImpostoAPAS where [TB_DD_ImpostoAPAS].[cdPedido] point to this entity (FK_ImpostoAPAS_Pedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ImpostoAPAS> TB_DD_ImpostoAPAS { get; set; } // TB_DD_ImpostoAPAS.FK_ImpostoAPAS_Pedido
        /// <summary>
        /// Child TB_DD_ItensPedidoCongresso where [TB_DD_ItensPedidoCongresso].[TB_Pedido_cdPedido] point to this entity (FK_TB_DD_ItensPedidoCongresso_TB_Pedido_cdPedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ItensPedidoCongresso> TB_DD_ItensPedidoCongresso { get; set; } // TB_DD_ItensPedidoCongresso.FK_TB_DD_ItensPedidoCongresso_TB_Pedido_cdPedido
        /// <summary>
        /// Child TB_DD_ItensPedidoOperacional where [TB_DD_ItensPedidoOperacional].[TB_Pedido_cdPedido] point to this entity (FK_TB_DD_ItensPedidoOperacional_TB_Pedido_cdPedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ItensPedidoOperacional> TB_DD_ItensPedidoOperacional { get; set; } // TB_DD_ItensPedidoOperacional.FK_TB_DD_ItensPedidoOperacional_TB_Pedido_cdPedido
        /// <summary>
        /// Child TB_DD_Pagamento where [TB_DD_Pagamento].[cdPedido] point to this entity (FK_TB_DD_Pagamento_cdPedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Pagamento> TB_DD_Pagamento { get; set; } // TB_DD_Pagamento.FK_TB_DD_Pagamento_cdPedido
        /// <summary>
        /// Child TB_DD_ParcelamentoBoleto where [TB_DD_ParcelamentoBoleto].[cdPedido] point to this entity (FK_TB_DD_ParcelamentoBoleto_cdPedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ParcelamentoBoleto> TB_DD_ParcelamentoBoleto { get; set; } // TB_DD_ParcelamentoBoleto.FK_TB_DD_ParcelamentoBoleto_cdPedido
        /// <summary>
        /// Child TB_DD_PlacaIdentificacao where [TB_DD_PlacaIdentificacao].[cdPedido] point to this entity (TB_DD_PlacaIdentificacao_cdPedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_PlacaIdentificacao> TB_DD_PlacaIdentificacao { get; set; } // TB_DD_PlacaIdentificacao.TB_DD_PlacaIdentificacao_cdPedido
        /// <summary>
        /// Child TB_DD_PlacaIdentificacaoColuna where [TB_DD_PlacaIdentificacaoColuna].[cdPedido] point to this entity (FK_TB_DD_PlacaIdentificacaoColuna_TB_DD_Pedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_PlacaIdentificacaoColuna> TB_DD_PlacaIdentificacaoColuna_cdPedido { get; set; } // TB_DD_PlacaIdentificacaoColuna.FK_TB_DD_PlacaIdentificacaoColuna_TB_DD_Pedido
        /// <summary>
        /// Child TB_DD_PlacaIdentificacaoColuna where [TB_DD_PlacaIdentificacaoColuna].[cdPedido] point to this entity (TB_DD_PlacaIdentificacaoColuna_cdPedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_PlacaIdentificacaoColuna> TB_DD_PlacaIdentificacaoColuna1 { get; set; } // TB_DD_PlacaIdentificacaoColuna.TB_DD_PlacaIdentificacaoColuna_cdPedido
        /// <summary>
        /// Child TB_DD_Ticket where [TB_DD_Ticket].[cdPedido] point to this entity (FK_Ticket_Pedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Ticket> TB_DD_Ticket { get; set; } // TB_DD_Ticket.FK_Ticket_Pedido
        /// <summary>
        /// Child TB_LG_GeralDepFinanceiro where [TB_LG_GeralDepFinanceiro].[cdPedidoAfetado] point to this entity (FK_GeralDepFinanceiro_cdPedidoAfetado)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_LG_GeralDepFinanceiro> TB_LG_GeralDepFinanceiro { get; set; } // TB_LG_GeralDepFinanceiro.FK_GeralDepFinanceiro_cdPedidoAfetado
        /// <summary>
        /// Child TB_RF_ImpostoPedido where [TB_RF_ImpostoPedido].[cdPedido] point to this entity (FK_TB_RF_ImpostoPedido_cdPedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_RF_ImpostoPedido> TB_RF_ImpostoPedido { get; set; } // TB_RF_ImpostoPedido.FK_TB_RF_ImpostoPedido_cdPedido
        /// <summary>
        /// Child TB_RF_PreenchimentoFormulario where [TB_RF_PreenchimentoFormulario].[TB_Pedido_cdPedido] point to this entity (FK_TB_RF_PreenchimentoFormulario_TB_DD_Pedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_RF_PreenchimentoFormulario> TB_RF_PreenchimentoFormulario { get; set; } // TB_RF_PreenchimentoFormulario.FK_TB_RF_PreenchimentoFormulario_TB_DD_Pedido

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_Pedido].([TB_Edicao_cdEdicao]) (FK_TB_DD_Pedido_TB_CF_Edicao_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_Pedido_TB_CF_Edicao_cdEdicao

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_Pedido].([TB_Empresa_cdEmpresa]) (FK_TB_DD_Pedido_TB_DD_Empresa_cdEmpresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_DD_Pedido_TB_DD_Empresa_cdEmpresa

        /// <summary>
        /// Parent TB_RF_TipoPedido pointed by [TB_DD_Pedido].([cdTipoPedido]) (FK_TB_DD_Pedido_TB_RF_TipoPedido)
        /// </summary>
        public virtual TB_RF_TipoPedido TB_RF_TipoPedido { get; set; } // FK_TB_DD_Pedido_TB_RF_TipoPedido

        public TB_DD_Pedido()
        {
            dsLiberado = "N";
            TB_AS_CodigoPromocional_tb_Pedido = new System.Collections.Generic.List<TB_AS_CodigoPromocional_tb_Pedido>();
            TB_AS_Pedido_FormaPagamento = new System.Collections.Generic.List<TB_AS_Pedido_FormaPagamento>();
            TB_AS_Pedido_Recibo = new System.Collections.Generic.List<TB_AS_Pedido_Recibo>();
            TB_DD_Cnab = new System.Collections.Generic.List<TB_DD_Cnab>();
            TB_DD_Credencial = new System.Collections.Generic.List<TB_DD_Credencial>();
            TB_DD_ImpostoAPAS = new System.Collections.Generic.List<TB_DD_ImpostoAPAS>();
            TB_DD_ItensPedidoCongresso = new System.Collections.Generic.List<TB_DD_ItensPedidoCongresso>();
            TB_DD_ItensPedidoOperacional = new System.Collections.Generic.List<TB_DD_ItensPedidoOperacional>();
            TB_DD_Pagamento = new System.Collections.Generic.List<TB_DD_Pagamento>();
            TB_DD_ParcelamentoBoleto = new System.Collections.Generic.List<TB_DD_ParcelamentoBoleto>();
            TB_DD_PlacaIdentificacao = new System.Collections.Generic.List<TB_DD_PlacaIdentificacao>();
            TB_DD_PlacaIdentificacaoColuna_cdPedido = new System.Collections.Generic.List<TB_DD_PlacaIdentificacaoColuna>();
            TB_DD_PlacaIdentificacaoColuna1 = new System.Collections.Generic.List<TB_DD_PlacaIdentificacaoColuna>();
            TB_DD_Ticket = new System.Collections.Generic.List<TB_DD_Ticket>();
            TB_LG_GeralDepFinanceiro = new System.Collections.Generic.List<TB_LG_GeralDepFinanceiro>();
            TB_RF_ImpostoPedido = new System.Collections.Generic.List<TB_RF_ImpostoPedido>();
            TB_RF_PreenchimentoFormulario = new System.Collections.Generic.List<TB_RF_PreenchimentoFormulario>();
        }
    }

    // TB_DD_Pergunta
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Pergunta
    {
        public int cdPergunta { get; set; } // cdPergunta (Primary key)
        public string dsPortugues { get; set; } // dsPortugues (length: 255)
        public string dsIngles { get; set; } // dsIngles (length: 255)
        public string dsEspanhol { get; set; } // dsEspanhol (length: 255)
        public string fgSituacao { get; set; } // fgSituacao (length: 1)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_Questionario where [TB_DD_Questionario].[TB_Pergunta_cdPergunta] point to this entity (FK_TB_DD_Questionario_TB_DD_Pergunta)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Questionario> TB_DD_Questionario { get; set; } // TB_DD_Questionario.FK_TB_DD_Questionario_TB_DD_Pergunta

        public TB_DD_Pergunta()
        {
            TB_DD_Questionario = new System.Collections.Generic.List<TB_DD_Questionario>();
        }
    }

    // TB_DD_PermissaoCorCarpeteColuna
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_PermissaoCorCarpeteColuna
    {
        public int cdPermissao { get; set; } // cdPermissao (Primary key)
        public int cdFormulario { get; set; } // cdFormulario
        public int cdContrato { get; set; } // cdContrato
        public string fgCarpete { get; set; } // fgCarpete (length: 1)
        public string fgColuna { get; set; } // fgColuna (length: 1)

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_FormularioManualEletronico pointed by [TB_DD_PermissaoCorCarpeteColuna].([cdFormulario]) (FK_TB_DD_PermissaoCorCarpeteColuna_TB_CF_FormularioManualEletronico)
        /// </summary>
        public virtual TB_CF_FormularioManualEletronico TB_CF_FormularioManualEletronico { get; set; } // FK_TB_DD_PermissaoCorCarpeteColuna_TB_CF_FormularioManualEletronico

        /// <summary>
        /// Parent TB_RF_TipoContrato pointed by [TB_DD_PermissaoCorCarpeteColuna].([cdContrato]) (FK_TB_DD_PermissaoCorCarpeteColuna_TB_RF_TipoContrato)
        /// </summary>
        public virtual TB_RF_TipoContrato TB_RF_TipoContrato { get; set; } // FK_TB_DD_PermissaoCorCarpeteColuna_TB_RF_TipoContrato
    }

    // TB_DD_Pessoa
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Pessoa
    {
        public int cdPessoa { get; set; } // cdPessoa (Primary key)
        public System.DateTime? dtInclusao { get; set; } // dtInclusao
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public string dsEmailLogin { get; set; } // dsEmailLogin (length: 100)
        public string dsSenha { get; set; } // dsSenha (length: 100)
        public string dsCPF { get; set; } // dsCPF (length: 100)
        public string dsRG { get; set; } // dsRG (length: 100)
        public string dsNome { get; set; } // dsNome
        public string dsSobreNome { get; set; } // dsSobreNome
        public string nmCompleto { get; set; } // nmCompleto
        public string nmCracha { get; set; } // nmCracha
        public int? dsTratamento { get; set; } // dsTratamento
        public string dsSexo { get; set; } // dsSexo
        public System.DateTime? dtNascimento { get; set; } // dtNascimento
        public string dsCargo { get; set; } // dsCargo (length: 100)
        public string dsProfissao { get; set; } // dsProfissao (length: 100)
        public string dsDepartamento { get; set; } // dsDepartamento (length: 100)
        public string dsLogradouroPessoal { get; set; } // dsLogradouroPessoal (length: 100)
        public string dsComplementoLogradouroPessoal { get; set; } // dsComplementoLogradouroPessoal (length: 100)
        public string dsNumeroLogradouroPessoal { get; set; } // dsNumeroLogradouroPessoal (length: 100)
        public string dsCepLogradouroPessoal { get; set; } // dsCepLogradouroPessoal (length: 100)
        public string dsBairroLogradouroPessoal { get; set; } // dsBairroLogradouroPessoal (length: 100)
        public string dsCidadeLogradouroPessoal { get; set; } // dsCidadeLogradouroPessoal (length: 100)
        public string dsUfLogradouroPessoal { get; set; } // dsUfLogradouroPessoal (length: 100)
        public int TB_Pais_cdPaisPessoal { get; set; } // TB_Pais_cdPaisPessoal
        public string dsSmsCelularPessoal { get; set; } // dsSmsCelularPessoal (length: 100)
        public string dsMmsCelularPessoal { get; set; } // dsMmsCelularPessoal (length: 100)
        public string dsAutorizaEnvioEmailPessoal { get; set; } // dsAutorizaEnvioEmailPessoal (length: 100)
        public string dsEmail { get; set; } // dsEmail (length: 100)
        public string dsFoto { get; set; } // dsFoto (length: 100)
        public string Cpts { get; set; } // Cpts (length: 30)
        public string dsEmpresaRepresentada { get; set; } // dsEmpresaRepresentada
        public string dsInternacional { get; set; } // dsInternacional
        public string dsIdiomaPreferencial { get; set; } // dsIdiomaPreferencial
        public string dsDDITelefonePessoa1 { get; set; } // dsDDITelefonePessoa1
        public string dsDDDTelefonePessoa1 { get; set; } // dsDDDTelefonePessoa1
        public string dsNumeroTelefonePessoa1 { get; set; } // dsNumeroTelefonePessoa1
        public string dsRamalTelefonePessoa1 { get; set; } // dsRamalTelefonePessoa1
        public string dsDDITelefonePessoa2 { get; set; } // dsDDITelefonePessoa2
        public string dsDDDTelefonePessoa2 { get; set; } // dsDDDTelefonePessoa2
        public string dsNumeroTelefonePessoa2 { get; set; } // dsNumeroTelefonePessoa2
        public string dsRamalTelefonePessoa2 { get; set; } // dsRamalTelefonePessoa2
        public string dsDDITelefoneCelular { get; set; } // dsDDITelefoneCelular
        public string dsDDDTelefoneCelular { get; set; } // dsDDDTelefoneCelular
        public string dsNumeroTelefoneCelular { get; set; } // dsNumeroTelefoneCelular
        public string dsObservacao { get; set; } // dsObservacao
        public string dsResPessoa1 { get; set; } // dsResPessoa1 (length: 800)
        public string dsResPessoa2 { get; set; } // dsResPessoa2 (length: 800)
        public string dsResPessoa3 { get; set; } // dsResPessoa3 (length: 800)
        public string dsResPessoa4 { get; set; } // dsResPessoa4 (length: 800)
        public string dsResPessoa5 { get; set; } // dsResPessoa5 (length: 800)
        public byte[] imageCredencial { get; set; } // imageCredencial
        public string dsCEPSebrae { get; set; } // dsCEPSebrae (length: 100)
        public string dsLogradouroSebrae { get; set; } // dsLogradouroSebrae (length: 800)
        public string dsComplementoSebrae { get; set; } // dsComplementoSebrae (length: 800)
        public int? cdPaisSebrae { get; set; } // cdPaisSebrae
        public int? cdEstadoSebrae { get; set; } // cdEstadoSebrae
        public int? cdCidadeSebrae { get; set; } // cdCidadeSebrae
        public int? cdBairroSebrae { get; set; } // cdBairroSebrae
        public string dsNumeroLogradouroSebrae { get; set; } // dsNumeroLogradouroSebrae (length: 100)
        public string dsInscricaoBlastu { get; set; } // dsInscricaoBlastu (length: 100)
        public System.DateTime? dtinscricaoBlastu { get; set; } // dtinscricaoBlastu
        public int? cdPessoaOriginal { get; set; } // cdPessoaOriginal
        public string CONTACT_ID_CRM { get; set; } // CONTACT_ID_CRM (length: 800)
        public string dsEmailCorporativo { get; set; } // dsEmailCorporativo (length: 800)
        public string dsAreaAPAS { get; set; } // dsAreaAPAS (length: 800)
        public System.Guid? cdPessoaCongresso { get; set; } // cdPessoaCongresso
        public string ContatoTipoId { get; set; } // ContatoTipoId (length: 100)
        public string dsSolenidadeConfirmada { get; set; } // dsSolenidadeConfirmada (length: 100)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_Convite where [TB_DD_Convite].[cdPessoa] point to this entity (FK_TB_DD_Convite_TB_DD_Pessoa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Convite> TB_DD_Convite { get; set; } // TB_DD_Convite.FK_TB_DD_Convite_TB_DD_Pessoa
        /// <summary>
        /// Child TB_DD_EntregaCredencialVIP where [TB_DD_EntregaCredencialVIP].[TB_Pessoa_cdPessoa] point to this entity (FK_TB_DD_EntregaCredencialVIP_Pessoa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_EntregaCredencialVIP> TB_DD_EntregaCredencialVIP { get; set; } // TB_DD_EntregaCredencialVIP.FK_TB_DD_EntregaCredencialVIP_Pessoa
        /// <summary>
        /// Child TB_DD_GrupoResponsavelCongresso where [TB_DD_GrupoResponsavelCongresso].[cdPessoa] point to this entity (FK_GrupoResponsavel_Pessoal)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_GrupoResponsavelCongresso> TB_DD_GrupoResponsavelCongresso { get; set; } // TB_DD_GrupoResponsavelCongresso.FK_GrupoResponsavel_Pessoal
        /// <summary>
        /// Child TB_DD_ItensPedidoCongresso where [TB_DD_ItensPedidoCongresso].[TB_Pessoa_cdPessoa] point to this entity (FK_TB_DD_ItensPedidoCongresso_TB_DD_Pessoa_cdPessoa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ItensPedidoCongresso> TB_DD_ItensPedidoCongresso { get; set; } // TB_DD_ItensPedidoCongresso.FK_TB_DD_ItensPedidoCongresso_TB_DD_Pessoa_cdPessoa
        /// <summary>
        /// Child TB_DD_Registro where [TB_DD_Registro].[TB_Pessoa_cdPessoa] point to this entity (FK_TB_DD_Registro_TB_DD_Pessoa)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Registro> TB_DD_Registro { get; set; } // TB_DD_Registro.FK_TB_DD_Registro_TB_DD_Pessoa

        // Foreign keys

        /// <summary>
        /// Parent TB_RF_Saudacao pointed by [TB_DD_Pessoa].([dsTratamento]) (FK_TB_DD_Pessoa_TB_RF_Saudacao1)
        /// </summary>
        public virtual TB_RF_Saudacao TB_RF_Saudacao { get; set; } // FK_TB_DD_Pessoa_TB_RF_Saudacao1

        public TB_DD_Pessoa()
        {
            TB_DD_Convite = new System.Collections.Generic.List<TB_DD_Convite>();
            TB_DD_EntregaCredencialVIP = new System.Collections.Generic.List<TB_DD_EntregaCredencialVIP>();
            TB_DD_GrupoResponsavelCongresso = new System.Collections.Generic.List<TB_DD_GrupoResponsavelCongresso>();
            TB_DD_ItensPedidoCongresso = new System.Collections.Generic.List<TB_DD_ItensPedidoCongresso>();
            TB_DD_Registro = new System.Collections.Generic.List<TB_DD_Registro>();
        }
    }

    // TB_DD_PlacaIdentificacao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_PlacaIdentificacao
    {
        public int cdPreenchimento { get; set; } // cdPreenchimento (Primary key)
        public System.DateTime? DataInclusao { get; set; } // DataInclusao
        public int? UsuarioInsercao { get; set; } // UsuarioInsercao
        public System.DateTime? DataAlteracao { get; set; } // DataAlteracao
        public int? UsuarioAlteracao { get; set; } // UsuarioAlteracao
        public int? cdEmpresa { get; set; } // cdEmpresa
        public int? cdStand { get; set; } // cdStand
        public string dsPlaca { get; set; } // dsPlaca (length: 255)
        public int? cdEdicao { get; set; } // cdEdicao
        public int? cdPedido { get; set; } // cdPedido

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_PlacaIdentificacao].([cdEdicao]) (TB_DD_PlacaIdentificacao_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // TB_DD_PlacaIdentificacao_cdEdicao

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_PlacaIdentificacao].([cdEmpresa]) (FK_TB_DD_PlacaIdentificacao_TB_DD_Empresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_DD_PlacaIdentificacao_TB_DD_Empresa

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_DD_PlacaIdentificacao].([cdPedido]) (TB_DD_PlacaIdentificacao_cdPedido)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido { get; set; } // TB_DD_PlacaIdentificacao_cdPedido

        /// <summary>
        /// Parent TB_DD_Stand pointed by [TB_DD_PlacaIdentificacao].([cdStand]) (TB_DD_PlacaIdentificacao_cdStand)
        /// </summary>
        public virtual TB_DD_Stand TB_DD_Stand { get; set; } // TB_DD_PlacaIdentificacao_cdStand
    }

    // TB_DD_PlacaIdentificacaoColuna
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_PlacaIdentificacaoColuna
    {
        public int cdPreenchimento { get; set; } // cdPreenchimento (Primary key)
        public System.DateTime? DataInclusao { get; set; } // DataInclusao
        public int? UsuarioInsercao { get; set; } // UsuarioInsercao
        public System.DateTime? DataAlteracao { get; set; } // DataAlteracao
        public int? UsuarioAlteracao { get; set; } // UsuarioAlteracao
        public int? cdEmpresa { get; set; } // cdEmpresa
        public int? cdStand { get; set; } // cdStand
        public string dsPlaca { get; set; } // dsPlaca (length: 255)
        public int? cdEdicao { get; set; } // cdEdicao
        public int? cdPedido { get; set; } // cdPedido

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_PlacaIdentificacaoColuna].([cdEdicao]) (FK_TB_DD_PlacaIdentificacaoColuna_TB_CF_Edicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_PlacaIdentificacaoColuna_TB_CF_Edicao

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_PlacaIdentificacaoColuna].([cdEmpresa]) (FK_TB_DD_PlacaIdentificacaoColuna_TB_DD_Empresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_DD_PlacaIdentificacaoColuna_TB_DD_Empresa

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_DD_PlacaIdentificacaoColuna].([cdPedido]) (FK_TB_DD_PlacaIdentificacaoColuna_TB_DD_Pedido)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido_cdPedido { get; set; } // FK_TB_DD_PlacaIdentificacaoColuna_TB_DD_Pedido

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_DD_PlacaIdentificacaoColuna].([cdPedido]) (TB_DD_PlacaIdentificacaoColuna_cdPedido)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido1 { get; set; } // TB_DD_PlacaIdentificacaoColuna_cdPedido

        /// <summary>
        /// Parent TB_DD_Stand pointed by [TB_DD_PlacaIdentificacaoColuna].([cdStand]) (FK_TB_DD_PlacaIdentificacaoColuna_TB_DD_Stand)
        /// </summary>
        public virtual TB_DD_Stand TB_DD_Stand { get; set; } // FK_TB_DD_PlacaIdentificacaoColuna_TB_DD_Stand
    }

    // TB_DD_PrecoProdutoCongresso
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_PrecoProdutoCongresso
    {
        public int cdPreco { get; set; } // cdPreco (Primary key)
        public int TB_ProdutoCongresso_cdProduto { get; set; } // TB_ProdutoCongresso_cdProduto
        public System.DateTime? dtLimite { get; set; } // dtLimite
        public string dsValor { get; set; } // dsValor (length: 100)
        public string dsValorUS { get; set; } // dsValorUS (length: 50)
        public string dsSituacao { get; set; } // dsSituacao (length: 50)
        public int? cdSubCategoria { get; set; } // cdSubCategoria
        public string dsCC { get; set; } // dsCC (length: 100)
        public string dsCR { get; set; } // dsCR (length: 100)
        public string dsCO { get; set; } // dsCO (length: 100)
        public string dsPR { get; set; } // dsPR (length: 100)
        public string dsCodigoNumerico { get; set; } // dsCodigoNumerico (length: 800)

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_ProdutoCongresso pointed by [TB_DD_PrecoProdutoCongresso].([TB_ProdutoCongresso_cdProduto]) (FK_TB_DD_PrecoProdutoCongresso_TB_DD_ProdutoCongresso)
        /// </summary>
        public virtual TB_DD_ProdutoCongresso TB_DD_ProdutoCongresso { get; set; } // FK_TB_DD_PrecoProdutoCongresso_TB_DD_ProdutoCongresso
    }

    // TB_DD_PrecoProdutoOperacional
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_PrecoProdutoOperacional
    {
        public int cdPreco { get; set; } // cdPreco (Primary key)
        public int TB_ProdutoOperacional_cdProduto { get; set; } // TB_ProdutoOperacional_cdProduto
        public System.DateTime? dtLimite { get; set; } // dtLimite
        public string dsValor { get; set; } // dsValor (length: 100)
        public string dsValorUS { get; set; } // dsValorUS (length: 100)
        public string dsSituacao { get; set; } // dsSituacao (length: 10)

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_ProdutoOperacional pointed by [TB_DD_PrecoProdutoOperacional].([TB_ProdutoOperacional_cdProduto]) (FK_TB_DD_PrecoProdutoOperacional_TB_DD_ProdutoOperacional)
        /// </summary>
        public virtual TB_DD_ProdutoOperacional TB_DD_ProdutoOperacional { get; set; } // FK_TB_DD_PrecoProdutoOperacional_TB_DD_ProdutoOperacional
    }

    // TB_DD_PresencaCampanhaSejaBemVindo
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_PresencaCampanhaSejaBemVindo
    {
        public int cdPresencaCampanha { get; set; } // cdPresencaCampanha (Primary key)
        public string dsCodigoBarras { get; set; } // dsCodigoBarras (length: 200)
        public System.DateTime dtInclusao { get; set; } // dtInclusao
        public int? cdDialogue { get; set; } // cdDialogue
        public System.DateTime? dtDialogue { get; set; } // dtDialogue
    }

    // TB_DD_ProdutoCongresso
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ProdutoCongresso
    {
        public int cdProduto { get; set; } // cdProduto (Primary key)
        public int? cdEdicao { get; set; } // cdEdicao
        public string nmProdutoPortugues { get; set; } // nmProdutoPortugues
        public string nmProdutoIngles { get; set; } // nmProdutoIngles
        public string dsProdutoPortugues { get; set; } // dsProdutoPortugues
        public string dsProdutoIngles { get; set; } // dsProdutoIngles
        public string dsGrupo { get; set; } // dsGrupo
        public string dsSituacao { get; set; } // dsSituacao (length: 50)
        public string nmProdutoEspanhol { get; set; } // nmProdutoEspanhol
        public string dsProdutoEspanhol { get; set; } // dsProdutoEspanhol
        public int? nuOrdem { get; set; } // nuOrdem
        public string TipoProduto { get; set; } // TipoProduto (length: 50)
        public string dsSigla { get; set; } // dsSigla (length: 100)
        public string dsDia { get; set; } // dsDia (length: 100)
        public int? cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public System.DateTime? dtCadastro { get; set; } // dtCadastro
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public System.DateTime? dtLimite { get; set; } // dtLimite
        public int? cdSistema { get; set; } // cdSistema
        public int? cdGrupoProdutoCongresso { get; set; } // cdGrupoProdutoCongresso
        public int? nuVagas { get; set; } // nuVagas
        public string dsProdutoCongressoPagote { get; set; } // dsProdutoCongressoPagote (length: 500)
        public int? nuSubProduto { get; set; } // nuSubProduto
        public int? nuIdGrupo { get; set; } // nuIdGrupo
        public int? nuQtdColunaSubProduto { get; set; } // nuQtdColunaSubProduto
        public string dsQtdCargaHoraria { get; set; } // dsQtdCargaHoraria (length: 50)
        public System.DateTime? dtPalestra { get; set; } // dtPalestra
        public string dsLocalizacao { get; set; } // dsLocalizacao (length: 100)
        public System.DateTime? dtPalestraFim { get; set; } // dtPalestraFim
        public int? qtdImpressao { get; set; } // qtdImpressao
        public string dsSala { get; set; } // dsSala (length: 500)
        public string dsCompraObrigatoria { get; set; } // dsCompraObrigatoria (length: 1)
        public string dsCC { get; set; } // dsCC
        public string dsCR { get; set; } // dsCR
        public string dsCO { get; set; } // dsCO

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_ItensPedidoCongresso where [TB_DD_ItensPedidoCongresso].[TB_ProdutoCongresso_cdProduto] point to this entity (FK_TB_DD_ItensPedidoCongresso_TB_DD_ProdutoCongresso_cdProduto)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ItensPedidoCongresso> TB_DD_ItensPedidoCongresso { get; set; } // TB_DD_ItensPedidoCongresso.FK_TB_DD_ItensPedidoCongresso_TB_DD_ProdutoCongresso_cdProduto
        /// <summary>
        /// Child TB_DD_PrecoProdutoCongresso where [TB_DD_PrecoProdutoCongresso].[TB_ProdutoCongresso_cdProduto] point to this entity (FK_TB_DD_PrecoProdutoCongresso_TB_DD_ProdutoCongresso)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_PrecoProdutoCongresso> TB_DD_PrecoProdutoCongresso { get; set; } // TB_DD_PrecoProdutoCongresso.FK_TB_DD_PrecoProdutoCongresso_TB_DD_ProdutoCongresso
        /// <summary>
        /// Child TB_DD_Ticket where [TB_DD_Ticket].[cdProduto] point to this entity (FK_Ticket_ProdutoCongresso)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Ticket> TB_DD_Ticket { get; set; } // TB_DD_Ticket.FK_Ticket_ProdutoCongresso

        public TB_DD_ProdutoCongresso()
        {
            TB_DD_ItensPedidoCongresso = new System.Collections.Generic.List<TB_DD_ItensPedidoCongresso>();
            TB_DD_PrecoProdutoCongresso = new System.Collections.Generic.List<TB_DD_PrecoProdutoCongresso>();
            TB_DD_Ticket = new System.Collections.Generic.List<TB_DD_Ticket>();
        }
    }

    // TB_DD_ProdutoExpositores
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ProdutoExpositores
    {
        public int cdProdutoExpositores { get; set; } // cdProdutoExpositores (Primary key)
        public string dsProdutoExpositorPortugues { get; set; } // dsProdutoExpositorPortugues (length: 800)
        public string dsProdutoExpositorIngles { get; set; } // dsProdutoExpositorIngles (length: 800)
        public System.DateTime dtInclusao { get; set; } // dtInclusao
        public int cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public int cdEdicao { get; set; } // cdEdicao
        public string dsSituacao { get; set; } // dsSituacao (length: 10)
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_ProdutoExpositores].([cdEdicao]) (FK_TB_DD_ProdutoExpositores_TB_CF_Edicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_ProdutoExpositores_TB_CF_Edicao
    }

    // TB_DD_ProdutoOperacional
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ProdutoOperacional
    {
        public int cdProduto { get; set; } // cdProduto (Primary key)
        public int TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public int? nuOrdem { get; set; } // nuOrdem
        public string dsProduto { get; set; } // dsProduto
        public string dsProdutoIngles { get; set; } // dsProdutoIngles
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public string SiglaImagem { get; set; } // SiglaImagem
        public int? cdFormulario { get; set; } // cdFormulario
        public int? cdCategoria { get; set; } // cdCategoria
        public string dsTipoRegra { get; set; } // dsTipoRegra (length: 50)
        public string dsQuantidade { get; set; } // dsQuantidade
        public string vlAdicional { get; set; } // vlAdicional (length: 100)
        public string dsFecharPedidoAutomatico { get; set; } // dsFecharPedidoAutomatico (length: 1)
        public string dsIncideImposto { get; set; } // dsIncideImposto (length: 1)
        public string dsCC { get; set; } // dsCC
        public string dsCR { get; set; } // dsCR
        public string dsCO { get; set; } // dsCO

        // Reverse navigation

        /// <summary>
        /// Child TB_AS_RegraMarketingSimples_ProdutoOperacional where [TB_AS_RegraMarketingSimples_ProdutoOperacional].[cdProduto] point to this entity (FK_RegraMarketingSimples_ProdutoOperacional_Produto)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_RegraMarketingSimples_ProdutoOperacional> TB_AS_RegraMarketingSimples_ProdutoOperacional { get; set; } // TB_AS_RegraMarketingSimples_ProdutoOperacional.FK_RegraMarketingSimples_ProdutoOperacional_Produto
        /// <summary>
        /// Child TB_CF_FormularioGenerico where [TB_CF_FormularioGenerico].[cdProdutoOperacional] point to this entity (FK_FormularioGenerico_ProdutoOperacional)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_CF_FormularioGenerico> TB_CF_FormularioGenerico { get; set; } // TB_CF_FormularioGenerico.FK_FormularioGenerico_ProdutoOperacional
        /// <summary>
        /// Child TB_CF_VendaDados where [TB_CF_VendaDados].[cdProdutoOperacional] point to this entity (PK_VendaDados_ProdutoOperacional)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_CF_VendaDados> TB_CF_VendaDados { get; set; } // TB_CF_VendaDados.PK_VendaDados_ProdutoOperacional
        /// <summary>
        /// Child TB_DD_BloqueioContratoProduto where [TB_DD_BloqueioContratoProduto].[cdProduto] point to this entity (FK_TB_DD_BloqueioContratoProduto_cdProduto)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_BloqueioContratoProduto> TB_DD_BloqueioContratoProduto { get; set; } // TB_DD_BloqueioContratoProduto.FK_TB_DD_BloqueioContratoProduto_cdProduto
        /// <summary>
        /// Child TB_DD_CarrinhoOperacional where [TB_DD_CarrinhoOperacional].[cdProduto] point to this entity (FK_TB_DD_CarrinhoOperacional_TB_DD_ProdutoOperacional)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_CarrinhoOperacional> TB_DD_CarrinhoOperacional { get; set; } // TB_DD_CarrinhoOperacional.FK_TB_DD_CarrinhoOperacional_TB_DD_ProdutoOperacional
        /// <summary>
        /// Child TB_DD_ItensPedidoOperacional where [TB_DD_ItensPedidoOperacional].[TB_ProdutoOperacional_cdProduto] point to this entity (FK_TB_DD_ItensPedidoOperacional_TB_DD_ProdutoOperacional_cdProduto)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ItensPedidoOperacional> TB_DD_ItensPedidoOperacional { get; set; } // TB_DD_ItensPedidoOperacional.FK_TB_DD_ItensPedidoOperacional_TB_DD_ProdutoOperacional_cdProduto
        /// <summary>
        /// Child TB_DD_PrecoProdutoOperacional where [TB_DD_PrecoProdutoOperacional].[TB_ProdutoOperacional_cdProduto] point to this entity (FK_TB_DD_PrecoProdutoOperacional_TB_DD_ProdutoOperacional)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_PrecoProdutoOperacional> TB_DD_PrecoProdutoOperacional { get; set; } // TB_DD_PrecoProdutoOperacional.FK_TB_DD_PrecoProdutoOperacional_TB_DD_ProdutoOperacional
        /// <summary>
        /// Child TB_DD_QuestionarioManual where [TB_DD_QuestionarioManual].[cdProdutoOperacional] point to this entity (FK_TB_DD_QuestionarioManual_TB_DD_ProdutoOperacional)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_QuestionarioManual> TB_DD_QuestionarioManual { get; set; } // TB_DD_QuestionarioManual.FK_TB_DD_QuestionarioManual_TB_DD_ProdutoOperacional

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_ProdutoOperacional].([TB_Edicao_cdEdicao]) (FK_TB_DD_ProdutoOperacional_TB_CF_Edicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_ProdutoOperacional_TB_CF_Edicao

        /// <summary>
        /// Parent TB_RF_Categoria pointed by [TB_DD_ProdutoOperacional].([cdCategoria]) (FK_TB_DD_ProdutoOperacional_TB_RF_Categoria)
        /// </summary>
        public virtual TB_RF_Categoria TB_RF_Categoria { get; set; } // FK_TB_DD_ProdutoOperacional_TB_RF_Categoria

        public TB_DD_ProdutoOperacional()
        {
            TB_AS_RegraMarketingSimples_ProdutoOperacional = new System.Collections.Generic.List<TB_AS_RegraMarketingSimples_ProdutoOperacional>();
            TB_CF_FormularioGenerico = new System.Collections.Generic.List<TB_CF_FormularioGenerico>();
            TB_CF_VendaDados = new System.Collections.Generic.List<TB_CF_VendaDados>();
            TB_DD_BloqueioContratoProduto = new System.Collections.Generic.List<TB_DD_BloqueioContratoProduto>();
            TB_DD_CarrinhoOperacional = new System.Collections.Generic.List<TB_DD_CarrinhoOperacional>();
            TB_DD_ItensPedidoOperacional = new System.Collections.Generic.List<TB_DD_ItensPedidoOperacional>();
            TB_DD_PrecoProdutoOperacional = new System.Collections.Generic.List<TB_DD_PrecoProdutoOperacional>();
            TB_DD_QuestionarioManual = new System.Collections.Generic.List<TB_DD_QuestionarioManual>();
        }
    }

    // TB_DD_Questionario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Questionario
    {
        public int cdQuestionario { get; set; } // cdQuestionario (Primary key)
        public int? nuPergunta { get; set; } // nuPergunta
        public int TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public int TB_Pergunta_cdPergunta { get; set; } // TB_Pergunta_cdPergunta
        public string dsTipoAlternativa { get; set; } // dsTipoAlternativa (length: 100)
        public string dsObrigatoria { get; set; } // dsObrigatoria (length: 100)
        public int? TB_TipoCredenciamento_cdTipo { get; set; } // TB_TipoCredenciamento_cdTipo
        public int cdSistema { get; set; } // cdSistema
        public string fgSituacao { get; set; } // fgSituacao (length: 100)
        public System.DateTime? dtCancelado { get; set; } // dtCancelado
        public int? cdUsuarioCancelado { get; set; } // cdUsuarioCancelado

        // Reverse navigation

        /// <summary>
        /// Child TB_AS_Alternativa_Questionario where [TB_AS_Alternativa_Questionario].[cdQuestionario] point to this entity (FK_TB_AS_Alternativa_Questionario_TB_DD_Questionario)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_Alternativa_Questionario> TB_AS_Alternativa_Questionario { get; set; } // TB_AS_Alternativa_Questionario.FK_TB_AS_Alternativa_Questionario_TB_DD_Questionario
        /// <summary>
        /// Child TB_DD_Resposta where [TB_DD_Resposta].[cdQuestionario] point to this entity (FK_TB_DD_Resposta_TB_DD_Questionario)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Resposta> TB_DD_Resposta { get; set; } // TB_DD_Resposta.FK_TB_DD_Resposta_TB_DD_Questionario

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Pergunta pointed by [TB_DD_Questionario].([TB_Pergunta_cdPergunta]) (FK_TB_DD_Questionario_TB_DD_Pergunta)
        /// </summary>
        public virtual TB_DD_Pergunta TB_DD_Pergunta { get; set; } // FK_TB_DD_Questionario_TB_DD_Pergunta

        public TB_DD_Questionario()
        {
            TB_AS_Alternativa_Questionario = new System.Collections.Generic.List<TB_AS_Alternativa_Questionario>();
            TB_DD_Resposta = new System.Collections.Generic.List<TB_DD_Resposta>();
        }
    }

    // TB_DD_QuestionarioManual
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_QuestionarioManual
    {
        public int cdQuestionarioManual { get; set; } // cdQuestionarioManual (Primary key)
        public int cdProdutoOperacional { get; set; } // cdProdutoOperacional
        public int nuPergunta { get; set; } // nuPergunta
        public string dsPortugues { get; set; } // dsPortugues (length: 800)
        public string dsIngles { get; set; } // dsIngles (length: 800)
        public string dsEspanhol { get; set; } // dsEspanhol (length: 800)
        public string dsTipoAlternativa { get; set; } // dsTipoAlternativa (length: 1)
        public string dsObrigatoria { get; set; } // dsObrigatoria (length: 1)
        public string dsSituacao { get; set; } // dsSituacao (length: 800)
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public int cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_AlternativaQuestionarioManual where [TB_DD_AlternativaQuestionarioManual].[cdQuestionarioManual] point to this entity (FK_TB_DD_AlternativaQuestionarioManual_TB_DD_QuestionarioManual)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_AlternativaQuestionarioManual> TB_DD_AlternativaQuestionarioManual { get; set; } // TB_DD_AlternativaQuestionarioManual.FK_TB_DD_AlternativaQuestionarioManual_TB_DD_QuestionarioManual
        /// <summary>
        /// Child TB_DD_RespostaQuestionarioManual where [TB_DD_RespostaQuestionarioManual].[cdQuestionarioManual] point to this entity (FK_TB_DD_RespostaQuestionarioManual_TB_DD_QuestionarioManual)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_RespostaQuestionarioManual> TB_DD_RespostaQuestionarioManual { get; set; } // TB_DD_RespostaQuestionarioManual.FK_TB_DD_RespostaQuestionarioManual_TB_DD_QuestionarioManual

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_ProdutoOperacional pointed by [TB_DD_QuestionarioManual].([cdProdutoOperacional]) (FK_TB_DD_QuestionarioManual_TB_DD_ProdutoOperacional)
        /// </summary>
        public virtual TB_DD_ProdutoOperacional TB_DD_ProdutoOperacional { get; set; } // FK_TB_DD_QuestionarioManual_TB_DD_ProdutoOperacional

        public TB_DD_QuestionarioManual()
        {
            TB_DD_AlternativaQuestionarioManual = new System.Collections.Generic.List<TB_DD_AlternativaQuestionarioManual>();
            TB_DD_RespostaQuestionarioManual = new System.Collections.Generic.List<TB_DD_RespostaQuestionarioManual>();
        }
    }

    // TB_DD_ReenvioConvite
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ReenvioConvite
    {
        public int cdReenvio { get; set; } // cdReenvio (Primary key)
        public int IdEmpresa { get; set; } // IdEmpresa
        public System.DateTime? Permissao { get; set; } // Permissao

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_ReenvioConvite].([IdEmpresa]) (FK_TB_DD_ReenvioConvite_TB_DD_Empresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_DD_ReenvioConvite_TB_DD_Empresa
    }

    // TB_DD_ReenvioConviteVip
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ReenvioConviteVip
    {
        public int cdReenvio { get; set; } // cdReenvio (Primary key)
        public int? IdEmpresa { get; set; } // IdEmpresa
        public System.DateTime? Permissao { get; set; } // Permissao

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_ReenvioConviteVip].([IdEmpresa]) (FK_TB_DD_ReenvioConviteVip_TB_DD_Empresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_DD_ReenvioConviteVip_TB_DD_Empresa
    }

    // TB_DD_Registro
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Registro
    {
        public int cdRegistro { get; set; } // cdRegistro (Primary key)
        public string dsCodigoBarras { get; set; } // dsCodigoBarras (length: 100)
        public System.DateTime? dtInclusaoRegistro { get; set; } // dtInclusaoRegistro
        public int TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public int TB_Categoria_cdCategoria { get; set; } // TB_Categoria_cdCategoria
        public int TB_Pessoa_cdPessoa { get; set; } // TB_Pessoa_cdPessoa
        public int TB_Empresa_cdEmpresa { get; set; } // TB_Empresa_cdEmpresa
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public string dsPresenteEvento { get; set; } // dsPresenteEvento (length: 10)
        public string dsPresenteDia1 { get; set; } // dsPresenteDia1 (length: 1)
        public string dsPresenteDia2 { get; set; } // dsPresenteDia2 (length: 1)
        public string dsPresenteDia3 { get; set; } // dsPresenteDia3 (length: 1)
        public string dsPresenteDia4 { get; set; } // dsPresenteDia4 (length: 1)
        public string dsPresenteDia5 { get; set; } // dsPresenteDia5 (length: 1)
        public string dsPresenteDia6 { get; set; } // dsPresenteDia6 (length: 1)
        public string dsPresenteDia7 { get; set; } // dsPresenteDia7 (length: 1)
        public string dsPresenteDia8 { get; set; } // dsPresenteDia8 (length: 1)
        public string dsPresenteDia9 { get; set; } // dsPresenteDia9 (length: 1)
        public string dsPresenteDia10 { get; set; } // dsPresenteDia10 (length: 1)
        public int? TB_Pedido_cdPedido { get; set; } // TB_Pedido_cdPedido
        public string dsOrigemConvite { get; set; } // dsOrigemConvite (length: 100)
        public int? CdEmpresaExpositora { get; set; } // CdEmpresaExpositora
        public string dsOrigemInsercao { get; set; } // dsOrigemInsercao (length: 100)
        public int? cdSubCategoria { get; set; } // cdSubCategoria
        public string dsSubCategoriaValidacao { get; set; } // dsSubCategoriaValidacao (length: 800)
        public int? cdSistema { get; set; } // cdSistema
        public int? cdFormularioEasy { get; set; } // cdFormularioEasy
        public string dsSituacaoRegistro { get; set; } // dsSituacaoRegistro (length: 15)
        public System.DateTime? dtCancelaRegistro { get; set; } // dtCancelaRegistro
        public int? cdUsuarioCancelaRegistro { get; set; } // cdUsuarioCancelaRegistro
        public string fgEmitidoCertificado { get; set; } // fgEmitidoCertificado (length: 1)
        public System.DateTime? dtEmissaoCertificado { get; set; } // dtEmissaoCertificado
        public int? cdConviteAmigo { get; set; } // cdConviteAmigo
        public string dsTipoConvite { get; set; } // dsTipoConvite (length: 800)
        public string dsIdioma { get; set; } // dsIdioma (length: 100)
        public string dsTipoInscricao { get; set; } // dsTipoInscricao (length: 1)

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_Registro].([TB_Edicao_cdEdicao]) (FK_TB_DD_Registro_TB_CF_Edicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_Registro_TB_CF_Edicao

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_Registro].([TB_Empresa_cdEmpresa]) (FK_TB_DD_Registro_TB_DD_Empresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_DD_Registro_TB_DD_Empresa

        /// <summary>
        /// Parent TB_DD_Pessoa pointed by [TB_DD_Registro].([TB_Pessoa_cdPessoa]) (FK_TB_DD_Registro_TB_DD_Pessoa)
        /// </summary>
        public virtual TB_DD_Pessoa TB_DD_Pessoa { get; set; } // FK_TB_DD_Registro_TB_DD_Pessoa

        /// <summary>
        /// Parent TB_RF_Categoria pointed by [TB_DD_Registro].([TB_Categoria_cdCategoria]) (FK_TB_DD_Registro_TB_RF_Categoria)
        /// </summary>
        public virtual TB_RF_Categoria TB_RF_Categoria { get; set; } // FK_TB_DD_Registro_TB_RF_Categoria

        public TB_DD_Registro()
        {
            dtInclusaoRegistro = System.DateTime.Now;
            dsSituacaoRegistro = "ATIVO";
        }
    }

    // TB_DD_Regulamento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Regulamento
    {
        public int cdRegulamento { get; set; } // cdRegulamento (Primary key)
        public int? cdEdicao { get; set; } // cdEdicao
        public System.DateTime? DataInclusao { get; set; } // DataInclusao
        public int? UsuarioInsercao { get; set; } // UsuarioInsercao
        public System.DateTime? DataCancelamento { get; set; } // DataCancelamento
        public int? UsuarioCancelamento { get; set; } // UsuarioCancelamento
        public string UrlArquivo { get; set; } // UrlArquivo (length: 255)
        public string dsArquivo { get; set; } // dsArquivo (length: 255)
        public string dsIdioma { get; set; } // dsIdioma (length: 10)
        public string dsSituacao { get; set; } // dsSituacao (length: 20)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_LeituraRegulamento where [TB_DD_LeituraRegulamento].[cdRegulamento] point to this entity (FK_TB_DD_LeituraRegulamento_TB_DD_Regulamento)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_LeituraRegulamento> TB_DD_LeituraRegulamento_cdRegulamento { get; set; } // TB_DD_LeituraRegulamento.FK_TB_DD_LeituraRegulamento_TB_DD_Regulamento
        /// <summary>
        /// Child TB_DD_LeituraRegulamento where [TB_DD_LeituraRegulamento].[cdRegulamento] point to this entity (FK_TB_DD_LeituraRegulamento_cdRegulamento)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_LeituraRegulamento> TB_DD_LeituraRegulamento1 { get; set; } // TB_DD_LeituraRegulamento.FK_TB_DD_LeituraRegulamento_cdRegulamento

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_Regulamento].([cdEdicao]) (FK_TB_DD_Regulamento_TB_CF_Edicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_Regulamento_TB_CF_Edicao

        public TB_DD_Regulamento()
        {
            TB_DD_LeituraRegulamento_cdRegulamento = new System.Collections.Generic.List<TB_DD_LeituraRegulamento>();
            TB_DD_LeituraRegulamento1 = new System.Collections.Generic.List<TB_DD_LeituraRegulamento>();
        }
    }

    // TB_DD_Relatorio
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Relatorio
    {
        public int cdRelatorio { get; set; } // cdRelatorio (Primary key)
        public int? cdTipoRelatorio { get; set; } // cdTipoRelatorio
        public string dsRelatorio { get; set; } // dsRelatorio (length: 100)
        public string dsPaginaParametros { get; set; } // dsPaginaParametros (length: 100)
        public string dsHabilitado { get; set; } // dsHabilitado (length: 1)
        public int? cdEdicao { get; set; } // cdEdicao
        public int? nuOrdem { get; set; } // nuOrdem

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_Relatorio].([cdEdicao]) (FK_TB_DD_Relatorio_TB_CF_Edicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_Relatorio_TB_CF_Edicao

        /// <summary>
        /// Parent TB_RF_TipoRelatorio pointed by [TB_DD_Relatorio].([cdTipoRelatorio]) (FK_TB_DD_Relatorio_TB_RF_TipoRelatorio)
        /// </summary>
        public virtual TB_RF_TipoRelatorio TB_RF_TipoRelatorio { get; set; } // FK_TB_DD_Relatorio_TB_RF_TipoRelatorio
    }

    // TB_DD_ResponsavelCongresso
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ResponsavelCongresso
    {
        public int cdResponsavelCongresso { get; set; } // cdResponsavelCongresso (Primary key)
        public System.DateTime dtInclusao { get; set; } // dtInclusao
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public string dsNomeResponsavel { get; set; } // dsNomeResponsavel (length: 800)
        public string dsSobreNomeResponsavel { get; set; } // dsSobreNomeResponsavel (length: 800)
        public string dsCargoResponsavel { get; set; } // dsCargoResponsavel (length: 200)
        public string dsDepartamentoResponsavel { get; set; } // dsDepartamentoResponsavel (length: 500)
        public string dsEmailLoginResponsavel { get; set; } // dsEmailLoginResponsavel (length: 200)
        public string dsSenhaResponsavel { get; set; } // dsSenhaResponsavel (length: 200)
        public string dsDDITelefoneResponsavel { get; set; } // dsDDITelefoneResponsavel (length: 200)
        public string dsDDDTelefoneResponsavel { get; set; } // dsDDDTelefoneResponsavel (length: 200)
        public string dsNumeroTelefoneResponsavel { get; set; } // dsNumeroTelefoneResponsavel (length: 200)
        public string dsRamalTelefoneResponsavel { get; set; } // dsRamalTelefoneResponsavel (length: 200)
        public string dsDDICelularResponsavel { get; set; } // dsDDICelularResponsavel (length: 200)
        public string dsDDDCelularResponsavel { get; set; } // dsDDDCelularResponsavel (length: 200)
        public string dsNumeroCelularResponsavel { get; set; } // dsNumeroCelularResponsavel (length: 200)
        public string dsRazaoSocialResponsavel { get; set; } // dsRazaoSocialResponsavel (length: 500)
        public string dsNomeFantasiaResponsavel { get; set; } // dsNomeFantasiaResponsavel (length: 500)
        public string dsLogradouroResponsavel { get; set; } // dsLogradouroResponsavel (length: 200)
        public string dsComplementoLogradouroResponsavel { get; set; } // dsComplementoLogradouroResponsavel (length: 200)
        public string dsNumeroLogradouroResponsavel { get; set; } // dsNumeroLogradouroResponsavel (length: 200)
        public string dsCepLogradouroResponsavel { get; set; } // dsCepLogradouroResponsavel (length: 200)
        public string dsBairroLogradouroResponsavel { get; set; } // dsBairroLogradouroResponsavel (length: 200)
        public string dsCidadeLogradouroResponsavel { get; set; } // dsCidadeLogradouroResponsavel (length: 200)
        public string dsUfLogradouroResponsavel { get; set; } // dsUfLogradouroResponsavel (length: 200)
        public int TB_Pais_cdPaisResponsavel { get; set; } // TB_Pais_cdPaisResponsavel
        public string dsRes1Responsavel { get; set; } // dsRes1Responsavel (length: 800)
        public string dsRes2Responsavel { get; set; } // dsRes2Responsavel (length: 800)
        public string dsRes3Responsavel { get; set; } // dsRes3Responsavel (length: 800)
        public string dsRes4Responsavel { get; set; } // dsRes4Responsavel (length: 800)
        public string dsRes5Responsavel { get; set; } // dsRes5Responsavel (length: 800)
        public string dsAutorizaEnvioEmailPessoal { get; set; } // dsAutorizaEnvioEmailPessoal (length: 1)
        public string dsSmsCelularPessoal { get; set; } // dsSmsCelularPessoal (length: 1)
        public string dsEmailLoginResponsavelAutenticacao { get; set; } // dsEmailLoginResponsavelAutenticacao (length: 200)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_GrupoResponsavelCongresso where [TB_DD_GrupoResponsavelCongresso].[cdResponsavelCongresso] point to this entity (FK_GrupoResponsavel_Responsavel)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_GrupoResponsavelCongresso> TB_DD_GrupoResponsavelCongresso { get; set; } // TB_DD_GrupoResponsavelCongresso.FK_GrupoResponsavel_Responsavel

        public TB_DD_ResponsavelCongresso()
        {
            TB_DD_GrupoResponsavelCongresso = new System.Collections.Generic.List<TB_DD_GrupoResponsavelCongresso>();
        }
    }

    // TB_DD_ResponsavelEmpresa
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ResponsavelEmpresa
    {
        public int cdResponsavel { get; set; } // cdResponsavel (Primary key)
        public int TB_Empresa_cdEmpresa { get; set; } // TB_Empresa_cdEmpresa
        public string dsCpf { get; set; } // dsCpf (length: 100)
        public string dsRg { get; set; } // dsRg (length: 100)
        public string nmResponsavel { get; set; } // nmResponsavel (length: 100)
        public string dsCargo { get; set; } // dsCargo (length: 100)
        public string dsEmail { get; set; } // dsEmail (length: 100)
        public string dsDdiTel { get; set; } // dsDdiTel (length: 100)
        public string dsDddTel { get; set; } // dsDddTel (length: 100)
        public string dsTelefone { get; set; } // dsTelefone (length: 100)
        public string dsDddiFax { get; set; } // dsDddiFax (length: 100)
        public string dsDddFax { get; set; } // dsDddFax (length: 100)
        public string dsFax { get; set; } // dsFax (length: 100)
        public string nmEmpresa { get; set; } // nmEmpresa (length: 100)
        public string nmEmpresaCracha { get; set; } // nmEmpresaCracha (length: 100)
        public string dsRamoAtividade { get; set; } // dsRamoAtividade (length: 100)
        public string dsAreaAtuacao { get; set; } // dsAreaAtuacao (length: 100)
        public string dsCepLogradouro { get; set; } // dsCepLogradouro (length: 100)
        public string dsLogradouro { get; set; } // dsLogradouro (length: 100)
        public string dsNumeroLogradouro { get; set; } // dsNumeroLogradouro (length: 100)
        public string dsComplementoLogradouro { get; set; } // dsComplementoLogradouro (length: 100)
        public string dsBairroLogradouro { get; set; } // dsBairroLogradouro (length: 100)
        public string dsCidadeLogradouro { get; set; } // dsCidadeLogradouro (length: 100)
        public string dsUfLogradouro { get; set; } // dsUfLogradouro (length: 100)
        public string dsUsuario { get; set; } // dsUsuario (length: 100)
        public string dsSenha { get; set; } // dsSenha (length: 100)
        public string dsCancelado { get; set; } // dsCancelado (length: 1)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_CorCarpeteColuna where [TB_DD_CorCarpeteColuna].[UsuarioAlteracao] point to this entity (FK_TB_DD_CorCarpeteColuna_UsuarioAlteracao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_CorCarpeteColuna> UsuarioAlteracao { get; set; } // TB_DD_CorCarpeteColuna.FK_TB_DD_CorCarpeteColuna_UsuarioAlteracao
        /// <summary>
        /// Child TB_DD_CorCarpeteColuna where [TB_DD_CorCarpeteColuna].[UsuarioInsercao] point to this entity (FK_TB_DD_CorCarpeteColuna_UsuarioInsercao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_CorCarpeteColuna> UsuarioInsercao { get; set; } // TB_DD_CorCarpeteColuna.FK_TB_DD_CorCarpeteColuna_UsuarioInsercao
        /// <summary>
        /// Child TB_DD_DestinatarioComunicado where [TB_DD_DestinatarioComunicado].[cdUsuarioLeitura] point to this entity (FK_TB_DD_DestinatarioComunicado_cdUsuarioLeitura)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_DestinatarioComunicado> TB_DD_DestinatarioComunicado { get; set; } // TB_DD_DestinatarioComunicado.FK_TB_DD_DestinatarioComunicado_cdUsuarioLeitura

        public TB_DD_ResponsavelEmpresa()
        {
            UsuarioAlteracao = new System.Collections.Generic.List<TB_DD_CorCarpeteColuna>();
            UsuarioInsercao = new System.Collections.Generic.List<TB_DD_CorCarpeteColuna>();
            TB_DD_DestinatarioComunicado = new System.Collections.Generic.List<TB_DD_DestinatarioComunicado>();
        }
    }

    // TB_DD_Resposta
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Resposta
    {
        public int cdQuestionario { get; set; } // cdQuestionario
        public string TB_Registro_dsCodigoBarras { get; set; } // TB_Registro_dsCodigoBarras (length: 100)
        public int TB_Alternativa_cdAlternativa { get; set; } // TB_Alternativa_cdAlternativa
        public string dsOutros { get; set; } // dsOutros
        public System.Guid cdResposta { get; set; } // cdResposta (Primary key)

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Alternativa pointed by [TB_DD_Resposta].([TB_Alternativa_cdAlternativa]) (FK_TB_DD_Resposta_TB_DD_Alternativa)
        /// </summary>
        public virtual TB_DD_Alternativa TB_DD_Alternativa { get; set; } // FK_TB_DD_Resposta_TB_DD_Alternativa

        /// <summary>
        /// Parent TB_DD_Questionario pointed by [TB_DD_Resposta].([cdQuestionario]) (FK_TB_DD_Resposta_TB_DD_Questionario)
        /// </summary>
        public virtual TB_DD_Questionario TB_DD_Questionario { get; set; } // FK_TB_DD_Resposta_TB_DD_Questionario

        public TB_DD_Resposta()
        {
            cdResposta = System.Guid.NewGuid();
        }
    }

    // TB_DD_RespostaQuestionarioManual
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_RespostaQuestionarioManual
    {
        public System.Guid cdRespostaQuestionarioManual { get; set; } // cdRespostaQuestionarioManual (Primary key)
        public int cdItenPedidoOperacional { get; set; } // cdItenPedidoOperacional
        public int cdQuestionarioManual { get; set; } // cdQuestionarioManual
        public int cdAlternativaQuestionarioManual { get; set; } // cdAlternativaQuestionarioManual
        public int? cdSubAlternativaQuestionarioManual { get; set; } // cdSubAlternativaQuestionarioManual
        public string dsOutros { get; set; } // dsOutros (length: 800)
        public string dsSituacao { get; set; } // dsSituacao (length: 800)
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_AlternativaQuestionarioManual pointed by [TB_DD_RespostaQuestionarioManual].([cdAlternativaQuestionarioManual]) (FK_TB_DD_RespostaQuestionarioManual_TB_DD_AlternativaQuestionarioManual)
        /// </summary>
        public virtual TB_DD_AlternativaQuestionarioManual TB_DD_AlternativaQuestionarioManual { get; set; } // FK_TB_DD_RespostaQuestionarioManual_TB_DD_AlternativaQuestionarioManual

        /// <summary>
        /// Parent TB_DD_ItensPedidoOperacional pointed by [TB_DD_RespostaQuestionarioManual].([cdItenPedidoOperacional]) (FK_TB_DD_RespostaQuestionarioManual_TB_DD_ItensPedidoOperacional)
        /// </summary>
        public virtual TB_DD_ItensPedidoOperacional TB_DD_ItensPedidoOperacional { get; set; } // FK_TB_DD_RespostaQuestionarioManual_TB_DD_ItensPedidoOperacional

        /// <summary>
        /// Parent TB_DD_QuestionarioManual pointed by [TB_DD_RespostaQuestionarioManual].([cdQuestionarioManual]) (FK_TB_DD_RespostaQuestionarioManual_TB_DD_QuestionarioManual)
        /// </summary>
        public virtual TB_DD_QuestionarioManual TB_DD_QuestionarioManual { get; set; } // FK_TB_DD_RespostaQuestionarioManual_TB_DD_QuestionarioManual

        public TB_DD_RespostaQuestionarioManual()
        {
            cdRespostaQuestionarioManual = System.Guid.NewGuid();
        }
    }

    // TB_DD_Simultaneidade
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Simultaneidade
    {
        public int cdSimultaneidade { get; set; } // cdSimultaneidade (Primary key)
        public int cdEdicao { get; set; } // cdEdicao
        public int cdProduto { get; set; } // cdProduto
        public int cdProduto_Restrito { get; set; } // cdProduto_Restrito
        public string fgSituacao { get; set; } // fgSituacao (length: 1)
        public int? cdSistema { get; set; } // cdSistema
    }

    // TB_DD_SistemaHabilitadoToken
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_SistemaHabilitadoToken
    {
        public int cdSistemaHabilitado { get; set; } // cdSistemaHabilitado (Primary key)
        public int? cdToken { get; set; } // cdToken
        public string dsSistema { get; set; } // dsSistema
        public string flHabilitado { get; set; } // flHabilitado (length: 1)
        public string fgPortugues { get; set; } // fgPortugues (length: 1)
        public string fgIngles { get; set; } // fgIngles (length: 1)
        public string dsPadraoIdentificacaoSAC { get; set; } // dsPadraoIdentificacaoSAC (length: 100)
        public string dsHabilitarFoto { get; set; } // dsHabilitarFoto (length: 1)
        public string fgEspanhol { get; set; } // fgEspanhol (length: 1)
        public string fgValidaConviteExpositor { get; set; } // fgValidaConviteExpositor (length: 1)

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Token pointed by [TB_DD_SistemaHabilitadoToken].([cdToken]) (FK_TB_DD_SistemaHabilitadoToken)
        /// </summary>
        public virtual TB_DD_Token TB_DD_Token { get; set; } // FK_TB_DD_SistemaHabilitadoToken
    }

    // TB_DD_SiteColuna
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_SiteColuna
    {
        public int cdSite { get; set; } // cdSite (Primary key)
        public int TB_Stand_cdStand { get; set; } // TB_Stand_cdStand
        public string dsSite { get; set; } // dsSite
        public System.DateTime? dtInclusao { get; set; } // dtInclusao
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public int? TB_Pedido_cdPedido { get; set; } // TB_Pedido_cdPedido

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Stand pointed by [TB_DD_SiteColuna].([TB_Stand_cdStand]) (FK_TB_DD_SiteColuna_TB_DD_Stand)
        /// </summary>
        public virtual TB_DD_Stand TB_DD_Stand { get; set; } // FK_TB_DD_SiteColuna_TB_DD_Stand
    }

    // TB_DD_Stand
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Stand
    {
        public int cdStand { get; set; } // cdStand (Primary key)
        public int? TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public int TB_Empresa_cdEmpresa { get; set; } // TB_Empresa_cdEmpresa
        public int? cdTipoContrato { get; set; } // cdTipoContrato
        public string dsNumeroStand { get; set; } // dsNumeroStand (length: 100)
        public string dsEnderecoStand { get; set; } // dsEnderecoStand (length: 100)
        public string dsMetrosQuadrados { get; set; } // dsMetrosQuadrados (length: 100)
        public int? qtCaracteresCatalogo { get; set; } // qtCaracteresCatalogo
        public string nmEvento { get; set; } // nmEvento
        public string dsSituacao { get; set; } // dsSituacao (length: 50)
        public int? qtdConvitesEletronico { get; set; } // qtdConvitesEletronico
        public string dsPavilhao { get; set; } // dsPavilhao (length: 800)
        public string dsContratoLiberado { get; set; } // dsContratoLiberado (length: 1)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_CarrinhoOperacional where [TB_DD_CarrinhoOperacional].[cdStand] point to this entity (FK_TB_DD_CarrinhoOperacional_cdStand)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_CarrinhoOperacional> TB_DD_CarrinhoOperacional { get; set; } // TB_DD_CarrinhoOperacional.FK_TB_DD_CarrinhoOperacional_cdStand
        /// <summary>
        /// Child TB_DD_CatalogoOficial where [TB_DD_CatalogoOficial].[cdStand] point to this entity (FK_TB_DD_CatalogoOficial_cdStand)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_CatalogoOficial> TB_DD_CatalogoOficial { get; set; } // TB_DD_CatalogoOficial.FK_TB_DD_CatalogoOficial_cdStand
        /// <summary>
        /// Child TB_DD_ContratoExpositor where [TB_DD_ContratoExpositor].[TB_Stand_cdStand] point to this entity (FK_ContratoExpositor_cdStand)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ContratoExpositor> TB_DD_ContratoExpositor { get; set; } // TB_DD_ContratoExpositor.FK_ContratoExpositor_cdStand
        /// <summary>
        /// Child TB_DD_ConvitesImpressos where [TB_DD_ConvitesImpressos].[TB_Stand_cdStand] point to this entity (FK_TB_DD_ConvitesImpressos_TB_DD_Stand)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ConvitesImpressos> TB_DD_ConvitesImpressos { get; set; } // TB_DD_ConvitesImpressos.FK_TB_DD_ConvitesImpressos_TB_DD_Stand
        /// <summary>
        /// Child TB_DD_CorCarpeteColuna where [TB_DD_CorCarpeteColuna].[cdStand] point to this entity (FK_TB_DD_CorCarpeteColuna_cdStand)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_CorCarpeteColuna> TB_DD_CorCarpeteColuna { get; set; } // TB_DD_CorCarpeteColuna.FK_TB_DD_CorCarpeteColuna_cdStand
        /// <summary>
        /// Child TB_DD_Credencial where [TB_DD_Credencial].[cdStand] point to this entity (FK_TB_DD_Credencial_cdStand)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Credencial> TB_DD_Credencial { get; set; } // TB_DD_Credencial.FK_TB_DD_Credencial_cdStand
        /// <summary>
        /// Child TB_DD_EntregaCredencialVIP where [TB_DD_EntregaCredencialVIP].[cdStand] point to this entity (FK_TB_DD_EntregaCredencialVIP_Stand)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_EntregaCredencialVIP> TB_DD_EntregaCredencialVIP { get; set; } // TB_DD_EntregaCredencialVIP.FK_TB_DD_EntregaCredencialVIP_Stand
        /// <summary>
        /// Child TB_DD_ItensPedidoOperacional where [TB_DD_ItensPedidoOperacional].[cdStand] point to this entity (FK_TB_DD_ItensPedidoOperacional_cdStand)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ItensPedidoOperacional> TB_DD_ItensPedidoOperacional { get; set; } // TB_DD_ItensPedidoOperacional.FK_TB_DD_ItensPedidoOperacional_cdStand
        /// <summary>
        /// Child TB_DD_MaquinaseVeiculos where [TB_DD_MaquinaseVeiculos].[TB_Stand_cdStand] point to this entity (FK_TB_DD_MaquinaseVeiculos_TB_DD_Stand)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_MaquinaseVeiculos> TB_DD_MaquinaseVeiculos { get; set; } // TB_DD_MaquinaseVeiculos.FK_TB_DD_MaquinaseVeiculos_TB_DD_Stand
        /// <summary>
        /// Child TB_DD_Movimentacao where [TB_DD_Movimentacao].[cdStand] point to this entity (FK_TB_DD_Movimentacao_TB_DD_Stand)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Movimentacao> TB_DD_Movimentacao { get; set; } // TB_DD_Movimentacao.FK_TB_DD_Movimentacao_TB_DD_Stand
        /// <summary>
        /// Child TB_DD_PlacaIdentificacao where [TB_DD_PlacaIdentificacao].[cdStand] point to this entity (TB_DD_PlacaIdentificacao_cdStand)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_PlacaIdentificacao> TB_DD_PlacaIdentificacao { get; set; } // TB_DD_PlacaIdentificacao.TB_DD_PlacaIdentificacao_cdStand
        /// <summary>
        /// Child TB_DD_PlacaIdentificacaoColuna where [TB_DD_PlacaIdentificacaoColuna].[cdStand] point to this entity (FK_TB_DD_PlacaIdentificacaoColuna_TB_DD_Stand)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_PlacaIdentificacaoColuna> TB_DD_PlacaIdentificacaoColuna { get; set; } // TB_DD_PlacaIdentificacaoColuna.FK_TB_DD_PlacaIdentificacaoColuna_TB_DD_Stand
        /// <summary>
        /// Child TB_DD_SiteColuna where [TB_DD_SiteColuna].[TB_Stand_cdStand] point to this entity (FK_TB_DD_SiteColuna_TB_DD_Stand)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_SiteColuna> TB_DD_SiteColuna { get; set; } // TB_DD_SiteColuna.FK_TB_DD_SiteColuna_TB_DD_Stand
        /// <summary>
        /// Child TB_DD_ValorContrato where [TB_DD_ValorContrato].[cdStand] point to this entity (FK_ValorContato_TB_DD_Stand)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ValorContrato> TB_DD_ValorContrato { get; set; } // TB_DD_ValorContrato.FK_ValorContato_TB_DD_Stand
        /// <summary>
        /// Child TB_LG_FormularioManualEletronico where [TB_LG_FormularioManualEletronico].[cdStand] point to this entity (FK_TB_LG_FormularioManualEletronico_TB_DD_Stand)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_LG_FormularioManualEletronico> TB_LG_FormularioManualEletronico { get; set; } // TB_LG_FormularioManualEletronico.FK_TB_LG_FormularioManualEletronico_TB_DD_Stand

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_DD_Stand].([TB_Edicao_cdEdicao]) (FK_TB_DD_Stand_TB_CF_Edicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_DD_Stand_TB_CF_Edicao

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_DD_Stand].([TB_Empresa_cdEmpresa]) (FK_TB_DD_Stand_TB_DD_Empresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_DD_Stand_TB_DD_Empresa

        /// <summary>
        /// Parent TB_RF_TipoContrato pointed by [TB_DD_Stand].([cdTipoContrato]) (FK_TB_DD_Stand_TB_RF_TipoContrato)
        /// </summary>
        public virtual TB_RF_TipoContrato TB_RF_TipoContrato { get; set; } // FK_TB_DD_Stand_TB_RF_TipoContrato

        public TB_DD_Stand()
        {
            TB_DD_CarrinhoOperacional = new System.Collections.Generic.List<TB_DD_CarrinhoOperacional>();
            TB_DD_CatalogoOficial = new System.Collections.Generic.List<TB_DD_CatalogoOficial>();
            TB_DD_ContratoExpositor = new System.Collections.Generic.List<TB_DD_ContratoExpositor>();
            TB_DD_ConvitesImpressos = new System.Collections.Generic.List<TB_DD_ConvitesImpressos>();
            TB_DD_CorCarpeteColuna = new System.Collections.Generic.List<TB_DD_CorCarpeteColuna>();
            TB_DD_Credencial = new System.Collections.Generic.List<TB_DD_Credencial>();
            TB_DD_EntregaCredencialVIP = new System.Collections.Generic.List<TB_DD_EntregaCredencialVIP>();
            TB_DD_ItensPedidoOperacional = new System.Collections.Generic.List<TB_DD_ItensPedidoOperacional>();
            TB_DD_MaquinaseVeiculos = new System.Collections.Generic.List<TB_DD_MaquinaseVeiculos>();
            TB_DD_Movimentacao = new System.Collections.Generic.List<TB_DD_Movimentacao>();
            TB_DD_PlacaIdentificacao = new System.Collections.Generic.List<TB_DD_PlacaIdentificacao>();
            TB_DD_PlacaIdentificacaoColuna = new System.Collections.Generic.List<TB_DD_PlacaIdentificacaoColuna>();
            TB_DD_SiteColuna = new System.Collections.Generic.List<TB_DD_SiteColuna>();
            TB_DD_ValorContrato = new System.Collections.Generic.List<TB_DD_ValorContrato>();
            TB_LG_FormularioManualEletronico = new System.Collections.Generic.List<TB_LG_FormularioManualEletronico>();
        }
    }

    // TB_DD_SubAlternativaQuestionarioManual
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_SubAlternativaQuestionarioManual
    {
        public int cdSubAlternativaQuestionarioManual { get; set; } // cdSubAlternativaQuestionarioManual (Primary key)
        public int cdAlternativaQuestionarioManual { get; set; } // cdAlternativaQuestionarioManual
        public int nuOrdem { get; set; } // nuOrdem
        public string dsPortugues { get; set; } // dsPortugues (length: 800)
        public string dsIngles { get; set; } // dsIngles (length: 800)
        public string dsEspanhol { get; set; } // dsEspanhol (length: 800)
        public string dsOutros { get; set; } // dsOutros (length: 1)
        public string dsSituacao { get; set; } // dsSituacao (length: 800)
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public int cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_AlternativaQuestionarioManual pointed by [TB_DD_SubAlternativaQuestionarioManual].([cdAlternativaQuestionarioManual]) (FK_TB_DD_SubAlternativaQuestionarioManual_TB_DD_AlternativaQuestionarioManual)
        /// </summary>
        public virtual TB_DD_AlternativaQuestionarioManual TB_DD_AlternativaQuestionarioManual { get; set; } // FK_TB_DD_SubAlternativaQuestionarioManual_TB_DD_AlternativaQuestionarioManual
    }

    // TB_DD_SubCategoria
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_SubCategoria
    {
        public int cdSubCategoria { get; set; } // cdSubCategoria (Primary key)
        public int cdSistema { get; set; } // cdSistema
        public string nmSubCategoriaPortugues { get; set; } // nmSubCategoriaPortugues (length: 200)
        public string dsSubCategoriaPortugues { get; set; } // dsSubCategoriaPortugues
        public string nmSubCategoriaIngles { get; set; } // nmSubCategoriaIngles (length: 200)
        public string dsSubCategoriaIngles { get; set; } // dsSubCategoriaIngles
        public string nmSubCategoriaEspanhol { get; set; } // nmSubCategoriaEspanhol (length: 200)
        public string dsSubCategoriaEspanhol { get; set; } // dsSubCategoriaEspanhol
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public int cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao
        public string fgGrupo { get; set; } // fgGrupo (length: 1)
        public string fgIndividual { get; set; } // fgIndividual (length: 1)
        public string fgHabilitaPalavra { get; set; } // fgHabilitaPalavra (length: 1)
        public string fgHabilitaValidacao { get; set; } // fgHabilitaValidacao (length: 1)
        public int? nuOrdem { get; set; } // nuOrdem
        public string EventoGuid { get; set; } // EventoGuid (length: 800)
        public string dsSigla { get; set; } // dsSigla (length: 100)

        // Reverse navigation

        /// <summary>
        /// Child TB_RF_SubCategoriaValidacao where [TB_RF_SubCategoriaValidacao].[cdSubCategoria] point to this entity (FK_SubCategoriaValidacao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_RF_SubCategoriaValidacao> TB_RF_SubCategoriaValidacao { get; set; } // TB_RF_SubCategoriaValidacao.FK_SubCategoriaValidacao

        public TB_DD_SubCategoria()
        {
            TB_RF_SubCategoriaValidacao = new System.Collections.Generic.List<TB_RF_SubCategoriaValidacao>();
        }
    }

    // TB_DD_Ticket
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Ticket
    {
        public int cdTicket { get; set; } // cdTicket (Primary key)
        public int cdPedido { get; set; } // cdPedido
        public string dsTicket { get; set; } // dsTicket (length: 100)
        public int cdProduto { get; set; } // cdProduto
        public string dsCodigoBarras { get; set; } // dsCodigoBarras (length: 100)
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public System.DateTime? dtUtilizacao { get; set; } // dtUtilizacao
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public string dsChaveUtilizacaoLocal { get; set; } // dsChaveUtilizacaoLocal (length: 800)

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_DD_Ticket].([cdPedido]) (FK_Ticket_Pedido)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido { get; set; } // FK_Ticket_Pedido

        /// <summary>
        /// Parent TB_DD_ProdutoCongresso pointed by [TB_DD_Ticket].([cdProduto]) (FK_Ticket_ProdutoCongresso)
        /// </summary>
        public virtual TB_DD_ProdutoCongresso TB_DD_ProdutoCongresso { get; set; } // FK_Ticket_ProdutoCongresso
    }

    // TB_DD_Token
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Token
    {
        public int cdToken { get; set; } // cdToken (Primary key)
        public int cdEdicao { get; set; } // cdEdicao
        public string dsToken { get; set; } // dsToken
        public string flHabilitado { get; set; } // flHabilitado (length: 1)
        public string dsLocalizacao { get; set; } // dsLocalizacao (length: 100)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_SistemaHabilitadoToken where [TB_DD_SistemaHabilitadoToken].[cdToken] point to this entity (FK_TB_DD_SistemaHabilitadoToken)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_SistemaHabilitadoToken> TB_DD_SistemaHabilitadoToken { get; set; } // TB_DD_SistemaHabilitadoToken.FK_TB_DD_SistemaHabilitadoToken

        public TB_DD_Token()
        {
            TB_DD_SistemaHabilitadoToken = new System.Collections.Generic.List<TB_DD_SistemaHabilitadoToken>();
        }
    }

    // TB_DD_Usuario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Usuario
    {
        public int cdUsuario { get; set; } // cdUsuario (Primary key)
        public string nmUsuario { get; set; } // nmUsuario (length: 100)
        public string dsSenha { get; set; } // dsSenha (length: 100)
        public string dsLogin { get; set; } // dsLogin (length: 100)
        public string dsTelefone { get; set; } // dsTelefone (length: 30)
        public string dsEmail { get; set; } // dsEmail (length: 100)
        public string Status { get; set; } // Status (length: 30)
        public string dsGrupo { get; set; } // dsGrupo (length: 50)

        // Reverse navigation

        /// <summary>
        /// Child TB_CF_Permissoes where [TB_CF_Permissoes].[cdUsuario] point to this entity (FK_TB_CF_Permissoes_cdUsuario)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_CF_Permissoes> TB_CF_Permissoes { get; set; } // TB_CF_Permissoes.FK_TB_CF_Permissoes_cdUsuario
        /// <summary>
        /// Child TB_DD_Agendamento where [TB_DD_Agendamento].[cdUsuario] point to this entity (FK_DD_Agendamento_Usuario)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Agendamento> TB_DD_Agendamento { get; set; } // TB_DD_Agendamento.FK_DD_Agendamento_Usuario
        /// <summary>
        /// Child TB_DD_LimiteAplicacao where [TB_DD_LimiteAplicacao].[cdUsuario_Alteracao] point to this entity (FK_TB_DD_LimiteAplicacao_TB_DD_Usuario_Alteracao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_LimiteAplicacao> cdUsuario_Alteracao { get; set; } // TB_DD_LimiteAplicacao.FK_TB_DD_LimiteAplicacao_TB_DD_Usuario_Alteracao
        /// <summary>
        /// Child TB_DD_LimiteAplicacao where [TB_DD_LimiteAplicacao].[cdUsuario_Inclusao] point to this entity (FK_TB_DD_LimiteAplicacao_TB_DD_Usuario_Inclusao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_LimiteAplicacao> cdUsuario_Inclusao { get; set; } // TB_DD_LimiteAplicacao.FK_TB_DD_LimiteAplicacao_TB_DD_Usuario_Inclusao

        public TB_DD_Usuario()
        {
            TB_CF_Permissoes = new System.Collections.Generic.List<TB_CF_Permissoes>();
            TB_DD_Agendamento = new System.Collections.Generic.List<TB_DD_Agendamento>();
            cdUsuario_Alteracao = new System.Collections.Generic.List<TB_DD_LimiteAplicacao>();
            cdUsuario_Inclusao = new System.Collections.Generic.List<TB_DD_LimiteAplicacao>();
        }
    }

    // TB_DD_ValorContrato
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ValorContrato
    {
        public System.Guid cdValorContrato { get; set; } // cdValorContrato (Primary key)
        public int cdStand { get; set; } // cdStand
        public decimal vlContrato { get; set; } // vlContrato
        public decimal vlArea { get; set; } // vlArea
        public decimal vlTipoMontagem { get; set; } // vlTipoMontagem
        public int qtdParcelas { get; set; } // qtdParcelas
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public int cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public System.DateTime? dtCancelado { get; set; } // dtCancelado
        public int? cdUsuarioCancelado { get; set; } // cdUsuarioCancelado
        public decimal? vlSinal { get; set; } // vlSinal
        public int? qtdParcelasMontagem { get; set; } // qtdParcelasMontagem

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_ValorContratoParcela where [TB_DD_ValorContratoParcela].[cdValorContrato] point to this entity (FK_ValorContratoParcela_ValorContrato)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ValorContratoParcela> TB_DD_ValorContratoParcela { get; set; } // TB_DD_ValorContratoParcela.FK_ValorContratoParcela_ValorContrato

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Stand pointed by [TB_DD_ValorContrato].([cdStand]) (FK_ValorContato_TB_DD_Stand)
        /// </summary>
        public virtual TB_DD_Stand TB_DD_Stand { get; set; } // FK_ValorContato_TB_DD_Stand

        public TB_DD_ValorContrato()
        {
            cdValorContrato = System.Guid.NewGuid();
            dsSituacao = "ATIVO";
            TB_DD_ValorContratoParcela = new System.Collections.Generic.List<TB_DD_ValorContratoParcela>();
        }
    }

    // TB_DD_ValorContratoParcela
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ValorContratoParcela
    {
        public System.Guid cdValorContratoParcela { get; set; } // cdValorContratoParcela (Primary key)
        public System.Guid cdValorContrato { get; set; } // cdValorContrato
        public decimal vlParcela { get; set; } // vlParcela
        public int nuParcela { get; set; } // nuParcela
        public System.DateTime dtVencimento { get; set; } // dtVencimento
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public string tipoParcela { get; set; } // tipoParcela (length: 10)

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_ValorContrato pointed by [TB_DD_ValorContratoParcela].([cdValorContrato]) (FK_ValorContratoParcela_ValorContrato)
        /// </summary>
        public virtual TB_DD_ValorContrato TB_DD_ValorContrato { get; set; } // FK_ValorContratoParcela_ValorContrato

        public TB_DD_ValorContratoParcela()
        {
            cdValorContratoParcela = System.Guid.NewGuid();
            dsSituacao = "ATIVO";
        }
    }

    // TB_LG_Acesso
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_Acesso
    {
        public int cdLogAcesso { get; set; } // cdLogAcesso (Primary key)
        public System.DateTime? dtLog { get; set; } // dtLog
        public int? cdEmpresa { get; set; } // cdEmpresa
        public int? cdUsuario { get; set; } // cdUsuario
        public int? cdEdicao { get; set; } // cdEdicao

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_LG_Acesso].([cdEdicao]) (FK_TB_LG_Acesso_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_LG_Acesso_cdEdicao

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_LG_Acesso].([cdEmpresa]) (FK_TB_LG_Acesso_cdEmpresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_LG_Acesso_cdEmpresa
    }

    // TB_LG_AlteraSubCategoria
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_AlteraSubCategoria
    {
        public int cdLogAlteracaoSubCategoria { get; set; } // cdLogAlteracaoSubCategoria (Primary key)
        public string dsCodigoBarras { get; set; } // dsCodigoBarras (length: 100)
        public int cdSubCategoria { get; set; } // cdSubCategoria
        public string dsPalavra { get; set; } // dsPalavra (length: 800)
        public System.DateTime dtAlteracao { get; set; } // dtAlteracao
        public int cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao
    }

    // TB_LG_CancelamentoAutomaticoPedido
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_CancelamentoAutomaticoPedido
    {
        public int cdCancelaPedido { get; set; } // cdCancelaPedido (Primary key)
        public int cdPedido { get; set; } // cdPedido
        public System.DateTime dtCancelamento { get; set; } // dtCancelamento
        public string dsInformativo { get; set; } // dsInformativo (length: 800)
    }

    // TB_LG_Erro
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_Erro
    {
        public int cdLogErro { get; set; } // cdLogErro (Primary key)
        public int cdFormulario { get; set; } // cdFormulario
        public string dsLogErro { get; set; } // dsLogErro (length: 800)
        public System.DateTime dtLogErro { get; set; } // dtLogErro
        public string dsMetodo { get; set; } // dsMetodo (length: 50)

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Formulario pointed by [TB_LG_Erro].([cdFormulario]) (FK_TB_LG_Erro_TB_CF_Formulario)
        /// </summary>
        public virtual TB_CF_Formulario TB_CF_Formulario { get; set; } // FK_TB_LG_Erro_TB_CF_Formulario
    }

    // TB_LG_FormularioManualEletronico
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_FormularioManualEletronico
    {
        public int cdPreenchimento { get; set; } // cdPreenchimento (Primary key)
        public System.DateTime dtAcesso { get; set; } // dtAcesso
        public int cdFormulario { get; set; } // cdFormulario
        public int cdEdicao { get; set; } // cdEdicao
        public int cdStand { get; set; } // cdStand
        public int? cdResponsavel { get; set; } // cdResponsavel

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_LG_FormularioManualEletronico].([cdEdicao]) (FK_TB_LG_FormularioManualEletronico_TB_CF_Edicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_LG_FormularioManualEletronico_TB_CF_Edicao

        /// <summary>
        /// Parent TB_CF_FormularioManualEletronico pointed by [TB_LG_FormularioManualEletronico].([cdFormulario]) (FK_TB_LG_FormularioManualEletronico_TB_CF_FormularioManualEletronico)
        /// </summary>
        public virtual TB_CF_FormularioManualEletronico TB_CF_FormularioManualEletronico { get; set; } // FK_TB_LG_FormularioManualEletronico_TB_CF_FormularioManualEletronico

        /// <summary>
        /// Parent TB_DD_Stand pointed by [TB_LG_FormularioManualEletronico].([cdStand]) (FK_TB_LG_FormularioManualEletronico_TB_DD_Stand)
        /// </summary>
        public virtual TB_DD_Stand TB_DD_Stand { get; set; } // FK_TB_LG_FormularioManualEletronico_TB_DD_Stand
    }

    // TB_LG_Geral
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_Geral
    {
        public int cdLog { get; set; } // cdLog (Primary key)
        public string dsLog { get; set; } // dsLog
        public System.DateTime? dtLog { get; set; } // dtLog
        public int cdFormulario { get; set; } // cdFormulario
        public string Origem { get; set; } // Origem (length: 800)
        public int? cdUsuario { get; set; } // cdUsuario

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Formulario pointed by [TB_LG_Geral].([cdFormulario]) (FK_TB_LG_Geral_TB_CF_Formulario)
        /// </summary>
        public virtual TB_CF_Formulario TB_CF_Formulario { get; set; } // FK_TB_LG_Geral_TB_CF_Formulario
    }

    // TB_LG_GeralDepEasy
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_GeralDepEasy
    {
        public System.Guid TB_LG_GeralUsuariocdLog { get; set; } // TB_LG_GeralUsuariocdLog (Primary key)
        public int? TB_LG_GeralRFAcao_cdAcao { get; set; } // TB_LG_GeralRFAcao_cdAcao
        public string dsAcao { get; set; } // dsAcao

        // Foreign keys

        /// <summary>
        /// Parent TB_LG_RF_GeralAcao pointed by [TB_LG_GeralDepEasy].([TB_LG_GeralRFAcao_cdAcao]) (FK_GeralDepEasy_cdAcao)
        /// </summary>
        public virtual TB_LG_RF_GeralAcao TB_LG_RF_GeralAcao { get; set; } // FK_GeralDepEasy_cdAcao

        /// <summary>
        /// Parent TB_LG_RF_GeralUsuario pointed by [TB_LG_GeralDepEasy].([TB_LG_GeralUsuariocdLog]) (FK_GeralDepEasy_UsuariocdLog)
        /// </summary>
        public virtual TB_LG_RF_GeralUsuario TB_LG_RF_GeralUsuario { get; set; } // FK_GeralDepEasy_UsuariocdLog
    }

    // TB_LG_GeralDepFinanceiro
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_GeralDepFinanceiro
    {
        public System.Guid TB_LG_GeralUsuariocdLog { get; set; } // TB_LG_GeralUsuariocdLog (Primary key)
        public int? TB_LG_GeralRFAcao_cdAcao { get; set; } // TB_LG_GeralRFAcao_cdAcao
        public int? cdPedidoAfetado { get; set; } // cdPedidoAfetado
        public string dsAcao { get; set; } // dsAcao

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_LG_GeralDepFinanceiro].([cdPedidoAfetado]) (FK_GeralDepFinanceiro_cdPedidoAfetado)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido { get; set; } // FK_GeralDepFinanceiro_cdPedidoAfetado

        /// <summary>
        /// Parent TB_LG_RF_GeralAcao pointed by [TB_LG_GeralDepFinanceiro].([TB_LG_GeralRFAcao_cdAcao]) (FK_GeralDepFinanceiro_cdAcao)
        /// </summary>
        public virtual TB_LG_RF_GeralAcao TB_LG_RF_GeralAcao { get; set; } // FK_GeralDepFinanceiro_cdAcao

        /// <summary>
        /// Parent TB_LG_RF_GeralUsuario pointed by [TB_LG_GeralDepFinanceiro].([TB_LG_GeralUsuariocdLog]) (FK_GeralDepFinanceiro_UsuariocdLog)
        /// </summary>
        public virtual TB_LG_RF_GeralUsuario TB_LG_RF_GeralUsuario { get; set; } // FK_GeralDepFinanceiro_UsuariocdLog
    }

    // TB_LG_GeralDepUsuario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_GeralDepUsuario
    {
        public System.Guid TB_LG_GeralUsuariocdLog { get; set; } // TB_LG_GeralUsuariocdLog (Primary key)
        public int? TB_LG_GeralRFAcao_cdAcao { get; set; } // TB_LG_GeralRFAcao_cdAcao
        public int? cdUsuarioAfetado { get; set; } // cdUsuarioAfetado
        public string dsAcao { get; set; } // dsAcao

        // Foreign keys

        /// <summary>
        /// Parent TB_LG_RF_GeralAcao pointed by [TB_LG_GeralDepUsuario].([TB_LG_GeralRFAcao_cdAcao]) (FK_GeralDepUsuario_cdAcao)
        /// </summary>
        public virtual TB_LG_RF_GeralAcao TB_LG_RF_GeralAcao { get; set; } // FK_GeralDepUsuario_cdAcao

        /// <summary>
        /// Parent TB_LG_RF_GeralUsuario pointed by [TB_LG_GeralDepUsuario].([TB_LG_GeralUsuariocdLog]) (FK_GeralDepUsuario_UsuariocdLog)
        /// </summary>
        public virtual TB_LG_RF_GeralUsuario TB_LG_RF_GeralUsuario { get; set; } // FK_GeralDepUsuario_UsuariocdLog
    }

    // TB_LG_PaypalPlus
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_PaypalPlus
    {
        public int cdPaypalPlus { get; set; } // cdPaypalPlus (Primary key)
        public int? cdEdicao { get; set; } // cdEdicao
        public int? cdPedido { get; set; } // cdPedido
        public System.DateTime? dtErro { get; set; } // dtErro
        public string dsMensagem { get; set; } // dsMensagem (length: 2000)
    }

    // TB_LG_RF_GeralAcao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_RF_GeralAcao
    {
        public int cdAcao { get; set; } // cdAcao (Primary key)
        public string dsAcao { get; set; } // dsAcao (length: 100)

        // Reverse navigation

        /// <summary>
        /// Child TB_LG_GeralDepEasy where [TB_LG_GeralDepEasy].[TB_LG_GeralRFAcao_cdAcao] point to this entity (FK_GeralDepEasy_cdAcao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_LG_GeralDepEasy> TB_LG_GeralDepEasy { get; set; } // TB_LG_GeralDepEasy.FK_GeralDepEasy_cdAcao
        /// <summary>
        /// Child TB_LG_GeralDepFinanceiro where [TB_LG_GeralDepFinanceiro].[TB_LG_GeralRFAcao_cdAcao] point to this entity (FK_GeralDepFinanceiro_cdAcao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_LG_GeralDepFinanceiro> TB_LG_GeralDepFinanceiro { get; set; } // TB_LG_GeralDepFinanceiro.FK_GeralDepFinanceiro_cdAcao
        /// <summary>
        /// Child TB_LG_GeralDepUsuario where [TB_LG_GeralDepUsuario].[TB_LG_GeralRFAcao_cdAcao] point to this entity (FK_GeralDepUsuario_cdAcao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_LG_GeralDepUsuario> TB_LG_GeralDepUsuario { get; set; } // TB_LG_GeralDepUsuario.FK_GeralDepUsuario_cdAcao

        public TB_LG_RF_GeralAcao()
        {
            TB_LG_GeralDepEasy = new System.Collections.Generic.List<TB_LG_GeralDepEasy>();
            TB_LG_GeralDepFinanceiro = new System.Collections.Generic.List<TB_LG_GeralDepFinanceiro>();
            TB_LG_GeralDepUsuario = new System.Collections.Generic.List<TB_LG_GeralDepUsuario>();
        }
    }

    // TB_LG_RF_GeralDepartamento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_RF_GeralDepartamento
    {
        public int cdDepartamento { get; set; } // cdDepartamento (Primary key)
        public string dsDepartamento { get; set; } // dsDepartamento (length: 150)

        // Reverse navigation

        /// <summary>
        /// Child TB_LG_RF_GeralUsuario where [TB_LG_RF_GeralUsuario].[cdDepartamento] point to this entity (FK_GeralUsuario_cdDepartamento)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_LG_RF_GeralUsuario> TB_LG_RF_GeralUsuario { get; set; } // TB_LG_RF_GeralUsuario.FK_GeralUsuario_cdDepartamento

        public TB_LG_RF_GeralDepartamento()
        {
            TB_LG_RF_GeralUsuario = new System.Collections.Generic.List<TB_LG_RF_GeralUsuario>();
        }
    }

    // TB_LG_RF_GeralUsuario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_RF_GeralUsuario
    {
        public System.Guid cdLog { get; set; } // cdLog (Primary key)
        public System.DateTime? dtLog { get; set; } // dtLog
        public int? cdUsuario { get; set; } // cdUsuario
        public int? cdDepartamento { get; set; } // cdDepartamento

        // Reverse navigation

        /// <summary>
        /// Parent (One-to-One) TB_LG_RF_GeralUsuario pointed by [TB_LG_GeralDepEasy].[TB_LG_GeralUsuariocdLog] (FK_GeralDepEasy_UsuariocdLog)
        /// </summary>
        public virtual TB_LG_GeralDepEasy TB_LG_GeralDepEasy { get; set; } // TB_LG_GeralDepEasy.FK_GeralDepEasy_UsuariocdLog
        /// <summary>
        /// Parent (One-to-One) TB_LG_RF_GeralUsuario pointed by [TB_LG_GeralDepFinanceiro].[TB_LG_GeralUsuariocdLog] (FK_GeralDepFinanceiro_UsuariocdLog)
        /// </summary>
        public virtual TB_LG_GeralDepFinanceiro TB_LG_GeralDepFinanceiro { get; set; } // TB_LG_GeralDepFinanceiro.FK_GeralDepFinanceiro_UsuariocdLog
        /// <summary>
        /// Parent (One-to-One) TB_LG_RF_GeralUsuario pointed by [TB_LG_GeralDepUsuario].[TB_LG_GeralUsuariocdLog] (FK_GeralDepUsuario_UsuariocdLog)
        /// </summary>
        public virtual TB_LG_GeralDepUsuario TB_LG_GeralDepUsuario { get; set; } // TB_LG_GeralDepUsuario.FK_GeralDepUsuario_UsuariocdLog

        // Foreign keys

        /// <summary>
        /// Parent TB_LG_RF_GeralDepartamento pointed by [TB_LG_RF_GeralUsuario].([cdDepartamento]) (FK_GeralUsuario_cdDepartamento)
        /// </summary>
        public virtual TB_LG_RF_GeralDepartamento TB_LG_RF_GeralDepartamento { get; set; } // FK_GeralUsuario_cdDepartamento

        public TB_LG_RF_GeralUsuario()
        {
            cdLog = System.Guid.NewGuid();
        }
    }

    // TB_RF_AliquotaImposto
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_AliquotaImposto
    {
        public int cdAliquota { get; set; } // cdAliquota (Primary key)
        public int? cdEdicao { get; set; } // cdEdicao
        public int? cdEmpresa { get; set; } // cdEmpresa
        public string dsImposto { get; set; } // dsImposto (length: 20)
        public string dsAliquota { get; set; } // dsAliquota (length: 40)
        public string dsSituacao { get; set; } // dsSituacao (length: 10)
        public int? cdConfiguracao { get; set; } // cdConfiguracao
        public System.DateTime? dtCadastro { get; set; } // dtCadastro
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public int? cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_RF_AliquotaImposto].([cdEdicao]) (FK_TB_RF_AliquotaImposto_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_RF_AliquotaImposto_cdEdicao

        /// <summary>
        /// Parent TB_DD_Empresa pointed by [TB_RF_AliquotaImposto].([cdEmpresa]) (FK_TB_RF_AliquotaImposto_cdEmpresa)
        /// </summary>
        public virtual TB_DD_Empresa TB_DD_Empresa { get; set; } // FK_TB_RF_AliquotaImposto_cdEmpresa
    }

    // TB_RF_AlternativaPadrao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_AlternativaPadrao
    {
        public int cdAlternativaPadrao { get; set; } // cdAlternativaPadrao (Primary key)
        public string dsPortugues { get; set; } // dsPortugues (length: 255)
        public string dsIngles { get; set; } // dsIngles (length: 255)
        public string dsEspanhol { get; set; } // dsEspanhol (length: 255)

        // Reverse navigation

        /// <summary>
        /// Child TB_AS_AlternativaPadrao_Questionario where [TB_AS_AlternativaPadrao_Questionario].[cdAlternativaPadraoSub] point to this entity (FK_AlternativaPadrao_Questionario_AlternativaPadraoSub)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_AlternativaPadrao_Questionario> cdAlternativaPadraoSub { get; set; } // TB_AS_AlternativaPadrao_Questionario.FK_AlternativaPadrao_Questionario_AlternativaPadraoSub
        /// <summary>
        /// Child TB_AS_AlternativaPadrao_Questionario where [TB_AS_AlternativaPadrao_Questionario].[cdAlternativaPadrao] point to this entity (FK_AlternativaPadrao_Questionario_AlternativaPadrao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_AlternativaPadrao_Questionario> TB_AS_AlternativaPadrao_Questionario_cdAlternativaPadrao { get; set; } // TB_AS_AlternativaPadrao_Questionario.FK_AlternativaPadrao_Questionario_AlternativaPadrao

        public TB_RF_AlternativaPadrao()
        {
            TB_AS_AlternativaPadrao_Questionario_cdAlternativaPadrao = new System.Collections.Generic.List<TB_AS_AlternativaPadrao_Questionario>();
            cdAlternativaPadraoSub = new System.Collections.Generic.List<TB_AS_AlternativaPadrao_Questionario>();
        }
    }

    // TB_RF_AreaAtuacao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_AreaAtuacao
    {
        public int cdAreaAtuacao { get; set; } // cdAreaAtuacao (Primary key)
        public string dsAreaAtuacaoPortugues { get; set; } // dsAreaAtuacaoPortugues
        public string dsAreaAtuacaoIngles { get; set; } // dsAreaAtuacaoIngles
        public int? cdEdicao { get; set; } // cdEdicao
        public string dsSituacao { get; set; } // dsSituacao (length: 255)

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_RF_AreaAtuacao].([cdEdicao]) (FK_TB_RF_AreaAtuacao_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_RF_AreaAtuacao_cdEdicao
    }

    // TB_RF_Atividades
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_Atividades
    {
        public int cdAtividade { get; set; } // cdAtividade (Primary key)
        public string dsAtividade { get; set; } // dsAtividade
        public int? dsNivel { get; set; } // dsNivel
        public int? TB_Edicao_cdEdicao { get; set; } // TB_Edicao_cdEdicao
        public string dsAtividadeIng { get; set; } // dsAtividadeIng

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_RF_Atividades].([TB_Edicao_cdEdicao]) (FK_TB_RF_Atividades_TB_CF_Edicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_RF_Atividades_TB_CF_Edicao
    }

    // TB_RF_Categoria
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_Categoria
    {
        public int cdCategoria { get; set; } // cdCategoria (Primary key)
        public string nmCategoria { get; set; } // nmCategoria (length: 100)
        public string dsSistema { get; set; } // dsSistema (length: 100)

        // Reverse navigation

        /// <summary>
        /// Child TB_AS_TipoCredenciamento_Edicao where [TB_AS_TipoCredenciamento_Edicao].[cdCategoria] point to this entity (FK_TB_AS_TipoCredenciamento_Edicao_TB_RF_Categoria)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_TipoCredenciamento_Edicao> TB_AS_TipoCredenciamento_Edicao { get; set; } // TB_AS_TipoCredenciamento_Edicao.FK_TB_AS_TipoCredenciamento_Edicao_TB_RF_Categoria
        /// <summary>
        /// Child TB_DD_Codigos where [TB_DD_Codigos].[TB_Categoria_cdCategoria] point to this entity (FK_TB_DD_Codigos_TB_RF_Categoria_cdCategoria)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Codigos> TB_DD_Codigos { get; set; } // TB_DD_Codigos.FK_TB_DD_Codigos_TB_RF_Categoria_cdCategoria
        /// <summary>
        /// Child TB_DD_Movimentacao where [TB_DD_Movimentacao].[TB_Categoria_cdCategoria] point to this entity (FK_TB_DD_Movimentacao_TB_RF_Categoria)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Movimentacao> TB_DD_Movimentacao { get; set; } // TB_DD_Movimentacao.FK_TB_DD_Movimentacao_TB_RF_Categoria
        /// <summary>
        /// Child TB_DD_ProdutoOperacional where [TB_DD_ProdutoOperacional].[cdCategoria] point to this entity (FK_TB_DD_ProdutoOperacional_TB_RF_Categoria)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_ProdutoOperacional> TB_DD_ProdutoOperacional { get; set; } // TB_DD_ProdutoOperacional.FK_TB_DD_ProdutoOperacional_TB_RF_Categoria
        /// <summary>
        /// Child TB_DD_Registro where [TB_DD_Registro].[TB_Categoria_cdCategoria] point to this entity (FK_TB_DD_Registro_TB_RF_Categoria)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Registro> TB_DD_Registro { get; set; } // TB_DD_Registro.FK_TB_DD_Registro_TB_RF_Categoria

        public TB_RF_Categoria()
        {
            TB_AS_TipoCredenciamento_Edicao = new System.Collections.Generic.List<TB_AS_TipoCredenciamento_Edicao>();
            TB_DD_Codigos = new System.Collections.Generic.List<TB_DD_Codigos>();
            TB_DD_Movimentacao = new System.Collections.Generic.List<TB_DD_Movimentacao>();
            TB_DD_ProdutoOperacional = new System.Collections.Generic.List<TB_DD_ProdutoOperacional>();
            TB_DD_Registro = new System.Collections.Generic.List<TB_DD_Registro>();
        }
    }

    // TB_RF_CnaeSebrae
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_CnaeSebrae
    {
        public int cdCnaeSebrae { get; set; } // cdCnaeSebrae (Primary key)
        public string dsCodigo { get; set; } // dsCodigo (length: 100)
        public string DescCnaeFiscal { get; set; } // DescCnaeFiscal (length: 800)
        public string codSetor { get; set; } // codSetor (length: 100)
    }

    // TB_RF_ConfImpostoRetencaoValorAcimaDe
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_ConfImpostoRetencaoValorAcimaDe
    {
        public int cdConfImpostoRetencaoValorAcimaDe { get; set; } // cdConfImpostoRetencaoValorAcimaDe (Primary key)
        public int cdConfiguracao { get; set; } // cdConfiguracao
        public string dsImposto { get; set; } // dsImposto (length: 20)
        public decimal vlAcimaDe { get; set; } // vlAcimaDe
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public System.DateTime? dtCancelado { get; set; } // dtCancelado
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public int? cdEdicao { get; set; } // cdEdicao

        // Foreign keys

        /// <summary>
        /// Parent TB_RF_ConfImpostos pointed by [TB_RF_ConfImpostoRetencaoValorAcimaDe].([cdConfiguracao]) (FK_ConfImpostoRetencaoValorAcimaDe_tb_rf_ConfImpostos)
        /// </summary>
        public virtual TB_RF_ConfImpostos TB_RF_ConfImpostos { get; set; } // FK_ConfImpostoRetencaoValorAcimaDe_tb_rf_ConfImpostos

        public TB_RF_ConfImpostoRetencaoValorAcimaDe()
        {
            dsSituacao = "ATIVO";
        }
    }

    // TB_RF_ConfImpostos
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_ConfImpostos
    {
        public int cdConfiguracao { get; set; } // cdConfiguracao (Primary key)
        public string dsTipo { get; set; } // dsTipo (length: 30)
        public string IRRF { get; set; } // IRRF (length: 20)
        public string PCC { get; set; } // PCC (length: 20)
        public string ISS { get; set; } // ISS (length: 20)
        public System.DateTime? DtInclusao { get; set; } // DtInclusao
        public string Status { get; set; } // Status (length: 20)

        // Reverse navigation

        /// <summary>
        /// Child TB_RF_ConfImpostoRetencaoValorAcimaDe where [TB_RF_ConfImpostoRetencaoValorAcimaDe].[cdConfiguracao] point to this entity (FK_ConfImpostoRetencaoValorAcimaDe_tb_rf_ConfImpostos)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_RF_ConfImpostoRetencaoValorAcimaDe> TB_RF_ConfImpostoRetencaoValorAcimaDe { get; set; } // TB_RF_ConfImpostoRetencaoValorAcimaDe.FK_ConfImpostoRetencaoValorAcimaDe_tb_rf_ConfImpostos

        public TB_RF_ConfImpostos()
        {
            TB_RF_ConfImpostoRetencaoValorAcimaDe = new System.Collections.Generic.List<TB_RF_ConfImpostoRetencaoValorAcimaDe>();
        }
    }

    // TB_RF_Cor
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_Cor
    {
        public int cdCor { get; set; } // cdCor (Primary key)
        public int cdFormulario { get; set; } // cdFormulario
        public string dsTipo { get; set; } // dsTipo (length: 50)
        public string dsCor { get; set; } // dsCor (length: 255)
        public string fgSituacao { get; set; } // fgSituacao (length: 50)
        public string dsCorIng { get; set; } // dsCorIng (length: 255)

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_FormularioManualEletronico pointed by [TB_RF_Cor].([cdFormulario]) (FK_TB_RF_Cor_TB_CF_FormularioManualEletronico)
        /// </summary>
        public virtual TB_CF_FormularioManualEletronico TB_CF_FormularioManualEletronico { get; set; } // FK_TB_RF_Cor_TB_CF_FormularioManualEletronico
    }

    // TB_RF_Cotacao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_Cotacao
    {
        public int cdCotacao { get; set; } // cdCotacao (Primary key)
        public System.DateTime? dtCotacao { get; set; } // dtCotacao
        public string dsMoeda { get; set; } // dsMoeda (length: 30)
        public string vlCotacao { get; set; } // vlCotacao (length: 30)
        public int? cdUsuario { get; set; } // cdUsuario
    }

    // TB_RF_EventoStand
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_EventoStand
    {
        public int cdEventoStand { get; set; } // cdEventoStand (Primary key)
        public string dsEvento { get; set; } // dsEvento (length: 100)
    }

    // TB_RF_ExclusaoEmpresa
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_ExclusaoEmpresa
    {
        public int cdExclusao { get; set; } // cdExclusao (Primary key)
        public int cdEmpresa { get; set; } // cdEmpresa
        public string dsRazaoSocial { get; set; } // dsRazaoSocial
        public int cdUsuarioExclusao { get; set; } // cdUsuarioExclusao
        public System.DateTime dtExclusao { get; set; } // dtExclusao
    }

    // TB_RF_Faq
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_Faq
    {
        public int cdFaq { get; set; } // cdFaq (Primary key)
        public int? cdEdicao { get; set; } // cdEdicao
        public System.DateTime? DataInclusao { get; set; } // DataInclusao
        public System.DateTime? DataAlteracao { get; set; } // DataAlteracao
        public string dsTitulo { get; set; } // dsTitulo (length: 255)
        public string Status { get; set; } // Status (length: 10)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_FaqRespostas where [TB_DD_FaqRespostas].[cdFaq] point to this entity (FK_TB_DD_FaqRespostas_cdFaq)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_FaqRespostas> TB_DD_FaqRespostas { get; set; } // TB_DD_FaqRespostas.FK_TB_DD_FaqRespostas_cdFaq

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_RF_Faq].([cdEdicao]) (FK_TB_RF_Faq_cdEdicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_RF_Faq_cdEdicao

        public TB_RF_Faq()
        {
            TB_DD_FaqRespostas = new System.Collections.Generic.List<TB_DD_FaqRespostas>();
        }
    }

    // TB_RF_FormaPagamento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_FormaPagamento
    {
        public int cdPagamento { get; set; } // cdPagamento (Primary key)
        public string dsPagamento { get; set; } // dsPagamento (length: 100)
        public string dsExibirCaixa { get; set; } // dsExibirCaixa (length: 1)
        public string dsSolicitarSenhaSuperVisor { get; set; } // dsSolicitarSenhaSuperVisor (length: 1)

        // Reverse navigation

        /// <summary>
        /// Child TB_AS_Pedido_FormaPagamento where [TB_AS_Pedido_FormaPagamento].[TB_FormaPagamento_cdPagamento] point to this entity (FK_TB_AS_Pedido_FormaPagamento_TB_FormaPagamento_cdPagamento)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_Pedido_FormaPagamento> TB_AS_Pedido_FormaPagamento { get; set; } // TB_AS_Pedido_FormaPagamento.FK_TB_AS_Pedido_FormaPagamento_TB_FormaPagamento_cdPagamento
        /// <summary>
        /// Child TB_DD_Pagamento where [TB_DD_Pagamento].[cdFormaPagamento] point to this entity (FK_TB_DD_Pagamento_cdFormaPagamento)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Pagamento> TB_DD_Pagamento { get; set; } // TB_DD_Pagamento.FK_TB_DD_Pagamento_cdFormaPagamento

        public TB_RF_FormaPagamento()
        {
            dsSolicitarSenhaSuperVisor = "N";
            TB_AS_Pedido_FormaPagamento = new System.Collections.Generic.List<TB_AS_Pedido_FormaPagamento>();
            TB_DD_Pagamento = new System.Collections.Generic.List<TB_DD_Pagamento>();
        }
    }

    // TB_RF_Generica
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_Generica
    {
        public int cdReferencia { get; set; } // cdReferencia (Primary key)
        public string nmPort { get; set; } // nmPort (length: 1000)
        public string nmIngl { get; set; } // nmIngl (length: 1000)
        public string dsVinculo { get; set; } // dsVinculo (length: 1000)
        public string fgAtivo { get; set; } // fgAtivo (length: 1)
        public int? codigo { get; set; } // codigo
    }

    // TB_RF_GruposDadosCadastrais
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_GruposDadosCadastrais
    {
        public int cdGruposDadosCadastrais { get; set; } // cdGruposDadosCadastrais (Primary key)
        public string nmGruposDadosCadastrais { get; set; } // nmGruposDadosCadastrais (length: 255)

        // Reverse navigation

        /// <summary>
        /// Child TB_CF_EstruturaPadrao where [TB_CF_EstruturaPadrao].[cdGruposDadosCadastrais] point to this entity (FK_TB_CF_EstruturaPadrao_TB_RF_GruposDadosCadastrais)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_CF_EstruturaPadrao> TB_CF_EstruturaPadrao { get; set; } // TB_CF_EstruturaPadrao.FK_TB_CF_EstruturaPadrao_TB_RF_GruposDadosCadastrais

        public TB_RF_GruposDadosCadastrais()
        {
            TB_CF_EstruturaPadrao = new System.Collections.Generic.List<TB_CF_EstruturaPadrao>();
        }
    }

    // TB_RF_Idioma
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_Idioma
    {
        public int cdIdioma { get; set; } // cdIdioma (Primary key)
        public string nmIdioma { get; set; } // nmIdioma
    }

    // TB_RF_ImpostoPedido
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_ImpostoPedido
    {
        public int cdImposto { get; set; } // cdImposto (Primary key)
        public int? cdPedido { get; set; } // cdPedido
        public string dsImposto { get; set; } // dsImposto (length: 20)
        public string dsValor { get; set; } // dsValor (length: 10)
        public int? cdPedidoBaseCalculo { get; set; } // cdPedidoBaseCalculo
        public string cdUsuario { get; set; } // cdUsuario
        public string dsBaseCalculo { get; set; } // dsBaseCalculo (length: 100)
        public int? cdAliquota { get; set; } // cdAliquota
        public System.DateTime? dtVencimento { get; set; } // dtVencimento
        public int? cdEmpresa { get; set; } // cdEmpresa
        public System.DateTime? dtPedido { get; set; } // dtPedido
        public System.DateTime? dtCadastro { get; set; } // dtCadastro

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_RF_ImpostoPedido].([cdPedido]) (FK_TB_RF_ImpostoPedido_cdPedido)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido { get; set; } // FK_TB_RF_ImpostoPedido_cdPedido
    }

    // TB_RF_Leitura_Atividade
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_Leitura_Atividade
    {
        public int cdAtividade { get; set; } // cdAtividade (Primary key)
        public int cdLocal { get; set; } // cdLocal
        public string nmAtividade { get; set; } // nmAtividade
        public string dsMarcarPresencaDia { get; set; } // dsMarcarPresencaDia (length: 2)
        public System.DateTime? dtInicioAtividade { get; set; } // dtInicioAtividade
        public System.DateTime? dtFimAtividade { get; set; } // dtFimAtividade
        public string dsProdutosAutorizados { get; set; } // dsProdutosAutorizados
        public string fgSituacao { get; set; } // fgSituacao (length: 1)

        // Foreign keys

        /// <summary>
        /// Parent TB_RF_Leitura_Local pointed by [TB_RF_Leitura_Atividade].([cdLocal]) (FK_TB_RF_Leitura_Atividade)
        /// </summary>
        public virtual TB_RF_Leitura_Local TB_RF_Leitura_Local { get; set; } // FK_TB_RF_Leitura_Atividade
    }

    // TB_RF_Leitura_Local
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_Leitura_Local
    {
        public int cdLocal { get; set; } // cdLocal (Primary key)
        public string nmLocal { get; set; } // nmLocal
        public int? cdEdicao { get; set; } // cdEdicao
        public string fgSituacao { get; set; } // fgSituacao (length: 1)

        // Reverse navigation

        /// <summary>
        /// Child TB_RF_Leitura_Atividade where [TB_RF_Leitura_Atividade].[cdLocal] point to this entity (FK_TB_RF_Leitura_Atividade)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_RF_Leitura_Atividade> TB_RF_Leitura_Atividade { get; set; } // TB_RF_Leitura_Atividade.FK_TB_RF_Leitura_Atividade

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_RF_Leitura_Local].([cdEdicao]) (FK_TB_RF_Leitura_Local)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_RF_Leitura_Local

        public TB_RF_Leitura_Local()
        {
            TB_RF_Leitura_Atividade = new System.Collections.Generic.List<TB_RF_Leitura_Atividade>();
        }
    }

    // TB_RF_Moeda
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_Moeda
    {
        public int cdMoeda { get; set; } // cdMoeda (Primary key)
        public string nmMoeda { get; set; } // nmMoeda
    }

    // TB_RF_PerguntaPadrao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_PerguntaPadrao
    {
        public int cdPerguntaPadrao { get; set; } // cdPerguntaPadrao (Primary key)
        public string dsPortugues { get; set; } // dsPortugues (length: 255)
        public string dsIngles { get; set; } // dsIngles (length: 255)
        public string dsEspanhol { get; set; } // dsEspanhol (length: 255)
        public string fgSituacao { get; set; } // fgSituacao (length: 1)

        // Reverse navigation

        /// <summary>
        /// Child TB_RF_QuestionarioPadrao where [TB_RF_QuestionarioPadrao].[cdPerguntaPadrao] point to this entity (FK_QuestionarioPadrao_PerguntaPadrao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_RF_QuestionarioPadrao> TB_RF_QuestionarioPadrao { get; set; } // TB_RF_QuestionarioPadrao.FK_QuestionarioPadrao_PerguntaPadrao

        public TB_RF_PerguntaPadrao()
        {
            TB_RF_QuestionarioPadrao = new System.Collections.Generic.List<TB_RF_QuestionarioPadrao>();
        }
    }

    // TB_RF_PreenchimentoFormulario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_PreenchimentoFormulario
    {
        public int cdPreenchimento { get; set; } // cdPreenchimento (Primary key)
        public int? TB_Pedido_cdPedido { get; set; } // TB_Pedido_cdPedido
        public int? TB_FormularioManualEletronico_cdFormulario { get; set; } // TB_FormularioManualEletronico_cdFormulario
        public int? TB_FormularioManualEletronico_TB_Edicao_cdEdicao { get; set; } // TB_FormularioManualEletronico_TB_Edicao_cdEdicao

        // Foreign keys

        /// <summary>
        /// Parent TB_CF_Edicao pointed by [TB_RF_PreenchimentoFormulario].([TB_FormularioManualEletronico_TB_Edicao_cdEdicao]) (FK_TB_RF_PreenchimentoFormulario_TB_CF_Edicao)
        /// </summary>
        public virtual TB_CF_Edicao TB_CF_Edicao { get; set; } // FK_TB_RF_PreenchimentoFormulario_TB_CF_Edicao

        /// <summary>
        /// Parent TB_CF_FormularioManualEletronico pointed by [TB_RF_PreenchimentoFormulario].([TB_FormularioManualEletronico_cdFormulario]) (FK_TB_RF_PreenchimentoFormulario_TB_CF_FormularioManualEletronico)
        /// </summary>
        public virtual TB_CF_FormularioManualEletronico TB_CF_FormularioManualEletronico { get; set; } // FK_TB_RF_PreenchimentoFormulario_TB_CF_FormularioManualEletronico

        /// <summary>
        /// Parent TB_DD_Pedido pointed by [TB_RF_PreenchimentoFormulario].([TB_Pedido_cdPedido]) (FK_TB_RF_PreenchimentoFormulario_TB_DD_Pedido)
        /// </summary>
        public virtual TB_DD_Pedido TB_DD_Pedido { get; set; } // FK_TB_RF_PreenchimentoFormulario_TB_DD_Pedido
    }

    // TB_RF_QuestionarioPadrao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_QuestionarioPadrao
    {
        public int cdQuestionarioPadrao { get; set; } // cdQuestionarioPadrao (Primary key)
        public int nuPergunta { get; set; } // nuPergunta
        public int cdPerguntaPadrao { get; set; } // cdPerguntaPadrao
        public string dsTipoAlternativa { get; set; } // dsTipoAlternativa (length: 100)
        public string dsObrigatoria { get; set; } // dsObrigatoria (length: 100)
        public int cdCategoriaSistema { get; set; } // cdCategoriaSistema

        // Reverse navigation

        /// <summary>
        /// Child TB_AS_AlternativaPadrao_Questionario where [TB_AS_AlternativaPadrao_Questionario].[cdQuestionarioPadrao] point to this entity (FK_AlternativaPadrao_Questionario_QuestionarioPadrao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_AlternativaPadrao_Questionario> TB_AS_AlternativaPadrao_Questionario { get; set; } // TB_AS_AlternativaPadrao_Questionario.FK_AlternativaPadrao_Questionario_QuestionarioPadrao

        // Foreign keys

        /// <summary>
        /// Parent TB_RF_PerguntaPadrao pointed by [TB_RF_QuestionarioPadrao].([cdPerguntaPadrao]) (FK_QuestionarioPadrao_PerguntaPadrao)
        /// </summary>
        public virtual TB_RF_PerguntaPadrao TB_RF_PerguntaPadrao { get; set; } // FK_QuestionarioPadrao_PerguntaPadrao

        public TB_RF_QuestionarioPadrao()
        {
            TB_AS_AlternativaPadrao_Questionario = new System.Collections.Generic.List<TB_AS_AlternativaPadrao_Questionario>();
        }
    }

    // TB_RF_RamoAtividade
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_RamoAtividade
    {
        public int cdRamoAtividade { get; set; } // cdRamoAtividade (Primary key)
        public string nmRamo { get; set; } // nmRamo
    }

    // TB_RF_RegraMarketingSimples
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_RegraMarketingSimples
    {
        public int cdRegraMarketingSimples { get; set; } // cdRegraMarketingSimples (Primary key)
        public string dsRegraMarketingSimplesPT { get; set; } // dsRegraMarketingSimplesPT (length: 500)
        public string dsRegraMarketingSimplesEN { get; set; } // dsRegraMarketingSimplesEN (length: 500)
        public string dsRegraMarketingSimplesES { get; set; } // dsRegraMarketingSimplesES (length: 500)
        public string dsSigla { get; set; } // dsSigla (length: 100)
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public int cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao
        public string dsSituacao { get; set; } // dsSituacao (length: 100)

        // Reverse navigation

        /// <summary>
        /// Child TB_AS_RegraMarketingSimples_ProdutoOperacional where [TB_AS_RegraMarketingSimples_ProdutoOperacional].[cdRegraMarketingSimples] point to this entity (FK_RegraMarketingSimples_ProdutoOperacional_Marketing)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_RegraMarketingSimples_ProdutoOperacional> TB_AS_RegraMarketingSimples_ProdutoOperacional { get; set; } // TB_AS_RegraMarketingSimples_ProdutoOperacional.FK_RegraMarketingSimples_ProdutoOperacional_Marketing

        public TB_RF_RegraMarketingSimples()
        {
            dsSituacao = "ATIVO";
            TB_AS_RegraMarketingSimples_ProdutoOperacional = new System.Collections.Generic.List<TB_AS_RegraMarketingSimples_ProdutoOperacional>();
        }
    }

    // TB_RF_Saudacao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_Saudacao
    {
        public int cdSaudacao { get; set; } // cdSaudacao (Primary key)
        public string dsPortugues { get; set; } // dsPortugues (length: 50)
        public string dsIngles { get; set; } // dsIngles (length: 50)
        public string dsEspanhol { get; set; } // dsEspanhol (length: 50)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_Pessoa where [TB_DD_Pessoa].[dsTratamento] point to this entity (FK_TB_DD_Pessoa_TB_RF_Saudacao1)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Pessoa> TB_DD_Pessoa { get; set; } // TB_DD_Pessoa.FK_TB_DD_Pessoa_TB_RF_Saudacao1

        public TB_RF_Saudacao()
        {
            TB_DD_Pessoa = new System.Collections.Generic.List<TB_DD_Pessoa>();
        }
    }

    // TB_RF_Sexo
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_Sexo
    {
        public int cdSexo { get; set; } // cdSexo (Primary key)
        public string dsSigla { get; set; } // dsSigla (length: 1)
        public string dsPortugues { get; set; } // dsPortugues (length: 100)
        public string dsIngles { get; set; } // dsIngles (length: 100)
        public string dsEspanhol { get; set; } // dsEspanhol (length: 100)
    }

    // TB_RF_SubCategoriaValidacao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_SubCategoriaValidacao
    {
        public int cdSubCategoriaValidacao { get; set; } // cdSubCategoriaValidacao (Primary key)
        public int cdSubCategoria { get; set; } // cdSubCategoria
        public string dsCodigoValidacao { get; set; } // dsCodigoValidacao (length: 800)
        public string dsSituacao { get; set; } // dsSituacao (length: 100)
        public int cdUsuarioCadastro { get; set; } // cdUsuarioCadastro
        public System.DateTime dtCadastro { get; set; } // dtCadastro
        public int? cdUsuarioAlteracao { get; set; } // cdUsuarioAlteracao
        public System.DateTime? dtAlteracao { get; set; } // dtAlteracao

        // Foreign keys

        /// <summary>
        /// Parent TB_DD_SubCategoria pointed by [TB_RF_SubCategoriaValidacao].([cdSubCategoria]) (FK_SubCategoriaValidacao)
        /// </summary>
        public virtual TB_DD_SubCategoria TB_DD_SubCategoria { get; set; } // FK_SubCategoriaValidacao

        public TB_RF_SubCategoriaValidacao()
        {
            dsSituacao = "ATIVO";
        }
    }

    // TB_RF_TipoCarta
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoCarta
    {
        public int cdTipoCarta { get; set; } // cdTipoCarta (Primary key)
        public string nmTipoCartaPortugues { get; set; } // nmTipoCartaPortugues (length: 200)
        public string nmTipoCartaIngles { get; set; } // nmTipoCartaIngles (length: 200)
        public string nmTipoCartaEspanhol { get; set; } // nmTipoCartaEspanhol (length: 200)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_EstruturaCartaPersonalizada where [TB_DD_EstruturaCartaPersonalizada].[cdTipoCarta] point to this entity (FK_TB_DD_EstruturaCartaPersonalizada_TB_RF_TipoCarta)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_EstruturaCartaPersonalizada> TB_DD_EstruturaCartaPersonalizada { get; set; } // TB_DD_EstruturaCartaPersonalizada.FK_TB_DD_EstruturaCartaPersonalizada_TB_RF_TipoCarta

        public TB_RF_TipoCarta()
        {
            TB_DD_EstruturaCartaPersonalizada = new System.Collections.Generic.List<TB_DD_EstruturaCartaPersonalizada>();
        }
    }

    // TB_RF_TipoContrato
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoContrato
    {
        public int cdTipoContrato { get; set; } // cdTipoContrato (Primary key)
        public string dsContrato { get; set; } // dsContrato
        public string dsSituacao { get; set; } // dsSituacao
        public string dsContratoIng { get; set; } // dsContratoIng (length: 50)
        public string dsDescricao { get; set; } // dsDescricao
        public string dsDescricaoIng { get; set; } // dsDescricaoIng

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_BloqueioContratoProduto where [TB_DD_BloqueioContratoProduto].[cdTipoContrato] point to this entity (FK_TB_DD_BloqueioContratoProduto_cdTipoContrato)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_BloqueioContratoProduto> TB_DD_BloqueioContratoProduto { get; set; } // TB_DD_BloqueioContratoProduto.FK_TB_DD_BloqueioContratoProduto_cdTipoContrato
        /// <summary>
        /// Child TB_DD_PermissaoCorCarpeteColuna where [TB_DD_PermissaoCorCarpeteColuna].[cdContrato] point to this entity (FK_TB_DD_PermissaoCorCarpeteColuna_TB_RF_TipoContrato)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_PermissaoCorCarpeteColuna> TB_DD_PermissaoCorCarpeteColuna { get; set; } // TB_DD_PermissaoCorCarpeteColuna.FK_TB_DD_PermissaoCorCarpeteColuna_TB_RF_TipoContrato
        /// <summary>
        /// Child TB_DD_Stand where [TB_DD_Stand].[cdTipoContrato] point to this entity (FK_TB_DD_Stand_TB_RF_TipoContrato)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Stand> TB_DD_Stand { get; set; } // TB_DD_Stand.FK_TB_DD_Stand_TB_RF_TipoContrato

        public TB_RF_TipoContrato()
        {
            TB_DD_BloqueioContratoProduto = new System.Collections.Generic.List<TB_DD_BloqueioContratoProduto>();
            TB_DD_PermissaoCorCarpeteColuna = new System.Collections.Generic.List<TB_DD_PermissaoCorCarpeteColuna>();
            TB_DD_Stand = new System.Collections.Generic.List<TB_DD_Stand>();
        }
    }

    // TB_RF_TipoCredenciamento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoCredenciamento
    {
        public int cdTipo { get; set; } // cdTipo (Primary key)
        public string dsTipo { get; set; } // dsTipo (length: 100)
        public System.DateTime? dtInicial { get; set; } // dtInicial
        public System.DateTime? dtFinal { get; set; } // dtFinal
        public string dsUrlAplicacao { get; set; } // dsUrlAplicacao (length: 100)

        // Reverse navigation

        /// <summary>
        /// Child TB_AS_TipoCredenciamento_Edicao where [TB_AS_TipoCredenciamento_Edicao].[TB_TipoCredenciamento_cdTipo] point to this entity (FK_TB_AS_TipoCredenciamento_Edicao_TB_RF_TipoCredenciamento)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_AS_TipoCredenciamento_Edicao> TB_AS_TipoCredenciamento_Edicao { get; set; } // TB_AS_TipoCredenciamento_Edicao.FK_TB_AS_TipoCredenciamento_Edicao_TB_RF_TipoCredenciamento

        public TB_RF_TipoCredenciamento()
        {
            TB_AS_TipoCredenciamento_Edicao = new System.Collections.Generic.List<TB_AS_TipoCredenciamento_Edicao>();
        }
    }

    // TB_RF_TipoDocumento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoDocumento
    {
        public int cdTipoDocumento { get; set; } // cdTipoDocumento (Primary key)
        public string dsDocumento { get; set; } // dsDocumento (length: 100)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_Empresa where [TB_DD_Empresa].[cdTipoDocumento] point to this entity (FK_TB_DD_Empresa_cdTipoDocumento)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Empresa> TB_DD_Empresa { get; set; } // TB_DD_Empresa.FK_TB_DD_Empresa_cdTipoDocumento

        public TB_RF_TipoDocumento()
        {
            TB_DD_Empresa = new System.Collections.Generic.List<TB_DD_Empresa>();
        }
    }

    // TB_RF_TipoEmpresa
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoEmpresa
    {
        public int cdTipo { get; set; } // cdTipo (Primary key)
        public string dsTipo { get; set; } // dsTipo (length: 100)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_Empresa where [TB_DD_Empresa].[TB_TipoEmpresa_cdTipo] point to this entity (FK_TB_DD_Empresa_TB_RF_TipoEmpresa_cdTipo)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Empresa> TB_DD_Empresa { get; set; } // TB_DD_Empresa.FK_TB_DD_Empresa_TB_RF_TipoEmpresa_cdTipo

        public TB_RF_TipoEmpresa()
        {
            TB_DD_Empresa = new System.Collections.Generic.List<TB_DD_Empresa>();
        }
    }

    // TB_RF_TipoFormulario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoFormulario
    {
        public int cdTipoFormulario { get; set; } // cdTipoFormulario (Primary key)
        public string dsTipoFormulario { get; set; } // dsTipoFormulario (length: 200)
        public int? cdTipoFormularioCopia { get; set; } // cdTipoFormularioCopia

        // Reverse navigation

        /// <summary>
        /// Child TB_CF_Formulario where [TB_CF_Formulario].[cdTipoFormulario] point to this entity (FK_TB_CF_Formulario_TB_RF_TipoFormulario)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_CF_Formulario> TB_CF_Formulario { get; set; } // TB_CF_Formulario.FK_TB_CF_Formulario_TB_RF_TipoFormulario

        public TB_RF_TipoFormulario()
        {
            TB_CF_Formulario = new System.Collections.Generic.List<TB_CF_Formulario>();
        }
    }

    // TB_RF_TipoObjeto
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoObjeto
    {
        public int cdTipoObjeto { get; set; } // cdTipoObjeto (Primary key)
        public string nmTipoObjeto { get; set; } // nmTipoObjeto (length: 200)

        // Reverse navigation

        /// <summary>
        /// Child TB_CF_EstruturaPadrao where [TB_CF_EstruturaPadrao].[cdTipoObjetoEspanhol] point to this entity (FK_TB_CF_EstruturaPadrao_TB_RF_TipoObjetoEspanhol)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_CF_EstruturaPadrao> cdTipoObjetoEspanhol { get; set; } // TB_CF_EstruturaPadrao.FK_TB_CF_EstruturaPadrao_TB_RF_TipoObjetoEspanhol
        /// <summary>
        /// Child TB_CF_EstruturaPadrao where [TB_CF_EstruturaPadrao].[cdTipoObjetoIngles] point to this entity (FK_TB_CF_EstruturaPadrao_TB_RF_TipoObjetoIngles)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_CF_EstruturaPadrao> cdTipoObjetoIngles { get; set; } // TB_CF_EstruturaPadrao.FK_TB_CF_EstruturaPadrao_TB_RF_TipoObjetoIngles
        /// <summary>
        /// Child TB_CF_EstruturaPadrao where [TB_CF_EstruturaPadrao].[cdTipoObjetoPortugues] point to this entity (FK_TB_CF_EstruturaPadrao_TB_RF_TipoObjeto)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_CF_EstruturaPadrao> cdTipoObjetoPortugues { get; set; } // TB_CF_EstruturaPadrao.FK_TB_CF_EstruturaPadrao_TB_RF_TipoObjeto

        public TB_RF_TipoObjeto()
        {
            cdTipoObjetoEspanhol = new System.Collections.Generic.List<TB_CF_EstruturaPadrao>();
            cdTipoObjetoIngles = new System.Collections.Generic.List<TB_CF_EstruturaPadrao>();
            cdTipoObjetoPortugues = new System.Collections.Generic.List<TB_CF_EstruturaPadrao>();
        }
    }

    // TB_RF_TipoPedido
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoPedido
    {
        public int cdTipoPedido { get; set; } // cdTipoPedido (Primary key)
        public string dsTipoPedido { get; set; } // dsTipoPedido (length: 100)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_Pedido where [TB_DD_Pedido].[cdTipoPedido] point to this entity (FK_TB_DD_Pedido_TB_RF_TipoPedido)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Pedido> TB_DD_Pedido { get; set; } // TB_DD_Pedido.FK_TB_DD_Pedido_TB_RF_TipoPedido

        public TB_RF_TipoPedido()
        {
            TB_DD_Pedido = new System.Collections.Generic.List<TB_DD_Pedido>();
        }
    }

    // TB_RF_TipoRelatorio
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoRelatorio
    {
        public int cdTipo { get; set; } // cdTipo (Primary key)
        public string dsRelatorio { get; set; } // dsRelatorio (length: 100)
        public string fgHabilitado { get; set; } // fgHabilitado (length: 10)

        // Reverse navigation

        /// <summary>
        /// Child TB_DD_Relatorio where [TB_DD_Relatorio].[cdTipoRelatorio] point to this entity (FK_TB_DD_Relatorio_TB_RF_TipoRelatorio)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_DD_Relatorio> TB_DD_Relatorio { get; set; } // TB_DD_Relatorio.FK_TB_DD_Relatorio_TB_RF_TipoRelatorio

        public TB_RF_TipoRelatorio()
        {
            TB_DD_Relatorio = new System.Collections.Generic.List<TB_DD_Relatorio>();
        }
    }

    // TB_RF_TipoValidacao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoValidacao
    {
        public int cdTipoValidacao { get; set; } // cdTipoValidacao (Primary key)
        public string nmTipoValidacao { get; set; } // nmTipoValidacao (length: 200)

        // Reverse navigation

        /// <summary>
        /// Child TB_CF_EstruturaPadrao where [TB_CF_EstruturaPadrao].[cdTipoValidacao] point to this entity (FK_tb_cf_EstruturaPadrao_tb_RF_TipoValidacao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_CF_EstruturaPadrao> TB_CF_EstruturaPadrao { get; set; } // TB_CF_EstruturaPadrao.FK_tb_cf_EstruturaPadrao_tb_RF_TipoValidacao
        /// <summary>
        /// Child TB_CF_FormularioGenerico where [TB_CF_FormularioGenerico].[cdTipoValidacao] point to this entity (FK_FormularioGenerico_TipoValidacao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_CF_FormularioGenerico> TB_CF_FormularioGenerico { get; set; } // TB_CF_FormularioGenerico.FK_FormularioGenerico_TipoValidacao
        /// <summary>
        /// Child TB_CF_VendaDados where [TB_CF_VendaDados].[cdTipoValidacao] point to this entity (PK_VendaDados_TipoValidacao)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<TB_CF_VendaDados> TB_CF_VendaDados { get; set; } // TB_CF_VendaDados.PK_VendaDados_TipoValidacao

        public TB_RF_TipoValidacao()
        {
            TB_CF_EstruturaPadrao = new System.Collections.Generic.List<TB_CF_EstruturaPadrao>();
            TB_CF_FormularioGenerico = new System.Collections.Generic.List<TB_CF_FormularioGenerico>();
            TB_CF_VendaDados = new System.Collections.Generic.List<TB_CF_VendaDados>();
        }
    }

    #endregion

    #region POCO Configuration

    // TB_AS_Alternativa_Questionario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_AS_Alternativa_QuestionarioConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_AS_Alternativa_Questionario>
    {
        public TB_AS_Alternativa_QuestionarioConfiguration()
            : this("dbo")
        {
        }

        public TB_AS_Alternativa_QuestionarioConfiguration(string schema)
        {
            ToTable("TB_AS_Alternativa_Questionario", schema);
            HasKey(x => x.cdAlternativa_Questionario);

            Property(x => x.cdAlternativa_Questionario).HasColumnName(@"cdAlternativa_Questionario").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdAlternativa).HasColumnName(@"cdAlternativa").HasColumnType("int").IsRequired();
            Property(x => x.cdQuestionario).HasColumnName(@"cdQuestionario").HasColumnType("int").IsRequired();
            Property(x => x.nuOrdem).HasColumnName(@"nuOrdem").HasColumnType("int").IsRequired();
            Property(x => x.dsSubAlternativaDa).HasColumnName(@"dsSubAlternativaDa").HasColumnType("int").IsRequired();
            Property(x => x.dsOutros).HasColumnName(@"dsOutros").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgSituacao).HasColumnName(@"fgSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dtCancelado).HasColumnName(@"dtCancelado").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioCancelado).HasColumnName(@"cdUsuarioCancelado").HasColumnType("int").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_DD_Alternativa_cdAlternativa).WithMany(b => b.TB_AS_Alternativa_Questionario_cdAlternativa).HasForeignKey(c => c.cdAlternativa).WillCascadeOnDelete(false); // FK_TB_AS_Alternativa_Questionario_TB_DD_Alternativa
            HasRequired(a => a.TB_DD_Alternativa_dsSubAlternativaDa).WithMany(b => b.dsSubAlternativaDa).HasForeignKey(c => c.dsSubAlternativaDa).WillCascadeOnDelete(false); // FK_TB_AS_Alternativa_Questionario_dsSubAlternativaDa
            HasRequired(a => a.TB_DD_Questionario).WithMany(b => b.TB_AS_Alternativa_Questionario).HasForeignKey(c => c.cdQuestionario).WillCascadeOnDelete(false); // FK_TB_AS_Alternativa_Questionario_TB_DD_Questionario
        }
    }

    // TB_AS_AlternativaPadrao_Questionario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_AS_AlternativaPadrao_QuestionarioConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_AS_AlternativaPadrao_Questionario>
    {
        public TB_AS_AlternativaPadrao_QuestionarioConfiguration()
            : this("dbo")
        {
        }

        public TB_AS_AlternativaPadrao_QuestionarioConfiguration(string schema)
        {
            ToTable("TB_AS_AlternativaPadrao_Questionario", schema);
            HasKey(x => x.cdAlternativaPadrao_Questionario);

            Property(x => x.cdAlternativaPadrao_Questionario).HasColumnName(@"cdAlternativaPadrao_Questionario").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdQuestionarioPadrao).HasColumnName(@"cdQuestionarioPadrao").HasColumnType("int").IsRequired();
            Property(x => x.cdAlternativaPadrao).HasColumnName(@"cdAlternativaPadrao").HasColumnType("int").IsRequired();
            Property(x => x.cdAlternativaPadraoSub).HasColumnName(@"cdAlternativaPadraoSub").HasColumnType("int").IsRequired();
            Property(x => x.nuOrdem).HasColumnName(@"nuOrdem").HasColumnType("int").IsRequired();
            Property(x => x.dsOutros).HasColumnName(@"dsOutros").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgSituacao).HasColumnName(@"fgSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);

            // Foreign keys
            HasRequired(a => a.TB_RF_AlternativaPadrao_cdAlternativaPadrao).WithMany(b => b.TB_AS_AlternativaPadrao_Questionario_cdAlternativaPadrao).HasForeignKey(c => c.cdAlternativaPadrao).WillCascadeOnDelete(false); // FK_AlternativaPadrao_Questionario_AlternativaPadrao
            HasRequired(a => a.TB_RF_AlternativaPadrao_cdAlternativaPadraoSub).WithMany(b => b.cdAlternativaPadraoSub).HasForeignKey(c => c.cdAlternativaPadraoSub).WillCascadeOnDelete(false); // FK_AlternativaPadrao_Questionario_AlternativaPadraoSub
            HasRequired(a => a.TB_RF_QuestionarioPadrao).WithMany(b => b.TB_AS_AlternativaPadrao_Questionario).HasForeignKey(c => c.cdQuestionarioPadrao).WillCascadeOnDelete(false); // FK_AlternativaPadrao_Questionario_QuestionarioPadrao
        }
    }

    // TB_AS_CodigoPromocional_tb_Pedido
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_AS_CodigoPromocional_tb_PedidoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_AS_CodigoPromocional_tb_Pedido>
    {
        public TB_AS_CodigoPromocional_tb_PedidoConfiguration()
            : this("dbo")
        {
        }

        public TB_AS_CodigoPromocional_tb_PedidoConfiguration(string schema)
        {
            ToTable("TB_AS_CodigoPromocional_tb_Pedido", schema);
            HasKey(x => x.cdPromocionalPedido);

            Property(x => x.cdPromocionalPedido).HasColumnName(@"cdPromocionalPedido").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdPromocional).HasColumnName(@"cdPromocional").HasColumnType("int").IsRequired();
            Property(x => x.cdPedido).HasColumnName(@"cdPedido").HasColumnType("int").IsRequired();
            Property(x => x.DtaInclusao).HasColumnName(@"DtaInclusao").HasColumnType("datetime").IsRequired();

            // Foreign keys
            HasRequired(a => a.TB_DD_CodigoPromocional).WithMany(b => b.TB_AS_CodigoPromocional_tb_Pedido).HasForeignKey(c => c.cdPromocional).WillCascadeOnDelete(false); // PK_TB_AS_CodigoPromocional_Pedido_CodigoPromocional
            HasRequired(a => a.TB_DD_Pedido).WithMany(b => b.TB_AS_CodigoPromocional_tb_Pedido).HasForeignKey(c => c.cdPedido).WillCascadeOnDelete(false); // PK_CodigoPromocional_Pedido
        }
    }

    // TB_AS_Filial_Funcionario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_AS_Filial_FuncionarioConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_AS_Filial_Funcionario>
    {
        public TB_AS_Filial_FuncionarioConfiguration()
            : this("dbo")
        {
        }

        public TB_AS_Filial_FuncionarioConfiguration(string schema)
        {
            ToTable("TB_AS_Filial_Funcionario", schema);
            HasKey(x => x.cdAssociacao);

            Property(x => x.cdAssociacao).HasColumnName(@"cdAssociacao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdFilial).HasColumnName(@"cdFilial").HasColumnType("int").IsOptional();
            Property(x => x.cdFuncionario).HasColumnName(@"cdFuncionario").HasColumnType("int").IsOptional();
            Property(x => x.fgHabilitado).HasColumnName(@"fgHabilitado").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);

            // Foreign keys
            HasOptional(a => a.TB_DD_Filial).WithMany(b => b.TB_AS_Filial_Funcionario).HasForeignKey(c => c.cdFilial).WillCascadeOnDelete(false); // FK_TB_AS_Filial_Funcionario_cdFilial
            HasOptional(a => a.TB_DD_Funcionario).WithMany(b => b.TB_AS_Filial_Funcionario).HasForeignKey(c => c.cdFuncionario).WillCascadeOnDelete(false); // FK_TB_AS_Filial_Funcionario_cdFuncionario
        }
    }

    // TB_AS_Pedido_FormaPagamento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_AS_Pedido_FormaPagamentoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_AS_Pedido_FormaPagamento>
    {
        public TB_AS_Pedido_FormaPagamentoConfiguration()
            : this("dbo")
        {
        }

        public TB_AS_Pedido_FormaPagamentoConfiguration(string schema)
        {
            ToTable("TB_AS_Pedido_FormaPagamento", schema);
            HasKey(x => x.cdPedidoFormaPagamento);

            Property(x => x.cdPedidoFormaPagamento).HasColumnName(@"cdPedidoFormaPagamento").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Pedido_cdPedido).HasColumnName(@"TB_Pedido_cdPedido").HasColumnType("int").IsRequired();
            Property(x => x.TB_FormaPagamento_cdPagamento).HasColumnName(@"TB_FormaPagamento_cdPagamento").HasColumnType("int").IsRequired();
            Property(x => x.dsValor).HasColumnName(@"dsValor").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtGeracao).HasColumnName(@"dtGeracao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdContaFaturamento).HasColumnName(@"cdContaFaturamento").HasColumnType("int").IsOptional();
            Property(x => x.cdContaCielo).HasColumnName(@"cdContaCielo").HasColumnType("int").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_DD_Pedido).WithMany(b => b.TB_AS_Pedido_FormaPagamento).HasForeignKey(c => c.TB_Pedido_cdPedido).WillCascadeOnDelete(false); // FK_TB_AS_Pedido_FormaPagamento_TB_Pedido_cdPedido
            HasRequired(a => a.TB_RF_FormaPagamento).WithMany(b => b.TB_AS_Pedido_FormaPagamento).HasForeignKey(c => c.TB_FormaPagamento_cdPagamento).WillCascadeOnDelete(false); // FK_TB_AS_Pedido_FormaPagamento_TB_FormaPagamento_cdPagamento
        }
    }

    // TB_AS_Pedido_Recibo
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_AS_Pedido_ReciboConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_AS_Pedido_Recibo>
    {
        public TB_AS_Pedido_ReciboConfiguration()
            : this("dbo")
        {
        }

        public TB_AS_Pedido_ReciboConfiguration(string schema)
        {
            ToTable("TB_AS_Pedido_Recibo", schema);
            HasKey(x => x.cdRecibo);

            Property(x => x.cdRecibo).HasColumnName(@"cdRecibo").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdPedido).HasColumnName(@"cdPedido").HasColumnType("int").IsRequired();
            Property(x => x.cdConfiguracao).HasColumnName(@"cdConfiguracao").HasColumnType("int").IsRequired();
            Property(x => x.dsNumero).HasColumnName(@"dsNumero").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dtEmissao).HasColumnName(@"dtEmissao").HasColumnType("datetime").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_CF_Recibo).WithMany(b => b.TB_AS_Pedido_Recibo).HasForeignKey(c => c.cdConfiguracao).WillCascadeOnDelete(false); // FK_TB_AS_Pedido_Recibo_TB_CF_Recibo
            HasRequired(a => a.TB_DD_Pedido).WithMany(b => b.TB_AS_Pedido_Recibo).HasForeignKey(c => c.cdPedido).WillCascadeOnDelete(false); // FK_TB_AS_Pedido_Recibo_TB_DD_Pedido
        }
    }

    // TB_AS_RegraMarketingSimples_ProdutoOperacional
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_AS_RegraMarketingSimples_ProdutoOperacionalConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_AS_RegraMarketingSimples_ProdutoOperacional>
    {
        public TB_AS_RegraMarketingSimples_ProdutoOperacionalConfiguration()
            : this("dbo")
        {
        }

        public TB_AS_RegraMarketingSimples_ProdutoOperacionalConfiguration(string schema)
        {
            ToTable("TB_AS_RegraMarketingSimples_ProdutoOperacional", schema);
            HasKey(x => x.cdRegraProdutoOperacional);

            Property(x => x.cdRegraProdutoOperacional).HasColumnName(@"cdRegraProdutoOperacional").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdRegraMarketingSimples).HasColumnName(@"cdRegraMarketingSimples").HasColumnType("int").IsRequired();
            Property(x => x.cdProduto).HasColumnName(@"cdProduto").HasColumnType("int").IsRequired();
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsRequired();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsMetragem).HasColumnName(@"dsMetragem").HasColumnType("numeric").IsOptional().HasPrecision(10, 4);
            Property(x => x.vlMinimo).HasColumnName(@"vlMinimo").HasColumnType("numeric").IsOptional().HasPrecision(10, 2);
            Property(x => x.fgRegraUmUm).HasColumnName(@"fgRegraUmUm").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgRegraMeioMeio).HasColumnName(@"fgRegraMeioMeio").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);

            // Foreign keys
            HasRequired(a => a.TB_DD_ProdutoOperacional).WithMany(b => b.TB_AS_RegraMarketingSimples_ProdutoOperacional).HasForeignKey(c => c.cdProduto).WillCascadeOnDelete(false); // FK_RegraMarketingSimples_ProdutoOperacional_Produto
            HasRequired(a => a.TB_RF_RegraMarketingSimples).WithMany(b => b.TB_AS_RegraMarketingSimples_ProdutoOperacional).HasForeignKey(c => c.cdRegraMarketingSimples).WillCascadeOnDelete(false); // FK_RegraMarketingSimples_ProdutoOperacional_Marketing
        }
    }

    // TB_AS_TipoCredenciamento_Edicao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_AS_TipoCredenciamento_EdicaoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_AS_TipoCredenciamento_Edicao>
    {
        public TB_AS_TipoCredenciamento_EdicaoConfiguration()
            : this("dbo")
        {
        }

        public TB_AS_TipoCredenciamento_EdicaoConfiguration(string schema)
        {
            ToTable("TB_AS_TipoCredenciamento_Edicao", schema);
            HasKey(x => x.cdTipoCredenciamentoHasEdicao);

            Property(x => x.cdTipoCredenciamentoHasEdicao).HasColumnName(@"cdTipoCredenciamentoHasEdicao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_TipoCredenciamento_cdTipo).HasColumnName(@"TB_TipoCredenciamento_cdTipo").HasColumnType("int").IsRequired();
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.cdCategoria).HasColumnName(@"cdCategoria").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_RF_Categoria).WithMany(b => b.TB_AS_TipoCredenciamento_Edicao).HasForeignKey(c => c.cdCategoria).WillCascadeOnDelete(false); // FK_TB_AS_TipoCredenciamento_Edicao_TB_RF_Categoria
            HasRequired(a => a.TB_CF_Edicao).WithMany(b => b.TB_AS_TipoCredenciamento_Edicao).HasForeignKey(c => c.TB_Edicao_cdEdicao).WillCascadeOnDelete(false); // FK_TB_AS_TipoCredenciamento_Edicao_TB_CF_Edicao
            HasRequired(a => a.TB_RF_TipoCredenciamento).WithMany(b => b.TB_AS_TipoCredenciamento_Edicao).HasForeignKey(c => c.TB_TipoCredenciamento_cdTipo).WillCascadeOnDelete(false); // FK_TB_AS_TipoCredenciamento_Edicao_TB_RF_TipoCredenciamento
        }
    }

    // TB_CF_Edicao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_EdicaoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_CF_Edicao>
    {
        public TB_CF_EdicaoConfiguration()
            : this("dbo")
        {
        }

        public TB_CF_EdicaoConfiguration(string schema)
        {
            ToTable("TB_CF_Edicao", schema);
            HasKey(x => x.cdEdicao);

            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEvento).HasColumnName(@"cdEvento").HasColumnType("int").IsRequired();
            Property(x => x.dsEdicao).HasColumnName(@"dsEdicao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtRealizacao).HasColumnName(@"dtRealizacao").HasColumnType("datetime").IsOptional();
            Property(x => x.dsUF).HasColumnName(@"dsUF").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(20);
            Property(x => x.dsCidade).HasColumnName(@"dsCidade").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.AplicaImposto).HasColumnName(@"AplicaImposto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsMoeda).HasColumnName(@"dsMoeda").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsDiretorioManual).HasColumnName(@"dsDiretorioManual").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsUsuarioBoleto).HasColumnName(@"dsUsuarioBoleto").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsLinkManual).HasColumnName(@"dsLinkManual").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsAssuntoPor).HasColumnName(@"dsAssuntoPor").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsAssuntoIng).HasColumnName(@"dsAssuntoIng").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsAplicaRegraCredencial).HasColumnName(@"dsAplicaRegraCredencial").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.nmEvento).HasColumnName(@"nmEvento").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsEmailNotificacao1).HasColumnName(@"dsEmailNotificacao1").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsEmailNotificacao2).HasColumnName(@"dsEmailNotificacao2").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsEmailNotificacao3).HasColumnName(@"dsEmailNotificacao3").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsEmailNotificacao4).HasColumnName(@"dsEmailNotificacao4").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsEmailNotificacao5).HasColumnName(@"dsEmailNotificacao5").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsUrlCredVisitante).HasColumnName(@"dsUrlCredVisitante").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsUrlCredVip).HasColumnName(@"dsUrlCredVip").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsUrlCredCng).HasColumnName(@"dsUrlCredCng").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsEmailSaida).HasColumnName(@"dsEmailSaida").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsSenhaEmail).HasColumnName(@"dsSenhaEmail").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsAliasEmail).HasColumnName(@"dsAliasEmail").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsAssuntoConviteEletronico).HasColumnName(@"dsAssuntoConviteEletronico").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsAssuntoConviteEletronicoVIP).HasColumnName(@"dsAssuntoConviteEletronicoVIP").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsAssuntoConviteEletronicoIng).HasColumnName(@"dsAssuntoConviteEletronicoIng").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsAssuntoConviteEletronicoVIPIng).HasColumnName(@"dsAssuntoConviteEletronicoVIPIng").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsTelefoneAtendimento).HasColumnName(@"dsTelefoneAtendimento").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsEmailAtendimento).HasColumnName(@"dsEmailAtendimento").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.qtDiasCarenciaBoletoManual).HasColumnName(@"qtDiasCarenciaBoletoManual").HasColumnType("int").IsOptional();
            Property(x => x.dtLimiteVencimentoBoletoManual).HasColumnName(@"dtLimiteVencimentoBoletoManual").HasColumnType("date").IsOptional();
            Property(x => x.dsHabilitado).HasColumnName(@"dsHabilitado").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsAssuntoCredenciamentoConfirmacaoInscricaoPortugues).HasColumnName(@"dsAssuntoCredenciamentoConfirmacaoInscricaoPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsAssuntoCredenciamentoConfirmacaoInscricaoIngles).HasColumnName(@"dsAssuntoCredenciamentoConfirmacaoInscricaoIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsAssuntoCredenciamentoConfirmacaoInscricaoEspanhol).HasColumnName(@"dsAssuntoCredenciamentoConfirmacaoInscricaoEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsAssuntoCredenciamentoConfirmacaoPagamentoPortugues).HasColumnName(@"dsAssuntoCredenciamentoConfirmacaoPagamentoPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsAssuntoCredenciamentoConfirmacaoPagamentoIngles).HasColumnName(@"dsAssuntoCredenciamentoConfirmacaoPagamentoIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsAssuntoCredenciamentoConfirmacaoPagamentoEspanhol).HasColumnName(@"dsAssuntoCredenciamentoConfirmacaoPagamentoEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsAssuntoCredenciamentoConfirmacaoPedidoPortugues).HasColumnName(@"dsAssuntoCredenciamentoConfirmacaoPedidoPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsAssuntoCredenciamentoConfirmacaoPedidoIngles).HasColumnName(@"dsAssuntoCredenciamentoConfirmacaoPedidoIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsAssuntoCredenciamentoConfirmacaoPedidoEspanhol).HasColumnName(@"dsAssuntoCredenciamentoConfirmacaoPedidoEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsCieloLoja).HasColumnName(@"dsCieloLoja").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsCieloLojaChave).HasColumnName(@"dsCieloLojaChave").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.fgHabilitadoCampanhaSejaBemVindo).HasColumnName(@"fgHabilitadoCampanhaSejaBemVindo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgPermiteSelecao).HasColumnName(@"fgPermiteSelecao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);

            // Foreign keys
            HasRequired(a => a.TB_DD_Evento).WithMany(b => b.TB_CF_Edicao).HasForeignKey(c => c.cdEvento).WillCascadeOnDelete(false); // FK_TB_CF_Edicao_TB_DD_Evento
        }
    }

    // TB_CF_EstruturaPadrao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_EstruturaPadraoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_CF_EstruturaPadrao>
    {
        public TB_CF_EstruturaPadraoConfiguration()
            : this("dbo")
        {
        }

        public TB_CF_EstruturaPadraoConfiguration(string schema)
        {
            ToTable("TB_CF_EstruturaPadrao", schema);
            HasKey(x => x.cdEstruturaPadrao);

            Property(x => x.cdEstruturaPadrao).HasColumnName(@"cdEstruturaPadrao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdFormulario).HasColumnName(@"cdFormulario").HasColumnType("int").IsRequired();
            Property(x => x.cdTipoObjetoPortugues).HasColumnName(@"cdTipoObjetoPortugues").HasColumnType("int").IsRequired();
            Property(x => x.cdTipoValidacao).HasColumnName(@"cdTipoValidacao").HasColumnType("int").IsRequired();
            Property(x => x.dsPortugues).HasColumnName(@"dsPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsIngles).HasColumnName(@"dsIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsEspanhol).HasColumnName(@"dsEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.nmObjeto).HasColumnName(@"nmObjeto").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(200);
            Property(x => x.fgObrigatorioPortugues).HasColumnName(@"fgObrigatorioPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgObrigatorioIngles).HasColumnName(@"fgObrigatorioIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgObrigatorioEspanhol).HasColumnName(@"fgObrigatorioEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsMensagemObrigatorioPortugues).HasColumnName(@"dsMensagemObrigatorioPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsMensagemObrigatorioIngles).HasColumnName(@"dsMensagemObrigatorioIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsMensagemObrigatorioEspanhol).HasColumnName(@"dsMensagemObrigatorioEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.nuQtdCaracteres).HasColumnName(@"nuQtdCaracteres").HasColumnType("int").IsRequired();
            Property(x => x.nuWidthObjeto).HasColumnName(@"nuWidthObjeto").HasColumnType("int").IsRequired();
            Property(x => x.nuOrdem).HasColumnName(@"nuOrdem").HasColumnType("int").IsRequired();
            Property(x => x.nmGrupoExibicao).HasColumnName(@"nmGrupoExibicao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdTipoObjetoIngles).HasColumnName(@"cdTipoObjetoIngles").HasColumnType("int").IsOptional();
            Property(x => x.cdTipoObjetoEspanhol).HasColumnName(@"cdTipoObjetoEspanhol").HasColumnType("int").IsOptional();
            Property(x => x.cdGruposDadosCadastrais).HasColumnName(@"cdGruposDadosCadastrais").HasColumnType("int").IsOptional();
            Property(x => x.fgBasico).HasColumnName(@"fgBasico").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgCompleto).HasColumnName(@"fgCompleto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);

            // Foreign keys
            HasOptional(a => a.TB_RF_GruposDadosCadastrais).WithMany(b => b.TB_CF_EstruturaPadrao).HasForeignKey(c => c.cdGruposDadosCadastrais).WillCascadeOnDelete(false); // FK_TB_CF_EstruturaPadrao_TB_RF_GruposDadosCadastrais
            HasOptional(a => a.TB_RF_TipoObjeto_cdTipoObjetoEspanhol).WithMany(b => b.cdTipoObjetoEspanhol).HasForeignKey(c => c.cdTipoObjetoEspanhol).WillCascadeOnDelete(false); // FK_TB_CF_EstruturaPadrao_TB_RF_TipoObjetoEspanhol
            HasOptional(a => a.TB_RF_TipoObjeto_cdTipoObjetoIngles).WithMany(b => b.cdTipoObjetoIngles).HasForeignKey(c => c.cdTipoObjetoIngles).WillCascadeOnDelete(false); // FK_TB_CF_EstruturaPadrao_TB_RF_TipoObjetoIngles
            HasRequired(a => a.TB_CF_Formulario).WithMany(b => b.TB_CF_EstruturaPadrao).HasForeignKey(c => c.cdFormulario).WillCascadeOnDelete(false); // FK_TB_CF_EstruturaPadrao_TB_CF_Formulario
            HasRequired(a => a.TB_RF_TipoObjeto_cdTipoObjetoPortugues).WithMany(b => b.cdTipoObjetoPortugues).HasForeignKey(c => c.cdTipoObjetoPortugues).WillCascadeOnDelete(false); // FK_TB_CF_EstruturaPadrao_TB_RF_TipoObjeto
            HasRequired(a => a.TB_RF_TipoValidacao).WithMany(b => b.TB_CF_EstruturaPadrao).HasForeignKey(c => c.cdTipoValidacao).WillCascadeOnDelete(false); // FK_tb_cf_EstruturaPadrao_tb_RF_TipoValidacao
        }
    }

    // TB_CF_ExecaoFormularioManualEletronico
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_ExecaoFormularioManualEletronicoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_CF_ExecaoFormularioManualEletronico>
    {
        public TB_CF_ExecaoFormularioManualEletronicoConfiguration()
            : this("dbo")
        {
        }

        public TB_CF_ExecaoFormularioManualEletronicoConfiguration(string schema)
        {
            ToTable("TB_CF_ExecaoFormularioManualEletronico", schema);
            HasKey(x => x.cdExecao);

            Property(x => x.cdExecao).HasColumnName(@"cdExecao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dtExcecao).HasColumnName(@"dtExcecao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdFormulario).HasColumnName(@"cdFormulario").HasColumnType("int").IsOptional();
            Property(x => x.cdStand).HasColumnName(@"cdStand").HasColumnType("int").IsOptional();
            Property(x => x.dtLimite).HasColumnName(@"dtLimite").HasColumnType("datetime").IsOptional();
            Property(x => x.qtdCredencial).HasColumnName(@"qtdCredencial").HasColumnType("int").IsOptional();
            Property(x => x.cdUsuario).HasColumnName(@"cdUsuario").HasColumnType("int").IsOptional();
            Property(x => x.fgHabilitado).HasColumnName(@"fgHabilitado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
        }
    }

    // TB_CF_Formulario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_FormularioConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_CF_Formulario>
    {
        public TB_CF_FormularioConfiguration()
            : this("dbo")
        {
        }

        public TB_CF_FormularioConfiguration(string schema)
        {
            ToTable("TB_CF_Formulario", schema);
            HasKey(x => x.cdFormulario);

            Property(x => x.cdFormulario).HasColumnName(@"cdFormulario").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdTipoFormulario).HasColumnName(@"cdTipoFormulario").HasColumnType("int").IsRequired();
            Property(x => x.nmFormularioPortugues).HasColumnName(@"nmFormularioPortugues").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(200);
            Property(x => x.nmFormularioIngles).HasColumnName(@"nmFormularioIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.nmFormularioEspanhol).HasColumnName(@"nmFormularioEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsCodigoPagina).HasColumnName(@"dsCodigoPagina").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);

            // Foreign keys
            HasRequired(a => a.TB_RF_TipoFormulario).WithMany(b => b.TB_CF_Formulario).HasForeignKey(c => c.cdTipoFormulario).WillCascadeOnDelete(false); // FK_TB_CF_Formulario_TB_RF_TipoFormulario
        }
    }

    // TB_CF_FormularioGenerico
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_FormularioGenericoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_CF_FormularioGenerico>
    {
        public TB_CF_FormularioGenericoConfiguration()
            : this("dbo")
        {
        }

        public TB_CF_FormularioGenericoConfiguration(string schema)
        {
            ToTable("TB_CF_FormularioGenerico", schema);
            HasKey(x => x.cdFormularioGenerico);

            Property(x => x.cdFormularioGenerico).HasColumnName(@"cdFormularioGenerico").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdProdutoOperacional).HasColumnName(@"cdProdutoOperacional").HasColumnType("int").IsRequired();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsObjeto).HasColumnName(@"dsObjeto").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.nuOrdem).HasColumnName(@"nuOrdem").HasColumnType("int").IsRequired();
            Property(x => x.qtdCaracteres).HasColumnName(@"qtdCaracteres").HasColumnType("int").IsRequired();
            Property(x => x.cdTipoValidacao).HasColumnName(@"cdTipoValidacao").HasColumnType("int").IsRequired();
            Property(x => x.nmPortugues).HasColumnName(@"nmPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.nmIngles).HasColumnName(@"nmIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.nmEspanhol).HasColumnName(@"nmEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.fgHabilitaPortugues).HasColumnName(@"fgHabilitaPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgHabilitaIngles).HasColumnName(@"fgHabilitaIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgHabilitaEspanhol).HasColumnName(@"fgHabilitaEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgObrigatorioPortugues).HasColumnName(@"fgObrigatorioPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgObrigatorioIngles).HasColumnName(@"fgObrigatorioIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgObrigatorioEspanhol).HasColumnName(@"fgObrigatorioEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsPreenchimentoObrigatorioPortugues).HasColumnName(@"dsPreenchimentoObrigatorioPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsPreenchimentoObrigatorioIngles).HasColumnName(@"dsPreenchimentoObrigatorioIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsPreenchimentoObrigatorioEspanhol).HasColumnName(@"dsPreenchimentoObrigatorioEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsPreenchimentoIncorretoPortugues).HasColumnName(@"dsPreenchimentoIncorretoPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsPreenchimentoIncorretoIngles).HasColumnName(@"dsPreenchimentoIncorretoIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsPreenchimentoIncorretoEspanhol).HasColumnName(@"dsPreenchimentoIncorretoEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsRequired();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_DD_ProdutoOperacional).WithMany(b => b.TB_CF_FormularioGenerico).HasForeignKey(c => c.cdProdutoOperacional).WillCascadeOnDelete(false); // FK_FormularioGenerico_ProdutoOperacional
            HasRequired(a => a.TB_RF_TipoValidacao).WithMany(b => b.TB_CF_FormularioGenerico).HasForeignKey(c => c.cdTipoValidacao).WillCascadeOnDelete(false); // FK_FormularioGenerico_TipoValidacao
        }
    }

    // TB_CF_FormularioManualEletronico
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_FormularioManualEletronicoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_CF_FormularioManualEletronico>
    {
        public TB_CF_FormularioManualEletronicoConfiguration()
            : this("dbo")
        {
        }

        public TB_CF_FormularioManualEletronicoConfiguration(string schema)
        {
            ToTable("TB_CF_FormularioManualEletronico", schema);
            HasKey(x => x.cdFormulario);

            Property(x => x.cdFormulario).HasColumnName(@"cdFormulario").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.nuOrdem).HasColumnName(@"nuOrdem").HasColumnType("int").IsOptional();
            Property(x => x.nmFormulario).HasColumnName(@"nmFormulario").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsFormulario).HasColumnName(@"dsFormulario").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.nmFormularioIngl).HasColumnName(@"nmFormularioIngl").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsFormularioIngl).HasColumnName(@"dsFormularioIngl").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsUrl).HasColumnName(@"dsUrl").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dtLimite).HasColumnName(@"dtLimite").HasColumnType("datetime").IsOptional();
            Property(x => x.dsObrigatoriedade).HasColumnName(@"dsObrigatoriedade").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsExpositor).HasColumnName(@"dsExpositor").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsMontadora).HasColumnName(@"dsMontadora").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsServico).HasColumnName(@"dsServico").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCarrinho).HasColumnName(@"dsCarrinho").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsQtdFracao).HasColumnName(@"dsQtdFracao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsTipo).HasColumnName(@"dsTipo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsVendaUnica).HasColumnName(@"dsVendaUnica").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
            Property(x => x.dsHabilitado).HasColumnName(@"dsHabilitado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsPermiteImportacaoCredencial).HasColumnName(@"dsPermiteImportacaoCredencial").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsPermiteAlteracaoEmpresaCracha).HasColumnName(@"dsPermiteAlteracaoEmpresaCracha").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsTipoCredencial).HasColumnName(@"dsTipoCredencial").HasColumnType("int").IsOptional();
            Property(x => x.dsCPFObrigatorio).HasColumnName(@"dsCPFObrigatorio").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsPassportObrigatorio).HasColumnName(@"dsPassportObrigatorio").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsTopo).HasColumnName(@"dsTopo").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsRodape).HasColumnName(@"dsRodape").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsTopoIng).HasColumnName(@"dsTopoIng").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsRodapeIng).HasColumnName(@"dsRodapeIng").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsCarta).HasColumnName(@"dsCarta").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsCartaIng).HasColumnName(@"dsCartaIng").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.fgHabilitaVendaDados).HasColumnName(@"fgHabilitaVendaDados").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsIncideImposto).HasColumnName(@"dsIncideImposto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsCaex).HasColumnName(@"dsCaex").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);

            // Foreign keys
            HasRequired(a => a.TB_CF_Edicao).WithMany(b => b.TB_CF_FormularioManualEletronico).HasForeignKey(c => c.TB_Edicao_cdEdicao).WillCascadeOnDelete(false); // FK_TB_CF_FormularioManualEletronico_cdEdicao
        }
    }

    // TB_CF_FormularioSistemaIntegrado
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_FormularioSistemaIntegradoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_CF_FormularioSistemaIntegrado>
    {
        public TB_CF_FormularioSistemaIntegradoConfiguration()
            : this("dbo")
        {
        }

        public TB_CF_FormularioSistemaIntegradoConfiguration(string schema)
        {
            ToTable("TB_CF_FormularioSistemaIntegrado", schema);
            HasKey(x => x.cdFormulario);

            Property(x => x.cdFormulario).HasColumnName(@"cdFormulario").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsFormulario).HasColumnName(@"dsFormulario").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsGrupo).HasColumnName(@"dsGrupo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsStatus).HasColumnName(@"dsStatus").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
        }
    }

    // TB_CF_INT_Geral
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_INT_GeralConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_CF_INT_Geral>
    {
        public TB_CF_INT_GeralConfiguration()
            : this("dbo")
        {
        }

        public TB_CF_INT_GeralConfiguration(string schema)
        {
            ToTable("TB_CF_INT_Geral", schema);
            HasKey(x => x.cdConfig);

            Property(x => x.cdConfig).HasColumnName(@"cdConfig").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.wkDiretorioFotoFuncionario).HasColumnName(@"wkDiretorioFotoFuncionario").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
        }
    }

    // TB_CF_Permissoes
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_PermissoesConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_CF_Permissoes>
    {
        public TB_CF_PermissoesConfiguration()
            : this("dbo")
        {
        }

        public TB_CF_PermissoesConfiguration(string schema)
        {
            ToTable("TB_CF_Permissoes", schema);
            HasKey(x => x.cdPermissao);

            Property(x => x.cdPermissao).HasColumnName(@"cdPermissao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdFormulario).HasColumnName(@"cdFormulario").HasColumnType("int").IsRequired();
            Property(x => x.cdUsuario).HasColumnName(@"cdUsuario").HasColumnType("int").IsRequired();
            Property(x => x.dsRead).HasColumnName(@"dsRead").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(30);
            Property(x => x.dsWrite).HasColumnName(@"dsWrite").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(30);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsRequired();

            // Foreign keys
            HasRequired(a => a.TB_CF_Edicao).WithMany(b => b.TB_CF_Permissoes).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_CF_Permissoes_cdEdicao
            HasRequired(a => a.TB_CF_FormularioSistemaIntegrado).WithMany(b => b.TB_CF_Permissoes).HasForeignKey(c => c.cdFormulario).WillCascadeOnDelete(false); // FK_TB_CF_Permissoes_TB_CF_FormularioSistemaIntegrado
            HasRequired(a => a.TB_DD_Usuario).WithMany(b => b.TB_CF_Permissoes).HasForeignKey(c => c.cdUsuario).WillCascadeOnDelete(false); // FK_TB_CF_Permissoes_cdUsuario
        }
    }

    // TB_CF_Recibo
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_ReciboConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_CF_Recibo>
    {
        public TB_CF_ReciboConfiguration()
            : this("dbo")
        {
        }

        public TB_CF_ReciboConfiguration(string schema)
        {
            ToTable("TB_CF_Recibo", schema);
            HasKey(x => x.cdConfiguracao);

            Property(x => x.cdConfiguracao).HasColumnName(@"cdConfiguracao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsEmpresa).HasColumnName(@"dsEmpresa").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsCNPJ).HasColumnName(@"dsCNPJ").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(20);
            Property(x => x.dsLogradouro).HasColumnName(@"dsLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsLogradouroNumero).HasColumnName(@"dsLogradouroNumero").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsLogradouroBairro).HasColumnName(@"dsLogradouroBairro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsLogradouroCep).HasColumnName(@"dsLogradouroCep").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsLogradouroCidade).HasColumnName(@"dsLogradouroCidade").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsLogradouroEstado).HasColumnName(@"dsLogradouroEstado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsInicioRecibo).HasColumnName(@"dsInicioRecibo").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(200);
            Property(x => x.fgHabilitado).HasColumnName(@"fgHabilitado").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
        }
    }

    // TB_CF_VendaDados
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_CF_VendaDadosConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_CF_VendaDados>
    {
        public TB_CF_VendaDadosConfiguration()
            : this("dbo")
        {
        }

        public TB_CF_VendaDadosConfiguration(string schema)
        {
            ToTable("TB_CF_VendaDados", schema);
            HasKey(x => x.cdVendaDados);

            Property(x => x.cdVendaDados).HasColumnName(@"cdVendaDados").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdProdutoOperacional).HasColumnName(@"cdProdutoOperacional").HasColumnType("int").IsRequired();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsObjeto).HasColumnName(@"dsObjeto").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.nuOrdem).HasColumnName(@"nuOrdem").HasColumnType("int").IsRequired();
            Property(x => x.qtdCaracteres).HasColumnName(@"qtdCaracteres").HasColumnType("int").IsOptional();
            Property(x => x.cdTipoValidacao).HasColumnName(@"cdTipoValidacao").HasColumnType("int").IsRequired();
            Property(x => x.nmPortugues).HasColumnName(@"nmPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.nmIngles).HasColumnName(@"nmIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.nmEspanhol).HasColumnName(@"nmEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.fgHabilitaPortugues).HasColumnName(@"fgHabilitaPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgHabilitaIngles).HasColumnName(@"fgHabilitaIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgHabilitaEspanhol).HasColumnName(@"fgHabilitaEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgObrigatorioPortugues).HasColumnName(@"fgObrigatorioPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgObrigatorioIngles).HasColumnName(@"fgObrigatorioIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgObrigatorioEspanhol).HasColumnName(@"fgObrigatorioEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsPreenchimentoObrigatorioPortugues).HasColumnName(@"dsPreenchimentoObrigatorioPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsPreenchimentoObrigatorioIngles).HasColumnName(@"dsPreenchimentoObrigatorioIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsPreenchimentoObrigatorioEspanhol).HasColumnName(@"dsPreenchimentoObrigatorioEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsPreenchimentoIncorretoPortugues).HasColumnName(@"dsPreenchimentoIncorretoPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsPreenchimentoIncorretoIngles).HasColumnName(@"dsPreenchimentoIncorretoIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsPreenchimentoIncorretoEspanhol).HasColumnName(@"dsPreenchimentoIncorretoEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsRequired();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_DD_ProdutoOperacional).WithMany(b => b.TB_CF_VendaDados).HasForeignKey(c => c.cdProdutoOperacional).WillCascadeOnDelete(false); // PK_VendaDados_ProdutoOperacional
            HasRequired(a => a.TB_RF_TipoValidacao).WithMany(b => b.TB_CF_VendaDados).HasForeignKey(c => c.cdTipoValidacao).WillCascadeOnDelete(false); // PK_VendaDados_TipoValidacao
        }
    }

    // TB_DD_Agendamento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_AgendamentoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Agendamento>
    {
        public TB_DD_AgendamentoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_AgendamentoConfiguration(string schema)
        {
            ToTable("TB_DD_Agendamento", schema);
            HasKey(x => x.cdAgendamento);

            Property(x => x.cdAgendamento).HasColumnName(@"cdAgendamento").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdUsuario).HasColumnName(@"cdUsuario").HasColumnType("int").IsRequired();
            Property(x => x.dtAgendamento).HasColumnName(@"dtAgendamento").HasColumnType("datetime").IsRequired();
            Property(x => x.dsNome).HasColumnName(@"dsNome").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsTelefone).HasColumnName(@"dsTelefone").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsStatus).HasColumnName(@"dsStatus").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(50);
            Property(x => x.dsObs).HasColumnName(@"dsObs").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);

            // Foreign keys
            HasRequired(a => a.TB_DD_Usuario).WithMany(b => b.TB_DD_Agendamento).HasForeignKey(c => c.cdUsuario).WillCascadeOnDelete(false); // FK_DD_Agendamento_Usuario
        }
    }

    // TB_DD_Alternativa
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_AlternativaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Alternativa>
    {
        public TB_DD_AlternativaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_AlternativaConfiguration(string schema)
        {
            ToTable("TB_DD_Alternativa", schema);
            HasKey(x => x.cdAlternativa);

            Property(x => x.cdAlternativa).HasColumnName(@"cdAlternativa").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsSigla).HasColumnName(@"dsSigla").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsPortugues).HasColumnName(@"dsPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsIngles).HasColumnName(@"dsIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsEspanhol).HasColumnName(@"dsEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
        }
    }

    // TB_DD_AlternativaQuestionarioManual
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_AlternativaQuestionarioManualConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_AlternativaQuestionarioManual>
    {
        public TB_DD_AlternativaQuestionarioManualConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_AlternativaQuestionarioManualConfiguration(string schema)
        {
            ToTable("TB_DD_AlternativaQuestionarioManual", schema);
            HasKey(x => x.cdAlternativaQuestionarioManual);

            Property(x => x.cdAlternativaQuestionarioManual).HasColumnName(@"cdAlternativaQuestionarioManual").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdQuestionarioManual).HasColumnName(@"cdQuestionarioManual").HasColumnType("int").IsRequired();
            Property(x => x.nuOrdem).HasColumnName(@"nuOrdem").HasColumnType("int").IsRequired();
            Property(x => x.dsPortugues).HasColumnName(@"dsPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsIngles).HasColumnName(@"dsIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsEspanhol).HasColumnName(@"dsEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsOutros).HasColumnName(@"dsOutros").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsRequired();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_DD_QuestionarioManual).WithMany(b => b.TB_DD_AlternativaQuestionarioManual).HasForeignKey(c => c.cdQuestionarioManual).WillCascadeOnDelete(false); // FK_TB_DD_AlternativaQuestionarioManual_TB_DD_QuestionarioManual
        }
    }

    // TB_DD_App_ConvideAmigo
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_App_ConvideAmigoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_App_ConvideAmigo>
    {
        public TB_DD_App_ConvideAmigoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_App_ConvideAmigoConfiguration(string schema)
        {
            ToTable("TB_DD_App_ConvideAmigo", schema);
            HasKey(x => x.cdConvite);

            Property(x => x.cdConvite).HasColumnName(@"cdConvite").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsRequired();
            Property(x => x.TB_Pessoa_cdPessoa).HasColumnName(@"TB_Pessoa_cdPessoa").HasColumnType("int").IsRequired();
            Property(x => x.dsNomeConvidado).HasColumnName(@"dsNomeConvidado").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(250);
            Property(x => x.dsEmailConvidado).HasColumnName(@"dsEmailConvidado").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(250);
            Property(x => x.dsCodigoBarras).HasColumnName(@"dsCodigoBarras").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.fgEfetivado).HasColumnName(@"fgEfetivado").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dtEfetivado).HasColumnName(@"dtEfetivado").HasColumnType("datetime").IsOptional();
        }
    }

    // TB_DD_App_NoticiaAviso
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_App_NoticiaAvisoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_App_NoticiaAviso>
    {
        public TB_DD_App_NoticiaAvisoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_App_NoticiaAvisoConfiguration(string schema)
        {
            ToTable("TB_DD_App_NoticiaAviso", schema);
            HasKey(x => x.cdNoticiaAviso);

            Property(x => x.cdNoticiaAviso).HasColumnName(@"cdNoticiaAviso").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsRequired();
            Property(x => x.dsTituloPortugues).HasColumnName(@"dsTituloPortugues").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(2000);
            Property(x => x.dsNoticiaAvisoPortugues).HasColumnName(@"dsNoticiaAvisoPortugues").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(2000);
            Property(x => x.dsTituloIngles).HasColumnName(@"dsTituloIngles").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(2000);
            Property(x => x.dsNoticiaAvisoIngles).HasColumnName(@"dsNoticiaAvisoIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(2000);
            Property(x => x.dsTipo).HasColumnName(@"dsTipo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(15);
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();
        }
    }

    // TB_DD_AprovacaoReprovacaoDeProjeto
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_AprovacaoReprovacaoDeProjetoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_AprovacaoReprovacaoDeProjeto>
    {
        public TB_DD_AprovacaoReprovacaoDeProjetoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_AprovacaoReprovacaoDeProjetoConfiguration(string schema)
        {
            ToTable("TB_DD_AprovacaoReprovacaoDeProjeto", schema);
            HasKey(x => x.cdAprovacaoReprovacao);

            Property(x => x.cdAprovacaoReprovacao).HasColumnName(@"cdAprovacaoReprovacao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Empresa_cdEmpresa).HasColumnName(@"TB_Empresa_cdEmpresa").HasColumnType("int").IsRequired();
            Property(x => x.TB_Stand_cdStand).HasColumnName(@"TB_Stand_cdStand").HasColumnType("int").IsRequired();
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.dsDocumento).HasColumnName(@"dsDocumento").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(50);
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioInclusao).HasColumnName(@"cdUsuarioInclusao").HasColumnType("int").IsRequired();
            Property(x => x.dtAprovacaoReprovacao).HasColumnName(@"dtAprovacaoReprovacao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAprovacaoReprovacao).HasColumnName(@"cdUsuarioAprovacaoReprovacao").HasColumnType("int").IsOptional();
            Property(x => x.dtAltercao).HasColumnName(@"dtAltercao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();
        }
    }

    // TB_DD_AssistentePreenchimento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_AssistentePreenchimentoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_AssistentePreenchimento>
    {
        public TB_DD_AssistentePreenchimentoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_AssistentePreenchimentoConfiguration(string schema)
        {
            ToTable("TB_DD_AssistentePreenchimento", schema);
            HasKey(x => x.cdAssistentePreenchimento);

            Property(x => x.cdAssistentePreenchimento).HasColumnName(@"cdAssistentePreenchimento").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdUsuarioInclusao).HasColumnName(@"cdUsuarioInclusao").HasColumnType("int").IsRequired();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsRequired();
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsRequired();
            Property(x => x.dsNomeFormulario).HasColumnName(@"dsNomeFormulario").HasColumnType("varchar(max)").IsRequired().IsUnicode(false);
            Property(x => x.cdFormulario).HasColumnName(@"cdFormulario").HasColumnType("int").IsRequired();
            Property(x => x.dsTextoPortugues).HasColumnName(@"dsTextoPortugues").HasColumnType("varchar(max)").IsRequired().IsUnicode(false);
            Property(x => x.dsTextoIngles).HasColumnName(@"dsTextoIngles").HasColumnType("varchar(max)").IsRequired().IsUnicode(false);
            Property(x => x.fgHabilitado).HasColumnName(@"fgHabilitado").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.cdSistema).HasColumnName(@"cdSistema").HasColumnType("int").IsOptional();
            Property(x => x.dsTextoEspanhol).HasColumnName(@"dsTextoEspanhol").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
        }
    }

    // TB_DD_ATOM
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ATOMConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ATOM>
    {
        public TB_DD_ATOMConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ATOMConfiguration(string schema)
        {
            ToTable("TB_DD_ATOM", schema);
            HasKey(x => x.cdGeracao);

            Property(x => x.cdGeracao).HasColumnName(@"cdGeracao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dtCriacao).HasColumnName(@"dtCriacao").HasColumnType("datetime").IsRequired();
            Property(x => x.UsuarioInsercao).HasColumnName(@"UsuarioInsercao").HasColumnType("int").IsRequired();
            Property(x => x.DataGeracaoArquivo).HasColumnName(@"DataGeracaoArquivo").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(500);
            Property(x => x.DAT).HasColumnName(@"DAT").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(500);
            Property(x => x.DEM).HasColumnName(@"DEM").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(500);
            Property(x => x.TRA).HasColumnName(@"TRA").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(500);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_ATOM).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_DD_Atom_Edicao
        }
    }

    // TB_DD_BloqueioContratoProduto
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_BloqueioContratoProdutoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_BloqueioContratoProduto>
    {
        public TB_DD_BloqueioContratoProdutoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_BloqueioContratoProdutoConfiguration(string schema)
        {
            ToTable("TB_DD_BloqueioContratoProduto", schema);
            HasKey(x => x.cdPermissao);

            Property(x => x.cdPermissao).HasColumnName(@"cdPermissao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdProduto).HasColumnName(@"cdProduto").HasColumnType("int").IsOptional();
            Property(x => x.cdTipoContrato).HasColumnName(@"cdTipoContrato").HasColumnType("int").IsOptional();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(20);

            // Foreign keys
            HasOptional(a => a.TB_DD_ProdutoOperacional).WithMany(b => b.TB_DD_BloqueioContratoProduto).HasForeignKey(c => c.cdProduto).WillCascadeOnDelete(false); // FK_TB_DD_BloqueioContratoProduto_cdProduto
            HasOptional(a => a.TB_RF_TipoContrato).WithMany(b => b.TB_DD_BloqueioContratoProduto).HasForeignKey(c => c.cdTipoContrato).WillCascadeOnDelete(false); // FK_TB_DD_BloqueioContratoProduto_cdTipoContrato
        }
    }

    // TB_DD_CarrinhoOperacional
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_CarrinhoOperacionalConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_CarrinhoOperacional>
    {
        public TB_DD_CarrinhoOperacionalConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_CarrinhoOperacionalConfiguration(string schema)
        {
            ToTable("TB_DD_CarrinhoOperacional", schema);
            HasKey(x => x.cdItem);

            Property(x => x.cdItem).HasColumnName(@"cdItem").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEmpresa).HasColumnName(@"cdEmpresa").HasColumnType("int").IsOptional();
            Property(x => x.cdStand).HasColumnName(@"cdStand").HasColumnType("int").IsOptional();
            Property(x => x.cdProduto).HasColumnName(@"cdProduto").HasColumnType("int").IsOptional();
            Property(x => x.QtdProduto).HasColumnName(@"QtdProduto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdPedido).HasColumnName(@"cdPedido").HasColumnType("int").IsOptional();
            Property(x => x.cdFormulario).HasColumnName(@"cdFormulario").HasColumnType("int").IsOptional();
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.cdPessoa).HasColumnName(@"cdPessoa").HasColumnType("int").IsOptional();
            Property(x => x.CodigoBarras).HasColumnName(@"CodigoBarras").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(30);
            Property(x => x.dsDescricaoOpcional).HasColumnName(@"dsDescricaoOpcional").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.cdEmpresaSegundario).HasColumnName(@"cdEmpresaSegundario").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_CarrinhoOperacional).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_CarrinhoOperacional_cdEdicao
            HasOptional(a => a.TB_CF_FormularioManualEletronico).WithMany(b => b.TB_DD_CarrinhoOperacional).HasForeignKey(c => c.cdFormulario).WillCascadeOnDelete(false); // FK_TB_DD_CarrinhoOperacional_cdFormulario
            HasOptional(a => a.TB_DD_Empresa).WithMany(b => b.TB_DD_CarrinhoOperacional).HasForeignKey(c => c.cdEmpresa).WillCascadeOnDelete(false); // FK_TB_DD_CarrinhoOperacional_cdEmpresa
            HasOptional(a => a.TB_DD_ProdutoOperacional).WithMany(b => b.TB_DD_CarrinhoOperacional).HasForeignKey(c => c.cdProduto).WillCascadeOnDelete(false); // FK_TB_DD_CarrinhoOperacional_TB_DD_ProdutoOperacional
            HasOptional(a => a.TB_DD_Stand).WithMany(b => b.TB_DD_CarrinhoOperacional).HasForeignKey(c => c.cdStand).WillCascadeOnDelete(false); // FK_TB_DD_CarrinhoOperacional_cdStand
        }
    }

    // TB_DD_CarrinhoOperacional_VendaDados
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_CarrinhoOperacional_VendaDadosConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_CarrinhoOperacional_VendaDados>
    {
        public TB_DD_CarrinhoOperacional_VendaDadosConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_CarrinhoOperacional_VendaDadosConfiguration(string schema)
        {
            ToTable("TB_DD_CarrinhoOperacional_VendaDados", schema);
            HasKey(x => x.cdCarrinhoOperacionalVendaDados);

            Property(x => x.cdCarrinhoOperacionalVendaDados).HasColumnName(@"cdCarrinhoOperacionalVendaDados").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdCarrinhoOperacional).HasColumnName(@"cdCarrinhoOperacional").HasColumnType("int").IsRequired();
            Property(x => x.cdVendaDados).HasColumnName(@"cdVendaDados").HasColumnType("int").IsRequired();
            Property(x => x.dsConteudo).HasColumnName(@"dsConteudo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsRequired();

            // Foreign keys
            HasRequired(a => a.TB_CF_VendaDados).WithMany(b => b.TB_DD_CarrinhoOperacional_VendaDados).HasForeignKey(c => c.cdVendaDados).WillCascadeOnDelete(false); // FK_DD_CarrinhoOperacional_VendaDados_CF_VendaDados
            HasRequired(a => a.TB_DD_CarrinhoOperacional).WithMany(b => b.TB_DD_CarrinhoOperacional_VendaDados).HasForeignKey(c => c.cdCarrinhoOperacional).WillCascadeOnDelete(false); // FK_DD_CarrinhoOperacional_VendaDados_DD_CarrinhoOperacional
        }
    }

    // TB_DD_CatalogoOficial
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_CatalogoOficialConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_CatalogoOficial>
    {
        public TB_DD_CatalogoOficialConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_CatalogoOficialConfiguration(string schema)
        {
            ToTable("TB_DD_CatalogoOficial", schema);
            HasKey(x => x.cdCatalogoOficial);

            Property(x => x.cdCatalogoOficial).HasColumnName(@"cdCatalogoOficial").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEmpresa).HasColumnName(@"cdEmpresa").HasColumnType("int").IsRequired();
            Property(x => x.cdStand).HasColumnName(@"cdStand").HasColumnType("int").IsOptional();
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.TB_Pais_cdPais).HasColumnName(@"TB_Pais_cdPais").HasColumnType("int").IsRequired();
            Property(x => x.dsNomeFantasia).HasColumnName(@"dsNomeFantasia").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsLogradouro).HasColumnName(@"dsLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsComplementoLogradouro).HasColumnName(@"dsComplementoLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsNumeroLogradouro).HasColumnName(@"dsNumeroLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCepLogradouro).HasColumnName(@"dsCepLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsBairroLogradouro).HasColumnName(@"dsBairroLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCidadeLogradouro).HasColumnName(@"dsCidadeLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsUfLogradouro).HasColumnName(@"dsUfLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsEmail).HasColumnName(@"dsEmail").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsSite).HasColumnName(@"dsSite").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsSegmento).HasColumnName(@"dsSegmento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsLocalizacaoPlanta).HasColumnName(@"dsLocalizacaoPlanta").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsAreaAtuacao).HasColumnName(@"dsAreaAtuacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsTexto).HasColumnName(@"dsTexto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(300);
            Property(x => x.dsIncialEmpresa).HasColumnName(@"dsIncialEmpresa").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsAtividadePor).HasColumnName(@"dsAtividadePor").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsAtividadeIng).HasColumnName(@"dsAtividadeIng").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsTelefone).HasColumnName(@"dsTelefone").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsFax).HasColumnName(@"dsFax").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsContato).HasColumnName(@"dsContato").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCargo).HasColumnName(@"dsCargo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsContato2).HasColumnName(@"dsContato2").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsCargo2).HasColumnName(@"dsCargo2").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsCargo3).HasColumnName(@"dsCargo3").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsContato3).HasColumnName(@"dsContato3").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsLetra).HasColumnName(@"dsLetra").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(2);
            Property(x => x.dsAtividades).HasColumnName(@"dsAtividades").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsNumeroStandIndoor).HasColumnName(@"dsNumeroStandIndoor").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsNumeroStandOutdoor).HasColumnName(@"dsNumeroStandOutdoor").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDDITelefone).HasColumnName(@"dsDDITelefone").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDDDTelefone).HasColumnName(@"dsDDDTelefone").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDDIFax).HasColumnName(@"dsDDIFax").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDDDFax).HasColumnName(@"dsDDDFax").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsAtividadePrincipal).HasColumnName(@"dsAtividadePrincipal").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsAtividadeSecundaria).HasColumnName(@"dsAtividadeSecundaria").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsAtividadeTerciaria).HasColumnName(@"dsAtividadeTerciaria").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsResCatalogo1).HasColumnName(@"dsResCatalogo1").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsResCatalogo2).HasColumnName(@"dsResCatalogo2").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResCatalogo3).HasColumnName(@"dsResCatalogo3").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResCatalogo4).HasColumnName(@"dsResCatalogo4").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResCatalogo5).HasColumnName(@"dsResCatalogo5").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResCatalogo6).HasColumnName(@"dsResCatalogo6").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsNumeroStand).HasColumnName(@"dsNumeroStand").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsResCatalogo7).HasColumnName(@"dsResCatalogo7").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);

            // Foreign keys
            HasOptional(a => a.TB_DD_Stand).WithMany(b => b.TB_DD_CatalogoOficial).HasForeignKey(c => c.cdStand).WillCascadeOnDelete(false); // FK_TB_DD_CatalogoOficial_cdStand
            HasRequired(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_CatalogoOficial).HasForeignKey(c => c.TB_Edicao_cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_CatalogoOficial_cdEdicao
            HasRequired(a => a.TB_DD_Empresa).WithMany(b => b.TB_DD_CatalogoOficial).HasForeignKey(c => c.cdEmpresa).WillCascadeOnDelete(false); // FK_TB_DD_CatalogoOficial_cdEmpresa
        }
    }

    // TB_DD_Cnab
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_CnabConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Cnab>
    {
        public TB_DD_CnabConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_CnabConfiguration(string schema)
        {
            ToTable("TB_DD_Cnab", schema);
            HasKey(x => x.cdLinha);

            Property(x => x.cdLinha).HasColumnName(@"cdLinha").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.NuSequenciaArquivo).HasColumnName(@"NuSequenciaArquivo").HasColumnType("int").IsOptional();
            Property(x => x.NuLoteRetorno).HasColumnName(@"NuLoteRetorno").HasColumnType("int").IsOptional();
            Property(x => x.NuAgencia).HasColumnName(@"NuAgencia").HasColumnType("int").IsOptional();
            Property(x => x.NuConta).HasColumnName(@"NuConta").HasColumnType("int").IsOptional();
            Property(x => x.cdPedido).HasColumnName(@"cdPedido").HasColumnType("int").IsRequired();
            Property(x => x.vlPago).HasColumnName(@"vlPago").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtPagamento).HasColumnName(@"dtPagamento").HasColumnType("datetime").IsOptional();
            Property(x => x.dtProcessamentoArquivo).HasColumnName(@"dtProcessamentoArquivo").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioProcessamento).HasColumnName(@"cdUsuarioProcessamento").HasColumnType("int").IsOptional();
            Property(x => x.Chave).HasColumnName(@"Chave").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);

            // Foreign keys
            HasRequired(a => a.TB_DD_Pedido).WithMany(b => b.TB_DD_Cnab).HasForeignKey(c => c.cdPedido).WillCascadeOnDelete(false); // FK_TB_DD_Cnab_TB_DD_Pedido
        }
    }

    // TB_DD_CodigoPromocional
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_CodigoPromocionalConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_CodigoPromocional>
    {
        public TB_DD_CodigoPromocionalConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_CodigoPromocionalConfiguration(string schema)
        {
            ToTable("TB_DD_CodigoPromocional", schema);
            HasKey(x => x.cdPromocional);

            Property(x => x.cdPromocional).HasColumnName(@"cdPromocional").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsPalavra).HasColumnName(@"dsPalavra").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsPromocional).HasColumnName(@"dsPromocional").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsDesconto).HasColumnName(@"dsDesconto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
            Property(x => x.dsTipo).HasColumnName(@"dsTipo").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(10);
            Property(x => x.dsUso).HasColumnName(@"dsUso").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(10);
            Property(x => x.cdProduto).HasColumnName(@"cdProduto").HasColumnType("int").IsOptional();
            Property(x => x.cdSistema).HasColumnName(@"cdSistema").HasColumnType("int").IsOptional();
            Property(x => x.fgSituacao).HasColumnName(@"fgSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
            Property(x => x.dsQuantidade).HasColumnName(@"dsQuantidade").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
        }
    }

    // TB_DD_Codigos
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_CodigosConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Codigos>
    {
        public TB_DD_CodigosConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_CodigosConfiguration(string schema)
        {
            ToTable("TB_DD_Codigos", schema);
            HasKey(x => x.ObjRef);

            Property(x => x.ObjRef).HasColumnName(@"ObjRef").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.CodFicha).HasColumnName(@"CodFicha").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.USO).HasColumnName(@"USO").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.CodigoBarras).HasColumnName(@"CodigoBarras").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.Internacional).HasColumnName(@"Internacional").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.Cobrado).HasColumnName(@"Cobrado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.TB_Categoria_cdCategoria).HasColumnName(@"TB_Categoria_cdCategoria").HasColumnType("int").IsOptional();
            Property(x => x.TipoCodigo).HasColumnName(@"TipoCodigo").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.TipoConvite).HasColumnName(@"TipoConvite").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.vlFicha).HasColumnName(@"vlFicha").HasColumnType("numeric").IsOptional().HasPrecision(18, 2);
            Property(x => x.flVendido).HasColumnName(@"flVendido").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);

            // Foreign keys
            HasOptional(a => a.TB_RF_Categoria).WithMany(b => b.TB_DD_Codigos).HasForeignKey(c => c.TB_Categoria_cdCategoria).WillCascadeOnDelete(false); // FK_TB_DD_Codigos_TB_RF_Categoria_cdCategoria
        }
    }

    // TB_DD_Coletor
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ColetorConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Coletor>
    {
        public TB_DD_ColetorConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ColetorConfiguration(string schema)
        {
            ToTable("TB_DD_Coletor", schema);
            HasKey(x => x.cdColetor);

            Property(x => x.dsColetor).HasColumnName(@"dsColetor").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.TB_Registro_dsCodigoBarras).HasColumnName(@"TB_Registro_dsCodigoBarras").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtEntrada).HasColumnName(@"dtEntrada").HasColumnType("datetime").IsOptional();
            Property(x => x.dtSaida).HasColumnName(@"dtSaida").HasColumnType("datetime").IsOptional();
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.dsAcesso).HasColumnName(@"dsAcesso").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.cdColetor).HasColumnName(@"cdColetor").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsSiglaProduto).HasColumnName(@"dsSiglaProduto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsCodigoCompleto).HasColumnName(@"dsCodigoCompleto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsTemAcompanhante).HasColumnName(@"dsTemAcompanhante").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdColetorSalaPalestra).HasColumnName(@"cdColetorSalaPalestra").HasColumnType("uniqueidentifier").IsOptional();
            Property(x => x.cdColetorSalaPalestraVoucher).HasColumnName(@"cdColetorSalaPalestraVoucher").HasColumnType("uniqueidentifier").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_Coletor).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_Coletor_cdEdicao
        }
    }

    // TB_DD_Comunicados
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ComunicadosConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Comunicados>
    {
        public TB_DD_ComunicadosConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ComunicadosConfiguration(string schema)
        {
            ToTable("TB_DD_Comunicados", schema);
            HasKey(x => x.cdComunicado);

            Property(x => x.cdComunicado).HasColumnName(@"cdComunicado").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.DataInclusao).HasColumnName(@"DataInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioInclusao).HasColumnName(@"cdUsuarioInclusao").HasColumnType("int").IsOptional();
            Property(x => x.dsAssunto).HasColumnName(@"dsAssunto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsMensagem).HasColumnName(@"dsMensagem").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsStatus).HasColumnName(@"dsStatus").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
            Property(x => x.dsAssuntoIng).HasColumnName(@"dsAssuntoIng").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsMensagemIng).HasColumnName(@"dsMensagemIng").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_Comunicados).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_Comunicados_cdEdicao
        }
    }

    // TB_DD_ConfExtracaoPersonalizada
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ConfExtracaoPersonalizadaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ConfExtracaoPersonalizada>
    {
        public TB_DD_ConfExtracaoPersonalizadaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ConfExtracaoPersonalizadaConfiguration(string schema)
        {
            ToTable("TB_DD_ConfExtracaoPersonalizada", schema);
            HasKey(x => x.cdConfExtracaoPersonalizada);

            Property(x => x.cdConfExtracaoPersonalizada).HasColumnName(@"cdConfExtracaoPersonalizada").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdExtracaoPersonalizada).HasColumnName(@"cdExtracaoPersonalizada").HasColumnType("int").IsRequired();
            Property(x => x.dsTipo).HasColumnName(@"dsTipo").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsValor).HasColumnName(@"dsValor").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(500);

            // Foreign keys
            HasRequired(a => a.TB_DD_ExtracaoPersonalizada).WithMany(b => b.TB_DD_ConfExtracaoPersonalizada).HasForeignKey(c => c.cdExtracaoPersonalizada).WillCascadeOnDelete(false); // FK_ConfExtracaoPersonalizada
        }
    }

    // TB_DD_ContratoExpositor
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ContratoExpositorConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ContratoExpositor>
    {
        public TB_DD_ContratoExpositorConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ContratoExpositorConfiguration(string schema)
        {
            ToTable("TB_DD_ContratoExpositor", schema);
            HasKey(x => x.cdContrato);

            Property(x => x.cdContrato).HasColumnName(@"cdContrato").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Empresa_cdEmpresa).HasColumnName(@"TB_Empresa_cdEmpresa").HasColumnType("int").IsRequired();
            Property(x => x.TB_Stand_cdStand).HasColumnName(@"TB_Stand_cdStand").HasColumnType("int").IsRequired();
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.dsNomeExpositorContrato).HasColumnName(@"dsNomeExpositorContrato").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsRgExpositorContrato).HasColumnName(@"dsRgExpositorContrato").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCpfExpositorContrato).HasColumnName(@"dsCpfExpositorContrato").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsNomeTestemunhaContrato).HasColumnName(@"dsNomeTestemunhaContrato").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsRgTestemunhaContrato).HasColumnName(@"dsRgTestemunhaContrato").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCpfTestemunhaContrato).HasColumnName(@"dsCpfTestemunhaContrato").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsNomeFuncionarioPrimario).HasColumnName(@"dsNomeFuncionarioPrimario").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsCpfFuncionarioPrimario).HasColumnName(@"dsCpfFuncionarioPrimario").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCargoFuncionarioPrimario).HasColumnName(@"dsCargoFuncionarioPrimario").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsEmailFuncionarioPrimario).HasColumnName(@"dsEmailFuncionarioPrimario").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsTelefoneFuncionarioPrimario).HasColumnName(@"dsTelefoneFuncionarioPrimario").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsNomeFuncionarioSecundario).HasColumnName(@"dsNomeFuncionarioSecundario").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsCpfFuncionarioSecundario).HasColumnName(@"dsCpfFuncionarioSecundario").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCargoFuncionarioSecundario).HasColumnName(@"dsCargoFuncionarioSecundario").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsEmailFuncionarioSecundario).HasColumnName(@"dsEmailFuncionarioSecundario").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsTelefoneFuncionarioSecundario).HasColumnName(@"dsTelefoneFuncionarioSecundario").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsOptional();
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_ContratoExpositor).HasForeignKey(c => c.TB_Edicao_cdEdicao).WillCascadeOnDelete(false); // FK_ContratoExpositor_cdEdicao
            HasRequired(a => a.TB_DD_Empresa).WithMany(b => b.TB_DD_ContratoExpositor).HasForeignKey(c => c.TB_Empresa_cdEmpresa).WillCascadeOnDelete(false); // FK_ContratoExpositor_cdEmpresa
            HasRequired(a => a.TB_DD_Stand).WithMany(b => b.TB_DD_ContratoExpositor).HasForeignKey(c => c.TB_Stand_cdStand).WillCascadeOnDelete(false); // FK_ContratoExpositor_cdStand
        }
    }

    // TB_DD_Convite
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ConviteConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Convite>
    {
        public TB_DD_ConviteConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ConviteConfiguration(string schema)
        {
            ToTable("TB_DD_Convite", schema);
            HasKey(x => x.cdConvite);

            Property(x => x.cdConvite).HasColumnName(@"cdConvite").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsNome).HasColumnName(@"dsNome").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsEmailConvidado).HasColumnName(@"dsEmailConvidado").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(255);
            Property(x => x.nmEvento).HasColumnName(@"nmEvento").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dtConvite).HasColumnName(@"dtConvite").HasColumnType("datetime").IsRequired();
            Property(x => x.cdPessoa).HasColumnName(@"cdPessoa").HasColumnType("int").IsOptional();
            Property(x => x.dsEmail).HasColumnName(@"dsEmail").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);

            // Foreign keys
            HasOptional(a => a.TB_DD_Pessoa).WithMany(b => b.TB_DD_Convite).HasForeignKey(c => c.cdPessoa).WillCascadeOnDelete(false); // FK_TB_DD_Convite_TB_DD_Pessoa
        }
    }

    // TB_DD_ConviteEletronico
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ConviteEletronicoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ConviteEletronico>
    {
        public TB_DD_ConviteEletronicoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ConviteEletronicoConfiguration(string schema)
        {
            ToTable("TB_DD_ConviteEletronico", schema);
            HasKey(x => x.ObjRef);

            Property(x => x.ObjRef).HasColumnName(@"ObjRef").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.IdEmpresa).HasColumnName(@"IdEmpresa").HasColumnType("int").IsOptional();
            Property(x => x.NomeConvidado).HasColumnName(@"NomeConvidado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(60);
            Property(x => x.EmailConvidado).HasColumnName(@"EmailConvidado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Efetivado).HasColumnName(@"Efetivado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.DataInclusao).HasColumnName(@"DataInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.DataEfetivacao).HasColumnName(@"DataEfetivacao").HasColumnType("datetime").IsOptional();
            Property(x => x.IdEmpresaEfetivada).HasColumnName(@"IdEmpresaEfetivada").HasColumnType("int").IsOptional();
            Property(x => x.CodigoBarras).HasColumnName(@"CodigoBarras").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(20);
            Property(x => x.IdTransacao).HasColumnName(@"IdTransacao").HasColumnType("int").IsOptional();
            Property(x => x.Empresa).HasColumnName(@"Empresa").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.Evento).HasColumnName(@"Evento").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.fgConviteEnviado).HasColumnName(@"fgConviteEnviado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);

            // Foreign keys
            HasOptional(a => a.TB_DD_Empresa).WithMany(b => b.TB_DD_ConviteEletronico).HasForeignKey(c => c.IdEmpresa).WillCascadeOnDelete(false); // FK_TB_DD_ConviteEletronico_IdEmpresa
            HasRequired(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_ConviteEletronico).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_ConviteEletronico_cdEdicao
        }
    }

    // TB_DD_ConviteEletronicoCancelado
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ConviteEletronicoCanceladoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ConviteEletronicoCancelado>
    {
        public TB_DD_ConviteEletronicoCanceladoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ConviteEletronicoCanceladoConfiguration(string schema)
        {
            ToTable("TB_DD_ConviteEletronicoCancelado", schema);
            HasKey(x => x.cdConviteCancelado);

            Property(x => x.cdConviteCancelado).HasColumnName(@"cdConviteCancelado").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dtCancelamento).HasColumnName(@"dtCancelamento").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioCancelamento).HasColumnName(@"cdUsuarioCancelamento").HasColumnType("int").IsRequired();
            Property(x => x.ObjRef).HasColumnName(@"ObjRef").HasColumnType("int").IsRequired();
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.IdEmpresa).HasColumnName(@"IdEmpresa").HasColumnType("int").IsRequired();
            Property(x => x.NomeConvidado).HasColumnName(@"NomeConvidado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(60);
            Property(x => x.EmailConvidado).HasColumnName(@"EmailConvidado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Efetivado).HasColumnName(@"Efetivado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.DataInclusao).HasColumnName(@"DataInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.DataEfetivacao).HasColumnName(@"DataEfetivacao").HasColumnType("datetime").IsOptional();
            Property(x => x.IdEmpresaEfetivada).HasColumnName(@"IdEmpresaEfetivada").HasColumnType("int").IsOptional();
            Property(x => x.CodigoBarras).HasColumnName(@"CodigoBarras").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(20);
            Property(x => x.IdTransacao).HasColumnName(@"IdTransacao").HasColumnType("int").IsOptional();
            Property(x => x.Empresa).HasColumnName(@"Empresa").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.Evento).HasColumnName(@"Evento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.fgConviteEnviado).HasColumnName(@"fgConviteEnviado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
        }
    }

    // TB_DD_ConviteEletronicoVip
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ConviteEletronicoVipConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ConviteEletronicoVip>
    {
        public TB_DD_ConviteEletronicoVipConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ConviteEletronicoVipConfiguration(string schema)
        {
            ToTable("TB_DD_ConviteEletronicoVip", schema);
            HasKey(x => x.ObjRef);

            Property(x => x.ObjRef).HasColumnName(@"ObjRef").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.IdEmpresa).HasColumnName(@"IdEmpresa").HasColumnType("int").IsOptional();
            Property(x => x.NomeConvidado).HasColumnName(@"NomeConvidado").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.EmailConvidado).HasColumnName(@"EmailConvidado").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.Efetivado).HasColumnName(@"Efetivado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.DataInclusao).HasColumnName(@"DataInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.DataEfetivacao).HasColumnName(@"DataEfetivacao").HasColumnType("datetime").IsOptional();
            Property(x => x.IdEmpresaEfetivada).HasColumnName(@"IdEmpresaEfetivada").HasColumnType("int").IsOptional();
            Property(x => x.CodigoBarras).HasColumnName(@"CodigoBarras").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(20);
            Property(x => x.IdTransacao).HasColumnName(@"IdTransacao").HasColumnType("int").IsOptional();
            Property(x => x.Chave).HasColumnName(@"Chave").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.cdPessoa).HasColumnName(@"cdPessoa").HasColumnType("int").IsOptional();
            Property(x => x.Enviado).HasColumnName(@"Enviado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Empresa).HasColumnName(@"Empresa").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.Evento).HasColumnName(@"Evento").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.TipoConvite).HasColumnName(@"TipoConvite").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);

            // Foreign keys
            HasOptional(a => a.TB_DD_Empresa).WithMany(b => b.TB_DD_ConviteEletronicoVip).HasForeignKey(c => c.IdEmpresa).WillCascadeOnDelete(false); // FK_TB_DD_ConviteEletronicoVip_IdEmpresa
            HasRequired(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_ConviteEletronicoVip).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_ConviteEletronicoVip_cdEdicao
        }
    }

    // TB_DD_ConvitesImpressos
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ConvitesImpressosConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ConvitesImpressos>
    {
        public TB_DD_ConvitesImpressosConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ConvitesImpressosConfiguration(string schema)
        {
            ToTable("TB_DD_ConvitesImpressos", schema);
            HasKey(x => x.cdConvite);

            Property(x => x.cdConvite).HasColumnName(@"cdConvite").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Stand_cdStand).HasColumnName(@"TB_Stand_cdStand").HasColumnType("int").IsRequired();
            Property(x => x.dsQtdPort).HasColumnName(@"dsQtdPort").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsQtdIng).HasColumnName(@"dsQtdIng").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsQtdEsp).HasColumnName(@"dsQtdEsp").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsNomeContato).HasColumnName(@"dsNomeContato").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsLogradouro).HasColumnName(@"dsLogradouro").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsNumero).HasColumnName(@"dsNumero").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsCep).HasColumnName(@"dsCep").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsCidade).HasColumnName(@"dsCidade").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsEstado).HasColumnName(@"dsEstado").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsTelefone).HasColumnName(@"dsTelefone").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsFax).HasColumnName(@"dsFax").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsEmail).HasColumnName(@"dsEmail").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);

            // Foreign keys
            HasRequired(a => a.TB_DD_Stand).WithMany(b => b.TB_DD_ConvitesImpressos).HasForeignKey(c => c.TB_Stand_cdStand).WillCascadeOnDelete(false); // FK_TB_DD_ConvitesImpressos_TB_DD_Stand
        }
    }

    // TB_DD_CorCarpeteColuna
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_CorCarpeteColunaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_CorCarpeteColuna>
    {
        public TB_DD_CorCarpeteColunaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_CorCarpeteColunaConfiguration(string schema)
        {
            ToTable("TB_DD_CorCarpeteColuna", schema);
            HasKey(x => x.cdPreenchimento);

            Property(x => x.cdPreenchimento).HasColumnName(@"cdPreenchimento").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.cdEmpresa).HasColumnName(@"cdEmpresa").HasColumnType("int").IsOptional();
            Property(x => x.cdStand).HasColumnName(@"cdStand").HasColumnType("int").IsOptional();
            Property(x => x.DataInclusao).HasColumnName(@"DataInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.UsuarioInsercao).HasColumnName(@"UsuarioInsercao").HasColumnType("int").IsOptional();
            Property(x => x.DataAlteracao).HasColumnName(@"DataAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.UsuarioAlteracao).HasColumnName(@"UsuarioAlteracao").HasColumnType("int").IsOptional();
            Property(x => x.TipoMontagem).HasColumnName(@"TipoMontagem").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
            Property(x => x.cdCorCarpete).HasColumnName(@"cdCorCarpete").HasColumnType("int").IsOptional();
            Property(x => x.cdCorColuna).HasColumnName(@"cdCorColuna").HasColumnType("int").IsOptional();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_CorCarpeteColuna).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_CorCarpeteColuna_cdEdicao
            HasOptional(a => a.TB_DD_Empresa).WithMany(b => b.TB_DD_CorCarpeteColuna).HasForeignKey(c => c.cdEmpresa).WillCascadeOnDelete(false); // FK_TB_DD_CorCarpeteColuna_cdEmpresa
            HasOptional(a => a.TB_DD_ResponsavelEmpresa_UsuarioAlteracao).WithMany(b => b.UsuarioAlteracao).HasForeignKey(c => c.UsuarioAlteracao).WillCascadeOnDelete(false); // FK_TB_DD_CorCarpeteColuna_UsuarioAlteracao
            HasOptional(a => a.TB_DD_ResponsavelEmpresa_UsuarioInsercao).WithMany(b => b.UsuarioInsercao).HasForeignKey(c => c.UsuarioInsercao).WillCascadeOnDelete(false); // FK_TB_DD_CorCarpeteColuna_UsuarioInsercao
            HasOptional(a => a.TB_DD_Stand).WithMany(b => b.TB_DD_CorCarpeteColuna).HasForeignKey(c => c.cdStand).WillCascadeOnDelete(false); // FK_TB_DD_CorCarpeteColuna_cdStand
        }
    }

    // TB_DD_Credencial
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_CredencialConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Credencial>
    {
        public TB_DD_CredencialConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_CredencialConfiguration(string schema)
        {
            ToTable("TB_DD_Credencial", schema);
            HasKey(x => x.cdCredencial);

            Property(x => x.cdCredencial).HasColumnName(@"cdCredencial").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Registro_dsCodigoBarras).HasColumnName(@"TB_Registro_dsCodigoBarras").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.TB_Pedido_cdPedido).HasColumnName(@"TB_Pedido_cdPedido").HasColumnType("int").IsRequired();
            Property(x => x.TB_Empresa_CdEmpresaPrimario).HasColumnName(@"TB_Empresa_CdEmpresaPrimario").HasColumnType("int").IsOptional();
            Property(x => x.TB_Empresa_CdEmpresaSegundario).HasColumnName(@"TB_Empresa_CdEmpresaSegundario").HasColumnType("int").IsOptional();
            Property(x => x.cdStand).HasColumnName(@"cdStand").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_DD_Empresa_TB_Empresa_CdEmpresaPrimario).WithMany(b => b.TB_Empresa_CdEmpresaPrimario).HasForeignKey(c => c.TB_Empresa_CdEmpresaPrimario).WillCascadeOnDelete(false); // FK_TB_DD_Credencial_TB_DD_Empresa_CdEmpresaPrimario
            HasOptional(a => a.TB_DD_Empresa1).WithMany(b => b.TB_DD_Credencial_TB_Empresa_CdEmpresaPrimario).HasForeignKey(c => c.TB_Empresa_CdEmpresaPrimario).WillCascadeOnDelete(false); // FK_TB_DD_Credencial_TB_DD_Empresa_CdEmpresaSegundario
            HasOptional(a => a.TB_DD_Stand).WithMany(b => b.TB_DD_Credencial).HasForeignKey(c => c.cdStand).WillCascadeOnDelete(false); // FK_TB_DD_Credencial_cdStand
            HasRequired(a => a.TB_DD_Pedido).WithMany(b => b.TB_DD_Credencial).HasForeignKey(c => c.TB_Pedido_cdPedido).WillCascadeOnDelete(false); // FK_TB_DD_Credencial_TB_Pedido_cdPedido
        }
    }

    // TB_DD_DestinatarioComunicado
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_DestinatarioComunicadoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_DestinatarioComunicado>
    {
        public TB_DD_DestinatarioComunicadoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_DestinatarioComunicadoConfiguration(string schema)
        {
            ToTable("TB_DD_DestinatarioComunicado", schema);
            HasKey(x => x.ObjRef);

            Property(x => x.ObjRef).HasColumnName(@"ObjRef").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdDestinatarioComunicado).HasColumnName(@"cdDestinatarioComunicado").HasColumnType("int").IsRequired();
            Property(x => x.cdComunicado).HasColumnName(@"cdComunicado").HasColumnType("int").IsOptional();
            Property(x => x.cdEmpresa).HasColumnName(@"cdEmpresa").HasColumnType("int").IsOptional();
            Property(x => x.Lida).HasColumnName(@"Lida").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
            Property(x => x.DataLeitura).HasColumnName(@"DataLeitura").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioLeitura).HasColumnName(@"cdUsuarioLeitura").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_DD_ResponsavelEmpresa).WithMany(b => b.TB_DD_DestinatarioComunicado).HasForeignKey(c => c.cdUsuarioLeitura).WillCascadeOnDelete(false); // FK_TB_DD_DestinatarioComunicado_cdUsuarioLeitura
        }
    }

    // TB_DD_EletricaBasica
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_EletricaBasicaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_EletricaBasica>
    {
        public TB_DD_EletricaBasicaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_EletricaBasicaConfiguration(string schema)
        {
            ToTable("TB_DD_EletricaBasica", schema);
            HasKey(x => x.cdEletricaBasica);

            Property(x => x.cdEletricaBasica).HasColumnName(@"cdEletricaBasica").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.qtEletricaBasica).HasColumnName(@"qtEletricaBasica").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.qtEletricaMinima).HasColumnName(@"qtEletricaMinima").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_EletricaBasica).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_EletricaBasica_cdStand
        }
    }

    // TB_DD_EmailMarketingImportados
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_EmailMarketingImportadosConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_EmailMarketingImportados>
    {
        public TB_DD_EmailMarketingImportadosConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_EmailMarketingImportadosConfiguration(string schema)
        {
            ToTable("TB_DD_EmailMarketingImportados", schema);
            HasKey(x => x.cdEmailMarketingImportado);

            Property(x => x.cdEmailMarketingImportado).HasColumnName(@"cdEmailMarketingImportado").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEmailMarketing).HasColumnName(@"cdEmailMarketing").HasColumnType("int").IsRequired();
            Property(x => x.dsEmail).HasColumnName(@"dsEmail").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsNome).HasColumnName(@"dsNome").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsRequired();
            Property(x => x.dtProcessado).HasColumnName(@"dtProcessado").HasColumnType("datetime").IsOptional();
            Property(x => x.fgProcessado).HasColumnName(@"fgProcessado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.cdDialogue).HasColumnName(@"cdDialogue").HasColumnType("int").IsOptional();
            Property(x => x.dsOrganizacao).HasColumnName(@"dsOrganizacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsContato).HasColumnName(@"dsContato").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsEmailContato).HasColumnName(@"dsEmailContato").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsCodigoBarras).HasColumnName(@"dsCodigoBarras").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
        }
    }

    // TB_DD_EmissaoCredencial
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_EmissaoCredencialConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_EmissaoCredencial>
    {
        public TB_DD_EmissaoCredencialConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_EmissaoCredencialConfiguration(string schema)
        {
            ToTable("TB_DD_EmissaoCredencial", schema);
            HasKey(x => x.cdEmissao);

            Property(x => x.cdEmissao).HasColumnName(@"cdEmissao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Registro_dsCodigoBarras).HasColumnName(@"TB_Registro_dsCodigoBarras").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtDataEmissao).HasColumnName(@"dtDataEmissao").HasColumnType("datetime").IsOptional();
            Property(x => x.dsOrigemImpressao).HasColumnName(@"dsOrigemImpressao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);

        }
    }

    // TB_DD_Empresa
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_EmpresaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Empresa>
    {
        public TB_DD_EmpresaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_EmpresaConfiguration(string schema)
        {
            ToTable("TB_DD_Empresa", schema);
            HasKey(x => x.cdEmpresa);

            Property(x => x.cdEmpresa).HasColumnName(@"cdEmpresa").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_TipoEmpresa_cdTipo).HasColumnName(@"TB_TipoEmpresa_cdTipo").HasColumnType("int").IsRequired();
            Property(x => x.cdTipoDocumento).HasColumnName(@"cdTipoDocumento").HasColumnType("int").IsOptional();
            Property(x => x.dsCnpj).HasColumnName(@"dsCnpj").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsInscricaoEstadual).HasColumnName(@"dsInscricaoEstadual").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsInscricaoMunicipal).HasColumnName(@"dsInscricaoMunicipal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsRazaoSocial).HasColumnName(@"dsRazaoSocial").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsNomeFantasia).HasColumnName(@"dsNomeFantasia").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsEmpresaCracha).HasColumnName(@"dsEmpresaCracha").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsLogradouroComercial).HasColumnName(@"dsLogradouroComercial").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsComplementoLogradouroComercial).HasColumnName(@"dsComplementoLogradouroComercial").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsNumeroLogradouroComercial).HasColumnName(@"dsNumeroLogradouroComercial").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCepLogradouroComercial).HasColumnName(@"dsCepLogradouroComercial").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsBairroLogradouroComercial).HasColumnName(@"dsBairroLogradouroComercial").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCidadeLogradouroComercial).HasColumnName(@"dsCidadeLogradouroComercial").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsUfLogradouroComercial).HasColumnName(@"dsUfLogradouroComercial").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.TB_Pais_cdPaisComercial).HasColumnName(@"TB_Pais_cdPaisComercial").HasColumnType("int").IsRequired();
            Property(x => x.dsSite).HasColumnName(@"dsSite").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsDDITelefoneEmpresa1).HasColumnName(@"dsDDITelefoneEmpresa1").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDDDTelefoneEmpresa1).HasColumnName(@"dsDDDTelefoneEmpresa1").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsNumeroTelefoneEmpresa1).HasColumnName(@"dsNumeroTelefoneEmpresa1").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsRamalTelefoneEmpresa1).HasColumnName(@"dsRamalTelefoneEmpresa1").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDDITelefoneEmpresa2).HasColumnName(@"dsDDITelefoneEmpresa2").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDDDTelefoneEmpresa2).HasColumnName(@"dsDDDTelefoneEmpresa2").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsNumeroTelefoneEmpresa2).HasColumnName(@"dsNumeroTelefoneEmpresa2").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsRamalTelefoneEmpresa2).HasColumnName(@"dsRamalTelefoneEmpresa2").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDDIFaxEmpresa).HasColumnName(@"dsDDIFaxEmpresa").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDDDFaxEmpresa).HasColumnName(@"dsDDDFaxEmpresa").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsNumeroFaxEmpresa).HasColumnName(@"dsNumeroFaxEmpresa").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsRamoAtividade).HasColumnName(@"dsRamoAtividade").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsAreaAtuacao).HasColumnName(@"dsAreaAtuacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsFaturamento).HasColumnName(@"dsFaturamento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.EnEmail).HasColumnName(@"EnEmail").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.SINDIPROM).HasColumnName(@"SINDIPROM").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.NRegistroPoliciaFederal).HasColumnName(@"NRegistroPoliciaFederal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(30);
            Property(x => x.dsVeiculo).HasColumnName(@"dsVeiculo").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsEditoria).HasColumnName(@"dsEditoria").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsTwitter).HasColumnName(@"dsTwitter").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsFacebook).HasColumnName(@"dsFacebook").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsObservacao).HasColumnName(@"dsObservacao").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsIdioma).HasColumnName(@"dsIdioma").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
            Property(x => x.dsResPessoa1).HasColumnName(@"dsResPessoa1").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsResPessoa2).HasColumnName(@"dsResPessoa2").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsResPessoa3).HasColumnName(@"dsResPessoa3").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsResPessoa4).HasColumnName(@"dsResPessoa4").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsResPessoa5).HasColumnName(@"dsResPessoa5").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsResPessoa6).HasColumnName(@"dsResPessoa6").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsResPessoa7).HasColumnName(@"dsResPessoa7").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsResPessoa8).HasColumnName(@"dsResPessoa8").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsResPessoa9).HasColumnName(@"dsResPessoa9").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsResPessoa10).HasColumnName(@"dsResPessoa10").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdCnaeSebrae).HasColumnName(@"cdCnaeSebrae").HasColumnType("int").IsOptional();
            Property(x => x.dsCEPEmpresaSebrae).HasColumnName(@"dsCEPEmpresaSebrae").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsLogradouroEmpresaSebrae).HasColumnName(@"dsLogradouroEmpresaSebrae").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsComplementoEmpresaSebrae).HasColumnName(@"dsComplementoEmpresaSebrae").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.cdPaisEmpresaSebrae).HasColumnName(@"cdPaisEmpresaSebrae").HasColumnType("int").IsOptional();
            Property(x => x.cdEstadoEmpresaSebrae).HasColumnName(@"cdEstadoEmpresaSebrae").HasColumnType("int").IsOptional();
            Property(x => x.cdCidadeEmpresaSebrae).HasColumnName(@"cdCidadeEmpresaSebrae").HasColumnType("int").IsOptional();
            Property(x => x.cdBairroEmpresaSebrae).HasColumnName(@"cdBairroEmpresaSebrae").HasColumnType("int").IsOptional();
            Property(x => x.dtAberturaSebrae).HasColumnName(@"dtAberturaSebrae").HasColumnType("datetime").IsOptional();
            Property(x => x.CodConstSebrae).HasColumnName(@"CodConstSebrae").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.CodProdutoRuralSebrae).HasColumnName(@"CodProdutoRuralSebrae").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.CodDapSebrae).HasColumnName(@"CodDapSebrae").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.CodPescadorSebrae).HasColumnName(@"CodPescadorSebrae").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsNumeroLogradouroEmpresaSebrae).HasColumnName(@"dsNumeroLogradouroEmpresaSebrae").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsRegimeTributacao).HasColumnName(@"dsRegimeTributacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.cdEmpresaOriginal).HasColumnName(@"cdEmpresaOriginal").HasColumnType("int").IsOptional();
            Property(x => x.dsResEmpresa01).HasColumnName(@"dsResEmpresa01").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.ACCOUNT_ID_CRM).HasColumnName(@"ACCOUNT_ID_CRM").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.CodigoClienteAPAS).HasColumnName(@"CodigoClienteAPAS").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsRegionalDistrital).HasColumnName(@"dsRegionalDistrital").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.cdCoIrmas).HasColumnName(@"cdCoIrmas").HasColumnType("int").IsOptional();
            Property(x => x.dsSenha).HasColumnName(@"dsSenha").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.nmResponsavel).HasColumnName(@"nmResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsEmailResponsavel).HasColumnName(@"dsEmailResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsPerfil).HasColumnName(@"dsPerfil").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsCodigoCNAE).HasColumnName(@"dsCodigoCNAE").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResEmpresa1).HasColumnName(@"dsResEmpresa1").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResEmpresa2).HasColumnName(@"dsResEmpresa2").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResEmpresa3).HasColumnName(@"dsResEmpresa3").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResEmpresa4).HasColumnName(@"dsResEmpresa4").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResEmpresa5).HasColumnName(@"dsResEmpresa5").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResEmpresa6).HasColumnName(@"dsResEmpresa6").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResEmpresa7).HasColumnName(@"dsResEmpresa7").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResEmpresa8).HasColumnName(@"dsResEmpresa8").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResEmpresa9).HasColumnName(@"dsResEmpresa9").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResEmpresa10).HasColumnName(@"dsResEmpresa10").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);

            // Foreign keys
            HasOptional(a => a.TB_RF_TipoDocumento).WithMany(b => b.TB_DD_Empresa).HasForeignKey(c => c.cdTipoDocumento).WillCascadeOnDelete(false); // FK_TB_DD_Empresa_cdTipoDocumento
            HasRequired(a => a.TB_RF_TipoEmpresa).WithMany(b => b.TB_DD_Empresa).HasForeignKey(c => c.TB_TipoEmpresa_cdTipo).WillCascadeOnDelete(false); // FK_TB_DD_Empresa_TB_RF_TipoEmpresa_cdTipo
        }
    }

    // TB_DD_EntregaCredencialVIP
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_EntregaCredencialVIPConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_EntregaCredencialVIP>
    {
        public TB_DD_EntregaCredencialVIPConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_EntregaCredencialVIPConfiguration(string schema)
        {
            ToTable("TB_DD_EntregaCredencialVIP", schema);
            HasKey(x => x.cdEntrega);

            Property(x => x.cdEntrega).HasColumnName(@"cdEntrega").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Empresa_cdEmpresa).HasColumnName(@"TB_Empresa_cdEmpresa").HasColumnType("int").IsRequired();
            Property(x => x.dsRazaoSocial).HasColumnName(@"dsRazaoSocial").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsLogradouro).HasColumnName(@"dsLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsComplementoLogradouro).HasColumnName(@"dsComplementoLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsNumeroLogradouro).HasColumnName(@"dsNumeroLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCepLogradouro).HasColumnName(@"dsCepLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsBairroLogradouro).HasColumnName(@"dsBairroLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCidadeLogradouro).HasColumnName(@"dsCidadeLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsUfLogradouro).HasColumnName(@"dsUfLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsEmail).HasColumnName(@"dsEmail").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsResponsavel).HasColumnName(@"dsResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsNumeroStand).HasColumnName(@"dsNumeroStand").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsNumeroTelefone).HasColumnName(@"dsNumeroTelefone").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(15);
            Property(x => x.dsNumeroCelular).HasColumnName(@"dsNumeroCelular").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(15);
            Property(x => x.dsTipoEnvio).HasColumnName(@"dsTipoEnvio").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.TB_Pessoa_cdPessoa).HasColumnName(@"TB_Pessoa_cdPessoa").HasColumnType("int").IsRequired();
            Property(x => x.cdStand).HasColumnName(@"cdStand").HasColumnType("int").IsRequired();

            // Foreign keys
            HasRequired(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_EntregaCredencialVIP).HasForeignKey(c => c.TB_Edicao_cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_EntregaCredencialVIP_Edicao
            HasRequired(a => a.TB_DD_Empresa).WithMany(b => b.TB_DD_EntregaCredencialVIP).HasForeignKey(c => c.TB_Empresa_cdEmpresa).WillCascadeOnDelete(false); // FK_TB_DD_EntregaCredencialVIP_Empresa
            HasRequired(a => a.TB_DD_Pessoa).WithMany(b => b.TB_DD_EntregaCredencialVIP).HasForeignKey(c => c.TB_Pessoa_cdPessoa).WillCascadeOnDelete(false); // FK_TB_DD_EntregaCredencialVIP_Pessoa
            HasRequired(a => a.TB_DD_Stand).WithMany(b => b.TB_DD_EntregaCredencialVIP).HasForeignKey(c => c.cdStand).WillCascadeOnDelete(false); // FK_TB_DD_EntregaCredencialVIP_Stand
        }
    }

    // TB_DD_EstruturaCartaPersonalizada
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_EstruturaCartaPersonalizadaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_EstruturaCartaPersonalizada>
    {
        public TB_DD_EstruturaCartaPersonalizadaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_EstruturaCartaPersonalizadaConfiguration(string schema)
        {
            ToTable("TB_DD_EstruturaCartaPersonalizada", schema);
            HasKey(x => x.cdEstruturaCartaPersonalizada);

            Property(x => x.cdEstruturaCartaPersonalizada).HasColumnName(@"cdEstruturaCartaPersonalizada").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdFormulario).HasColumnName(@"cdFormulario").HasColumnType("int").IsRequired();
            Property(x => x.cdTipoCarta).HasColumnName(@"cdTipoCarta").HasColumnType("int").IsRequired();
            Property(x => x.cdUsuarioInclusao).HasColumnName(@"cdUsuarioInclusao").HasColumnType("int").IsRequired();
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.dsTextoCartaPortugues).HasColumnName(@"dsTextoCartaPortugues").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsTextoCartaIngles).HasColumnName(@"dsTextoCartaIngles").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsTextoCartaEspanhol).HasColumnName(@"dsTextoCartaEspanhol").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsRodapeCartaPortugues).HasColumnName(@"dsRodapeCartaPortugues").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsRodapeCartaIngles).HasColumnName(@"dsRodapeCartaIngles").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsRodapeCartaEspanhol).HasColumnName(@"dsRodapeCartaEspanhol").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.cdSistema).HasColumnName(@"cdSistema").HasColumnType("int").IsOptional();
            Property(x => x.dsCorTituloRodapePortugues).HasColumnName(@"dsCorTituloRodapePortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsCorTituloRodapeIngles).HasColumnName(@"dsCorTituloRodapeIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsCorTituloRodapeEspanhol).HasColumnName(@"dsCorTituloRodapeEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);

            // Foreign keys
            HasRequired(a => a.TB_RF_TipoCarta).WithMany(b => b.TB_DD_EstruturaCartaPersonalizada).HasForeignKey(c => c.cdTipoCarta).WillCascadeOnDelete(false); // FK_TB_DD_EstruturaCartaPersonalizada_TB_RF_TipoCarta
        }
    }

    // TB_DD_EstruturaPersonalizada
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_EstruturaPersonalizadaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_EstruturaPersonalizada>
    {
        public TB_DD_EstruturaPersonalizadaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_EstruturaPersonalizadaConfiguration(string schema)
        {
            ToTable("TB_DD_EstruturaPersonalizada", schema);
            HasKey(x => x.cdEstruturaPersonalizada);

            Property(x => x.cdEstruturaPersonalizada).HasColumnName(@"cdEstruturaPersonalizada").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdFormulario).HasColumnName(@"cdFormulario").HasColumnType("int").IsRequired();
            Property(x => x.cdEstruturaPadrao).HasColumnName(@"cdEstruturaPadrao").HasColumnType("int").IsRequired();
            Property(x => x.dsPortugues).HasColumnName(@"dsPortugues").HasColumnType("varchar(max)").IsRequired().IsUnicode(false);
            Property(x => x.dsIngles).HasColumnName(@"dsIngles").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsEspanhol).HasColumnName(@"dsEspanhol").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.fgObrigatorioPortugues).HasColumnName(@"fgObrigatorioPortugues").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgObrigatorioIngles).HasColumnName(@"fgObrigatorioIngles").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgObrigatorioEspanhol).HasColumnName(@"fgObrigatorioEspanhol").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsMensagemObrigatorioPortugues).HasColumnName(@"dsMensagemObrigatorioPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsMensagemObrigatorioIngles).HasColumnName(@"dsMensagemObrigatorioIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsMensagemObrigatorioEspanhol).HasColumnName(@"dsMensagemObrigatorioEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.fgExibirPortugues).HasColumnName(@"fgExibirPortugues").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgExibirIngles).HasColumnName(@"fgExibirIngles").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgExibirEspanhol).HasColumnName(@"fgExibirEspanhol").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsRequired();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();
            Property(x => x.nuOrdemPortugues).HasColumnName(@"nuOrdemPortugues").HasColumnType("int").IsOptional();
            Property(x => x.nuOrdemIngles).HasColumnName(@"nuOrdemIngles").HasColumnType("int").IsOptional();
            Property(x => x.nuOrdemEspanhol).HasColumnName(@"nuOrdemEspanhol").HasColumnType("int").IsOptional();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.cdTipoValidacao).HasColumnName(@"cdTipoValidacao").HasColumnType("int").IsOptional();
            Property(x => x.nuCaracteres).HasColumnName(@"nuCaracteres").HasColumnType("int").IsOptional();
            Property(x => x.dsMensagemPreenchimentoIncorretoPortugues).HasColumnName(@"dsMensagemPreenchimentoIncorretoPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsMensagemPreenchimentoIncorretoIngles).HasColumnName(@"dsMensagemPreenchimentoIncorretoIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsMensagemPreenchimentoIncorretoEspanhol).HasColumnName(@"dsMensagemPreenchimentoIncorretoEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.cdSistema).HasColumnName(@"cdSistema").HasColumnType("int").IsOptional();
            Property(x => x.cdFormularioManual).HasColumnName(@"cdFormularioManual").HasColumnType("int").IsOptional();
            Property(x => x.nuEspaco).HasColumnName(@"nuEspaco").HasColumnType("int").IsRequired();
            Property(x => x.dsRestringirDuplicidadeCNPJ).HasColumnName(@"dsRestringirDuplicidadeCNPJ").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsQtdDuplicidadeCNPJ).HasColumnName(@"dsQtdDuplicidadeCNPJ").HasColumnType("int").IsOptional();
            Property(x => x.dsExcecoesDuplicidadeCNPJ).HasColumnName(@"dsExcecoesDuplicidadeCNPJ").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsHabilitarRestricaoCampo).HasColumnName(@"dsHabilitarRestricaoCampo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsRestricoesCampo).HasColumnName(@"dsRestricoesCampo").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsHabilitarRestricaoCampoNaoPermitirDuplicidade).HasColumnName(@"dsHabilitarRestricaoCampoNaoPermitirDuplicidade").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);

            // Foreign keys
            HasRequired(a => a.TB_CF_EstruturaPadrao).WithMany(b => b.TB_DD_EstruturaPersonalizada).HasForeignKey(c => c.cdEstruturaPadrao).WillCascadeOnDelete(false); // FK_TB_DD_EstruturaPersonalizada_TB_CF_Manual
            HasRequired(a => a.TB_CF_Formulario).WithMany(b => b.TB_DD_EstruturaPersonalizada).HasForeignKey(c => c.cdFormulario).WillCascadeOnDelete(false); // FK_tb_dd_EstruturaPersonalizada_tb_cf_formulario
        }
    }

    // TB_DD_Evento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_EventoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Evento>
    {
        public TB_DD_EventoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_EventoConfiguration(string schema)
        {
            ToTable("TB_DD_Evento", schema);
            HasKey(x => x.cdEvento);

            Property(x => x.cdEvento).HasColumnName(@"cdEvento").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.nmEvento).HasColumnName(@"nmEvento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
        }
    }

    // TB_DD_ExtracaoPersonalizada
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ExtracaoPersonalizadaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ExtracaoPersonalizada>
    {
        public TB_DD_ExtracaoPersonalizadaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ExtracaoPersonalizadaConfiguration(string schema)
        {
            ToTable("TB_DD_ExtracaoPersonalizada", schema);
            HasKey(x => x.cdExtracaoPersonalizada);

            Property(x => x.cdExtracaoPersonalizada).HasColumnName(@"cdExtracaoPersonalizada").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsNomeRelatorioExtracaoPersonalizada).HasColumnName(@"dsNomeRelatorioExtracaoPersonalizada").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdUsuarioInclusao).HasColumnName(@"cdUsuarioInclusao").HasColumnType("int").IsRequired();
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
        }
    }

    // TB_DD_FaqRespostas
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_FaqRespostasConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_FaqRespostas>
    {
        public TB_DD_FaqRespostasConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_FaqRespostasConfiguration(string schema)
        {
            ToTable("TB_DD_FaqRespostas", schema);
            HasKey(x => x.cdFaqRespostas);

            Property(x => x.cdFaqRespostas).HasColumnName(@"cdFaqRespostas").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdFaq).HasColumnName(@"cdFaq").HasColumnType("int").IsOptional();
            Property(x => x.DataInclusao).HasColumnName(@"DataInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.DataAlteracao).HasColumnName(@"DataAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.dsResposta).HasColumnName(@"dsResposta").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);

            // Foreign keys
            HasOptional(a => a.TB_RF_Faq).WithMany(b => b.TB_DD_FaqRespostas).HasForeignKey(c => c.cdFaq).WillCascadeOnDelete(false); // FK_TB_DD_FaqRespostas_cdFaq
        }
    }

    // TB_DD_Filial
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_FilialConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Filial>
    {
        public TB_DD_FilialConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_FilialConfiguration(string schema)
        {
            ToTable("TB_DD_Filial", schema);
            HasKey(x => x.cdFilial);

            Property(x => x.cdFilial).HasColumnName(@"cdFilial").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dtAbertura).HasColumnName(@"dtAbertura").HasColumnType("datetime").IsOptional();
            Property(x => x.dtEncerramento).HasColumnName(@"dtEncerramento").HasColumnType("datetime").IsOptional();
            Property(x => x.nmFilial).HasColumnName(@"nmFilial").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.flHabilitado).HasColumnName(@"flHabilitado").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
        }
    }

    // TB_DD_FotoPessoa
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_FotoPessoaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_FotoPessoa>
    {
        public TB_DD_FotoPessoaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_FotoPessoaConfiguration(string schema)
        {
            ToTable("TB_DD_FotoPessoa", schema);
            HasKey(x => x.cdPessoa);

            Property(x => x.cdPessoa).HasColumnName(@"cdPessoa").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.imgBinary).HasColumnName(@"imgBinary").HasColumnType("varbinary(max)").IsOptional();
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsRequired();
        }
    }

    // TB_DD_Funcionario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_FuncionarioConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Funcionario>
    {
        public TB_DD_FuncionarioConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_FuncionarioConfiguration(string schema)
        {
            ToTable("TB_DD_Funcionario", schema);
            HasKey(x => x.cdFuncionario);

            Property(x => x.cdFuncionario).HasColumnName(@"cdFuncionario").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.nmCompleto).HasColumnName(@"nmCompleto").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsCargo).HasColumnName(@"dsCargo").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDepartamento).HasColumnName(@"dsDepartamento").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsEmailLogin).HasColumnName(@"dsEmailLogin").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsSenha).HasColumnName(@"dsSenha").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsTelefoneResidencial).HasColumnName(@"dsTelefoneResidencial").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsTelefoneCelular).HasColumnName(@"dsTelefoneCelular").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsNextel).HasColumnName(@"dsNextel").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsIDNextel).HasColumnName(@"dsIDNextel").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.flHabilitado).HasColumnName(@"flHabilitado").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
        }
    }

    // TB_DD_Geradora_Registro
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_Geradora_RegistroConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Geradora_Registro>
    {
        public TB_DD_Geradora_RegistroConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_Geradora_RegistroConfiguration(string schema)
        {
            ToTable("TB_DD_Geradora_Registro", schema);
            HasKey(x => x.cdCodigo);

            Property(x => x.cdCodigo).HasColumnName(@"cdCodigo").HasColumnType("bigint").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dtGeracao).HasColumnName(@"dtGeracao").HasColumnType("datetime").IsOptional();
            Property(x => x.dsDestino).HasColumnName(@"dsDestino").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.wkRowID).HasColumnName(@"wkRowID").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
        }
    }

    // TB_DD_GrupoProdutoCongresso
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_GrupoProdutoCongressoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_GrupoProdutoCongresso>
    {
        public TB_DD_GrupoProdutoCongressoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_GrupoProdutoCongressoConfiguration(string schema)
        {
            ToTable("TB_DD_GrupoProdutoCongresso", schema);
            HasKey(x => x.cdGrupoProdutoCongresso);

            Property(x => x.cdGrupoProdutoCongresso).HasColumnName(@"cdGrupoProdutoCongresso").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdSistema).HasColumnName(@"cdSistema").HasColumnType("int").IsRequired();
            Property(x => x.dsPortugues).HasColumnName(@"dsPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsIngles).HasColumnName(@"dsIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsEspanhol).HasColumnName(@"dsEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioInclusao).HasColumnName(@"cdUsuarioInclusao").HasColumnType("int").IsRequired();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.nuOrdem).HasColumnName(@"nuOrdem").HasColumnType("int").IsRequired();
            Property(x => x.dsIdGrupo).HasColumnName(@"dsIdGrupo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsCor).HasColumnName(@"dsCor").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.nuQtdProdutoPorGrupo).HasColumnName(@"nuQtdProdutoPorGrupo").HasColumnType("int").IsOptional();
        }
    }

    // TB_DD_GrupoResponsavelCongresso
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_GrupoResponsavelCongressoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_GrupoResponsavelCongresso>
    {
        public TB_DD_GrupoResponsavelCongressoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_GrupoResponsavelCongressoConfiguration(string schema)
        {
            ToTable("TB_DD_GrupoResponsavelCongresso", schema);
            HasKey(x => x.cdGrupoResponsavelCongresso);

            Property(x => x.cdGrupoResponsavelCongresso).HasColumnName(@"cdGrupoResponsavelCongresso").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdResponsavelCongresso).HasColumnName(@"cdResponsavelCongresso").HasColumnType("int").IsRequired();
            Property(x => x.cdPessoa).HasColumnName(@"cdPessoa").HasColumnType("int").IsRequired();
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.cdSistema).HasColumnName(@"cdSistema").HasColumnType("int").IsRequired();
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsRequired();
            Property(x => x.dsCodigoBarras).HasColumnName(@"dsCodigoBarras").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(200);

            // Foreign keys
            HasRequired(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_GrupoResponsavelCongresso).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_GrupoResponsavel_Edicao
            HasRequired(a => a.TB_DD_Pessoa).WithMany(b => b.TB_DD_GrupoResponsavelCongresso).HasForeignKey(c => c.cdPessoa).WillCascadeOnDelete(false); // FK_GrupoResponsavel_Pessoal
            HasRequired(a => a.TB_DD_ResponsavelCongresso).WithMany(b => b.TB_DD_GrupoResponsavelCongresso).HasForeignKey(c => c.cdResponsavelCongresso).WillCascadeOnDelete(false); // FK_GrupoResponsavel_Responsavel
        }
    }

    // TB_DD_HelpDesk
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_HelpDeskConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_HelpDesk>
    {
        public TB_DD_HelpDeskConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_HelpDeskConfiguration(string schema)
        {
            ToTable("TB_DD_HelpDesk", schema);
            HasKey(x => x.cdChamado);

            Property(x => x.cdChamado).HasColumnName(@"cdChamado").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.cdEmpresa).HasColumnName(@"cdEmpresa").HasColumnType("int").IsOptional();
            Property(x => x.cdPessoa).HasColumnName(@"cdPessoa").HasColumnType("int").IsOptional();
            Property(x => x.DataInclusao).HasColumnName(@"DataInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.dsAssunto).HasColumnName(@"dsAssunto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsStatus).HasColumnName(@"dsStatus").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_HelpDesk).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_HelpDesk_cdEdicao
            HasOptional(a => a.TB_DD_Empresa).WithMany(b => b.TB_DD_HelpDesk).HasForeignKey(c => c.cdEmpresa).WillCascadeOnDelete(false); // FK_TB_DD_HelpDesk_cdEmpresa
        }
    }

    // TB_DD_ImagemAssistentePreenchimento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ImagemAssistentePreenchimentoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ImagemAssistentePreenchimento>
    {
        public TB_DD_ImagemAssistentePreenchimentoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ImagemAssistentePreenchimentoConfiguration(string schema)
        {
            ToTable("TB_DD_ImagemAssistentePreenchimento", schema);
            HasKey(x => x.cdImagemAssistente);

            Property(x => x.cdImagemAssistente).HasColumnName(@"cdImagemAssistente").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdUsuarioInclusao).HasColumnName(@"cdUsuarioInclusao").HasColumnType("int").IsRequired();
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsRequired();
            Property(x => x.dsCaminho).HasColumnName(@"dsCaminho").HasColumnType("varchar(max)").IsRequired().IsUnicode(false);
            Property(x => x.cdSistema).HasColumnName(@"cdSistema").HasColumnType("int").IsOptional();
            Property(x => x.dsUrl).HasColumnName(@"dsUrl").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
        }
    }

    // TB_DD_ImportacaoPessoa
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ImportacaoPessoaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ImportacaoPessoa>
    {
        public TB_DD_ImportacaoPessoaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ImportacaoPessoaConfiguration(string schema)
        {
            ToTable("TB_DD_ImportacaoPessoa", schema);
            HasKey(x => x.cdImportacaoPessoa);

            Property(x => x.cdImportacaoPessoa).HasColumnName(@"cdImportacaoPessoa").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsCPF).HasColumnName(@"dsCPF").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.nmCompleto).HasColumnName(@"nmCompleto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.nmCracha).HasColumnName(@"nmCracha").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCargo).HasColumnName(@"dsCargo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsEmail).HasColumnName(@"dsEmail").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.dtImportacao).HasColumnName(@"dtImportacao").HasColumnType("datetime").IsOptional();
            Property(x => x.dsImportado).HasColumnName(@"dsImportado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.Res1).HasColumnName(@"Res1").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Res2).HasColumnName(@"Res2").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Res3).HasColumnName(@"Res3").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Res4).HasColumnName(@"Res4").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Res5).HasColumnName(@"Res5").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdUsuario_Insercao).HasColumnName(@"cdUsuario_Insercao").HasColumnType("int").IsOptional();
            Property(x => x.cdEmpresa_Insercao).HasColumnName(@"cdEmpresa_Insercao").HasColumnType("int").IsOptional();
            Property(x => x.cdUsuario_Importacao).HasColumnName(@"cdUsuario_Importacao").HasColumnType("int").IsOptional();
            Property(x => x.TB_Categoria_cdCategoria).HasColumnName(@"TB_Categoria_cdCategoria").HasColumnType("int").IsOptional();
            Property(x => x.RowId).HasColumnName(@"RowId").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
        }
    }

    // TB_DD_ImpostoAPAS
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ImpostoAPASConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ImpostoAPAS>
    {
        public TB_DD_ImpostoAPASConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ImpostoAPASConfiguration(string schema)
        {
            ToTable("TB_DD_ImpostoAPAS", schema);
            HasKey(x => x.cdImpostoAPAS);

            Property(x => x.cdImpostoAPAS).HasColumnName(@"cdImpostoAPAS").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdPedido).HasColumnName(@"cdPedido").HasColumnType("int").IsRequired();
            Property(x => x.cdItem).HasColumnName(@"cdItem").HasColumnType("int").IsRequired();
            Property(x => x.cdNumeroProduto).HasColumnName(@"cdNumeroProduto").HasColumnType("int").IsRequired();
            Property(x => x.cdSequencial).HasColumnName(@"cdSequencial").HasColumnType("int").IsRequired();
            Property(x => x.dsBilhete).HasColumnName(@"dsBilhete").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(800);
            Property(x => x.vlImposto).HasColumnName(@"vlImposto").HasColumnType("numeric").IsRequired().HasPrecision(18, 2);
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsRequired();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);

            // Foreign keys
            HasRequired(a => a.TB_DD_ItensPedidoCongresso).WithMany(b => b.TB_DD_ImpostoAPAS).HasForeignKey(c => c.cdItem).WillCascadeOnDelete(false); // FK_ImpostoAPAS_ItemPedidoCongresso
            HasRequired(a => a.TB_DD_Pedido).WithMany(b => b.TB_DD_ImpostoAPAS).HasForeignKey(c => c.cdPedido).WillCascadeOnDelete(false); // FK_ImpostoAPAS_Pedido
        }
    }

    // TB_DD_InteracaoHelp
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_InteracaoHelpConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_InteracaoHelp>
    {
        public TB_DD_InteracaoHelpConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_InteracaoHelpConfiguration(string schema)
        {
            ToTable("TB_DD_InteracaoHelp", schema);
            HasKey(x => x.cdInteracao);

            Property(x => x.cdInteracao).HasColumnName(@"cdInteracao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdChamado).HasColumnName(@"cdChamado").HasColumnType("int").IsOptional();
            Property(x => x.DataInclusao).HasColumnName(@"DataInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdPessoa).HasColumnName(@"cdPessoa").HasColumnType("int").IsOptional();
            Property(x => x.dsMensagem).HasColumnName(@"dsMensagem").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
        }
    }

    // TB_DD_ItensLote
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ItensLoteConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ItensLote>
    {
        public TB_DD_ItensLoteConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ItensLoteConfiguration(string schema)
        {
            ToTable("TB_DD_ItensLote", schema);
            HasKey(x => x.cdItem);

            Property(x => x.cdItem).HasColumnName(@"cdItem").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsCodigoBarras).HasColumnName(@"dsCodigoBarras").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdLote).HasColumnName(@"cdLote").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_DD_LoteImpressao).WithMany(b => b.TB_DD_ItensLote).HasForeignKey(c => c.cdLote).WillCascadeOnDelete(false); // FK_TB_DD_ItensLote_cdLote
        }
    }

    // TB_DD_ItensPedidoCongresso
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ItensPedidoCongressoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ItensPedidoCongresso>
    {
        public TB_DD_ItensPedidoCongressoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ItensPedidoCongressoConfiguration(string schema)
        {
            ToTable("TB_DD_ItensPedidoCongresso", schema);
            HasKey(x => x.cdItem);

            Property(x => x.cdItem).HasColumnName(@"cdItem").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Pessoa_cdPessoa).HasColumnName(@"TB_Pessoa_cdPessoa").HasColumnType("int").IsRequired();
            Property(x => x.TB_ProdutoCongresso_cdProduto).HasColumnName(@"TB_ProdutoCongresso_cdProduto").HasColumnType("int").IsRequired();
            Property(x => x.TB_Pedido_cdPedido).HasColumnName(@"TB_Pedido_cdPedido").HasColumnType("int").IsRequired();
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.cdPreco).HasColumnName(@"cdPreco").HasColumnType("int").IsOptional();
            Property(x => x.dtItem).HasColumnName(@"dtItem").HasColumnType("datetime").IsOptional();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCodigoBarras).HasColumnName(@"dsCodigoBarras").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsNome).HasColumnName(@"dsNome").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsEmail).HasColumnName(@"dsEmail").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.cdTicket).HasColumnName(@"cdTicket").HasColumnType("int").IsOptional();
            Property(x => x.cdItemTroca).HasColumnName(@"cdItemTroca").HasColumnType("int").IsOptional();
            Property(x => x.cdPessoalSubstituida).HasColumnName(@"cdPessoalSubstituida").HasColumnType("int").IsOptional();
            Property(x => x.dtsubstituicao).HasColumnName(@"dtsubstituicao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioSubstituicao).HasColumnName(@"cdUsuarioSubstituicao").HasColumnType("int").IsOptional();
            Property(x => x.cdFicha).HasColumnName(@"cdFicha").HasColumnType("int").IsOptional();
            Property(x => x.dsIngressoEntregue).HasColumnName(@"dsIngressoEntregue").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.cdPessoaCongresso).HasColumnName(@"cdPessoaCongresso").HasColumnType("uniqueidentifier").IsOptional();

            Property(x => x.Valor_Item).HasColumnName(@"Valor_Item").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Vlr_Desconto_Adm).HasColumnName(@"Vlr_Desconto_Adm").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Valor_Desconto_Regra).HasColumnName(@"Valor_Desconto_Regra").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Valor_Desconto_Codigo_Promocao).HasColumnName(@"Valor_Desconto_Codigo_Promocao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Valor_Liquido).HasColumnName(@"Valor_Liquido").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);

            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioInclusao).HasColumnName(@"cdUsuarioInclusao").HasColumnType("int").IsOptional();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_ItensPedidoCongresso).HasForeignKey(c => c.TB_Edicao_cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_ItensPedidoCongresso_TB_CF_Edicao_cdEdicao
            HasRequired(a => a.TB_DD_Pedido).WithMany(b => b.TB_DD_ItensPedidoCongresso).HasForeignKey(c => c.TB_Pedido_cdPedido).WillCascadeOnDelete(false); // FK_TB_DD_ItensPedidoCongresso_TB_Pedido_cdPedido
            HasRequired(a => a.TB_DD_Pessoa).WithMany(b => b.TB_DD_ItensPedidoCongresso).HasForeignKey(c => c.TB_Pessoa_cdPessoa).WillCascadeOnDelete(false); // FK_TB_DD_ItensPedidoCongresso_TB_DD_Pessoa_cdPessoa
            HasRequired(a => a.TB_DD_ProdutoCongresso).WithMany(b => b.TB_DD_ItensPedidoCongresso).HasForeignKey(c => c.TB_ProdutoCongresso_cdProduto).WillCascadeOnDelete(false); // FK_TB_DD_ItensPedidoCongresso_TB_DD_ProdutoCongresso_cdProduto
        }
    }

    // TB_DD_ItensPedidoOperacional
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ItensPedidoOperacionalConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ItensPedidoOperacional>
    {
        public TB_DD_ItensPedidoOperacionalConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ItensPedidoOperacionalConfiguration(string schema)
        {
            ToTable("TB_DD_ItensPedidoOperacional", schema);
            HasKey(x => x.cdItem);

            Property(x => x.cdItem).HasColumnName(@"cdItem").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_ProdutoOperacional_cdProduto).HasColumnName(@"TB_ProdutoOperacional_cdProduto").HasColumnType("int").IsRequired();
            Property(x => x.TB_Pedido_cdPedido).HasColumnName(@"TB_Pedido_cdPedido").HasColumnType("int").IsRequired();
            Property(x => x.QtdProduto).HasColumnName(@"QtdProduto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsValor).HasColumnName(@"dsValor").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdStand).HasColumnName(@"cdStand").HasColumnType("int").IsOptional();
            Property(x => x.cdPessoa).HasColumnName(@"cdPessoa").HasColumnType("int").IsOptional();
            Property(x => x.CodigoBarras).HasColumnName(@"CodigoBarras").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(30);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
            Property(x => x.dsDescricaoOpcional).HasColumnName(@"dsDescricaoOpcional").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.cdEmpresaSegundario).HasColumnName(@"cdEmpresaSegundario").HasColumnType("int").IsOptional();
            Property(x => x.dsReservado1).HasColumnName(@"dsReservado1").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado2).HasColumnName(@"dsReservado2").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado3).HasColumnName(@"dsReservado3").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado4).HasColumnName(@"dsReservado4").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado5).HasColumnName(@"dsReservado5").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado6).HasColumnName(@"dsReservado6").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado7).HasColumnName(@"dsReservado7").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado8).HasColumnName(@"dsReservado8").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado9).HasColumnName(@"dsReservado9").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado10).HasColumnName(@"dsReservado10").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado11).HasColumnName(@"dsReservado11").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado12).HasColumnName(@"dsReservado12").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado13).HasColumnName(@"dsReservado13").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado14).HasColumnName(@"dsReservado14").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado15).HasColumnName(@"dsReservado15").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado16).HasColumnName(@"dsReservado16").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado17).HasColumnName(@"dsReservado17").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado18).HasColumnName(@"dsReservado18").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado19).HasColumnName(@"dsReservado19").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsReservado20).HasColumnName(@"dsReservado20").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);

            // Foreign keys
            HasOptional(a => a.TB_DD_Stand).WithMany(b => b.TB_DD_ItensPedidoOperacional).HasForeignKey(c => c.cdStand).WillCascadeOnDelete(false); // FK_TB_DD_ItensPedidoOperacional_cdStand
            HasRequired(a => a.TB_DD_Pedido).WithMany(b => b.TB_DD_ItensPedidoOperacional).HasForeignKey(c => c.TB_Pedido_cdPedido).WillCascadeOnDelete(false); // FK_TB_DD_ItensPedidoOperacional_TB_Pedido_cdPedido
            HasRequired(a => a.TB_DD_ProdutoOperacional).WithMany(b => b.TB_DD_ItensPedidoOperacional).HasForeignKey(c => c.TB_ProdutoOperacional_cdProduto).WillCascadeOnDelete(false); // FK_TB_DD_ItensPedidoOperacional_TB_DD_ProdutoOperacional_cdProduto
        }
    }

    // TB_DD_LeituraRegulamento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_LeituraRegulamentoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_LeituraRegulamento>
    {
        public TB_DD_LeituraRegulamentoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_LeituraRegulamentoConfiguration(string schema)
        {
            ToTable("TB_DD_LeituraRegulamento", schema);
            HasKey(x => x.cdLeituraRegulamento);

            Property(x => x.cdLeituraRegulamento).HasColumnName(@"cdLeituraRegulamento").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.DataDownload).HasColumnName(@"DataDownload").HasColumnType("datetime").IsOptional();
            Property(x => x.cdRegulamento).HasColumnName(@"cdRegulamento").HasColumnType("int").IsOptional();
            Property(x => x.cdUsuario).HasColumnName(@"cdUsuario").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_DD_Regulamento_cdRegulamento).WithMany(b => b.TB_DD_LeituraRegulamento_cdRegulamento).HasForeignKey(c => c.cdRegulamento).WillCascadeOnDelete(false); // FK_TB_DD_LeituraRegulamento_TB_DD_Regulamento
            HasOptional(a => a.TB_DD_Regulamento1).WithMany(b => b.TB_DD_LeituraRegulamento1).HasForeignKey(c => c.cdRegulamento).WillCascadeOnDelete(false); // FK_TB_DD_LeituraRegulamento_cdRegulamento
        }
    }

    // TB_DD_LimiteAplicacao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_LimiteAplicacaoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_LimiteAplicacao>
    {
        public TB_DD_LimiteAplicacaoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_LimiteAplicacaoConfiguration(string schema)
        {
            ToTable("TB_DD_LimiteAplicacao", schema);
            HasKey(x => x.cdLimiteAplicacao);

            Property(x => x.cdLimiteAplicacao).HasColumnName(@"cdLimiteAplicacao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dtInicio).HasColumnName(@"dtInicio").HasColumnType("datetime").IsRequired();
            Property(x => x.dtLimite).HasColumnName(@"dtLimite").HasColumnType("datetime").IsRequired();
            Property(x => x.nmMensagemIngles).HasColumnName(@"nmMensagemIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.nmMensagemEspanhol).HasColumnName(@"nmMensagemEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsMensagemPortugues).HasColumnName(@"dsMensagemPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.cdUsuario_Inclusao).HasColumnName(@"cdUsuario_Inclusao").HasColumnType("int").IsRequired();
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuario_Alteracao).HasColumnName(@"cdUsuario_Alteracao").HasColumnType("int").IsRequired();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsRequired();

            // Foreign keys
            HasRequired(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_LimiteAplicacao).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_LimiteAplicacao_TB_CF_Edicao
            HasRequired(a => a.TB_DD_Usuario_cdUsuario_Alteracao).WithMany(b => b.cdUsuario_Alteracao).HasForeignKey(c => c.cdUsuario_Alteracao).WillCascadeOnDelete(false); // FK_TB_DD_LimiteAplicacao_TB_DD_Usuario_Alteracao
            HasRequired(a => a.TB_DD_Usuario_cdUsuario_Inclusao).WithMany(b => b.cdUsuario_Inclusao).HasForeignKey(c => c.cdUsuario_Inclusao).WillCascadeOnDelete(false); // FK_TB_DD_LimiteAplicacao_TB_DD_Usuario_Inclusao
        }
    }

    // TB_DD_LogImportacaoAPAS
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_LogImportacaoAPASConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_LogImportacaoAPAS>
    {
        public TB_DD_LogImportacaoAPASConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_LogImportacaoAPASConfiguration(string schema)
        {
            ToTable("TB_DD_LogImportacaoAPAS", schema);
            HasKey(x => x.cdLogImportacao);

            Property(x => x.cdLogImportacao).HasColumnName(@"cdLogImportacao").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dtImportacao).HasColumnName(@"dtImportacao").HasColumnType("datetime").IsOptional();
            Property(x => x.lote).HasColumnName(@"lote").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.Tipo_de_Convite).HasColumnName(@"Tipo_de_Convite").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.CONTACT_ID_CRM).HasColumnName(@"CONTACT_ID_CRM").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.NOME).HasColumnName(@"NOME").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.NOME_CRACHA).HasColumnName(@"NOME_CRACHA").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.DATA_DE_NASCIMENTO).HasColumnName(@"DATA_DE_NASCIMENTO").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.MES_DE_NASCIMENTO).HasColumnName(@"MES_DE_NASCIMENTO").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.ANO_DE_NASCIMENTO).HasColumnName(@"ANO_DE_NASCIMENTO").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.SEXO).HasColumnName(@"SEXO").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.CARGO).HasColumnName(@"CARGO").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.AREA).HasColumnName(@"AREA").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.CPF).HasColumnName(@"CPF").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.Contato_DDI_TELEFONE).HasColumnName(@"Contato_DDI_TELEFONE").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.Contato_DDD_TELEFONE).HasColumnName(@"Contato_DDD_TELEFONE").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.Contato_TELEFONE).HasColumnName(@"Contato_TELEFONE").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.DDI_CELULAR).HasColumnName(@"DDI_CELULAR").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.DDD_CELULAR).HasColumnName(@"DDD_CELULAR").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.CELULAR).HasColumnName(@"CELULAR").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.E_MAIL_Profissional).HasColumnName(@"E_MAIL_Profissional").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.E_MAIL_Pessoal).HasColumnName(@"E_MAIL_Pessoal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.ACCOUNT_ID_CRM).HasColumnName(@"ACCOUNT_ID_CRM").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.RAZAO_SOCIAL).HasColumnName(@"RAZAO_SOCIAL").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.NOME_FANTASIA).HasColumnName(@"NOME_FANTASIA").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.CNPJ).HasColumnName(@"CNPJ").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.Codigo_de_Cliente_APAS).HasColumnName(@"Codigo_de_Cliente_APAS").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.Nivel_Hierarquico).HasColumnName(@"Nivel_Hierarquico").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.ATIVIDADE_DE_ATUACAO).HasColumnName(@"ATIVIDADE_DE_ATUACAO").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.REGIONAL_DISTRITAL).HasColumnName(@"REGIONAL_DISTRITAL").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.ASSOCIADO).HasColumnName(@"ASSOCIADO").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.DDI_TELEFONE).HasColumnName(@"DDI_TELEFONE").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.DDD_TELEFONE).HasColumnName(@"DDD_TELEFONE").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.TELEFONE).HasColumnName(@"TELEFONE").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.CEP).HasColumnName(@"CEP").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.ENDERECO).HasColumnName(@"ENDERECO").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.NUMERO).HasColumnName(@"NUMERO").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.COMPLEMENTO).HasColumnName(@"COMPLEMENTO").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.BAIRRO).HasColumnName(@"BAIRRO").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.CIDADE).HasColumnName(@"CIDADE").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.ESTADO).HasColumnName(@"ESTADO").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.PAIS).HasColumnName(@"PAIS").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.EMAIL).HasColumnName(@"EMAIL").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.SITE).HasColumnName(@"SITE").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsImportado).HasColumnName(@"dsImportado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
        }
    }

    // TB_DD_LoteImpressao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_LoteImpressaoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_LoteImpressao>
    {
        public TB_DD_LoteImpressaoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_LoteImpressaoConfiguration(string schema)
        {
            ToTable("TB_DD_LoteImpressao", schema);
            HasKey(x => x.cdLote);

            Property(x => x.cdLote).HasColumnName(@"cdLote").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEmpresa).HasColumnName(@"cdEmpresa").HasColumnType("int").IsOptional();
            Property(x => x.cdUsuario).HasColumnName(@"cdUsuario").HasColumnType("int").IsOptional();
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_DD_Empresa).WithMany(b => b.TB_DD_LoteImpressao).HasForeignKey(c => c.cdEmpresa).WillCascadeOnDelete(false); // FK_TB_DD_LoteImpressao_TB_DD_Empresa
        }
    }

    // TB_DD_MaquinaseVeiculos
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_MaquinaseVeiculosConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_MaquinaseVeiculos>
    {
        public TB_DD_MaquinaseVeiculosConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_MaquinaseVeiculosConfiguration(string schema)
        {
            ToTable("TB_DD_MaquinaseVeiculos", schema);
            HasKey(x => x.cdMaquinas);

            Property(x => x.cdMaquinas).HasColumnName(@"cdMaquinas").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Stand_cdStand).HasColumnName(@"TB_Stand_cdStand").HasColumnType("int").IsRequired();
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.dsTipoMaquinario).HasColumnName(@"dsTipoMaquinario").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsLargura).HasColumnName(@"dsLargura").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsAltura).HasColumnName(@"dsAltura").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsProfundidade).HasColumnName(@"dsProfundidade").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsPeso).HasColumnName(@"dsPeso").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsFormaExposicao).HasColumnName(@"dsFormaExposicao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsFumaca).HasColumnName(@"dsFumaca").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsNivelBarulho).HasColumnName(@"dsNivelBarulho").HasColumnType("char").IsOptional().IsFixedLength().IsUnicode(false).HasMaxLength(100);

            // Foreign keys
            HasRequired(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_MaquinaseVeiculos).HasForeignKey(c => c.TB_Edicao_cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_MaquinaseVeiculos_TB_CF_Edicao
            HasRequired(a => a.TB_DD_Stand).WithMany(b => b.TB_DD_MaquinaseVeiculos).HasForeignKey(c => c.TB_Stand_cdStand).WillCascadeOnDelete(false); // FK_TB_DD_MaquinaseVeiculos_TB_DD_Stand
        }
    }

    // TB_DD_Monitoria
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_MonitoriaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Monitoria>
    {
        public TB_DD_MonitoriaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_MonitoriaConfiguration(string schema)
        {
            ToTable("TB_DD_Monitoria", schema);
            HasKey(x => x.cdAcesso);

            Property(x => x.cdAcesso).HasColumnName(@"cdAcesso").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.chSessao).HasColumnName(@"chSessao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsNavegador).HasColumnName(@"dsNavegador").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dtAcesso).HasColumnName(@"dtAcesso").HasColumnType("datetime").IsOptional();
            Property(x => x.dsSistema).HasColumnName(@"dsSistema").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.tl_Acesso).HasColumnName(@"tl_Acesso").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.tl_Escolha).HasColumnName(@"tl_Escolha").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dtEscolha).HasColumnName(@"dtEscolha").HasColumnType("datetime").IsOptional();
            Property(x => x.tl_Dados).HasColumnName(@"tl_Dados").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dtDados).HasColumnName(@"dtDados").HasColumnType("datetime").IsOptional();
            Property(x => x.tl_Questionario).HasColumnName(@"tl_Questionario").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dtQuestionario).HasColumnName(@"dtQuestionario").HasColumnType("datetime").IsOptional();
            Property(x => x.tl_liConcordo).HasColumnName(@"tl_liConcordo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dtliConcordo).HasColumnName(@"dtliConcordo").HasColumnType("datetime").IsOptional();
            Property(x => x.tl_Produto).HasColumnName(@"tl_Produto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dtProduto).HasColumnName(@"dtProduto").HasColumnType("datetime").IsOptional();
            Property(x => x.tl_Carta).HasColumnName(@"tl_Carta").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dtCarta).HasColumnName(@"dtCarta").HasColumnType("datetime").IsOptional();
            Property(x => x.dsIP).HasColumnName(@"dsIP").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsEmail).HasColumnName(@"dsEmail").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsFacebook).HasColumnName(@"dsFacebook").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsEvento).HasColumnName(@"dsEvento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.cdCategoria).HasColumnName(@"cdCategoria").HasColumnType("int").IsOptional();
            Property(x => x.dsTentativas).HasColumnName(@"dsTentativas").HasColumnType("int").IsOptional();
            Property(x => x.dsIdioma).HasColumnName(@"dsIdioma").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(2);
        }
    }

    // TB_DD_MonitoriaCielo
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_MonitoriaCieloConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_MonitoriaCielo>
    {
        public TB_DD_MonitoriaCieloConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_MonitoriaCieloConfiguration(string schema)
        {
            ToTable("TB_DD_MonitoriaCielo", schema);
            HasKey(x => x.cdMonitoria);

            Property(x => x.cdMonitoria).HasColumnName(@"cdMonitoria").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsMonitoria).HasColumnName(@"dsMonitoria").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dtMonitoria).HasColumnName(@"dtMonitoria").HasColumnType("datetime").IsOptional();
            Property(x => x.dsNavegador).HasColumnName(@"dsNavegador").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsIP).HasColumnName(@"dsIP").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsRowId).HasColumnName(@"dsRowId").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsParametrosRecebidos).HasColumnName(@"dsParametrosRecebidos").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.order_number).HasColumnName(@"order_number").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.amount).HasColumnName(@"amount").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.checkout_cielo_order_number).HasColumnName(@"checkout_cielo_order_number").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.created_date).HasColumnName(@"created_date").HasColumnType("datetime").IsOptional();
            Property(x => x.customer_name).HasColumnName(@"customer_name").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.customer_phone).HasColumnName(@"customer_phone").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.customer_identity).HasColumnName(@"customer_identity").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.customer_email).HasColumnName(@"customer_email").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.shipping_type).HasColumnName(@"shipping_type").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.payment_method_type).HasColumnName(@"payment_method_type").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.payment_method_brand).HasColumnName(@"payment_method_brand").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.payment_maskedcreditcard).HasColumnName(@"payment_maskedcreditcard").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.payment_installments).HasColumnName(@"payment_installments").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.payment_status).HasColumnName(@"payment_status").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.tid).HasColumnName(@"tid").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
        }
    }

    // TB_DD_MonitoriaPayPal
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_MonitoriaPayPalConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_MonitoriaPayPal>
    {
        public TB_DD_MonitoriaPayPalConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_MonitoriaPayPalConfiguration(string schema)
        {
            ToTable("TB_DD_MonitoriaPayPal", schema);
            HasKey(x => x.cdMonitoriaPaypal);

            Property(x => x.cdMonitoriaPaypal).HasColumnName(@"cdMonitoriaPaypal").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsMonitoriaPayPal).HasColumnName(@"dsMonitoriaPayPal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.numPedido).HasColumnName(@"numPedido").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.numSistema).HasColumnName(@"numSistema").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.dsFormAllKeys).HasColumnName(@"dsFormAllKeys").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(2000);
            Property(x => x.dsQueryStringAllKey).HasColumnName(@"dsQueryStringAllKey").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(2000);
        }
    }

    // TB_DD_MonitoriaRedeCard
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_MonitoriaRedeCardConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_MonitoriaRedeCard>
    {
        public TB_DD_MonitoriaRedeCardConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_MonitoriaRedeCardConfiguration(string schema)
        {
            ToTable("TB_DD_MonitoriaRedeCard", schema);
            HasKey(x => x.cdMonitoriaRedeCard);

            Property(x => x.cdMonitoriaRedeCard).HasColumnName(@"cdMonitoriaRedeCard").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsParametrosRecebidos).HasColumnName(@"dsParametrosRecebidos").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.cdPedido).HasColumnName(@"cdPedido").HasColumnType("int").IsRequired();
            Property(x => x.DataTransacao).HasColumnName(@"DataTransacao").HasColumnType("datetime").IsRequired();
            Property(x => x.Data).HasColumnName(@"Data").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.NumPedido).HasColumnName(@"NumPedido").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Nr_Cartao).HasColumnName(@"Nr_Cartao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Origem_Bin).HasColumnName(@"Origem_Bin").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.NumAutor).HasColumnName(@"NumAutor").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.NumCV).HasColumnName(@"NumCV").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.NumAutent).HasColumnName(@"NumAutent").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.NumSqn).HasColumnName(@"NumSqn").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Data_Expi).HasColumnName(@"Data_Expi").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.CodRet).HasColumnName(@"CodRet").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.MsgRet).HasColumnName(@"MsgRet").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.DataConfirmacao).HasColumnName(@"DataConfirmacao").HasColumnType("datetime").IsOptional();
            Property(x => x.dsMovimentacao).HasColumnName(@"dsMovimentacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
        }
    }

    // TB_DD_MonitoriaShopLineItau
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_MonitoriaShopLineItauConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_MonitoriaShopLineItau>
    {
        public TB_DD_MonitoriaShopLineItauConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_MonitoriaShopLineItauConfiguration(string schema)
        {
            ToTable("TB_DD_MonitoriaShopLineItau", schema);
            HasKey(x => x.cdMonitoriaShoplineItau);

            Property(x => x.cdMonitoriaShoplineItau).HasColumnName(@"cdMonitoriaShoplineItau").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dtProcessamento).HasColumnName(@"dtProcessamento").HasColumnType("datetime").IsOptional();
            Property(x => x.dsRowId).HasColumnName(@"dsRowId").HasColumnType("uniqueidentifier").IsRequired();
            Property(x => x.dsFormAllKeys).HasColumnName(@"dsFormAllKeys").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(8000);
            Property(x => x.dsQueryStringAllKey).HasColumnName(@"dsQueryStringAllKey").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(8000);
            Property(x => x.cdEvento).HasColumnName(@"cdEvento").HasColumnType("int").IsOptional();
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.dscodEmp).HasColumnName(@"dscodEmp").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsDC).HasColumnName(@"dsDC").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsPedido).HasColumnName(@"dsPedido").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsChave).HasColumnName(@"dsChave").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsTipPag).HasColumnName(@"dsTipPag").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsSitPag).HasColumnName(@"dsSitPag").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsDtPag).HasColumnName(@"dsDtPag").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsCodAut).HasColumnName(@"dsCodAut").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsNumId).HasColumnName(@"dsNumId").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsCompVend).HasColumnName(@"dsCompVend").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsTipCart).HasColumnName(@"dsTipCart").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
        }
    }

    // TB_DD_MonitoriaStone
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_MonitoriaStoneConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_MonitoriaStone>
    {
        public TB_DD_MonitoriaStoneConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_MonitoriaStoneConfiguration(string schema)
        {
            ToTable("TB_DD_MonitoriaStone", schema);
            HasKey(x => x.cdMonitoriaStone);

            Property(x => x.cdMonitoriaStone).HasColumnName(@"cdMonitoriaStone").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dtTransacao).HasColumnName(@"dtTransacao").HasColumnType("datetime").IsRequired();
            Property(x => x.cdPedido).HasColumnName(@"cdPedido").HasColumnType("int").IsRequired();
            Property(x => x.dsCodigoRetorno).HasColumnName(@"dsCodigoRetorno").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsChavePedido).HasColumnName(@"dsChavePedido").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsMensagem).HasColumnName(@"dsMensagem").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.MerchantKey).HasColumnName(@"MerchantKey").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.RequestKey).HasColumnName(@"RequestKey").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.AcquirerMessage).HasColumnName(@"AcquirerMessage").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.AcquirerName).HasColumnName(@"AcquirerName").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.AcquirerReturnCode).HasColumnName(@"AcquirerReturnCode").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.AffiliationCode).HasColumnName(@"AffiliationCode").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.AmountInCents).HasColumnName(@"AmountInCents").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.AuthorizationCode).HasColumnName(@"AuthorizationCode").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.AuthorizedAmountInCents).HasColumnName(@"AuthorizedAmountInCents").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.CapturedAmountInCents).HasColumnName(@"CapturedAmountInCents").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.CapturedDate).HasColumnName(@"CapturedDate").HasColumnType("datetime").IsOptional();
            Property(x => x.CreditCardBrand).HasColumnName(@"CreditCardBrand").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.InstantBuyKey).HasColumnName(@"InstantBuyKey").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.IsExpiredCreditCard).HasColumnName(@"IsExpiredCreditCard").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.MaskedCreditCardNumber).HasColumnName(@"MaskedCreditCardNumber").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.CreditCardOperation).HasColumnName(@"CreditCardOperation").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.CreditCardTransactionStatus).HasColumnName(@"CreditCardTransactionStatus").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.PaymentMethodName).HasColumnName(@"PaymentMethodName").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.TransactionIdentifier).HasColumnName(@"TransactionIdentifier").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.TransactionKey).HasColumnName(@"TransactionKey").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.TransactionKeyToAcquirer).HasColumnName(@"TransactionKeyToAcquirer").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.TransactionReference).HasColumnName(@"TransactionReference").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.UniqueSequentialNumber).HasColumnName(@"UniqueSequentialNumber").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.OrderKey).HasColumnName(@"OrderKey").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.OrderReference).HasColumnName(@"OrderReference").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
        }
    }

    // TB_DD_Movimentacao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_MovimentacaoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Movimentacao>
    {
        public TB_DD_MovimentacaoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_MovimentacaoConfiguration(string schema)
        {
            ToTable("TB_DD_Movimentacao", schema);
            HasKey(x => x.cdMovimentacao);

            Property(x => x.cdMovimentacao).HasColumnName(@"cdMovimentacao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Categoria_cdCategoria).HasColumnName(@"TB_Categoria_cdCategoria").HasColumnType("int").IsRequired();
            Property(x => x.TB_Empresa_cdEmpresaPrimario).HasColumnName(@"TB_Empresa_cdEmpresaPrimario").HasColumnType("int").IsRequired();
            Property(x => x.TB_Empresa_cdEmpresaSecundario).HasColumnName(@"TB_Empresa_cdEmpresaSecundario").HasColumnType("int").IsRequired();
            Property(x => x.nuLimite).HasColumnName(@"nuLimite").HasColumnType("int").IsOptional();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdStand).HasColumnName(@"cdStand").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_DD_Stand).WithMany(b => b.TB_DD_Movimentacao).HasForeignKey(c => c.cdStand).WillCascadeOnDelete(false); // FK_TB_DD_Movimentacao_TB_DD_Stand
            HasRequired(a => a.TB_DD_Empresa_TB_Empresa_cdEmpresaPrimario).WithMany(b => b.TB_Empresa_cdEmpresaPrimario).HasForeignKey(c => c.TB_Empresa_cdEmpresaPrimario).WillCascadeOnDelete(false); // FK_TB_DD_Movimentacao_TB_DD_Empresa_cdEmpresaPrimario
            HasRequired(a => a.TB_DD_Empresa_TB_Empresa_cdEmpresaSecundario).WithMany(b => b.TB_Empresa_cdEmpresaSecundario).HasForeignKey(c => c.TB_Empresa_cdEmpresaSecundario).WillCascadeOnDelete(false); // FK_TB_DD_Movimentacao_TB_DD_Empresa_cdEmpresaSecundario
            HasRequired(a => a.TB_RF_Categoria).WithMany(b => b.TB_DD_Movimentacao).HasForeignKey(c => c.TB_Categoria_cdCategoria).WillCascadeOnDelete(false); // FK_TB_DD_Movimentacao_TB_RF_Categoria
        }
    }

    // TB_DD_News
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_NewsConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_News>
    {
        public TB_DD_NewsConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_NewsConfiguration(string schema)
        {
            ToTable("TB_DD_News", schema);
            HasKey(x => x.cdNews);

            Property(x => x.cdNews).HasColumnName(@"cdNews").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsTituloPortugues).HasColumnName(@"dsTituloPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsTituloIngles).HasColumnName(@"dsTituloIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsTituloEspanhol).HasColumnName(@"dsTituloEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsDescricaoPortugues).HasColumnName(@"dsDescricaoPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsDescricaoIngles).HasColumnName(@"dsDescricaoIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsDescricaoEspanhol).HasColumnName(@"dsDescricaoEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.cdSistema).HasColumnName(@"cdSistema").HasColumnType("int").IsRequired();
            Property(x => x.dtInicio).HasColumnName(@"dtInicio").HasColumnType("datetime").IsRequired();
            Property(x => x.dtFim).HasColumnName(@"dtFim").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioInclusao).HasColumnName(@"cdUsuarioInclusao").HasColumnType("int").IsRequired();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();
            Property(x => x.fgHabilitado).HasColumnName(@"fgHabilitado").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
        }
    }

    // TB_DD_Pagamento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_PagamentoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Pagamento>
    {
        public TB_DD_PagamentoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_PagamentoConfiguration(string schema)
        {
            ToTable("TB_DD_Pagamento", schema);
            HasKey(x => x.cdPagamento);

            Property(x => x.cdPagamento).HasColumnName(@"cdPagamento").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdPedido).HasColumnName(@"cdPedido").HasColumnType("int").IsOptional();
            Property(x => x.cdFormaPagamento).HasColumnName(@"cdFormaPagamento").HasColumnType("int").IsOptional();
            Property(x => x.dsValor).HasColumnName(@"dsValor").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Status).HasColumnName(@"Status").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.dtCancelamento).HasColumnName(@"dtCancelamento").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioInclusao).HasColumnName(@"cdUsuarioInclusao").HasColumnType("int").IsOptional();
            Property(x => x.cdUsuarioCancelamento).HasColumnName(@"cdUsuarioCancelamento").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_DD_Pedido).WithMany(b => b.TB_DD_Pagamento).HasForeignKey(c => c.cdPedido).WillCascadeOnDelete(false); // FK_TB_DD_Pagamento_cdPedido
            HasOptional(a => a.TB_RF_FormaPagamento).WithMany(b => b.TB_DD_Pagamento).HasForeignKey(c => c.cdFormaPagamento).WillCascadeOnDelete(false); // FK_TB_DD_Pagamento_cdFormaPagamento
        }
    }

    // TB_DD_ParcelamentoBoleto
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ParcelamentoBoletoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ParcelamentoBoleto>
    {
        public TB_DD_ParcelamentoBoletoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ParcelamentoBoletoConfiguration(string schema)
        {
            ToTable("TB_DD_ParcelamentoBoleto", schema);
            HasKey(x => x.cdBoleto);

            Property(x => x.cdBoleto).HasColumnName(@"cdBoleto").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdPedido).HasColumnName(@"cdPedido").HasColumnType("int").IsOptional();
            Property(x => x.nuParcela).HasColumnName(@"nuParcela").HasColumnType("int").IsOptional();
            Property(x => x.dtVencimento).HasColumnName(@"dtVencimento").HasColumnType("date").IsOptional();
            Property(x => x.dtPagamento).HasColumnName(@"dtPagamento").HasColumnType("date").IsOptional();
            Property(x => x.cdUsuarioBaixa).HasColumnName(@"cdUsuarioBaixa").HasColumnType("int").IsOptional();
            Property(x => x.dsValorParcela).HasColumnName(@"dsValorParcela").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsValorDesconto).HasColumnName(@"dsValorDesconto").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsValorOriginalParcela).HasColumnName(@"dsValorOriginalParcela").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();
            Property(x => x.dtCNAB).HasColumnName(@"dtCNAB").HasColumnType("datetime").IsOptional();
            Property(x => x.cdControle).HasColumnName(@"cdControle").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_DD_Pedido).WithMany(b => b.TB_DD_ParcelamentoBoleto).HasForeignKey(c => c.cdPedido).WillCascadeOnDelete(false); // FK_TB_DD_ParcelamentoBoleto_cdPedido
        }
    }

    // TB_DD_Pedido
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_PedidoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Pedido>
    {
        public TB_DD_PedidoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_PedidoConfiguration(string schema)
        {
            ToTable("TB_DD_Pedido", schema);
            HasKey(x => x.cdPedido);

            Property(x => x.cdPedido).HasColumnName(@"cdPedido").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.TB_Empresa_cdEmpresa).HasColumnName(@"TB_Empresa_cdEmpresa").HasColumnType("int").IsRequired();
            Property(x => x.dtPedido).HasColumnName(@"dtPedido").HasColumnType("datetime").IsOptional();
            Property(x => x.dtVencimento).HasColumnName(@"dtVencimento").HasColumnType("datetime").IsOptional();
            Property(x => x.dtPagamento).HasColumnName(@"dtPagamento").HasColumnType("datetime").IsOptional();
            Property(x => x.dsPedido).HasColumnName(@"dsPedido").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtCancelamento).HasColumnName(@"dtCancelamento").HasColumnType("datetime").IsOptional();
            Property(x => x.dsPago).HasColumnName(@"dsPago").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.vlTotal).HasColumnName(@"vlTotal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.vlDesconto).HasColumnName(@"vlDesconto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.nuParcela).HasColumnName(@"nuParcela").HasColumnType("int").IsOptional();
            Property(x => x.TipoPessoaFaturamento).HasColumnName(@"TipoPessoaFaturamento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.nmFaturamento).HasColumnName(@"nmFaturamento").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsCpfCnpjFaturamento).HasColumnName(@"dsCpfCnpjFaturamento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsEnderecoFaturamento).HasColumnName(@"dsEnderecoFaturamento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCepfaturamento).HasColumnName(@"dsCepfaturamento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsComplementoFaturamento).HasColumnName(@"dsComplementoFaturamento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCidadeFaturamento).HasColumnName(@"dsCidadeFaturamento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsUfFaturamento).HasColumnName(@"dsUfFaturamento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsInscricaoEstadualFaturamento).HasColumnName(@"dsInscricaoEstadualFaturamento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdPessoa).HasColumnName(@"cdPessoa").HasColumnType("int").IsOptional();
            Property(x => x.dsMoeda).HasColumnName(@"dsMoeda").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsEstorno).HasColumnName(@"dsEstorno").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.vlAcrescimo).HasColumnName(@"vlAcrescimo").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.UsuarioBaixaPagamento).HasColumnName(@"UsuarioBaixaPagamento").HasColumnType("int").IsOptional();
            Property(x => x.UsuarioCancelamento).HasColumnName(@"UsuarioCancelamento").HasColumnType("int").IsOptional();
            Property(x => x.dsObs).HasColumnName(@"dsObs").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdTipoPedido).HasColumnName(@"cdTipoPedido").HasColumnType("int").IsOptional();
            Property(x => x.dsIdioma).HasColumnName(@"dsIdioma").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdUsuario).HasColumnName(@"cdUsuario").HasColumnType("int").IsOptional();
            Property(x => x.dsNumeroFaturamento).HasColumnName(@"dsNumeroFaturamento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.oTid).HasColumnName(@"oTid").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.cdSistema).HasColumnName(@"cdSistema").HasColumnType("int").IsOptional();
            Property(x => x.cdResponsavelCongresso).HasColumnName(@"cdResponsavelCongresso").HasColumnType("int").IsOptional();
            Property(x => x.cdRegraDesconto).HasColumnName(@"cdRegraDesconto").HasColumnType("int").IsOptional();
            Property(x => x.dsTid).HasColumnName(@"dsTid").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsBandeiraCielo).HasColumnName(@"dsBandeiraCielo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdRegraDescontoPorProduto).HasColumnName(@"cdRegraDescontoPorProduto").HasColumnType("int").IsOptional();
            Property(x => x.dtCancelamentoAutomatico).HasColumnName(@"dtCancelamentoAutomatico").HasColumnType("datetime").IsOptional();
            Property(x => x.dsTipoEmpresa).HasColumnName(@"dsTipoEmpresa").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsDescontoAdministrativo).HasColumnName(@"dsDescontoAdministrativo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsAcrescimoAdministrativo).HasColumnName(@"dsAcrescimoAdministrativo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsLiberado).HasColumnName(@"dsLiberado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsSaleIdPaypalPlus).HasColumnName(@"dsSaleIdPaypalPlus").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dtSalesPaypalPlus).HasColumnName(@"dtSalesPaypalPlus").HasColumnType("datetime").IsOptional();
            Property(x => x.dsSalesStatusPayPalPlus).HasColumnName(@"dsSalesStatusPayPalPlus").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsIncideImposto).HasColumnName(@"dsIncideImposto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.vlImposto).HasColumnName(@"vlImposto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.vlParaCalculoImposto).HasColumnName(@"vlParaCalculoImposto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsComentarioLiberacao).HasColumnName(@"dsComentarioLiberacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsTipoPedido).HasColumnName(@"dsTipoPedido").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsObservacaoInterna).HasColumnName(@"dsObservacaoInterna").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(2000);
            Property(x => x.idPagamento).HasColumnName(@"idPagamento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_Pedido).HasForeignKey(c => c.TB_Edicao_cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_Pedido_TB_CF_Edicao_cdEdicao
            HasOptional(a => a.TB_RF_TipoPedido).WithMany(b => b.TB_DD_Pedido).HasForeignKey(c => c.cdTipoPedido).WillCascadeOnDelete(false); // FK_TB_DD_Pedido_TB_RF_TipoPedido
            HasRequired(a => a.TB_DD_Empresa).WithMany(b => b.TB_DD_Pedido).HasForeignKey(c => c.TB_Empresa_cdEmpresa).WillCascadeOnDelete(false); // FK_TB_DD_Pedido_TB_DD_Empresa_cdEmpresa
        }
    }

    // TB_DD_Pergunta
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_PerguntaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Pergunta>
    {
        public TB_DD_PerguntaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_PerguntaConfiguration(string schema)
        {
            ToTable("TB_DD_Pergunta", schema);
            HasKey(x => x.cdPergunta);

            Property(x => x.cdPergunta).HasColumnName(@"cdPergunta").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsPortugues).HasColumnName(@"dsPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsIngles).HasColumnName(@"dsIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsEspanhol).HasColumnName(@"dsEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.fgSituacao).HasColumnName(@"fgSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
        }
    }

    // TB_DD_PermissaoCorCarpeteColuna
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_PermissaoCorCarpeteColunaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_PermissaoCorCarpeteColuna>
    {
        public TB_DD_PermissaoCorCarpeteColunaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_PermissaoCorCarpeteColunaConfiguration(string schema)
        {
            ToTable("TB_DD_PermissaoCorCarpeteColuna", schema);
            HasKey(x => x.cdPermissao);

            Property(x => x.cdPermissao).HasColumnName(@"cdPermissao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdFormulario).HasColumnName(@"cdFormulario").HasColumnType("int").IsRequired();
            Property(x => x.cdContrato).HasColumnName(@"cdContrato").HasColumnType("int").IsRequired();
            Property(x => x.fgCarpete).HasColumnName(@"fgCarpete").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgColuna).HasColumnName(@"fgColuna").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);

            // Foreign keys
            HasRequired(a => a.TB_CF_FormularioManualEletronico).WithMany(b => b.TB_DD_PermissaoCorCarpeteColuna).HasForeignKey(c => c.cdFormulario).WillCascadeOnDelete(false); // FK_TB_DD_PermissaoCorCarpeteColuna_TB_CF_FormularioManualEletronico
            HasRequired(a => a.TB_RF_TipoContrato).WithMany(b => b.TB_DD_PermissaoCorCarpeteColuna).HasForeignKey(c => c.cdContrato).WillCascadeOnDelete(false); // FK_TB_DD_PermissaoCorCarpeteColuna_TB_RF_TipoContrato
        }
    }

    // TB_DD_Pessoa
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_PessoaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Pessoa>
    {
        public TB_DD_PessoaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_PessoaConfiguration(string schema)
        {
            ToTable("TB_DD_Pessoa", schema);
            HasKey(x => x.cdPessoa);

            Property(x => x.cdPessoa).HasColumnName(@"cdPessoa").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.dsEmailLogin).HasColumnName(@"dsEmailLogin").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsSenha).HasColumnName(@"dsSenha").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCPF).HasColumnName(@"dsCPF").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsRG).HasColumnName(@"dsRG").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsNome).HasColumnName(@"dsNome").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsSobreNome).HasColumnName(@"dsSobreNome").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.nmCompleto).HasColumnName(@"nmCompleto").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.nmCracha).HasColumnName(@"nmCracha").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsTratamento).HasColumnName(@"dsTratamento").HasColumnType("int").IsOptional();
            Property(x => x.dsSexo).HasColumnName(@"dsSexo").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dtNascimento).HasColumnName(@"dtNascimento").HasColumnType("datetime").IsOptional();
            Property(x => x.dsCargo).HasColumnName(@"dsCargo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsProfissao).HasColumnName(@"dsProfissao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsDepartamento).HasColumnName(@"dsDepartamento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsLogradouroPessoal).HasColumnName(@"dsLogradouroPessoal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsComplementoLogradouroPessoal).HasColumnName(@"dsComplementoLogradouroPessoal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsNumeroLogradouroPessoal).HasColumnName(@"dsNumeroLogradouroPessoal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCepLogradouroPessoal).HasColumnName(@"dsCepLogradouroPessoal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsBairroLogradouroPessoal).HasColumnName(@"dsBairroLogradouroPessoal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCidadeLogradouroPessoal).HasColumnName(@"dsCidadeLogradouroPessoal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsUfLogradouroPessoal).HasColumnName(@"dsUfLogradouroPessoal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.TB_Pais_cdPaisPessoal).HasColumnName(@"TB_Pais_cdPaisPessoal").HasColumnType("int").IsRequired();
            Property(x => x.dsSmsCelularPessoal).HasColumnName(@"dsSmsCelularPessoal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsMmsCelularPessoal).HasColumnName(@"dsMmsCelularPessoal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsAutorizaEnvioEmailPessoal).HasColumnName(@"dsAutorizaEnvioEmailPessoal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsEmail).HasColumnName(@"dsEmail").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsFoto).HasColumnName(@"dsFoto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Cpts).HasColumnName(@"Cpts").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(30);
            Property(x => x.dsEmpresaRepresentada).HasColumnName(@"dsEmpresaRepresentada").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsInternacional).HasColumnName(@"dsInternacional").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsIdiomaPreferencial).HasColumnName(@"dsIdiomaPreferencial").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDDITelefonePessoa1).HasColumnName(@"dsDDITelefonePessoa1").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDDDTelefonePessoa1).HasColumnName(@"dsDDDTelefonePessoa1").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsNumeroTelefonePessoa1).HasColumnName(@"dsNumeroTelefonePessoa1").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsRamalTelefonePessoa1).HasColumnName(@"dsRamalTelefonePessoa1").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDDITelefonePessoa2).HasColumnName(@"dsDDITelefonePessoa2").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDDDTelefonePessoa2).HasColumnName(@"dsDDDTelefonePessoa2").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsNumeroTelefonePessoa2).HasColumnName(@"dsNumeroTelefonePessoa2").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsRamalTelefonePessoa2).HasColumnName(@"dsRamalTelefonePessoa2").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDDITelefoneCelular).HasColumnName(@"dsDDITelefoneCelular").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDDDTelefoneCelular).HasColumnName(@"dsDDDTelefoneCelular").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsNumeroTelefoneCelular).HasColumnName(@"dsNumeroTelefoneCelular").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsObservacao).HasColumnName(@"dsObservacao").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsResPessoa1).HasColumnName(@"dsResPessoa1").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResPessoa2).HasColumnName(@"dsResPessoa2").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResPessoa3).HasColumnName(@"dsResPessoa3").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResPessoa4).HasColumnName(@"dsResPessoa4").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsResPessoa5).HasColumnName(@"dsResPessoa5").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.imageCredencial).HasColumnName(@"imageCredencial").HasColumnType("varbinary(max)").IsOptional();
            Property(x => x.dsCEPSebrae).HasColumnName(@"dsCEPSebrae").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsLogradouroSebrae).HasColumnName(@"dsLogradouroSebrae").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsComplementoSebrae).HasColumnName(@"dsComplementoSebrae").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.cdPaisSebrae).HasColumnName(@"cdPaisSebrae").HasColumnType("int").IsOptional();
            Property(x => x.cdEstadoSebrae).HasColumnName(@"cdEstadoSebrae").HasColumnType("int").IsOptional();
            Property(x => x.cdCidadeSebrae).HasColumnName(@"cdCidadeSebrae").HasColumnType("int").IsOptional();
            Property(x => x.cdBairroSebrae).HasColumnName(@"cdBairroSebrae").HasColumnType("int").IsOptional();
            Property(x => x.dsNumeroLogradouroSebrae).HasColumnName(@"dsNumeroLogradouroSebrae").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsInscricaoBlastu).HasColumnName(@"dsInscricaoBlastu").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtinscricaoBlastu).HasColumnName(@"dtinscricaoBlastu").HasColumnType("datetime").IsOptional();
            Property(x => x.cdPessoaOriginal).HasColumnName(@"cdPessoaOriginal").HasColumnType("int").IsOptional();
            Property(x => x.CONTACT_ID_CRM).HasColumnName(@"CONTACT_ID_CRM").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsEmailCorporativo).HasColumnName(@"dsEmailCorporativo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsAreaAPAS).HasColumnName(@"dsAreaAPAS").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.cdPessoaCongresso).HasColumnName(@"cdPessoaCongresso").HasColumnType("uniqueidentifier").IsOptional();
            Property(x => x.ContatoTipoId).HasColumnName(@"ContatoTipoId").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsSolenidadeConfirmada).HasColumnName(@"dsSolenidadeConfirmada").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);

            // Foreign keys
            HasOptional(a => a.TB_RF_Saudacao).WithMany(b => b.TB_DD_Pessoa).HasForeignKey(c => c.dsTratamento).WillCascadeOnDelete(false); // FK_TB_DD_Pessoa_TB_RF_Saudacao1
        }
    }

    // TB_DD_PlacaIdentificacao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_PlacaIdentificacaoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_PlacaIdentificacao>
    {
        public TB_DD_PlacaIdentificacaoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_PlacaIdentificacaoConfiguration(string schema)
        {
            ToTable("TB_DD_PlacaIdentificacao", schema);
            HasKey(x => x.cdPreenchimento);

            Property(x => x.cdPreenchimento).HasColumnName(@"cdPreenchimento").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.DataInclusao).HasColumnName(@"DataInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.UsuarioInsercao).HasColumnName(@"UsuarioInsercao").HasColumnType("int").IsOptional();
            Property(x => x.DataAlteracao).HasColumnName(@"DataAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.UsuarioAlteracao).HasColumnName(@"UsuarioAlteracao").HasColumnType("int").IsOptional();
            Property(x => x.cdEmpresa).HasColumnName(@"cdEmpresa").HasColumnType("int").IsOptional();
            Property(x => x.cdStand).HasColumnName(@"cdStand").HasColumnType("int").IsOptional();
            Property(x => x.dsPlaca).HasColumnName(@"dsPlaca").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.cdPedido).HasColumnName(@"cdPedido").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_PlacaIdentificacao).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // TB_DD_PlacaIdentificacao_cdEdicao
            HasOptional(a => a.TB_DD_Empresa).WithMany(b => b.TB_DD_PlacaIdentificacao).HasForeignKey(c => c.cdEmpresa).WillCascadeOnDelete(false); // FK_TB_DD_PlacaIdentificacao_TB_DD_Empresa
            HasOptional(a => a.TB_DD_Pedido).WithMany(b => b.TB_DD_PlacaIdentificacao).HasForeignKey(c => c.cdPedido).WillCascadeOnDelete(false); // TB_DD_PlacaIdentificacao_cdPedido
            HasOptional(a => a.TB_DD_Stand).WithMany(b => b.TB_DD_PlacaIdentificacao).HasForeignKey(c => c.cdStand).WillCascadeOnDelete(false); // TB_DD_PlacaIdentificacao_cdStand
        }
    }

    // TB_DD_PlacaIdentificacaoColuna
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_PlacaIdentificacaoColunaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_PlacaIdentificacaoColuna>
    {
        public TB_DD_PlacaIdentificacaoColunaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_PlacaIdentificacaoColunaConfiguration(string schema)
        {
            ToTable("TB_DD_PlacaIdentificacaoColuna", schema);
            HasKey(x => x.cdPreenchimento);

            Property(x => x.cdPreenchimento).HasColumnName(@"cdPreenchimento").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.DataInclusao).HasColumnName(@"DataInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.UsuarioInsercao).HasColumnName(@"UsuarioInsercao").HasColumnType("int").IsOptional();
            Property(x => x.DataAlteracao).HasColumnName(@"DataAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.UsuarioAlteracao).HasColumnName(@"UsuarioAlteracao").HasColumnType("int").IsOptional();
            Property(x => x.cdEmpresa).HasColumnName(@"cdEmpresa").HasColumnType("int").IsOptional();
            Property(x => x.cdStand).HasColumnName(@"cdStand").HasColumnType("int").IsOptional();
            Property(x => x.dsPlaca).HasColumnName(@"dsPlaca").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.cdPedido).HasColumnName(@"cdPedido").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_PlacaIdentificacaoColuna).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_PlacaIdentificacaoColuna_TB_CF_Edicao
            HasOptional(a => a.TB_DD_Empresa).WithMany(b => b.TB_DD_PlacaIdentificacaoColuna).HasForeignKey(c => c.cdEmpresa).WillCascadeOnDelete(false); // FK_TB_DD_PlacaIdentificacaoColuna_TB_DD_Empresa
            HasOptional(a => a.TB_DD_Pedido_cdPedido).WithMany(b => b.TB_DD_PlacaIdentificacaoColuna_cdPedido).HasForeignKey(c => c.cdPedido).WillCascadeOnDelete(false); // FK_TB_DD_PlacaIdentificacaoColuna_TB_DD_Pedido
            HasOptional(a => a.TB_DD_Pedido1).WithMany(b => b.TB_DD_PlacaIdentificacaoColuna1).HasForeignKey(c => c.cdPedido).WillCascadeOnDelete(false); // TB_DD_PlacaIdentificacaoColuna_cdPedido
            HasOptional(a => a.TB_DD_Stand).WithMany(b => b.TB_DD_PlacaIdentificacaoColuna).HasForeignKey(c => c.cdStand).WillCascadeOnDelete(false); // FK_TB_DD_PlacaIdentificacaoColuna_TB_DD_Stand
        }
    }

    // TB_DD_PrecoProdutoCongresso
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_PrecoProdutoCongressoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_PrecoProdutoCongresso>
    {
        public TB_DD_PrecoProdutoCongressoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_PrecoProdutoCongressoConfiguration(string schema)
        {
            ToTable("TB_DD_PrecoProdutoCongresso", schema);
            HasKey(x => x.cdPreco);

            Property(x => x.cdPreco).HasColumnName(@"cdPreco").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_ProdutoCongresso_cdProduto).HasColumnName(@"TB_ProdutoCongresso_cdProduto").HasColumnType("int").IsRequired();
            Property(x => x.dtLimite).HasColumnName(@"dtLimite").HasColumnType("datetime").IsOptional();
            Property(x => x.dsValor).HasColumnName(@"dsValor").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsValorUS).HasColumnName(@"dsValorUS").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.cdSubCategoria).HasColumnName(@"cdSubCategoria").HasColumnType("int").IsOptional();
            Property(x => x.dsCC).HasColumnName(@"dsCC").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCR).HasColumnName(@"dsCR").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCO).HasColumnName(@"dsCO").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsPR).HasColumnName(@"dsPR").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCodigoNumerico).HasColumnName(@"dsCodigoNumerico").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);

            // Foreign keys
            HasRequired(a => a.TB_DD_ProdutoCongresso).WithMany(b => b.TB_DD_PrecoProdutoCongresso).HasForeignKey(c => c.TB_ProdutoCongresso_cdProduto).WillCascadeOnDelete(false); // FK_TB_DD_PrecoProdutoCongresso_TB_DD_ProdutoCongresso
        }
    }

    // TB_DD_PrecoProdutoOperacional
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_PrecoProdutoOperacionalConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_PrecoProdutoOperacional>
    {
        public TB_DD_PrecoProdutoOperacionalConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_PrecoProdutoOperacionalConfiguration(string schema)
        {
            ToTable("TB_DD_PrecoProdutoOperacional", schema);
            HasKey(x => x.cdPreco);

            Property(x => x.cdPreco).HasColumnName(@"cdPreco").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_ProdutoOperacional_cdProduto).HasColumnName(@"TB_ProdutoOperacional_cdProduto").HasColumnType("int").IsRequired();
            Property(x => x.dtLimite).HasColumnName(@"dtLimite").HasColumnType("datetime").IsOptional();
            Property(x => x.dsValor).HasColumnName(@"dsValor").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsValorUS).HasColumnName(@"dsValorUS").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);

            // Foreign keys
            HasRequired(a => a.TB_DD_ProdutoOperacional).WithMany(b => b.TB_DD_PrecoProdutoOperacional).HasForeignKey(c => c.TB_ProdutoOperacional_cdProduto).WillCascadeOnDelete(false); // FK_TB_DD_PrecoProdutoOperacional_TB_DD_ProdutoOperacional
        }
    }

    // TB_DD_PresencaCampanhaSejaBemVindo
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_PresencaCampanhaSejaBemVindoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_PresencaCampanhaSejaBemVindo>
    {
        public TB_DD_PresencaCampanhaSejaBemVindoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_PresencaCampanhaSejaBemVindoConfiguration(string schema)
        {
            ToTable("TB_DD_PresencaCampanhaSejaBemVindo", schema);
            HasKey(x => x.cdPresencaCampanha);

            Property(x => x.cdPresencaCampanha).HasColumnName(@"cdPresencaCampanha").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsCodigoBarras).HasColumnName(@"dsCodigoBarras").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsRequired();
            Property(x => x.cdDialogue).HasColumnName(@"cdDialogue").HasColumnType("int").IsOptional();
            Property(x => x.dtDialogue).HasColumnName(@"dtDialogue").HasColumnType("datetime").IsOptional();
        }
    }

    // TB_DD_ProdutoCongresso
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ProdutoCongressoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ProdutoCongresso>
    {
        public TB_DD_ProdutoCongressoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ProdutoCongressoConfiguration(string schema)
        {
            ToTable("TB_DD_ProdutoCongresso", schema);
            HasKey(x => x.cdProduto);

            Property(x => x.cdProduto).HasColumnName(@"cdProduto").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.nmProdutoPortugues).HasColumnName(@"nmProdutoPortugues").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.nmProdutoIngles).HasColumnName(@"nmProdutoIngles").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsProdutoPortugues).HasColumnName(@"dsProdutoPortugues").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsProdutoIngles).HasColumnName(@"dsProdutoIngles").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsGrupo).HasColumnName(@"dsGrupo").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.nmProdutoEspanhol).HasColumnName(@"nmProdutoEspanhol").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsProdutoEspanhol).HasColumnName(@"dsProdutoEspanhol").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.nuOrdem).HasColumnName(@"nuOrdem").HasColumnType("int").IsOptional();
            Property(x => x.TipoProduto).HasColumnName(@"TipoProduto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.dsSigla).HasColumnName(@"dsSigla").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsDia).HasColumnName(@"dsDia").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsOptional();
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.dtLimite).HasColumnName(@"dtLimite").HasColumnType("datetime").IsOptional();
            Property(x => x.cdSistema).HasColumnName(@"cdSistema").HasColumnType("int").IsOptional();
            Property(x => x.cdGrupoProdutoCongresso).HasColumnName(@"cdGrupoProdutoCongresso").HasColumnType("int").IsOptional();
            Property(x => x.nuVagas).HasColumnName(@"nuVagas").HasColumnType("int").IsOptional();
            Property(x => x.dsProdutoCongressoPagote).HasColumnName(@"dsProdutoCongressoPagote").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.nuSubProduto).HasColumnName(@"nuSubProduto").HasColumnType("int").IsOptional();
            Property(x => x.nuIdGrupo).HasColumnName(@"nuIdGrupo").HasColumnType("int").IsOptional();
            Property(x => x.nuQtdColunaSubProduto).HasColumnName(@"nuQtdColunaSubProduto").HasColumnType("int").IsOptional();
            Property(x => x.dsQtdCargaHoraria).HasColumnName(@"dsQtdCargaHoraria").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.dtPalestra).HasColumnName(@"dtPalestra").HasColumnType("datetime").IsOptional();
            Property(x => x.dsLocalizacao).HasColumnName(@"dsLocalizacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtPalestraFim).HasColumnName(@"dtPalestraFim").HasColumnType("datetime").IsOptional();
            Property(x => x.qtdImpressao).HasColumnName(@"qtdImpressao").HasColumnType("int").IsOptional();
            Property(x => x.dsSala).HasColumnName(@"dsSala").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsCompraObrigatoria).HasColumnName(@"dsCompraObrigatoria").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsCC).HasColumnName(@"dsCC").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsCR).HasColumnName(@"dsCR").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsCO).HasColumnName(@"dsCO").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
        }
    }

    // TB_DD_ProdutoExpositores
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ProdutoExpositoresConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ProdutoExpositores>
    {
        public TB_DD_ProdutoExpositoresConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ProdutoExpositoresConfiguration(string schema)
        {
            ToTable("TB_DD_ProdutoExpositores", schema);
            HasKey(x => x.cdProdutoExpositores);

            Property(x => x.cdProdutoExpositores).HasColumnName(@"cdProdutoExpositores").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsProdutoExpositorPortugues).HasColumnName(@"dsProdutoExpositorPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsProdutoExpositorIngles).HasColumnName(@"dsProdutoExpositorIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsRequired();
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_ProdutoExpositores).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_ProdutoExpositores_TB_CF_Edicao
        }
    }

    // TB_DD_ProdutoOperacional
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ProdutoOperacionalConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ProdutoOperacional>
    {
        public TB_DD_ProdutoOperacionalConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ProdutoOperacionalConfiguration(string schema)
        {
            ToTable("TB_DD_ProdutoOperacional", schema);
            HasKey(x => x.cdProduto);

            Property(x => x.cdProduto).HasColumnName(@"cdProduto").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.nuOrdem).HasColumnName(@"nuOrdem").HasColumnType("int").IsOptional();
            Property(x => x.dsProduto).HasColumnName(@"dsProduto").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsProdutoIngles).HasColumnName(@"dsProdutoIngles").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.SiglaImagem).HasColumnName(@"SiglaImagem").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.cdFormulario).HasColumnName(@"cdFormulario").HasColumnType("int").IsOptional();
            Property(x => x.cdCategoria).HasColumnName(@"cdCategoria").HasColumnType("int").IsOptional();
            Property(x => x.dsTipoRegra).HasColumnName(@"dsTipoRegra").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.dsQuantidade).HasColumnName(@"dsQuantidade").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.vlAdicional).HasColumnName(@"vlAdicional").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsFecharPedidoAutomatico).HasColumnName(@"dsFecharPedidoAutomatico").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsIncideImposto).HasColumnName(@"dsIncideImposto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsCC).HasColumnName(@"dsCC").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsCR).HasColumnName(@"dsCR").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsCO).HasColumnName(@"dsCO").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);

            // Foreign keys
            HasOptional(a => a.TB_RF_Categoria).WithMany(b => b.TB_DD_ProdutoOperacional).HasForeignKey(c => c.cdCategoria).WillCascadeOnDelete(false); // FK_TB_DD_ProdutoOperacional_TB_RF_Categoria
            HasRequired(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_ProdutoOperacional).HasForeignKey(c => c.TB_Edicao_cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_ProdutoOperacional_TB_CF_Edicao
        }
    }

    // TB_DD_Questionario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_QuestionarioConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Questionario>
    {
        public TB_DD_QuestionarioConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_QuestionarioConfiguration(string schema)
        {
            ToTable("TB_DD_Questionario", schema);
            HasKey(x => x.cdQuestionario);

            Property(x => x.cdQuestionario).HasColumnName(@"cdQuestionario").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.nuPergunta).HasColumnName(@"nuPergunta").HasColumnType("int").IsOptional();
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.TB_Pergunta_cdPergunta).HasColumnName(@"TB_Pergunta_cdPergunta").HasColumnType("int").IsRequired();
            Property(x => x.dsTipoAlternativa).HasColumnName(@"dsTipoAlternativa").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsObrigatoria).HasColumnName(@"dsObrigatoria").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.TB_TipoCredenciamento_cdTipo).HasColumnName(@"TB_TipoCredenciamento_cdTipo").HasColumnType("int").IsOptional();
            Property(x => x.cdSistema).HasColumnName(@"cdSistema").HasColumnType("int").IsRequired();
            Property(x => x.fgSituacao).HasColumnName(@"fgSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtCancelado).HasColumnName(@"dtCancelado").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioCancelado).HasColumnName(@"cdUsuarioCancelado").HasColumnType("int").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_DD_Pergunta).WithMany(b => b.TB_DD_Questionario).HasForeignKey(c => c.TB_Pergunta_cdPergunta).WillCascadeOnDelete(false); // FK_TB_DD_Questionario_TB_DD_Pergunta
        }
    }

    // TB_DD_QuestionarioManual
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_QuestionarioManualConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_QuestionarioManual>
    {
        public TB_DD_QuestionarioManualConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_QuestionarioManualConfiguration(string schema)
        {
            ToTable("TB_DD_QuestionarioManual", schema);
            HasKey(x => x.cdQuestionarioManual);

            Property(x => x.cdQuestionarioManual).HasColumnName(@"cdQuestionarioManual").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdProdutoOperacional).HasColumnName(@"cdProdutoOperacional").HasColumnType("int").IsRequired();
            Property(x => x.nuPergunta).HasColumnName(@"nuPergunta").HasColumnType("int").IsRequired();
            Property(x => x.dsPortugues).HasColumnName(@"dsPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsIngles).HasColumnName(@"dsIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsEspanhol).HasColumnName(@"dsEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsTipoAlternativa).HasColumnName(@"dsTipoAlternativa").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsObrigatoria).HasColumnName(@"dsObrigatoria").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsRequired();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_DD_ProdutoOperacional).WithMany(b => b.TB_DD_QuestionarioManual).HasForeignKey(c => c.cdProdutoOperacional).WillCascadeOnDelete(false); // FK_TB_DD_QuestionarioManual_TB_DD_ProdutoOperacional
        }
    }

    // TB_DD_ReenvioConvite
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ReenvioConviteConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ReenvioConvite>
    {
        public TB_DD_ReenvioConviteConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ReenvioConviteConfiguration(string schema)
        {
            ToTable("TB_DD_ReenvioConvite", schema);
            HasKey(x => x.cdReenvio);

            Property(x => x.cdReenvio).HasColumnName(@"cdReenvio").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.IdEmpresa).HasColumnName(@"IdEmpresa").HasColumnType("int").IsRequired();
            Property(x => x.Permissao).HasColumnName(@"Permissao").HasColumnType("datetime").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_DD_Empresa).WithMany(b => b.TB_DD_ReenvioConvite).HasForeignKey(c => c.IdEmpresa).WillCascadeOnDelete(false); // FK_TB_DD_ReenvioConvite_TB_DD_Empresa
        }
    }

    // TB_DD_ReenvioConviteVip
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ReenvioConviteVipConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ReenvioConviteVip>
    {
        public TB_DD_ReenvioConviteVipConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ReenvioConviteVipConfiguration(string schema)
        {
            ToTable("TB_DD_ReenvioConviteVip", schema);
            HasKey(x => x.cdReenvio);

            Property(x => x.cdReenvio).HasColumnName(@"cdReenvio").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.IdEmpresa).HasColumnName(@"IdEmpresa").HasColumnType("int").IsOptional();
            Property(x => x.Permissao).HasColumnName(@"Permissao").HasColumnType("datetime").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_DD_Empresa).WithMany(b => b.TB_DD_ReenvioConviteVip).HasForeignKey(c => c.IdEmpresa).WillCascadeOnDelete(false); // FK_TB_DD_ReenvioConviteVip_TB_DD_Empresa
        }
    }

    // TB_DD_Registro
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_RegistroConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Registro>
    {
        public TB_DD_RegistroConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_RegistroConfiguration(string schema)
        {
            ToTable("TB_DD_Registro", schema);
            HasKey(x => x.cdRegistro);

            Property(x => x.cdRegistro).HasColumnName(@"cdRegistro").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsCodigoBarras).HasColumnName(@"dsCodigoBarras").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtInclusaoRegistro).HasColumnName(@"dtInclusaoRegistro").HasColumnType("datetime").IsOptional();
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.TB_Categoria_cdCategoria).HasColumnName(@"TB_Categoria_cdCategoria").HasColumnType("int").IsRequired();
            Property(x => x.TB_Pessoa_cdPessoa).HasColumnName(@"TB_Pessoa_cdPessoa").HasColumnType("int").IsRequired();
            Property(x => x.TB_Empresa_cdEmpresa).HasColumnName(@"TB_Empresa_cdEmpresa").HasColumnType("int").IsRequired();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsPresenteEvento).HasColumnName(@"dsPresenteEvento").HasColumnType("char").IsOptional().IsFixedLength().IsUnicode(false).HasMaxLength(10);
            Property(x => x.dsPresenteDia1).HasColumnName(@"dsPresenteDia1").HasColumnType("char").IsOptional().IsFixedLength().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsPresenteDia2).HasColumnName(@"dsPresenteDia2").HasColumnType("char").IsOptional().IsFixedLength().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsPresenteDia3).HasColumnName(@"dsPresenteDia3").HasColumnType("char").IsOptional().IsFixedLength().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsPresenteDia4).HasColumnName(@"dsPresenteDia4").HasColumnType("char").IsOptional().IsFixedLength().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsPresenteDia5).HasColumnName(@"dsPresenteDia5").HasColumnType("char").IsOptional().IsFixedLength().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsPresenteDia6).HasColumnName(@"dsPresenteDia6").HasColumnType("char").IsOptional().IsFixedLength().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsPresenteDia7).HasColumnName(@"dsPresenteDia7").HasColumnType("char").IsOptional().IsFixedLength().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsPresenteDia8).HasColumnName(@"dsPresenteDia8").HasColumnType("char").IsOptional().IsFixedLength().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsPresenteDia9).HasColumnName(@"dsPresenteDia9").HasColumnType("char").IsOptional().IsFixedLength().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsPresenteDia10).HasColumnName(@"dsPresenteDia10").HasColumnType("char").IsOptional().IsFixedLength().IsUnicode(false).HasMaxLength(1);
            Property(x => x.TB_Pedido_cdPedido).HasColumnName(@"TB_Pedido_cdPedido").HasColumnType("int").IsOptional();
            Property(x => x.dsOrigemConvite).HasColumnName(@"dsOrigemConvite").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.CdEmpresaExpositora).HasColumnName(@"CdEmpresaExpositora").HasColumnType("int").IsOptional();
            Property(x => x.dsOrigemInsercao).HasColumnName(@"dsOrigemInsercao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdSubCategoria).HasColumnName(@"cdSubCategoria").HasColumnType("int").IsOptional();
            Property(x => x.dsSubCategoriaValidacao).HasColumnName(@"dsSubCategoriaValidacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.cdSistema).HasColumnName(@"cdSistema").HasColumnType("int").IsOptional();
            Property(x => x.cdFormularioEasy).HasColumnName(@"cdFormularioEasy").HasColumnType("int").IsOptional();
            Property(x => x.dsSituacaoRegistro).HasColumnName(@"dsSituacaoRegistro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(15);
            Property(x => x.dtCancelaRegistro).HasColumnName(@"dtCancelaRegistro").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioCancelaRegistro).HasColumnName(@"cdUsuarioCancelaRegistro").HasColumnType("int").IsOptional();
            Property(x => x.fgEmitidoCertificado).HasColumnName(@"fgEmitidoCertificado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dtEmissaoCertificado).HasColumnName(@"dtEmissaoCertificado").HasColumnType("datetime").IsOptional();
            Property(x => x.cdConviteAmigo).HasColumnName(@"cdConviteAmigo").HasColumnType("int").IsOptional();
            Property(x => x.dsTipoConvite).HasColumnName(@"dsTipoConvite").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsIdioma).HasColumnName(@"dsIdioma").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsTipoInscricao).HasColumnName(@"dsTipoInscricao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);

            // Foreign keys
            HasRequired(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_Registro).HasForeignKey(c => c.TB_Edicao_cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_Registro_TB_CF_Edicao
            HasRequired(a => a.TB_DD_Empresa).WithMany(b => b.TB_DD_Registro).HasForeignKey(c => c.TB_Empresa_cdEmpresa).WillCascadeOnDelete(false); // FK_TB_DD_Registro_TB_DD_Empresa
            HasRequired(a => a.TB_DD_Pessoa).WithMany(b => b.TB_DD_Registro).HasForeignKey(c => c.TB_Pessoa_cdPessoa).WillCascadeOnDelete(false); // FK_TB_DD_Registro_TB_DD_Pessoa
            HasRequired(a => a.TB_RF_Categoria).WithMany(b => b.TB_DD_Registro).HasForeignKey(c => c.TB_Categoria_cdCategoria).WillCascadeOnDelete(false); // FK_TB_DD_Registro_TB_RF_Categoria
        }
    }

    // TB_DD_Regulamento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_RegulamentoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Regulamento>
    {
        public TB_DD_RegulamentoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_RegulamentoConfiguration(string schema)
        {
            ToTable("TB_DD_Regulamento", schema);
            HasKey(x => x.cdRegulamento);

            Property(x => x.cdRegulamento).HasColumnName(@"cdRegulamento").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.DataInclusao).HasColumnName(@"DataInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.UsuarioInsercao).HasColumnName(@"UsuarioInsercao").HasColumnType("int").IsOptional();
            Property(x => x.DataCancelamento).HasColumnName(@"DataCancelamento").HasColumnType("datetime").IsOptional();
            Property(x => x.UsuarioCancelamento).HasColumnName(@"UsuarioCancelamento").HasColumnType("int").IsOptional();
            Property(x => x.UrlArquivo).HasColumnName(@"UrlArquivo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsArquivo).HasColumnName(@"dsArquivo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsIdioma).HasColumnName(@"dsIdioma").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(20);

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_Regulamento).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_Regulamento_TB_CF_Edicao
        }
    }

    // TB_DD_Relatorio
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_RelatorioConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Relatorio>
    {
        public TB_DD_RelatorioConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_RelatorioConfiguration(string schema)
        {
            ToTable("TB_DD_Relatorio", schema);
            HasKey(x => x.cdRelatorio);

            Property(x => x.cdRelatorio).HasColumnName(@"cdRelatorio").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdTipoRelatorio).HasColumnName(@"cdTipoRelatorio").HasColumnType("int").IsOptional();
            Property(x => x.dsRelatorio).HasColumnName(@"dsRelatorio").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsPaginaParametros).HasColumnName(@"dsPaginaParametros").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsHabilitado).HasColumnName(@"dsHabilitado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.nuOrdem).HasColumnName(@"nuOrdem").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_Relatorio).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_Relatorio_TB_CF_Edicao
            HasOptional(a => a.TB_RF_TipoRelatorio).WithMany(b => b.TB_DD_Relatorio).HasForeignKey(c => c.cdTipoRelatorio).WillCascadeOnDelete(false); // FK_TB_DD_Relatorio_TB_RF_TipoRelatorio
        }
    }

    // TB_DD_ResponsavelCongresso
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ResponsavelCongressoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ResponsavelCongresso>
    {
        public TB_DD_ResponsavelCongressoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ResponsavelCongressoConfiguration(string schema)
        {
            ToTable("TB_DD_ResponsavelCongresso", schema);
            HasKey(x => x.cdResponsavelCongresso);

            Property(x => x.cdResponsavelCongresso).HasColumnName(@"cdResponsavelCongresso").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsRequired();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.dsNomeResponsavel).HasColumnName(@"dsNomeResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsSobreNomeResponsavel).HasColumnName(@"dsSobreNomeResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsCargoResponsavel).HasColumnName(@"dsCargoResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsDepartamentoResponsavel).HasColumnName(@"dsDepartamentoResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsEmailLoginResponsavel).HasColumnName(@"dsEmailLoginResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsSenhaResponsavel).HasColumnName(@"dsSenhaResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsDDITelefoneResponsavel).HasColumnName(@"dsDDITelefoneResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsDDDTelefoneResponsavel).HasColumnName(@"dsDDDTelefoneResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsNumeroTelefoneResponsavel).HasColumnName(@"dsNumeroTelefoneResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsRamalTelefoneResponsavel).HasColumnName(@"dsRamalTelefoneResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsDDICelularResponsavel).HasColumnName(@"dsDDICelularResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsDDDCelularResponsavel).HasColumnName(@"dsDDDCelularResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsNumeroCelularResponsavel).HasColumnName(@"dsNumeroCelularResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsRazaoSocialResponsavel).HasColumnName(@"dsRazaoSocialResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsNomeFantasiaResponsavel).HasColumnName(@"dsNomeFantasiaResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsLogradouroResponsavel).HasColumnName(@"dsLogradouroResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsComplementoLogradouroResponsavel).HasColumnName(@"dsComplementoLogradouroResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsNumeroLogradouroResponsavel).HasColumnName(@"dsNumeroLogradouroResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsCepLogradouroResponsavel).HasColumnName(@"dsCepLogradouroResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsBairroLogradouroResponsavel).HasColumnName(@"dsBairroLogradouroResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsCidadeLogradouroResponsavel).HasColumnName(@"dsCidadeLogradouroResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsUfLogradouroResponsavel).HasColumnName(@"dsUfLogradouroResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.TB_Pais_cdPaisResponsavel).HasColumnName(@"TB_Pais_cdPaisResponsavel").HasColumnType("int").IsRequired();
            Property(x => x.dsRes1Responsavel).HasColumnName(@"dsRes1Responsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsRes2Responsavel).HasColumnName(@"dsRes2Responsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsRes3Responsavel).HasColumnName(@"dsRes3Responsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsRes4Responsavel).HasColumnName(@"dsRes4Responsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsRes5Responsavel).HasColumnName(@"dsRes5Responsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsAutorizaEnvioEmailPessoal).HasColumnName(@"dsAutorizaEnvioEmailPessoal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsSmsCelularPessoal).HasColumnName(@"dsSmsCelularPessoal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsEmailLoginResponsavelAutenticacao).HasColumnName(@"dsEmailLoginResponsavelAutenticacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
        }
    }

    // TB_DD_ResponsavelEmpresa
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ResponsavelEmpresaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ResponsavelEmpresa>
    {
        public TB_DD_ResponsavelEmpresaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ResponsavelEmpresaConfiguration(string schema)
        {
            ToTable("TB_DD_ResponsavelEmpresa", schema);
            HasKey(x => x.cdResponsavel);

            Property(x => x.cdResponsavel).HasColumnName(@"cdResponsavel").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Empresa_cdEmpresa).HasColumnName(@"TB_Empresa_cdEmpresa").HasColumnType("int").IsRequired();
            Property(x => x.dsCpf).HasColumnName(@"dsCpf").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsRg).HasColumnName(@"dsRg").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.nmResponsavel).HasColumnName(@"nmResponsavel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCargo).HasColumnName(@"dsCargo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsEmail).HasColumnName(@"dsEmail").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsDdiTel).HasColumnName(@"dsDdiTel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsDddTel).HasColumnName(@"dsDddTel").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsTelefone).HasColumnName(@"dsTelefone").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsDddiFax).HasColumnName(@"dsDddiFax").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsDddFax).HasColumnName(@"dsDddFax").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsFax).HasColumnName(@"dsFax").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.nmEmpresa).HasColumnName(@"nmEmpresa").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.nmEmpresaCracha).HasColumnName(@"nmEmpresaCracha").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsRamoAtividade).HasColumnName(@"dsRamoAtividade").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsAreaAtuacao).HasColumnName(@"dsAreaAtuacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCepLogradouro).HasColumnName(@"dsCepLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsLogradouro).HasColumnName(@"dsLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsNumeroLogradouro).HasColumnName(@"dsNumeroLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsComplementoLogradouro).HasColumnName(@"dsComplementoLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsBairroLogradouro).HasColumnName(@"dsBairroLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCidadeLogradouro).HasColumnName(@"dsCidadeLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsUfLogradouro).HasColumnName(@"dsUfLogradouro").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsUsuario).HasColumnName(@"dsUsuario").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsSenha).HasColumnName(@"dsSenha").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsCancelado).HasColumnName(@"dsCancelado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
        }
    }

    // TB_DD_Resposta
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_RespostaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Resposta>
    {
        public TB_DD_RespostaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_RespostaConfiguration(string schema)
        {
            ToTable("TB_DD_Resposta", schema);
            HasKey(x => x.cdResposta);

            Property(x => x.cdQuestionario).HasColumnName(@"cdQuestionario").HasColumnType("int").IsRequired();
            Property(x => x.TB_Registro_dsCodigoBarras).HasColumnName(@"TB_Registro_dsCodigoBarras").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.TB_Alternativa_cdAlternativa).HasColumnName(@"TB_Alternativa_cdAlternativa").HasColumnType("int").IsRequired();
            Property(x => x.dsOutros).HasColumnName(@"dsOutros").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.cdResposta).HasColumnName(@"cdResposta").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            // Foreign keys
            HasRequired(a => a.TB_DD_Alternativa).WithMany(b => b.TB_DD_Resposta).HasForeignKey(c => c.TB_Alternativa_cdAlternativa).WillCascadeOnDelete(false); // FK_TB_DD_Resposta_TB_DD_Alternativa
            HasRequired(a => a.TB_DD_Questionario).WithMany(b => b.TB_DD_Resposta).HasForeignKey(c => c.cdQuestionario).WillCascadeOnDelete(false); // FK_TB_DD_Resposta_TB_DD_Questionario
        }
    }

    // TB_DD_RespostaQuestionarioManual
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_RespostaQuestionarioManualConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_RespostaQuestionarioManual>
    {
        public TB_DD_RespostaQuestionarioManualConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_RespostaQuestionarioManualConfiguration(string schema)
        {
            ToTable("TB_DD_RespostaQuestionarioManual", schema);
            HasKey(x => x.cdRespostaQuestionarioManual);

            Property(x => x.cdRespostaQuestionarioManual).HasColumnName(@"cdRespostaQuestionarioManual").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdItenPedidoOperacional).HasColumnName(@"cdItenPedidoOperacional").HasColumnType("int").IsRequired();
            Property(x => x.cdQuestionarioManual).HasColumnName(@"cdQuestionarioManual").HasColumnType("int").IsRequired();
            Property(x => x.cdAlternativaQuestionarioManual).HasColumnName(@"cdAlternativaQuestionarioManual").HasColumnType("int").IsRequired();
            Property(x => x.cdSubAlternativaQuestionarioManual).HasColumnName(@"cdSubAlternativaQuestionarioManual").HasColumnType("int").IsOptional();
            Property(x => x.dsOutros).HasColumnName(@"dsOutros").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_DD_AlternativaQuestionarioManual).WithMany(b => b.TB_DD_RespostaQuestionarioManual).HasForeignKey(c => c.cdAlternativaQuestionarioManual).WillCascadeOnDelete(false); // FK_TB_DD_RespostaQuestionarioManual_TB_DD_AlternativaQuestionarioManual
            HasRequired(a => a.TB_DD_ItensPedidoOperacional).WithMany(b => b.TB_DD_RespostaQuestionarioManual).HasForeignKey(c => c.cdItenPedidoOperacional).WillCascadeOnDelete(false); // FK_TB_DD_RespostaQuestionarioManual_TB_DD_ItensPedidoOperacional
            HasRequired(a => a.TB_DD_QuestionarioManual).WithMany(b => b.TB_DD_RespostaQuestionarioManual).HasForeignKey(c => c.cdQuestionarioManual).WillCascadeOnDelete(false); // FK_TB_DD_RespostaQuestionarioManual_TB_DD_QuestionarioManual
        }
    }

    // TB_DD_Simultaneidade
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_SimultaneidadeConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Simultaneidade>
    {
        public TB_DD_SimultaneidadeConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_SimultaneidadeConfiguration(string schema)
        {
            ToTable("TB_DD_Simultaneidade", schema);
            HasKey(x => x.cdSimultaneidade);

            Property(x => x.cdSimultaneidade).HasColumnName(@"cdSimultaneidade").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.cdProduto).HasColumnName(@"cdProduto").HasColumnType("int").IsRequired();
            Property(x => x.cdProduto_Restrito).HasColumnName(@"cdProduto_Restrito").HasColumnType("int").IsRequired();
            Property(x => x.fgSituacao).HasColumnName(@"fgSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.cdSistema).HasColumnName(@"cdSistema").HasColumnType("int").IsOptional();
        }
    }

    // TB_DD_SistemaHabilitadoToken
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_SistemaHabilitadoTokenConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_SistemaHabilitadoToken>
    {
        public TB_DD_SistemaHabilitadoTokenConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_SistemaHabilitadoTokenConfiguration(string schema)
        {
            ToTable("TB_DD_SistemaHabilitadoToken", schema);
            HasKey(x => x.cdSistemaHabilitado);

            Property(x => x.cdSistemaHabilitado).HasColumnName(@"cdSistemaHabilitado").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdToken).HasColumnName(@"cdToken").HasColumnType("int").IsOptional();
            Property(x => x.dsSistema).HasColumnName(@"dsSistema").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.flHabilitado).HasColumnName(@"flHabilitado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgPortugues).HasColumnName(@"fgPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgIngles).HasColumnName(@"fgIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsPadraoIdentificacaoSAC).HasColumnName(@"dsPadraoIdentificacaoSAC").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsHabilitarFoto).HasColumnName(@"dsHabilitarFoto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgEspanhol).HasColumnName(@"fgEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgValidaConviteExpositor).HasColumnName(@"fgValidaConviteExpositor").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);

            // Foreign keys
            HasOptional(a => a.TB_DD_Token).WithMany(b => b.TB_DD_SistemaHabilitadoToken).HasForeignKey(c => c.cdToken).WillCascadeOnDelete(false); // FK_TB_DD_SistemaHabilitadoToken
        }
    }

    // TB_DD_SiteColuna
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_SiteColunaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_SiteColuna>
    {
        public TB_DD_SiteColunaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_SiteColunaConfiguration(string schema)
        {
            ToTable("TB_DD_SiteColuna", schema);
            HasKey(x => x.cdSite);

            Property(x => x.cdSite).HasColumnName(@"cdSite").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Stand_cdStand).HasColumnName(@"TB_Stand_cdStand").HasColumnType("int").IsRequired();
            Property(x => x.dsSite).HasColumnName(@"dsSite").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dtInclusao).HasColumnName(@"dtInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.TB_Pedido_cdPedido).HasColumnName(@"TB_Pedido_cdPedido").HasColumnType("int").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_DD_Stand).WithMany(b => b.TB_DD_SiteColuna).HasForeignKey(c => c.TB_Stand_cdStand).WillCascadeOnDelete(false); // FK_TB_DD_SiteColuna_TB_DD_Stand
        }
    }

    // TB_DD_Stand
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_StandConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Stand>
    {
        public TB_DD_StandConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_StandConfiguration(string schema)
        {
            ToTable("TB_DD_Stand", schema);
            HasKey(x => x.cdStand);

            Property(x => x.cdStand).HasColumnName(@"cdStand").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.TB_Empresa_cdEmpresa).HasColumnName(@"TB_Empresa_cdEmpresa").HasColumnType("int").IsRequired();
            Property(x => x.cdTipoContrato).HasColumnName(@"cdTipoContrato").HasColumnType("int").IsOptional();
            Property(x => x.dsNumeroStand).HasColumnName(@"dsNumeroStand").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsEnderecoStand).HasColumnName(@"dsEnderecoStand").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsMetrosQuadrados).HasColumnName(@"dsMetrosQuadrados").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.qtCaracteresCatalogo).HasColumnName(@"qtCaracteresCatalogo").HasColumnType("int").IsOptional();
            Property(x => x.nmEvento).HasColumnName(@"nmEvento").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.qtdConvitesEletronico).HasColumnName(@"qtdConvitesEletronico").HasColumnType("int").IsOptional();
            Property(x => x.dsPavilhao).HasColumnName(@"dsPavilhao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsContratoLiberado).HasColumnName(@"dsContratoLiberado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_DD_Stand).HasForeignKey(c => c.TB_Edicao_cdEdicao).WillCascadeOnDelete(false); // FK_TB_DD_Stand_TB_CF_Edicao
            HasOptional(a => a.TB_RF_TipoContrato).WithMany(b => b.TB_DD_Stand).HasForeignKey(c => c.cdTipoContrato).WillCascadeOnDelete(false); // FK_TB_DD_Stand_TB_RF_TipoContrato
            HasRequired(a => a.TB_DD_Empresa).WithMany(b => b.TB_DD_Stand).HasForeignKey(c => c.TB_Empresa_cdEmpresa).WillCascadeOnDelete(false); // FK_TB_DD_Stand_TB_DD_Empresa
        }
    }

    // TB_DD_SubAlternativaQuestionarioManual
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_SubAlternativaQuestionarioManualConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_SubAlternativaQuestionarioManual>
    {
        public TB_DD_SubAlternativaQuestionarioManualConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_SubAlternativaQuestionarioManualConfiguration(string schema)
        {
            ToTable("TB_DD_SubAlternativaQuestionarioManual", schema);
            HasKey(x => x.cdSubAlternativaQuestionarioManual);

            Property(x => x.cdSubAlternativaQuestionarioManual).HasColumnName(@"cdSubAlternativaQuestionarioManual").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdAlternativaQuestionarioManual).HasColumnName(@"cdAlternativaQuestionarioManual").HasColumnType("int").IsRequired();
            Property(x => x.nuOrdem).HasColumnName(@"nuOrdem").HasColumnType("int").IsRequired();
            Property(x => x.dsPortugues).HasColumnName(@"dsPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsIngles).HasColumnName(@"dsIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsEspanhol).HasColumnName(@"dsEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsOutros).HasColumnName(@"dsOutros").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsRequired();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_DD_AlternativaQuestionarioManual).WithMany(b => b.TB_DD_SubAlternativaQuestionarioManual).HasForeignKey(c => c.cdAlternativaQuestionarioManual).WillCascadeOnDelete(false); // FK_TB_DD_SubAlternativaQuestionarioManual_TB_DD_AlternativaQuestionarioManual
        }
    }

    // TB_DD_SubCategoria
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_SubCategoriaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_SubCategoria>
    {
        public TB_DD_SubCategoriaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_SubCategoriaConfiguration(string schema)
        {
            ToTable("TB_DD_SubCategoria", schema);
            HasKey(x => x.cdSubCategoria);

            Property(x => x.cdSubCategoria).HasColumnName(@"cdSubCategoria").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdSistema).HasColumnName(@"cdSistema").HasColumnType("int").IsRequired();
            Property(x => x.nmSubCategoriaPortugues).HasColumnName(@"nmSubCategoriaPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsSubCategoriaPortugues).HasColumnName(@"dsSubCategoriaPortugues").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.nmSubCategoriaIngles).HasColumnName(@"nmSubCategoriaIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsSubCategoriaIngles).HasColumnName(@"dsSubCategoriaIngles").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.nmSubCategoriaEspanhol).HasColumnName(@"nmSubCategoriaEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.dsSubCategoriaEspanhol).HasColumnName(@"dsSubCategoriaEspanhol").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsRequired();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();
            Property(x => x.fgGrupo).HasColumnName(@"fgGrupo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgIndividual).HasColumnName(@"fgIndividual").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgHabilitaPalavra).HasColumnName(@"fgHabilitaPalavra").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.fgHabilitaValidacao).HasColumnName(@"fgHabilitaValidacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.nuOrdem).HasColumnName(@"nuOrdem").HasColumnType("int").IsOptional();
            Property(x => x.EventoGuid).HasColumnName(@"EventoGuid").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsSigla).HasColumnName(@"dsSigla").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
        }
    }

    // TB_DD_Ticket
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_TicketConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Ticket>
    {
        public TB_DD_TicketConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_TicketConfiguration(string schema)
        {
            ToTable("TB_DD_Ticket", schema);
            HasKey(x => x.cdTicket);

            Property(x => x.cdTicket).HasColumnName(@"cdTicket").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdPedido).HasColumnName(@"cdPedido").HasColumnType("int").IsRequired();
            Property(x => x.dsTicket).HasColumnName(@"dsTicket").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdProduto).HasColumnName(@"cdProduto").HasColumnType("int").IsRequired();
            Property(x => x.dsCodigoBarras).HasColumnName(@"dsCodigoBarras").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.dtUtilizacao).HasColumnName(@"dtUtilizacao").HasColumnType("datetime").IsOptional();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.dsChaveUtilizacaoLocal).HasColumnName(@"dsChaveUtilizacaoLocal").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);

            // Foreign keys
            HasRequired(a => a.TB_DD_Pedido).WithMany(b => b.TB_DD_Ticket).HasForeignKey(c => c.cdPedido).WillCascadeOnDelete(false); // FK_Ticket_Pedido
            HasRequired(a => a.TB_DD_ProdutoCongresso).WithMany(b => b.TB_DD_Ticket).HasForeignKey(c => c.cdProduto).WillCascadeOnDelete(false); // FK_Ticket_ProdutoCongresso
        }
    }

    // TB_DD_Token
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_TokenConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Token>
    {
        public TB_DD_TokenConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_TokenConfiguration(string schema)
        {
            ToTable("TB_DD_Token", schema);
            HasKey(x => x.cdToken);

            Property(x => x.cdToken).HasColumnName(@"cdToken").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.dsToken).HasColumnName(@"dsToken").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.flHabilitado).HasColumnName(@"flHabilitado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsLocalizacao).HasColumnName(@"dsLocalizacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
        }
    }

    // TB_DD_Usuario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_UsuarioConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_Usuario>
    {
        public TB_DD_UsuarioConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_UsuarioConfiguration(string schema)
        {
            ToTable("TB_DD_Usuario", schema);
            HasKey(x => x.cdUsuario);

            Property(x => x.cdUsuario).HasColumnName(@"cdUsuario").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.nmUsuario).HasColumnName(@"nmUsuario").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsSenha).HasColumnName(@"dsSenha").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsLogin).HasColumnName(@"dsLogin").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsTelefone).HasColumnName(@"dsTelefone").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(30);
            Property(x => x.dsEmail).HasColumnName(@"dsEmail").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.Status).HasColumnName(@"Status").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(30);
            Property(x => x.dsGrupo).HasColumnName(@"dsGrupo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
        }
    }

    // TB_DD_ValorContrato
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ValorContratoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ValorContrato>
    {
        public TB_DD_ValorContratoConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ValorContratoConfiguration(string schema)
        {
            ToTable("TB_DD_ValorContrato", schema);
            HasKey(x => x.cdValorContrato);

            Property(x => x.cdValorContrato).HasColumnName(@"cdValorContrato").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdStand).HasColumnName(@"cdStand").HasColumnType("int").IsRequired();
            Property(x => x.vlContrato).HasColumnName(@"vlContrato").HasColumnType("numeric").IsRequired().HasPrecision(18, 2);
            Property(x => x.vlArea).HasColumnName(@"vlArea").HasColumnType("numeric").IsRequired().HasPrecision(18, 2);
            Property(x => x.vlTipoMontagem).HasColumnName(@"vlTipoMontagem").HasColumnType("numeric").IsRequired().HasPrecision(18, 2);
            Property(x => x.qtdParcelas).HasColumnName(@"qtdParcelas").HasColumnType("int").IsRequired();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsRequired();
            Property(x => x.dtCancelado).HasColumnName(@"dtCancelado").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioCancelado).HasColumnName(@"cdUsuarioCancelado").HasColumnType("int").IsOptional();
            Property(x => x.vlSinal).HasColumnName(@"vlSinal").HasColumnType("numeric").IsOptional().HasPrecision(18, 2);
            Property(x => x.qtdParcelasMontagem).HasColumnName(@"qtdParcelasMontagem").HasColumnType("int").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_DD_Stand).WithMany(b => b.TB_DD_ValorContrato).HasForeignKey(c => c.cdStand).WillCascadeOnDelete(false); // FK_ValorContato_TB_DD_Stand
        }
    }

    // TB_DD_ValorContratoParcela
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_DD_ValorContratoParcelaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_DD_ValorContratoParcela>
    {
        public TB_DD_ValorContratoParcelaConfiguration()
            : this("dbo")
        {
        }

        public TB_DD_ValorContratoParcelaConfiguration(string schema)
        {
            ToTable("TB_DD_ValorContratoParcela", schema);
            HasKey(x => x.cdValorContratoParcela);

            Property(x => x.cdValorContratoParcela).HasColumnName(@"cdValorContratoParcela").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdValorContrato).HasColumnName(@"cdValorContrato").HasColumnType("uniqueidentifier").IsRequired();
            Property(x => x.vlParcela).HasColumnName(@"vlParcela").HasColumnType("numeric").IsRequired().HasPrecision(18, 2);
            Property(x => x.nuParcela).HasColumnName(@"nuParcela").HasColumnType("int").IsRequired();
            Property(x => x.dtVencimento).HasColumnName(@"dtVencimento").HasColumnType("datetime").IsRequired();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.tipoParcela).HasColumnName(@"tipoParcela").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);

            // Foreign keys
            HasRequired(a => a.TB_DD_ValorContrato).WithMany(b => b.TB_DD_ValorContratoParcela).HasForeignKey(c => c.cdValorContrato).WillCascadeOnDelete(false); // FK_ValorContratoParcela_ValorContrato
        }
    }

    // TB_LG_Acesso
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_AcessoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_LG_Acesso>
    {
        public TB_LG_AcessoConfiguration()
            : this("dbo")
        {
        }

        public TB_LG_AcessoConfiguration(string schema)
        {
            ToTable("TB_LG_Acesso", schema);
            HasKey(x => x.cdLogAcesso);

            Property(x => x.cdLogAcesso).HasColumnName(@"cdLogAcesso").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dtLog).HasColumnName(@"dtLog").HasColumnType("datetime").IsOptional();
            Property(x => x.cdEmpresa).HasColumnName(@"cdEmpresa").HasColumnType("int").IsOptional();
            Property(x => x.cdUsuario).HasColumnName(@"cdUsuario").HasColumnType("int").IsOptional();
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_LG_Acesso).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_LG_Acesso_cdEdicao
            HasOptional(a => a.TB_DD_Empresa).WithMany(b => b.TB_LG_Acesso).HasForeignKey(c => c.cdEmpresa).WillCascadeOnDelete(false); // FK_TB_LG_Acesso_cdEmpresa
        }
    }

    // TB_LG_AlteraSubCategoria
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_AlteraSubCategoriaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_LG_AlteraSubCategoria>
    {
        public TB_LG_AlteraSubCategoriaConfiguration()
            : this("dbo")
        {
        }

        public TB_LG_AlteraSubCategoriaConfiguration(string schema)
        {
            ToTable("TB_LG_AlteraSubCategoria", schema);
            HasKey(x => x.cdLogAlteracaoSubCategoria);

            Property(x => x.cdLogAlteracaoSubCategoria).HasColumnName(@"cdLogAlteracaoSubCategoria").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsCodigoBarras).HasColumnName(@"dsCodigoBarras").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdSubCategoria).HasColumnName(@"cdSubCategoria").HasColumnType("int").IsRequired();
            Property(x => x.dsPalavra).HasColumnName(@"dsPalavra").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsRequired();
        }
    }

    // TB_LG_CancelamentoAutomaticoPedido
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_CancelamentoAutomaticoPedidoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_LG_CancelamentoAutomaticoPedido>
    {
        public TB_LG_CancelamentoAutomaticoPedidoConfiguration()
            : this("dbo")
        {
        }

        public TB_LG_CancelamentoAutomaticoPedidoConfiguration(string schema)
        {
            ToTable("TB_LG_CancelamentoAutomaticoPedido", schema);
            HasKey(x => x.cdCancelaPedido);

            Property(x => x.cdCancelaPedido).HasColumnName(@"cdCancelaPedido").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdPedido).HasColumnName(@"cdPedido").HasColumnType("int").IsRequired();
            Property(x => x.dtCancelamento).HasColumnName(@"dtCancelamento").HasColumnType("datetime").IsRequired();
            Property(x => x.dsInformativo).HasColumnName(@"dsInformativo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
        }
    }

    // TB_LG_Erro
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_ErroConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_LG_Erro>
    {
        public TB_LG_ErroConfiguration()
            : this("dbo")
        {
        }

        public TB_LG_ErroConfiguration(string schema)
        {
            ToTable("TB_LG_Erro", schema);
            HasKey(x => x.cdLogErro);

            Property(x => x.cdLogErro).HasColumnName(@"cdLogErro").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdFormulario).HasColumnName(@"cdFormulario").HasColumnType("int").IsRequired();
            Property(x => x.dsLogErro).HasColumnName(@"dsLogErro").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dtLogErro).HasColumnName(@"dtLogErro").HasColumnType("datetime").IsRequired();
            Property(x => x.dsMetodo).HasColumnName(@"dsMetodo").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(50);

            // Foreign keys
            HasRequired(a => a.TB_CF_Formulario).WithMany(b => b.TB_LG_Erro).HasForeignKey(c => c.cdFormulario).WillCascadeOnDelete(false); // FK_TB_LG_Erro_TB_CF_Formulario
        }
    }

    // TB_LG_FormularioManualEletronico
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_FormularioManualEletronicoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_LG_FormularioManualEletronico>
    {
        public TB_LG_FormularioManualEletronicoConfiguration()
            : this("dbo")
        {
        }

        public TB_LG_FormularioManualEletronicoConfiguration(string schema)
        {
            ToTable("TB_LG_FormularioManualEletronico", schema);
            HasKey(x => x.cdPreenchimento);

            Property(x => x.cdPreenchimento).HasColumnName(@"cdPreenchimento").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dtAcesso).HasColumnName(@"dtAcesso").HasColumnType("datetime").IsRequired();
            Property(x => x.cdFormulario).HasColumnName(@"cdFormulario").HasColumnType("int").IsRequired();
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsRequired();
            Property(x => x.cdStand).HasColumnName(@"cdStand").HasColumnType("int").IsRequired();
            Property(x => x.cdResponsavel).HasColumnName(@"cdResponsavel").HasColumnType("int").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_CF_Edicao).WithMany(b => b.TB_LG_FormularioManualEletronico).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_LG_FormularioManualEletronico_TB_CF_Edicao
            HasRequired(a => a.TB_CF_FormularioManualEletronico).WithMany(b => b.TB_LG_FormularioManualEletronico).HasForeignKey(c => c.cdFormulario).WillCascadeOnDelete(false); // FK_TB_LG_FormularioManualEletronico_TB_CF_FormularioManualEletronico
            HasRequired(a => a.TB_DD_Stand).WithMany(b => b.TB_LG_FormularioManualEletronico).HasForeignKey(c => c.cdStand).WillCascadeOnDelete(false); // FK_TB_LG_FormularioManualEletronico_TB_DD_Stand
        }
    }

    // TB_LG_Geral
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_GeralConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_LG_Geral>
    {
        public TB_LG_GeralConfiguration()
            : this("dbo")
        {
        }

        public TB_LG_GeralConfiguration(string schema)
        {
            ToTable("TB_LG_Geral", schema);
            HasKey(x => x.cdLog);

            Property(x => x.cdLog).HasColumnName(@"cdLog").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsLog).HasColumnName(@"dsLog").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dtLog).HasColumnName(@"dtLog").HasColumnType("datetime").IsOptional();
            Property(x => x.cdFormulario).HasColumnName(@"cdFormulario").HasColumnType("int").IsRequired();
            Property(x => x.Origem).HasColumnName(@"Origem").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(800);
            Property(x => x.cdUsuario).HasColumnName(@"cdUsuario").HasColumnType("int").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_CF_Formulario).WithMany(b => b.TB_LG_Geral).HasForeignKey(c => c.cdFormulario).WillCascadeOnDelete(false); // FK_TB_LG_Geral_TB_CF_Formulario
        }
    }

    // TB_LG_GeralDepEasy
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_GeralDepEasyConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_LG_GeralDepEasy>
    {
        public TB_LG_GeralDepEasyConfiguration()
            : this("dbo")
        {
        }

        public TB_LG_GeralDepEasyConfiguration(string schema)
        {
            ToTable("TB_LG_GeralDepEasy", schema);
            HasKey(x => x.TB_LG_GeralUsuariocdLog);

            Property(x => x.TB_LG_GeralUsuariocdLog).HasColumnName(@"TB_LG_GeralUsuariocdLog").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.TB_LG_GeralRFAcao_cdAcao).HasColumnName(@"TB_LG_GeralRFAcao_cdAcao").HasColumnType("int").IsOptional();
            Property(x => x.dsAcao).HasColumnName(@"dsAcao").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);

            // Foreign keys
            HasOptional(a => a.TB_LG_RF_GeralAcao).WithMany(b => b.TB_LG_GeralDepEasy).HasForeignKey(c => c.TB_LG_GeralRFAcao_cdAcao).WillCascadeOnDelete(false); // FK_GeralDepEasy_cdAcao
            HasRequired(a => a.TB_LG_RF_GeralUsuario).WithOptional(b => b.TB_LG_GeralDepEasy).WillCascadeOnDelete(false); // FK_GeralDepEasy_UsuariocdLog
        }
    }

    // TB_LG_GeralDepFinanceiro
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_GeralDepFinanceiroConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_LG_GeralDepFinanceiro>
    {
        public TB_LG_GeralDepFinanceiroConfiguration()
            : this("dbo")
        {
        }

        public TB_LG_GeralDepFinanceiroConfiguration(string schema)
        {
            ToTable("TB_LG_GeralDepFinanceiro", schema);
            HasKey(x => x.TB_LG_GeralUsuariocdLog);

            Property(x => x.TB_LG_GeralUsuariocdLog).HasColumnName(@"TB_LG_GeralUsuariocdLog").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.TB_LG_GeralRFAcao_cdAcao).HasColumnName(@"TB_LG_GeralRFAcao_cdAcao").HasColumnType("int").IsOptional();
            Property(x => x.cdPedidoAfetado).HasColumnName(@"cdPedidoAfetado").HasColumnType("int").IsOptional();
            Property(x => x.dsAcao).HasColumnName(@"dsAcao").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);

            // Foreign keys
            HasOptional(a => a.TB_DD_Pedido).WithMany(b => b.TB_LG_GeralDepFinanceiro).HasForeignKey(c => c.cdPedidoAfetado).WillCascadeOnDelete(false); // FK_GeralDepFinanceiro_cdPedidoAfetado
            HasOptional(a => a.TB_LG_RF_GeralAcao).WithMany(b => b.TB_LG_GeralDepFinanceiro).HasForeignKey(c => c.TB_LG_GeralRFAcao_cdAcao).WillCascadeOnDelete(false); // FK_GeralDepFinanceiro_cdAcao
            HasRequired(a => a.TB_LG_RF_GeralUsuario).WithOptional(b => b.TB_LG_GeralDepFinanceiro).WillCascadeOnDelete(false); // FK_GeralDepFinanceiro_UsuariocdLog
        }
    }

    // TB_LG_GeralDepUsuario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_GeralDepUsuarioConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_LG_GeralDepUsuario>
    {
        public TB_LG_GeralDepUsuarioConfiguration()
            : this("dbo")
        {
        }

        public TB_LG_GeralDepUsuarioConfiguration(string schema)
        {
            ToTable("TB_LG_GeralDepUsuario", schema);
            HasKey(x => x.TB_LG_GeralUsuariocdLog);

            Property(x => x.TB_LG_GeralUsuariocdLog).HasColumnName(@"TB_LG_GeralUsuariocdLog").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.TB_LG_GeralRFAcao_cdAcao).HasColumnName(@"TB_LG_GeralRFAcao_cdAcao").HasColumnType("int").IsOptional();
            Property(x => x.cdUsuarioAfetado).HasColumnName(@"cdUsuarioAfetado").HasColumnType("int").IsOptional();
            Property(x => x.dsAcao).HasColumnName(@"dsAcao").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);

            // Foreign keys
            HasOptional(a => a.TB_LG_RF_GeralAcao).WithMany(b => b.TB_LG_GeralDepUsuario).HasForeignKey(c => c.TB_LG_GeralRFAcao_cdAcao).WillCascadeOnDelete(false); // FK_GeralDepUsuario_cdAcao
            HasRequired(a => a.TB_LG_RF_GeralUsuario).WithOptional(b => b.TB_LG_GeralDepUsuario).WillCascadeOnDelete(false); // FK_GeralDepUsuario_UsuariocdLog
        }
    }

    // TB_LG_PaypalPlus
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_PaypalPlusConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_LG_PaypalPlus>
    {
        public TB_LG_PaypalPlusConfiguration()
            : this("dbo")
        {
        }

        public TB_LG_PaypalPlusConfiguration(string schema)
        {
            ToTable("TB_LG_PaypalPlus", schema);
            HasKey(x => x.cdPaypalPlus);

            Property(x => x.cdPaypalPlus).HasColumnName(@"cdPaypalPlus").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.cdPedido).HasColumnName(@"cdPedido").HasColumnType("int").IsOptional();
            Property(x => x.dtErro).HasColumnName(@"dtErro").HasColumnType("datetime").IsOptional();
            Property(x => x.dsMensagem).HasColumnName(@"dsMensagem").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(2000);
        }
    }

    // TB_LG_RF_GeralAcao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_RF_GeralAcaoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_LG_RF_GeralAcao>
    {
        public TB_LG_RF_GeralAcaoConfiguration()
            : this("dbo")
        {
        }

        public TB_LG_RF_GeralAcaoConfiguration(string schema)
        {
            ToTable("TB_LG_RF_GeralAcao", schema);
            HasKey(x => x.cdAcao);

            Property(x => x.cdAcao).HasColumnName(@"cdAcao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.dsAcao).HasColumnName(@"dsAcao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
        }
    }

    // TB_LG_RF_GeralDepartamento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_RF_GeralDepartamentoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_LG_RF_GeralDepartamento>
    {
        public TB_LG_RF_GeralDepartamentoConfiguration()
            : this("dbo")
        {
        }

        public TB_LG_RF_GeralDepartamentoConfiguration(string schema)
        {
            ToTable("TB_LG_RF_GeralDepartamento", schema);
            HasKey(x => x.cdDepartamento);

            Property(x => x.cdDepartamento).HasColumnName(@"cdDepartamento").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.dsDepartamento).HasColumnName(@"dsDepartamento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(150);
        }
    }

    // TB_LG_RF_GeralUsuario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_LG_RF_GeralUsuarioConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_LG_RF_GeralUsuario>
    {
        public TB_LG_RF_GeralUsuarioConfiguration()
            : this("dbo")
        {
        }

        public TB_LG_RF_GeralUsuarioConfiguration(string schema)
        {
            ToTable("TB_LG_RF_GeralUsuario", schema);
            HasKey(x => x.cdLog);

            Property(x => x.cdLog).HasColumnName(@"cdLog").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.dtLog).HasColumnName(@"dtLog").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuario).HasColumnName(@"cdUsuario").HasColumnType("int").IsOptional();
            Property(x => x.cdDepartamento).HasColumnName(@"cdDepartamento").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_LG_RF_GeralDepartamento).WithMany(b => b.TB_LG_RF_GeralUsuario).HasForeignKey(c => c.cdDepartamento).WillCascadeOnDelete(false); // FK_GeralUsuario_cdDepartamento
        }
    }

    // TB_RF_AliquotaImposto
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_AliquotaImpostoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_AliquotaImposto>
    {
        public TB_RF_AliquotaImpostoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_AliquotaImpostoConfiguration(string schema)
        {
            ToTable("TB_RF_AliquotaImposto", schema);
            HasKey(x => x.cdAliquota);

            Property(x => x.cdAliquota).HasColumnName(@"cdAliquota").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.cdEmpresa).HasColumnName(@"cdEmpresa").HasColumnType("int").IsOptional();
            Property(x => x.dsImposto).HasColumnName(@"dsImposto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(20);
            Property(x => x.dsAliquota).HasColumnName(@"dsAliquota").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(40);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
            Property(x => x.cdConfiguracao).HasColumnName(@"cdConfiguracao").HasColumnType("int").IsOptional();
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsOptional();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_RF_AliquotaImposto).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_RF_AliquotaImposto_cdEdicao
            HasOptional(a => a.TB_DD_Empresa).WithMany(b => b.TB_RF_AliquotaImposto).HasForeignKey(c => c.cdEmpresa).WillCascadeOnDelete(false); // FK_TB_RF_AliquotaImposto_cdEmpresa
        }
    }

    // TB_RF_AlternativaPadrao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_AlternativaPadraoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_AlternativaPadrao>
    {
        public TB_RF_AlternativaPadraoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_AlternativaPadraoConfiguration(string schema)
        {
            ToTable("TB_RF_AlternativaPadrao", schema);
            HasKey(x => x.cdAlternativaPadrao);

            Property(x => x.cdAlternativaPadrao).HasColumnName(@"cdAlternativaPadrao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsPortugues).HasColumnName(@"dsPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsIngles).HasColumnName(@"dsIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsEspanhol).HasColumnName(@"dsEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
        }
    }

    // TB_RF_AreaAtuacao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_AreaAtuacaoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_AreaAtuacao>
    {
        public TB_RF_AreaAtuacaoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_AreaAtuacaoConfiguration(string schema)
        {
            ToTable("TB_RF_AreaAtuacao", schema);
            HasKey(x => x.cdAreaAtuacao);

            Property(x => x.cdAreaAtuacao).HasColumnName(@"cdAreaAtuacao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsAreaAtuacaoPortugues).HasColumnName(@"dsAreaAtuacaoPortugues").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsAreaAtuacaoIngles).HasColumnName(@"dsAreaAtuacaoIngles").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_RF_AreaAtuacao).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_RF_AreaAtuacao_cdEdicao
        }
    }

    // TB_RF_Atividades
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_AtividadesConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_Atividades>
    {
        public TB_RF_AtividadesConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_AtividadesConfiguration(string schema)
        {
            ToTable("TB_RF_Atividades", schema);
            HasKey(x => x.cdAtividade);

            Property(x => x.cdAtividade).HasColumnName(@"cdAtividade").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsAtividade).HasColumnName(@"dsAtividade").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsNivel).HasColumnName(@"dsNivel").HasColumnType("int").IsOptional();
            Property(x => x.TB_Edicao_cdEdicao).HasColumnName(@"TB_Edicao_cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.dsAtividadeIng).HasColumnName(@"dsAtividadeIng").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_RF_Atividades).HasForeignKey(c => c.TB_Edicao_cdEdicao).WillCascadeOnDelete(false); // FK_TB_RF_Atividades_TB_CF_Edicao
        }
    }

    // TB_RF_Categoria
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_CategoriaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_Categoria>
    {
        public TB_RF_CategoriaConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_CategoriaConfiguration(string schema)
        {
            ToTable("TB_RF_Categoria", schema);
            HasKey(x => x.cdCategoria);

            Property(x => x.cdCategoria).HasColumnName(@"cdCategoria").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.nmCategoria).HasColumnName(@"nmCategoria").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsSistema).HasColumnName(@"dsSistema").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
        }
    }

    // TB_RF_CnaeSebrae
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_CnaeSebraeConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_CnaeSebrae>
    {
        public TB_RF_CnaeSebraeConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_CnaeSebraeConfiguration(string schema)
        {
            ToTable("TB_RF_CnaeSebrae", schema);
            HasKey(x => x.cdCnaeSebrae);

            Property(x => x.cdCnaeSebrae).HasColumnName(@"cdCnaeSebrae").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsCodigo).HasColumnName(@"dsCodigo").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.DescCnaeFiscal).HasColumnName(@"DescCnaeFiscal").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(800);
            Property(x => x.codSetor).HasColumnName(@"codSetor").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
        }
    }

    // TB_RF_ConfImpostoRetencaoValorAcimaDe
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_ConfImpostoRetencaoValorAcimaDeConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_ConfImpostoRetencaoValorAcimaDe>
    {
        public TB_RF_ConfImpostoRetencaoValorAcimaDeConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_ConfImpostoRetencaoValorAcimaDeConfiguration(string schema)
        {
            ToTable("TB_RF_ConfImpostoRetencaoValorAcimaDe", schema);
            HasKey(x => x.cdConfImpostoRetencaoValorAcimaDe);

            Property(x => x.cdConfImpostoRetencaoValorAcimaDe).HasColumnName(@"cdConfImpostoRetencaoValorAcimaDe").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdConfiguracao).HasColumnName(@"cdConfiguracao").HasColumnType("int").IsRequired();
            Property(x => x.dsImposto).HasColumnName(@"dsImposto").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(20);
            Property(x => x.vlAcimaDe).HasColumnName(@"vlAcimaDe").HasColumnType("numeric").IsRequired().HasPrecision(18, 2);
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.dtCancelado).HasColumnName(@"dtCancelado").HasColumnType("datetime").IsOptional();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_RF_ConfImpostos).WithMany(b => b.TB_RF_ConfImpostoRetencaoValorAcimaDe).HasForeignKey(c => c.cdConfiguracao).WillCascadeOnDelete(false); // FK_ConfImpostoRetencaoValorAcimaDe_tb_rf_ConfImpostos
        }
    }

    // TB_RF_ConfImpostos
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_ConfImpostosConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_ConfImpostos>
    {
        public TB_RF_ConfImpostosConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_ConfImpostosConfiguration(string schema)
        {
            ToTable("TB_RF_ConfImpostos", schema);
            HasKey(x => x.cdConfiguracao);

            Property(x => x.cdConfiguracao).HasColumnName(@"cdConfiguracao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsTipo).HasColumnName(@"dsTipo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(30);
            Property(x => x.IRRF).HasColumnName(@"IRRF").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(20);
            Property(x => x.PCC).HasColumnName(@"PCC").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(20);
            Property(x => x.ISS).HasColumnName(@"ISS").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(20);
            Property(x => x.DtInclusao).HasColumnName(@"DtInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.Status).HasColumnName(@"Status").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(20);
        }
    }

    // TB_RF_Cor
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_CorConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_Cor>
    {
        public TB_RF_CorConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_CorConfiguration(string schema)
        {
            ToTable("TB_RF_Cor", schema);
            HasKey(x => x.cdCor);

            Property(x => x.cdCor).HasColumnName(@"cdCor").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdFormulario).HasColumnName(@"cdFormulario").HasColumnType("int").IsRequired();
            Property(x => x.dsTipo).HasColumnName(@"dsTipo").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(50);
            Property(x => x.dsCor).HasColumnName(@"dsCor").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(255);
            Property(x => x.fgSituacao).HasColumnName(@"fgSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(50);
            Property(x => x.dsCorIng).HasColumnName(@"dsCorIng").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);

            // Foreign keys
            HasRequired(a => a.TB_CF_FormularioManualEletronico).WithMany(b => b.TB_RF_Cor).HasForeignKey(c => c.cdFormulario).WillCascadeOnDelete(false); // FK_TB_RF_Cor_TB_CF_FormularioManualEletronico
        }
    }

    // TB_RF_Cotacao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_CotacaoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_Cotacao>
    {
        public TB_RF_CotacaoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_CotacaoConfiguration(string schema)
        {
            ToTable("TB_RF_Cotacao", schema);
            HasKey(x => x.cdCotacao);

            Property(x => x.cdCotacao).HasColumnName(@"cdCotacao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dtCotacao).HasColumnName(@"dtCotacao").HasColumnType("datetime").IsOptional();
            Property(x => x.dsMoeda).HasColumnName(@"dsMoeda").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(30);
            Property(x => x.vlCotacao).HasColumnName(@"vlCotacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(30);
            Property(x => x.cdUsuario).HasColumnName(@"cdUsuario").HasColumnType("int").IsOptional();
        }
    }

    // TB_RF_EventoStand
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_EventoStandConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_EventoStand>
    {
        public TB_RF_EventoStandConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_EventoStandConfiguration(string schema)
        {
            ToTable("TB_RF_EventoStand", schema);
            HasKey(x => x.cdEventoStand);

            Property(x => x.cdEventoStand).HasColumnName(@"cdEventoStand").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsEvento).HasColumnName(@"dsEvento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
        }
    }

    // TB_RF_ExclusaoEmpresa
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_ExclusaoEmpresaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_ExclusaoEmpresa>
    {
        public TB_RF_ExclusaoEmpresaConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_ExclusaoEmpresaConfiguration(string schema)
        {
            ToTable("TB_RF_ExclusaoEmpresa", schema);
            HasKey(x => x.cdExclusao);

            Property(x => x.cdExclusao).HasColumnName(@"cdExclusao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEmpresa).HasColumnName(@"cdEmpresa").HasColumnType("int").IsRequired();
            Property(x => x.dsRazaoSocial).HasColumnName(@"dsRazaoSocial").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.cdUsuarioExclusao).HasColumnName(@"cdUsuarioExclusao").HasColumnType("int").IsRequired();
            Property(x => x.dtExclusao).HasColumnName(@"dtExclusao").HasColumnType("datetime").IsRequired();
        }
    }

    // TB_RF_Faq
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_FaqConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_Faq>
    {
        public TB_RF_FaqConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_FaqConfiguration(string schema)
        {
            ToTable("TB_RF_Faq", schema);
            HasKey(x => x.cdFaq);

            Property(x => x.cdFaq).HasColumnName(@"cdFaq").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.DataInclusao).HasColumnName(@"DataInclusao").HasColumnType("datetime").IsOptional();
            Property(x => x.DataAlteracao).HasColumnName(@"DataAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.dsTitulo).HasColumnName(@"dsTitulo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.Status).HasColumnName(@"Status").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_RF_Faq).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_RF_Faq_cdEdicao
        }
    }

    // TB_RF_FormaPagamento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_FormaPagamentoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_FormaPagamento>
    {
        public TB_RF_FormaPagamentoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_FormaPagamentoConfiguration(string schema)
        {
            ToTable("TB_RF_FormaPagamento", schema);
            HasKey(x => x.cdPagamento);

            Property(x => x.cdPagamento).HasColumnName(@"cdPagamento").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsPagamento).HasColumnName(@"dsPagamento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsExibirCaixa).HasColumnName(@"dsExibirCaixa").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsSolicitarSenhaSuperVisor).HasColumnName(@"dsSolicitarSenhaSuperVisor").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
        }
    }

    // TB_RF_Generica
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_GenericaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_Generica>
    {
        public TB_RF_GenericaConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_GenericaConfiguration(string schema)
        {
            ToTable("TB_RF_Generica", schema);
            HasKey(x => x.cdReferencia);

            Property(x => x.cdReferencia).HasColumnName(@"cdReferencia").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.nmPort).HasColumnName(@"nmPort").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1000);
            Property(x => x.nmIngl).HasColumnName(@"nmIngl").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1000);
            Property(x => x.dsVinculo).HasColumnName(@"dsVinculo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1000);
            Property(x => x.fgAtivo).HasColumnName(@"fgAtivo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);
            Property(x => x.codigo).HasColumnName(@"codigo").HasColumnType("int").IsOptional();
        }
    }

    // TB_RF_GruposDadosCadastrais
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_GruposDadosCadastraisConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_GruposDadosCadastrais>
    {
        public TB_RF_GruposDadosCadastraisConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_GruposDadosCadastraisConfiguration(string schema)
        {
            ToTable("TB_RF_GruposDadosCadastrais", schema);
            HasKey(x => x.cdGruposDadosCadastrais);

            Property(x => x.cdGruposDadosCadastrais).HasColumnName(@"cdGruposDadosCadastrais").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.nmGruposDadosCadastrais).HasColumnName(@"nmGruposDadosCadastrais").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
        }
    }

    // TB_RF_Idioma
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_IdiomaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_Idioma>
    {
        public TB_RF_IdiomaConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_IdiomaConfiguration(string schema)
        {
            ToTable("TB_RF_Idioma", schema);
            HasKey(x => x.cdIdioma);

            Property(x => x.cdIdioma).HasColumnName(@"cdIdioma").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.nmIdioma).HasColumnName(@"nmIdioma").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
        }
    }

    // TB_RF_ImpostoPedido
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_ImpostoPedidoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_ImpostoPedido>
    {
        public TB_RF_ImpostoPedidoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_ImpostoPedidoConfiguration(string schema)
        {
            ToTable("TB_RF_ImpostoPedido", schema);
            HasKey(x => x.cdImposto);

            Property(x => x.cdImposto).HasColumnName(@"cdImposto").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdPedido).HasColumnName(@"cdPedido").HasColumnType("int").IsOptional();
            Property(x => x.dsImposto).HasColumnName(@"dsImposto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(20);
            Property(x => x.dsValor).HasColumnName(@"dsValor").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
            Property(x => x.cdPedidoBaseCalculo).HasColumnName(@"cdPedidoBaseCalculo").HasColumnType("int").IsOptional();
            Property(x => x.cdUsuario).HasColumnName(@"cdUsuario").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsBaseCalculo).HasColumnName(@"dsBaseCalculo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdAliquota).HasColumnName(@"cdAliquota").HasColumnType("int").IsOptional();
            Property(x => x.dtVencimento).HasColumnName(@"dtVencimento").HasColumnType("datetime").IsOptional();
            Property(x => x.cdEmpresa).HasColumnName(@"cdEmpresa").HasColumnType("int").IsOptional();
            Property(x => x.dtPedido).HasColumnName(@"dtPedido").HasColumnType("datetime").IsOptional();
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_DD_Pedido).WithMany(b => b.TB_RF_ImpostoPedido).HasForeignKey(c => c.cdPedido).WillCascadeOnDelete(false); // FK_TB_RF_ImpostoPedido_cdPedido
        }
    }

    // TB_RF_Leitura_Atividade
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_Leitura_AtividadeConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_Leitura_Atividade>
    {
        public TB_RF_Leitura_AtividadeConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_Leitura_AtividadeConfiguration(string schema)
        {
            ToTable("TB_RF_Leitura_Atividade", schema);
            HasKey(x => x.cdAtividade);

            Property(x => x.cdAtividade).HasColumnName(@"cdAtividade").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdLocal).HasColumnName(@"cdLocal").HasColumnType("int").IsRequired();
            Property(x => x.nmAtividade).HasColumnName(@"nmAtividade").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsMarcarPresencaDia).HasColumnName(@"dsMarcarPresencaDia").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(2);
            Property(x => x.dtInicioAtividade).HasColumnName(@"dtInicioAtividade").HasColumnType("datetime").IsOptional();
            Property(x => x.dtFimAtividade).HasColumnName(@"dtFimAtividade").HasColumnType("datetime").IsOptional();
            Property(x => x.dsProdutosAutorizados).HasColumnName(@"dsProdutosAutorizados").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.fgSituacao).HasColumnName(@"fgSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);

            // Foreign keys
            HasRequired(a => a.TB_RF_Leitura_Local).WithMany(b => b.TB_RF_Leitura_Atividade).HasForeignKey(c => c.cdLocal).WillCascadeOnDelete(false); // FK_TB_RF_Leitura_Atividade
        }
    }

    // TB_RF_Leitura_Local
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_Leitura_LocalConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_Leitura_Local>
    {
        public TB_RF_Leitura_LocalConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_Leitura_LocalConfiguration(string schema)
        {
            ToTable("TB_RF_Leitura_Local", schema);
            HasKey(x => x.cdLocal);

            Property(x => x.cdLocal).HasColumnName(@"cdLocal").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.nmLocal).HasColumnName(@"nmLocal").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.cdEdicao).HasColumnName(@"cdEdicao").HasColumnType("int").IsOptional();
            Property(x => x.fgSituacao).HasColumnName(@"fgSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(1);

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_RF_Leitura_Local).HasForeignKey(c => c.cdEdicao).WillCascadeOnDelete(false); // FK_TB_RF_Leitura_Local
        }
    }

    // TB_RF_Moeda
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_MoedaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_Moeda>
    {
        public TB_RF_MoedaConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_MoedaConfiguration(string schema)
        {
            ToTable("TB_RF_Moeda", schema);
            HasKey(x => x.cdMoeda);

            Property(x => x.cdMoeda).HasColumnName(@"cdMoeda").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.nmMoeda).HasColumnName(@"nmMoeda").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
        }
    }

    // TB_RF_PerguntaPadrao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_PerguntaPadraoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_PerguntaPadrao>
    {
        public TB_RF_PerguntaPadraoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_PerguntaPadraoConfiguration(string schema)
        {
            ToTable("TB_RF_PerguntaPadrao", schema);
            HasKey(x => x.cdPerguntaPadrao);

            Property(x => x.cdPerguntaPadrao).HasColumnName(@"cdPerguntaPadrao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsPortugues).HasColumnName(@"dsPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsIngles).HasColumnName(@"dsIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.dsEspanhol).HasColumnName(@"dsEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(255);
            Property(x => x.fgSituacao).HasColumnName(@"fgSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
        }
    }

    // TB_RF_PreenchimentoFormulario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_PreenchimentoFormularioConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_PreenchimentoFormulario>
    {
        public TB_RF_PreenchimentoFormularioConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_PreenchimentoFormularioConfiguration(string schema)
        {
            ToTable("TB_RF_PreenchimentoFormulario", schema);
            HasKey(x => x.cdPreenchimento);

            Property(x => x.cdPreenchimento).HasColumnName(@"cdPreenchimento").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.TB_Pedido_cdPedido).HasColumnName(@"TB_Pedido_cdPedido").HasColumnType("int").IsOptional();
            Property(x => x.TB_FormularioManualEletronico_cdFormulario).HasColumnName(@"TB_FormularioManualEletronico_cdFormulario").HasColumnType("int").IsOptional();
            Property(x => x.TB_FormularioManualEletronico_TB_Edicao_cdEdicao).HasColumnName(@"TB_FormularioManualEletronico_TB_Edicao_cdEdicao").HasColumnType("int").IsOptional();

            // Foreign keys
            HasOptional(a => a.TB_CF_Edicao).WithMany(b => b.TB_RF_PreenchimentoFormulario).HasForeignKey(c => c.TB_FormularioManualEletronico_TB_Edicao_cdEdicao).WillCascadeOnDelete(false); // FK_TB_RF_PreenchimentoFormulario_TB_CF_Edicao
            HasOptional(a => a.TB_CF_FormularioManualEletronico).WithMany(b => b.TB_RF_PreenchimentoFormulario).HasForeignKey(c => c.TB_FormularioManualEletronico_cdFormulario).WillCascadeOnDelete(false); // FK_TB_RF_PreenchimentoFormulario_TB_CF_FormularioManualEletronico
            HasOptional(a => a.TB_DD_Pedido).WithMany(b => b.TB_RF_PreenchimentoFormulario).HasForeignKey(c => c.TB_Pedido_cdPedido).WillCascadeOnDelete(false); // FK_TB_RF_PreenchimentoFormulario_TB_DD_Pedido
        }
    }

    // TB_RF_QuestionarioPadrao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_QuestionarioPadraoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_QuestionarioPadrao>
    {
        public TB_RF_QuestionarioPadraoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_QuestionarioPadraoConfiguration(string schema)
        {
            ToTable("TB_RF_QuestionarioPadrao", schema);
            HasKey(x => x.cdQuestionarioPadrao);

            Property(x => x.cdQuestionarioPadrao).HasColumnName(@"cdQuestionarioPadrao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.nuPergunta).HasColumnName(@"nuPergunta").HasColumnType("int").IsRequired();
            Property(x => x.cdPerguntaPadrao).HasColumnName(@"cdPerguntaPadrao").HasColumnType("int").IsRequired();
            Property(x => x.dsTipoAlternativa).HasColumnName(@"dsTipoAlternativa").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsObrigatoria).HasColumnName(@"dsObrigatoria").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdCategoriaSistema).HasColumnName(@"cdCategoriaSistema").HasColumnType("int").IsRequired();

            // Foreign keys
            HasRequired(a => a.TB_RF_PerguntaPadrao).WithMany(b => b.TB_RF_QuestionarioPadrao).HasForeignKey(c => c.cdPerguntaPadrao).WillCascadeOnDelete(false); // FK_QuestionarioPadrao_PerguntaPadrao
        }
    }

    // TB_RF_RamoAtividade
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_RamoAtividadeConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_RamoAtividade>
    {
        public TB_RF_RamoAtividadeConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_RamoAtividadeConfiguration(string schema)
        {
            ToTable("TB_RF_RamoAtividade", schema);
            HasKey(x => x.cdRamoAtividade);

            Property(x => x.cdRamoAtividade).HasColumnName(@"cdRamoAtividade").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.nmRamo).HasColumnName(@"nmRamo").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
        }
    }

    // TB_RF_RegraMarketingSimples
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_RegraMarketingSimplesConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_RegraMarketingSimples>
    {
        public TB_RF_RegraMarketingSimplesConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_RegraMarketingSimplesConfiguration(string schema)
        {
            ToTable("TB_RF_RegraMarketingSimples", schema);
            HasKey(x => x.cdRegraMarketingSimples);

            Property(x => x.cdRegraMarketingSimples).HasColumnName(@"cdRegraMarketingSimples").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsRegraMarketingSimplesPT).HasColumnName(@"dsRegraMarketingSimplesPT").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsRegraMarketingSimplesEN).HasColumnName(@"dsRegraMarketingSimplesEN").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsRegraMarketingSimplesES).HasColumnName(@"dsRegraMarketingSimplesES").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(500);
            Property(x => x.dsSigla).HasColumnName(@"dsSigla").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsRequired();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
        }
    }

    // TB_RF_Saudacao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_SaudacaoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_Saudacao>
    {
        public TB_RF_SaudacaoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_SaudacaoConfiguration(string schema)
        {
            ToTable("TB_RF_Saudacao", schema);
            HasKey(x => x.cdSaudacao);

            Property(x => x.cdSaudacao).HasColumnName(@"cdSaudacao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsPortugues).HasColumnName(@"dsPortugues").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
            Property(x => x.dsIngles).HasColumnName(@"dsIngles").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
            Property(x => x.dsEspanhol).HasColumnName(@"dsEspanhol").HasColumnType("nvarchar").IsOptional().HasMaxLength(50);
        }
    }

    // TB_RF_Sexo
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_SexoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_Sexo>
    {
        public TB_RF_SexoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_SexoConfiguration(string schema)
        {
            ToTable("TB_RF_Sexo", schema);
            HasKey(x => x.cdSexo);

            Property(x => x.cdSexo).HasColumnName(@"cdSexo").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsSigla).HasColumnName(@"dsSigla").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.dsPortugues).HasColumnName(@"dsPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsIngles).HasColumnName(@"dsIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dsEspanhol).HasColumnName(@"dsEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
        }
    }

    // TB_RF_SubCategoriaValidacao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_SubCategoriaValidacaoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_SubCategoriaValidacao>
    {
        public TB_RF_SubCategoriaValidacaoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_SubCategoriaValidacaoConfiguration(string schema)
        {
            ToTable("TB_RF_SubCategoriaValidacao", schema);
            HasKey(x => x.cdSubCategoriaValidacao);

            Property(x => x.cdSubCategoriaValidacao).HasColumnName(@"cdSubCategoriaValidacao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.cdSubCategoria).HasColumnName(@"cdSubCategoria").HasColumnType("int").IsRequired();
            Property(x => x.dsCodigoValidacao).HasColumnName(@"dsCodigoValidacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(800);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
            Property(x => x.cdUsuarioCadastro).HasColumnName(@"cdUsuarioCadastro").HasColumnType("int").IsRequired();
            Property(x => x.dtCadastro).HasColumnName(@"dtCadastro").HasColumnType("datetime").IsRequired();
            Property(x => x.cdUsuarioAlteracao).HasColumnName(@"cdUsuarioAlteracao").HasColumnType("int").IsOptional();
            Property(x => x.dtAlteracao).HasColumnName(@"dtAlteracao").HasColumnType("datetime").IsOptional();

            // Foreign keys
            HasRequired(a => a.TB_DD_SubCategoria).WithMany(b => b.TB_RF_SubCategoriaValidacao).HasForeignKey(c => c.cdSubCategoria).WillCascadeOnDelete(false); // FK_SubCategoriaValidacao
        }
    }

    // TB_RF_TipoCarta
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoCartaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_TipoCarta>
    {
        public TB_RF_TipoCartaConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_TipoCartaConfiguration(string schema)
        {
            ToTable("TB_RF_TipoCarta", schema);
            HasKey(x => x.cdTipoCarta);

            Property(x => x.cdTipoCarta).HasColumnName(@"cdTipoCarta").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.nmTipoCartaPortugues).HasColumnName(@"nmTipoCartaPortugues").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.nmTipoCartaIngles).HasColumnName(@"nmTipoCartaIngles").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.nmTipoCartaEspanhol).HasColumnName(@"nmTipoCartaEspanhol").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
        }
    }

    // TB_RF_TipoContrato
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoContratoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_TipoContrato>
    {
        public TB_RF_TipoContratoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_TipoContratoConfiguration(string schema)
        {
            ToTable("TB_RF_TipoContrato", schema);
            HasKey(x => x.cdTipoContrato);

            Property(x => x.cdTipoContrato).HasColumnName(@"cdTipoContrato").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsContrato).HasColumnName(@"dsContrato").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsSituacao).HasColumnName(@"dsSituacao").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsContratoIng).HasColumnName(@"dsContratoIng").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.dsDescricao).HasColumnName(@"dsDescricao").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.dsDescricaoIng).HasColumnName(@"dsDescricaoIng").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
        }
    }

    // TB_RF_TipoCredenciamento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoCredenciamentoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_TipoCredenciamento>
    {
        public TB_RF_TipoCredenciamentoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_TipoCredenciamentoConfiguration(string schema)
        {
            ToTable("TB_RF_TipoCredenciamento", schema);
            HasKey(x => x.cdTipo);

            Property(x => x.cdTipo).HasColumnName(@"cdTipo").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsTipo).HasColumnName(@"dsTipo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.dtInicial).HasColumnName(@"dtInicial").HasColumnType("datetime").IsOptional();
            Property(x => x.dtFinal).HasColumnName(@"dtFinal").HasColumnType("datetime").IsOptional();
            Property(x => x.dsUrlAplicacao).HasColumnName(@"dsUrlAplicacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
        }
    }

    // TB_RF_TipoDocumento
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoDocumentoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_TipoDocumento>
    {
        public TB_RF_TipoDocumentoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_TipoDocumentoConfiguration(string schema)
        {
            ToTable("TB_RF_TipoDocumento", schema);
            HasKey(x => x.cdTipoDocumento);

            Property(x => x.cdTipoDocumento).HasColumnName(@"cdTipoDocumento").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsDocumento).HasColumnName(@"dsDocumento").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
        }
    }

    // TB_RF_TipoEmpresa
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoEmpresaConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_TipoEmpresa>
    {
        public TB_RF_TipoEmpresaConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_TipoEmpresaConfiguration(string schema)
        {
            ToTable("TB_RF_TipoEmpresa", schema);
            HasKey(x => x.cdTipo);

            Property(x => x.cdTipo).HasColumnName(@"cdTipo").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsTipo).HasColumnName(@"dsTipo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
        }
    }

    // TB_RF_TipoFormulario
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoFormularioConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_TipoFormulario>
    {
        public TB_RF_TipoFormularioConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_TipoFormularioConfiguration(string schema)
        {
            ToTable("TB_RF_TipoFormulario", schema);
            HasKey(x => x.cdTipoFormulario);

            Property(x => x.cdTipoFormulario).HasColumnName(@"cdTipoFormulario").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsTipoFormulario).HasColumnName(@"dsTipoFormulario").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(200);
            Property(x => x.cdTipoFormularioCopia).HasColumnName(@"cdTipoFormularioCopia").HasColumnType("int").IsOptional();
        }
    }

    // TB_RF_TipoObjeto
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoObjetoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_TipoObjeto>
    {
        public TB_RF_TipoObjetoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_TipoObjetoConfiguration(string schema)
        {
            ToTable("TB_RF_TipoObjeto", schema);
            HasKey(x => x.cdTipoObjeto);

            Property(x => x.cdTipoObjeto).HasColumnName(@"cdTipoObjeto").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.nmTipoObjeto).HasColumnName(@"nmTipoObjeto").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
        }
    }

    // TB_RF_TipoPedido
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoPedidoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_TipoPedido>
    {
        public TB_RF_TipoPedidoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_TipoPedidoConfiguration(string schema)
        {
            ToTable("TB_RF_TipoPedido", schema);
            HasKey(x => x.cdTipoPedido);

            Property(x => x.cdTipoPedido).HasColumnName(@"cdTipoPedido").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsTipoPedido).HasColumnName(@"dsTipoPedido").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(100);
        }
    }

    // TB_RF_TipoRelatorio
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoRelatorioConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_TipoRelatorio>
    {
        public TB_RF_TipoRelatorioConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_TipoRelatorioConfiguration(string schema)
        {
            ToTable("TB_RF_TipoRelatorio", schema);
            HasKey(x => x.cdTipo);

            Property(x => x.cdTipo).HasColumnName(@"cdTipo").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.dsRelatorio).HasColumnName(@"dsRelatorio").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.fgHabilitado).HasColumnName(@"fgHabilitado").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
        }
    }

    // TB_RF_TipoValidacao
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public class TB_RF_TipoValidacaoConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TB_RF_TipoValidacao>
    {
        public TB_RF_TipoValidacaoConfiguration()
            : this("dbo")
        {
        }

        public TB_RF_TipoValidacaoConfiguration(string schema)
        {
            ToTable("TB_RF_TipoValidacao", schema);
            HasKey(x => x.cdTipoValidacao);

            Property(x => x.cdTipoValidacao).HasColumnName(@"cdTipoValidacao").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.nmTipoValidacao).HasColumnName(@"nmTipoValidacao").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
        }
    }

    #endregion
}
