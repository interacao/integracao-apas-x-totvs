﻿using System;
using System.Windows.Forms;

namespace apas_totvs_integration
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            var myController = new myController(this);
            button1.Enabled = false;
            await myController.Execute();
            button1.Enabled = true;
        }
    }
}
