﻿using EasyLibSys;
using IntegrationApasTotvs.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace apas_totvs_integration
{
    public class myController
    {
        private Form1 Form1View;

        DateTime _ultimaConsulta;
        DateTime _consultaAtual;
        List<ItemEnvio> _itemEnvioNew;
        List<ItemEnvio> _itemEnvioUpdated;
        ItemEnvioRepository itemEnvioRepository;

        public myController(Form1 form1)
        {
            Form1View = form1;

            //_ultimaConsulta = DateTime.Now;
            _ultimaConsulta = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 9, 0, 0);
            _consultaAtual = DateTime.Now;
            _itemEnvioNew = new List<ItemEnvio>();
            _itemEnvioUpdated = new List<ItemEnvio>();
            itemEnvioRepository = new ItemEnvioRepository();
        }

        public async Task Execute()
        {
            var cancellationToken = new CancellationToken();
            FWK_DAO_DD_ItensPedidoCongresso fwk_DAO_DD_ItensPedidoCongresso = new FWK_DAO_DD_ItensPedidoCongresso();
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    _itemEnvioNew.Clear();
                    _itemEnvioUpdated.Clear();

                    sendErro();

                    List<TB_DD_ItensPedidoCongresso> itensPedidoCongressoCollection = fwk_DAO_DD_ItensPedidoCongresso.MS_GetNewAndUpdated(_ultimaConsulta);
                    _consultaAtual = DateTime.Now;

                    foreach (TB_DD_ItensPedidoCongresso itemPedidoCongresso in itensPedidoCongressoCollection)
                    {
                        if (itemPedidoCongresso.dtInclusao.Value >= _ultimaConsulta)
                        {
                            var itemenvio = convertoToItemPedido(itemPedidoCongresso, "I");
                            if (verificarEnvio(itemenvio))
                                _itemEnvioNew.Add(itemenvio);
                        }
                        else
                        {
                            var itemenvio = convertoToItemPedido(itemPedidoCongresso, "E");
                            if (verificarEnvio(itemenvio))
                                _itemEnvioUpdated.Add(itemenvio);
                        }
                    }

                    if (_itemEnvioNew.Count > 0)
                        sendPedidoInsert(_itemEnvioNew);

                    if (_itemEnvioUpdated.Count > 0)
                        sendPedidoUpdate(_itemEnvioUpdated);

                    _consultaAtual.Add(new TimeSpan(-1, 0, 0));
                    _ultimaConsulta = _consultaAtual.Date;
                    await Task.Delay(TimeSpan.FromMinutes(5), cancellationToken);
                    //await Task.Delay(TimeSpan.FromSeconds(5), cancellationToken);
                }
                catch (Exception ex)
                {                    
                }                
            }
        }

        #region Internal Functions
        private ItemEnvio convertoToItemPedido(TB_DD_ItensPedidoCongresso itemCongresso, string operacao)
        {

            Random rnd = new Random();
            int ident = rnd.Next(1, 1000);
            var itemEnvio = new ItemEnvio();

            itemEnvio.acao = operacao;
            itemEnvio.data_venda = itemCongresso.dtItem.Value.ToString("yyyyMMdd");
            itemEnvio.ident_bancario = ident.ToString();
            itemEnvio.tipo = "CONVITE";
            itemEnvio.descricao_tipo = itemCongresso.TB_DD_ProdutoCongresso.nmProdutoPortugues + " - " + itemCongresso.TB_DD_Pedido.TB_DD_Empresa.dsRazaoSocial;
            itemEnvio.cc = itemCongresso.TB_DD_ProdutoCongresso.dsCC;
            itemEnvio.cr = itemCongresso.TB_DD_ProdutoCongresso.dsCR;
            itemEnvio.pr = "";
            itemEnvio.co = itemCongresso.TB_DD_ProdutoCongresso.dsCO;

            itemEnvio.cc = "85040";
            itemEnvio.cr = "063145";
            itemEnvio.pr = "104480";
            itemEnvio.co = "610173";

            itemEnvio.quantidade = 1;
            itemEnvio.valor_liquido = Convert.ToDecimal(itemCongresso.Valor_Liquido);
            itemEnvio.valor_bruto = Convert.ToDecimal(itemCongresso.Valor_Item) * itemEnvio.quantidade;
            itemEnvio.valor_desconto = Convert.ToDecimal(itemCongresso.Vlr_Desconto_Adm);
            itemEnvio.valor_acrescimo = 0;
            itemEnvio.valor_unitario = Convert.ToDecimal(itemCongresso.Valor_Item);
            itemEnvio.data_envio = DateTime.Now;
            itemEnvio.status_envio = 0;
            itemEnvio.pedido_origem = itemCongresso.TB_Pedido_cdPedido.ToString();
            itemEnvio.item_origem = itemCongresso.cdItem.ToString();


            var fwk_DAO_DD_Pagamento = new FWK_DAO_DD_Pagamento();
            var formapagamento = (fwk_DAO_DD_Pagamento.MS_ObterPorPedido(itemCongresso.TB_DD_Pedido.cdPedido)).TB_RF_FormaPagamento.dsPagamento;

            itemEnvio.tipo_pagamento = formapagamentoDic[formapagamento].ToString();
            itemEnvio.moeda = "1";

            return itemEnvio;
        }
        
        private void sendPedidoInsert(List<ItemEnvio> itensEnvio)
        {
            var json = JsonConvert.SerializeObject(new JsonSender(itensEnvio));
            var resp = sendMethod(json);
            if (!resp.Contains("errorMessages"))
            {
                var b = JsonConvert.DeserializeObject<RespostaTotvs>(resp);
                var xml = new XmlDocument();
                xml.LoadXml(b.XML);
                foreach (ItemEnvio item in itensEnvio)
                {
                    var x = xml.SelectSingleNode("APAS_INTFEIRA/IDENT_BANCARIO_" + item.ident_bancario + "/STATUS").InnerText;
                    item.status_envio = x == "OK" ? 1 : 0;
                }
                itemEnvioRepository.addRange(itensEnvio);
            }
        }

        private void sendPedidoUpdate(List<ItemEnvio> itensEnvio)
        {
            sendPedidoInsert(itensEnvio);

            itensEnvio = itensEnvio.FindAll(x => x.status_envio == 1);

            foreach (ItemEnvio item in itensEnvio)
            {
                item.acao = "I";
            }

            if(itensEnvio.Count > 0)
            {
                sendPedidoInsert(itensEnvio);
            }                      
        }

        private string sendMethod(string json)
        {
            string strResponseValue = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Settings.totvsURL);
            request.Method = "POST";
            request.ContentType = "application/json";
            using (StreamWriter swJSONPayload = new StreamWriter(request.GetRequestStream()))
            {
                swJSONPayload.Write(json);
                swJSONPayload.Close();
            }
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                    {
                        using (StreamReader reader = new StreamReader(responseStream))
                        {
                            strResponseValue = reader.ReadToEnd();
                            Form1View.listBoxLog.Items.Insert(0, strResponseValue);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                strResponseValue = "{\"errorMessages\":[\"" + ex.Message.ToString() + "\"],\"errors\":{}}";

            }
            finally
            {
                if (response != null)
                {
                    ((IDisposable)response).Dispose();
                }
            }
            return strResponseValue;
        }

        private void sendErro()
        {
            _itemEnvioNew = itemEnvioRepository.getWithError("I");
            _itemEnvioUpdated = itemEnvioRepository.getWithError("E");

            foreach (ItemEnvio item in _itemEnvioNew)
            {
                item.data_envio = DateTime.Now;
            }

            foreach (ItemEnvio item in _itemEnvioUpdated)
            {
                item.data_envio = DateTime.Now;
            }

            if (_itemEnvioNew.Count > 0)
                sendPedidoErroInsert(_itemEnvioNew);

            if (_itemEnvioUpdated.Count > 0)
                sendPedidoErroUpdate(_itemEnvioUpdated);

            _itemEnvioNew.Clear();
            _itemEnvioUpdated.Clear();
        }

        private void sendPedidoErroInsert(List<ItemEnvio> itensEnvio)
        {
            var json = JsonConvert.SerializeObject(new JsonSender(itensEnvio));
            var resp = sendMethod(json);
            if (!resp.Contains("errorMessages"))
            {
                var b = JsonConvert.DeserializeObject<RespostaTotvs>(resp);
                var xml = new XmlDocument();
                xml.LoadXml(b.XML);
                foreach (ItemEnvio item in itensEnvio)
                {
                    var x = xml.SelectSingleNode("APAS_INTFEIRA/IDENT_BANCARIO_" + item.ident_bancario + "/STATUS").InnerText;
                    item.status_envio = x == "OK" ? 1 : 0;
                    itemEnvioRepository.update(item);
                }                
            }
        }

        private void sendPedidoErroUpdate(List<ItemEnvio> itensEnvio)
        {
            sendPedidoErroInsert(itensEnvio);

            itensEnvio = itensEnvio.FindAll(x => x.status_envio == 1);

            foreach (ItemEnvio item in itensEnvio)
            {
                item.acao = "I";
            }

            if (itensEnvio.Count > 0)
            {
                sendPedidoErroInsert(itensEnvio);
            }
        }

        private bool verificarEnvio(ItemEnvio item)
        {
            var existente = itemEnvioRepository.getByIDBancario(item.ident_bancario);
            return existente == null;
        }
        #endregion

        Dictionary<string, int> formapagamentoDic = new Dictionary<string, int>()
        {
            {"BOLETO",1},
            {"VISA",2},
            {"MASTERCARD",2},
            {"AMEX",2},
            {"CIELO",2},
            {"CARTAO DE CREDITO",2},
            {"ELO",2},
            {"DINERS",2},
            {"DISCOVER",2},
            {"AURA",2},
            {"JCB",2},
            {"SOROCRED",2},
            {"AGIPLAN",2},
            {"BANESCARD",2},
            {"CABAL",2},
            {"CREDSYSTEM",2},
            {"CREDZ",2},
            {"HIPERCARD",2},
            {"VISA DEBITO",3},
            {"MASTERCARD DEBITO",3},
            {"DINHEIRO",4},
            {"MOEDA ESTRANGEIRA",4},
            {"DEPOSITO",5},
            {"PAGAMENTO A VISTA(TEF OU CDC)",5},
            {"OUTRO",6},
            {"INVOICE",6},
            {"CHEQUE",6},
            {"COMPROVANTE",6},
            {"EMPENHO",6},
            {"PROMOCIONAL",6},
            {"PAYPAL",6},
            {"CORTESIA",6},
            {"Associado",6}
        };
    }   
}
