namespace apas_totvs_integration
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ApiContext : DbContext
    {
        public ApiContext()
            : base("name=ApiContext")
        {
        }

        public virtual DbSet<ItemEnvio> ItemEnvio { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.acao)
                .IsUnicode(false);

            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.data_venda)
                .IsUnicode(false);

            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.ident_bancario)
                .IsUnicode(false);

            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.tipo)
                .IsUnicode(false);

            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.descricao_tipo)
                .IsUnicode(false);

            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.cc)
                .IsUnicode(false);

            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.cr)
                .IsUnicode(false);

            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.pr)
                .IsUnicode(false);

            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.co)
                .IsUnicode(false);

            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.quantidade)
                .HasPrecision(6, 2);

            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.valor_liquido)
                .HasPrecision(16, 2);

            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.valor_bruto)
                .HasPrecision(16, 2);

            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.valor_desconto)
                .HasPrecision(16, 2);

            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.valor_acrescimo)
                .HasPrecision(16, 2);

            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.valor_unitario)
                .HasPrecision(16, 2);

            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.tipo_pagamento)
                .IsUnicode(false);

            modelBuilder.Entity<ItemEnvio>()
                .Property(e => e.moeda)
                .IsUnicode(false);
        }
    }
}
