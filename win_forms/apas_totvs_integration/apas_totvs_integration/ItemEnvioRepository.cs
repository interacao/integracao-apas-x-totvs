﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.Migrations;

namespace apas_totvs_integration
{
    public class ItemEnvioRepository
    {
        private ApiContext _contexto = new ApiContext();

       
        public List<ItemEnvio> getAll()
        {
            return _contexto.ItemEnvio.ToList();
        }

        public ItemEnvio getByID(int ItemEnvioID)
        {
            return _contexto.ItemEnvio.FirstOrDefault(x => x.item_envio_id == ItemEnvioID);
        }

        public ItemEnvio getByIDBancario(string ident_bancario)
        {
            return _contexto.ItemEnvio.FirstOrDefault(x => x.ident_bancario == ident_bancario);
        }

        public void add(ItemEnvio item)
        {
            _contexto.ItemEnvio.Add(item);
            _contexto.SaveChanges();
        }

        public void addRange(List<ItemEnvio> itens)
        {
            _contexto.ItemEnvio.AddRange(itens);
            _contexto.SaveChanges();
        }

        public List<ItemEnvio> getBetweenDates(DateTime dataIni, DateTime dataFim)
        {
            return _contexto.ItemEnvio.Where(x => x.data_envio > dataIni && x.data_envio < dataFim).ToList();
        }

        public List<ItemEnvio> getAllWithError()
        {
            return _contexto.ItemEnvio.Where(x=> x.status_envio == 0).ToList();
        }

        public List<ItemEnvio> getWithError(string acao)
        {
            return _contexto.ItemEnvio.Where(x => x.status_envio == 0 && x.acao == acao).ToList();
        }

        public List<ItemEnvio> getWithOutError()
        {
            return _contexto.ItemEnvio.Where(x => x.status_envio == 1).ToList();
        }              

        public void update(ItemEnvio item)
        {
            _contexto.ItemEnvio.AddOrUpdate(item);
            _contexto.SaveChanges();
        }
    }
}
