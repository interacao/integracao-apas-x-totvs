﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace EasyLibSys
{
    public class FWK_DAO_DD_Pagamento
    {
        public Interanet_v4_easyContext DB_contexto = new Interanet_v4_easyContext(new ConnectionStringEasy().Conexao);

        public FWK_DAO_DD_Pagamento()
        {
            if (DB_contexto == null)
                DB_contexto = new Interanet_v4_easyContext(new ConnectionStringEasy().Conexao);
        }

        public List<TB_DD_Pagamento> MS_ObterTodos()
        {
            return (from TP in DB_contexto.TB_DD_Pagamento select TP).ToList();
        }

        public TB_DD_Pagamento MS_ObterPorID(int id)
        {
            var linq = from pagamento in DB_contexto.TB_DD_Pagamento
                       where pagamento.cdPagamento == id
                       select pagamento;

            return linq.SingleOrDefault();
        }

        public TB_DD_Pagamento MS_ObterPorPedido(int PedidoID)
        {
            var linq = from pagamento in DB_contexto.TB_DD_Pagamento
                       where pagamento.cdPedido == PedidoID &&
                       pagamento.Status == "ATIVO"
                       select pagamento;

            return linq.SingleOrDefault();
        }

    }
}
