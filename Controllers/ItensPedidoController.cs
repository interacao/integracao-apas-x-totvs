using System.Collections.Generic;
using IntegrationApasTotvs.Models;
using IntegrationApasTotvs.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace IntegrationApasTotvs.Controllers
{
    [Route("api/[Controller]")]
    public class ItensPedidoController: Controller
    {
        private readonly ITB_DD_ItensPedidoCongressoRepository _repo;

        public ItensPedidoController(ITB_DD_ItensPedidoCongressoRepository ItensPedidoRepository){
            _repo = ItensPedidoRepository;
        }

        [HttpGet]
        public IEnumerable<TB_DD_ItensPedidoCongresso> GetAll(){
            return _repo.GetAll();
        }




    }
}